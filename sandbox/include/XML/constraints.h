#ifndef _XML_CPP_BASE_H_
#define _XML_CPP_BASE_H_

#include <typeinfo>

#if 0
DTD<test> dtd = Element<int>("a").charData(),
                Element<double>("b").attr("value"),
                Element<manip>("op"),
                Element<test>("test")
                    | ((E(&test::a, "a"), E(&test::b, "b")) & *E("op"))
                    | (A(&test::a, "a"), A(&test::b, "b"))
                :
#endif

namespace XML {
    template <typename X>
        struct opt<X> {
            typedef X type;
        };

    struct cardinality_min_0 {};
    struct cardinality_min_1 {};
    struct cardinality_max_1 {};
    struct cardinality_max_N {};

    template <typename X>
        struct cardinality_min {
            typedef cardinality_min_1 min;
        };

    template <typename X>
        struct cardinality_min<opt<X>> {
            typedef cardinality_min_0 min;
        };

    template <typename X>
        struct cardinality_max {
            typedef cardinality_max_1 max;
        };

    template <typename X>
        struct cardinality_max<std::vector<X>> {
            typedef cardinality_max_N max;
        };

    template <typename X>
        struct auto_cardinality {
            typedef cardinality_min<X>::min min;
            typedef cardinality_max<X>::max max;
        };

    template <typename X>
        struct validator {
            size_t n_rep;
            typedef auto_cardinality<X> cardinality;
        };


    template <typename Represented>
    struct DTD {
        std::map<std::string, Element_Base*> elements;

        DTD() : elements() {}

        DTD<Represented>& operator = (Element_Base& e)
        {
        }

        DTD<Represented>& operator , (Element_Base& e)
        {
        }

        Represented operator () (std::istream& is)
        {
        }
    };

    template <typename X, typename Derived>
        struct constraint_spec {
            bool optional;
            std::string symbol;
            X field;

            constraint_spec(X* f, const char* sym, bool o=false)
                : optional(o), symbol(sym), field(f)
            {}

            constraint_spec(const constraint_spec<Derived>& e)
                : optional(e.optional), symbol(e.symbol), field(e.field)
            {}

            constraint_spec<X, Derived> operator * () const
            {
                return constraint_spec<X, Derived>(field, symbol, true);
            }
        };

    template <typename X>
        struct attribute_constraint : public constraint_spec<X, attribute_constraint> {
        };

    template <typename X>
        struct element_constraint : public constraint_spec<X, element_constraint> {
        };







    struct constraint_base {
        std::string name;
        bool is_opt;
        bool is_multiple;
        type_info eval_type;
        constraint_base(const char* c_name, bool opt, bool mult, const type_info& type)
            : name(c_name), is_opt(opt), is_multiple(mult), eval_type(type)
        {}
    };

    struct constraint_iterator_base {
        int counter;
        constraint_base* constraint;

        constraint_iterator_base(constraint_base* c)
            : counter(0), constraint(c)
        {}

        bool is_valid() const
        {
            return counter == 0 && constraint->is_opt
                || counter > 1 && constraint->is_multiple
                || counter == 1;
        }

        bool feed(const char* name)
        {
            if (constraint->name == name) {
                ++counter;
            }
            return is_valid();
        }

        bool finish() const
        {
            return is_valid();
        }
    };


    struct attr {
    };

    struct elt {
        const char* name;
        elt(const char* elt_name) : name(elt_name) {}
    };

    template <typename ConstraintType>
    struct opt {
        typedef typename ConstraintType::constraint_type constraint_type;
        Constraint<ConstraintType>* constraint;
        opt(Constraint<ConstraintType>& c) : constraint(c) {}
    };

    template <typename ConstraintType, typename OtherConstraintType>
    struct constraint_then {
        ConstraintType a;
        OtherConstraintType b;
        constraint_then(ConstraintType& a_, OtherConstraintType& b_)
            : a(a_), b(b_)
        {}
    };

    struct constraint_or {
    };

    template <typename ConstraintType>
    struct Constraint : public ConstraintType {
        template <typename CT1, typename CT2>
        Constraint(CT1& c1, CT2& c2)
            : ConstraintType(c1, c2)
        {}

        Constraint(typename ConstraintType::constraint_type& c)
            : ConstraintType(c)
        {}

        template <typename OtherConstraintType>
            Constraint<constraint_then<ConstraintType, OtherConstraintType> >
            operator & (Constraint<OtherConstraintType>& c) {
                return Constraint<constraint_then<ConstraintType, OtherConstraintType> >(*this, c);
            }
        template <typename OtherConstraintType>
            Constraint<constraint_or<ConstraintType, OtherConstraintType> >
            operator | (Constraint<OtherConstraintType>& c) {
                return Constraint<constraint_or<ConstraintType, OtherConstraintType> >(*this, c);
            }
    };

}

#endif

