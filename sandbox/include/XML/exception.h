#ifndef _XML_CPP_EXCEPTION_H_
#define _XML_CPP_EXCEPTION_H_

namespace XML {
struct UnexpectedElement : public std::exception {
    Element_Base* e;
    UnexpectedElement(Element_Base* _) : e(_) {}
    const char* what() const throw() {
        if (!e) {
            return "Null element.";
        }
        std::ostringstream oss;
        oss << "Expected one of:";
        OperationElement_Map::iterator i = e->elements.begin(), j = e->elements.end();
        for(; i != j; ++i) {
            oss << ' ' << i->first;
        }
        return oss.str().c_str();
    }
};
}

#endif

