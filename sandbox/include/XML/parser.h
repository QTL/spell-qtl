#ifndef _XML_CPP_PARSER_H_
#define _XML_CPP_PARSER_H_

namespace XML {

struct XMLReader {
    XML_Parser parser;
    Editor_Stack editors;
    std::vector<Element_Base*> elements;
    std::vector<Operation_Element_Base*> operations;

    XMLReader(/*Element_Base* root_element, */const XML_Char* encoding="UTF-8")
        : parser(XML_ParserCreate(encoding))
        , editors()
        , elements()
        , operations()
    {
        XML_SetElementHandler(parser, start_hnd, end_hnd);
        XML_SetElementHandler(parser, start_hnd, end_hnd);
        XML_SetCharacterDataHandler(parser, chardata_hnd);
        XML_SetCdataSectionHandler(parser, cdata_start_hnd, cdata_end_hnd);
        XML_SetUserData(parser, static_cast<void*>(this));
    }

    ~XMLReader()
    {
        XML_ParserFree(parser);
    }

    template <typename BoundStruc>
        void parse_and_eval(BoundStruc* ptr, Element<BoundStruc>* elt,
                            std::istream& is) {
            editors.clear();
            elements.clear();
            operations.clear();

            Element<BoundStruc> root("");
            root.contains(elt);

            enter_element(&root);

            Editor_Base ed(elt, ptr);
            editors.push_back(&ed);

            parse(is);
        }

    template <typename BoundStruc>
        BoundStruc* parse_and_eval(Element<BoundStruc>* elt,
                                   std::istream& is) {
            BoundStruc* ret = new BoundStruc();
            parse_and_eval(ret, elt, is);
            return ret;
        }

    void enter_element(Element_Base* elt)
    {
        elements.push_back(elt);
    }

    void leave_element()
    {
        elements.pop_back();
    }

    void parse(std::istream& is)
    {
        while (!is.eof()) {
            void *buff = XML_GetBuffer(parser, BUFF_SIZE);
            is.read((char*) buff, BUFF_SIZE);
            XML_ParseBuffer(parser, is.gcount(), is.eof());
        }
    }

    void parse(const char* path)
    {
        std::ifstream f(path);
        parse(f);
    }

    void _start(const XML_Char* name, const XML_Char** attrs)
    {
        std::cout << "> " << name << std::endl;
        OperationElement_Map::iterator oei = elements.back()->elements.find(std::string(name));
        if (oei == elements.back()->elements.end()) {
            throw UnexpectedElement(elements.back());
        }
        Operation_Element_Base* op = oei->second;
        Element_Base* elt = op->sub_elt;
        op->enter(editors);
        operations.push_back(op);

        while (*attrs) {
            std::string key(*attrs++);
            std::string value(*attrs++);
            Operation_Map::iterator op_i = elt->attributes.find(key);
            if (op_i != elt->attributes.end()) {
                (*op_i->second)(editors.back(), value);
            } else {
                /* whine */
            }
        }

        enter_element(elt);
    }

    void _end(const XML_Char* name)
    {
        Editor_Base* ed = editors.back();
        Element_Base* elt = elements.back();
        if (elt->data) {
            std::string data = ed->buffer.str();
            (*elt->data)(ed, data);
            std::cout << "processed data \"" << data << "\"" << std::endl;
        }
        std::cout << "< " << name << std::endl;
        operations.back()->leave(editors);
        operations.pop_back();
        leave_element();
    }

    static void start_hnd(void* userData, const XML_Char* name, const XML_Char** attrs) {
        static_cast<XMLReader*>(userData)->_start(name, attrs);
    }
    static void end_hnd(void* userData, const XML_Char* name) {
        static_cast<XMLReader*>(userData)->_end(name);
    }
    static void cdata_start_hnd(void* userData) {
        std::cout << "CDATA start" << std::endl;
    }
    static void cdata_end_hnd(void* userData) {
        std::cout << "CDATA end" << std::endl;
    }
    void _chardata_hnd(const XML_Char* data, int len)
    {
        std::string str(data, data + len);
        editors.back()->buffer << str;
    }
    static void chardata_hnd(void* userData, const XML_Char* data, int len) {
        std::cout << "CHAR DATA " << len << " \"" << std::string(data, data+len) << '"' << std::endl;
        static_cast<XMLReader*>(userData)->_chardata_hnd(data, len);
    }
};
}

#endif

