#ifndef _XML_CPP_EDITOR_H_
#define _XML_CPP_EDITOR_H_

namespace XML {
struct Editor_Base {
    Element_Base* spec;
    void* elt;
    std::ostringstream buffer;
    Bool_Map attributes;
    Bool_Map contents;
    Editor_Base(Element_Base* _spec, void* e)
        : spec(_spec)
        , elt(e)
        , buffer()
        , attributes(_spec->attribute_constraints)
        , contents(_spec->contents_constraints)
    {}
    virtual ~Editor_Base()
    {
        _and all_true;
        all_true = std::for_each(attributes.begin(), attributes.end(), all_true);
        if (!all_true._) {
            /* whine */
        }
    }
};
}

#endif

