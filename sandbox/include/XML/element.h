#ifndef _XML_CPP_ELEMENT_H_
#define _XML_CPP_ELEMENT_H_

#include <iostream>

#include <expat.h>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <typeinfo>
#include <algorithm>

#define BUFF_SIZE 1024


namespace XML {


template <typename BoundStruc>
struct Element_Mixin : public Element_Base {
    Element_Mixin(const char* name) : Element_Base(name) {}
    virtual ~Element_Mixin() {}
    template <typename FieldType>
        Element<BoundStruc>&
        attr(FieldType BoundStruc::* field, std::string attr_name, bool optional=false)
        {
            struct _op : public Operation_Base {
                FieldType BoundStruc::* field;
                _op(FieldType BoundStruc::* _) : field(_) {}
                void operator() (Editor_Base* ed, std::string& data) {
                    static_cast<BoundStruc*>(ed->elt)->* field = convert<FieldType>(data);
                }
            };
            dynamic_cast<Element<BoundStruc>*>(this)->attributes.insert(Op(attr_name, new _op(field)));
            dynamic_cast<Element<BoundStruc>*>(this)->attribute_constraints.insert(Constraint(attr_name, optional));
            return *dynamic_cast<Element<BoundStruc>*>(this);
        }

    template <typename FieldType>
        Element<BoundStruc>&
        attr(FieldType BoundStruc::* field, const char* attr_name, bool optional=false)
        {
            return attr<field>(std::string(attr_name), optional);
        }

    template <typename FieldType>
        Element<BoundStruc>&
        contains(FieldType BoundStruc::* field, Element<FieldType>* sub_elt, bool optional=false)
        {
            struct _ed : public Operation_Element_Base {
                FieldType BoundStruc::* field;
                _ed(Element<FieldType>* se, FieldType BoundStruc::* _)
                    : Operation_Element_Base(se), field(_)
                {}
                void enter(Editor_Stack& es)
                {
                    BoundStruc* top = static_cast<BoundStruc*>(es.back()->elt);
                    es.push_back(new Editor_Base(sub_elt, &(top ->* field)));
                }
                void leave(Editor_Stack& es)
                {
                    es.pop_back();
                    /*delete edit;*/
                }
            };
            dynamic_cast<Element<BoundStruc>*>(this)->elements.insert(OpElt(sub_elt->name, new _ed(sub_elt, field)));
            dynamic_cast<Element<BoundStruc>*>(this)->contents_constraints.insert(Constraint(sub_elt->name, optional));
            std::cout << "Element \"" << name << "\" contains \"" << sub_elt->name << '"' << std::endl;
            return *dynamic_cast<Element<BoundStruc>*>(this);
        }

    template <typename FieldType>
        Element<BoundStruc>&
        contains(FieldType* BoundStruc::* field, Element<FieldType>* sub_elt, bool optional=false)
        {
            struct _ed : public Operation_Element_Base {
                FieldType BoundStruc::* field;
                _ed(Element<FieldType>* se, FieldType BoundStruc::* _)
                    : Operation_Element_Base(se), field(_)
                {}
                void enter(Editor_Stack& es)
                {
                    BoundStruc* top = static_cast<BoundStruc*>(es.back()->elt);
                    es.push_back(new Editor_Base(sub_elt, new FieldType()));
                }
                void leave(Editor_Stack& es)
                {
                    FieldType* item = static_cast<FieldType*>(es.back()->elt);
                    es.pop_back();
                    BoundStruc* top = static_cast<BoundStruc*>(es.back()->elt);
                    top ->* field = item;
                }
            };
            dynamic_cast<Element<BoundStruc>*>(this)->elements.insert(OpElt(sub_elt->name, new _ed(sub_elt, field)));
            dynamic_cast<Element<BoundStruc>*>(this)->contents_constraints.insert(Constraint(sub_elt->name, optional));
            std::cout << "Element \"" << name << "\" contains (allocated pointer) \"" << sub_elt->name << '"' << std::endl;
            return *dynamic_cast<Element<BoundStruc>*>(this);
        }

    template <typename Operation>
        Element<BoundStruc>&
        contains(Element<Operation>* sub_elt, bool optional=false)
        {
            struct _ed : public Operation_Element_Base {
                _ed(Element<Operation>* se)
                    : Operation_Element_Base(se)
                {}
                void enter(Editor_Stack& es)
                {
                    BoundStruc* top = static_cast<BoundStruc*>(es.back()->elt);
                    es.push_back(new Editor_Base(sub_elt, new Operation()));
                }
                void leave(Editor_Stack& es)
                {
                    Operation* op = static_cast<Operation*>(es.back()->elt);
                    es.pop_back();
                    BoundStruc* top = static_cast<BoundStruc*>(es.back()->elt);
                    (*op)(top);
                }
            };
            dynamic_cast<Element<BoundStruc>*>(this)->elements.insert(OpElt(sub_elt->name, new _ed(sub_elt)));
            dynamic_cast<Element<BoundStruc>*>(this)->contents_constraints.insert(Constraint(sub_elt->name, optional));
            std::cout << "Element \"" << name << "\" contains (manipulator) \"" << sub_elt->name << '"' << std::endl;
            return *dynamic_cast<Element<BoundStruc>*>(this);
        }

    Element<BoundStruc>&
        contains(Element<BoundStruc>* sub_elt, bool optional=false)
        {
            struct _ed : public Operation_Element_Base {
                _ed(Element<BoundStruc>* se)
                    : Operation_Element_Base(se)
                {}
                void enter(Editor_Stack& es)
                {
                }
                void leave(Editor_Stack& es)
                {
                }
            };
            dynamic_cast<Element<BoundStruc>*>(this)->elements.insert(OpElt(sub_elt->name, new _ed(sub_elt)));
            dynamic_cast<Element<BoundStruc>*>(this)->contents_constraints.insert(Constraint(sub_elt->name, optional));
            std::cout << "Element \"" << name << "\" contains (transient) \"" << sub_elt->name << '"' << std::endl;
            return *dynamic_cast<Element<BoundStruc>*>(this);

        }
};

template <> struct Element_Mixin<int> : public Element_Base {
    Element_Mixin(const char* name) : Element_Base(name) {}
};
template <> struct Element_Mixin<short int> : public Element_Base {
    Element_Mixin(const char* name) : Element_Base(name) {}
};
template <> struct Element_Mixin<long int> : public Element_Base {
    Element_Mixin(const char* name) : Element_Base(name) {}
};
template <> struct Element_Mixin<long long int> : public Element_Base {
    Element_Mixin(const char* name) : Element_Base(name) {}
};

template <> struct Element_Mixin<unsigned int> : public Element_Base {
    Element_Mixin(const char* name) : Element_Base(name) {}
};
template <> struct Element_Mixin<unsigned short int> : public Element_Base {
    Element_Mixin(const char* name) : Element_Base(name) {}
};
template <> struct Element_Mixin<unsigned long int> : public Element_Base {
    Element_Mixin(const char* name) : Element_Base(name) {}
};
template <> struct Element_Mixin<unsigned long long int> : public Element_Base {
    Element_Mixin(const char* name) : Element_Base(name) {}
};

template <> struct Element_Mixin<float> : public Element_Base {
    Element_Mixin(const char* name) : Element_Base(name) {}
};
template <> struct Element_Mixin<double> : public Element_Base {
    Element_Mixin(const char* name) : Element_Base(name) {}
};
template <> struct Element_Mixin<std::string> : public Element_Base {
    Element_Mixin(const char* name) : Element_Base(name) {}
};

template <typename BoundStruc>
struct Element : public Element_Mixin<BoundStruc> {
    using Element_Mixin<BoundStruc>::attributes;
    using Element_Mixin<BoundStruc>::data;

    Element(const char* _name)
        : Element_Mixin<BoundStruc>(_name)
    {}

    const Element<BoundStruc>& attr(std::string attr_name)
    {
            struct _op : public Operation_Base {
                void operator() (Editor_Base* ed, std::string& data) {
                    std::istringstream(data) >> *static_cast<BoundStruc*>(ed->elt);
                }
            };
            attributes.insert(Op(attr_name, new _op()));
            return *this;
    }

    const Element<BoundStruc>& attr(const char* attr_name)
    {
        return attr(std::string(attr_name));
    }

    const Element<BoundStruc>& charData()
    {
            struct _op : public Operation_Base {
                void operator() (Editor_Base* ed, std::string& data) {
                    std::istringstream(data) >> *static_cast<BoundStruc*>(ed->elt);
                }
            };
            data = new _op();
            return *this;
    }

    void read(BoundStruc* ptr, std::istream& is, const XML_Char* encoding="UTF-8")
    {
        XMLReader(encoding).parse_and_eval(ptr, this, is);
    }

    BoundStruc* read(std::istream& is, const XML_Char* encoding="UTF-8")
    {
        return XMLReader(encoding).parse_and_eval(this, is);
    }
};

namespace construction {

struct binder_base {
    virtual void operator() (Element_Base*) = 0;
};

template <typename Struc, typename Field>
struct binder {

};

} /* namespace construction */
} /* namespace XML */

#endif

