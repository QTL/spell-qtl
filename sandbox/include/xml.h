#ifndef _XML_CPP_H_
#define _XML_CPP_H_

#include <iostream>

#include <expat.h>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <typeinfo>
#include <algorithm>

#include "XML/base.h"
#include "XML/exception.h"
#include "XML/editor.h"
#include "XML/parser.h"
#include "XML/element.h"

#endif

