#include "../include/excel.h"

int main(int argc, char** argv)
{
    xlnt::workbook wb;
    excel::stream s(&wb);
    
    s << excel::style("green-bg", [](xlnt::style& sty) { sty.fill(xlnt::fill().solid(xlnt::color::green())); });

    s << excel::move(2, 2) << "Toto" << excel::next_col << 42 << excel::next_row << "12/12/2012";
    
    s << excel::move(5, 1) << excel::merge(2, 5) << "MERGED" << excel::style("green-bg") << excel::align([](xlnt::alignment& al) { al.rotation(90).horizontal(xlnt::horizontal_alignment::center).vertical(xlnt::vertical_alignment::center); })
      << excel::next_col << 1 << excel::next_col << 2 << excel::next_col << 3 << excel::next_col << excel::formula("=SUM(C5:E5)")
      << excel::next_row << "Pouet ! ! !" << excel::next_col << 23.;
      
//     xlnt::border b;
    
//     b.side(xlnt::border_side::start, xlnt::border::border_property().color(xlnt::color::red()).style(xlnt::border_style::thin));
//     b.side(xlnt::border_side::end, xlnt::border::border_property().color(xlnt::color::blue()).style(xlnt::border_style::thick));

//     s << excel::border(2, 2, 4, 3, b);

    s << excel::border_box(2, 2, 4, 3, {{xlnt::color::red(), xlnt::border_style::thin}, {xlnt::color::blue(), xlnt::border_style::thick}});

    wb.save("prout.xlsx");
    
    return 0;
}
