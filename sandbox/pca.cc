//
// Created by daleroux on 24/02/17.
//

#include "eigen.h"

#include "error.h"
#include "input/read_trait.h"

int
main(int argc, char** argv)
{
    ifile f(argv[1]);
    auto traits = read_data::read_trait(f);
    std::vector<std::string> names;
    for (int i = 2; i < argc; ++i) {
        names.emplace_back(argv[i]);
    }
    pleiotropic_trait mt1(traits, names, "pleio");
    pleiotropic_trait mt2(traits, names, "pleio", 0);
    pleiotropic_trait mt3(traits, names, "pleio", 2.);
    return 0;
}