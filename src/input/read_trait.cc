/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "eigen.h"
#include "input.h"
//#include "input/read_trait.h"
//#include "error.h"
#include "io/output_impl.h"


static
bool read_value(file& is, double& value)
{
    std::string tmp;
    is >> tmp;
    if (tmp == "-" || tmp == "NA") {
        return false;
    }
    std::stringstream(tmp) >> value;
    /*std::cout << "read " << tmp << " => " << value << std::endl;*/
    return true;
}


namespace read_data {
std::vector<trait> read_trait(file& is, const std::vector<std::string>& filter_names)
{
    if (!is.good()) {
        throw input_exception(is, "couldn't read from file");
    }
    std::vector<trait> ret;
    std::string s;
    /*double d;*/
    MSG_DEBUG("Reading traits amongst " << filter_names);
    is >> std::noskipws;
    while (!is.eof()) {
        std::string name = read_star_name(is);
        ret.emplace_back();
        ret.back().name = name;
//        MSG_DEBUG("Reading single_trait " << ret.back()->name);
        size_t counter = 0;
        std::vector<double> values;
        while (!(is.eof()||ws_opt_nl(is))) {
            /*is >> d;*/
            double val;
            if (read_value(is, val)) {
                values.push_back(val);
                ret.back().good_indices.push_back(counter);
            }
            ++counter;
        }
        if (ret.back().good_indices.size() == 0 || (!!filter_names.size() && std::find(filter_names.begin(), filter_names.end(), name) == filter_names.end())) {
            ret.pop_back();
        } else {
            ret.back().dim_names = {ret.back().name};
            ret.back().values.resize(values.size(), 1);
            ret.back().values = Eigen::Map<Eigen::Matrix<double, -1, -1, Eigen::ColMajor>>(values.data(), values.size(), 1);
            ret.back().raw = ret.back().values;
            ret.back().P = MatrixXd::Ones(1, 1);
        }
       MSG_DEBUG("... read " << counter << " values (" << (counter - ret.back().values.size()) << " missing)");
    }
    MSG_DEBUG("all traits " << ret);
    return ret;
}
} /* namespace read_data */

#ifdef TEST_TRAIT
/*#include "file.h"*/

int main(int argc, char** argv)
{
    for (int i = 1; i < argc; ++i) {
        ifile ifs(argv[i]);
        std::cout << read_data::read_trait(ifs) << std::endl;
    }
    return 0;
}

#endif

