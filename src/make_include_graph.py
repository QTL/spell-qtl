import os
import sys

make_tree_cmd = (
    r"for i in `find . -iname '*.cc'`; do "
    + r"echo $i; g++ -H -MM -std=c++0x -I ../attic/ -I ../include/"
    + r" -I ~/MIAT/include/eigen3.2/ -I ~/MIAT/include/x2c/ $i 2>&1 | "
    + r"grep '^[.]\+ ' | "
    + r"grep -v 'usr[/]\(include\|lib\)' | "
    + r"grep -v 'Eigen[/]src' | "
    + r"grep -v '^Multiple\|^[.][^ ]*h$';"
    + r" done")

all_includes_cmd = r"find ../include -iname '*.h'"

#print make_tree_cmd

#os.system(make_tree_cmd + " > include_trees.txt")

trees = {}

current_src = []

all_files = {}
current_all_files = None


all_include_files = set()

try:
    os.mkdir('include-graphs')
except OSError, ose:
    pass

for f in os.popen(all_includes_cmd).xreadlines():
    all_include_files.add(os.path.relpath(os.path.realpath(f.strip())))


def parse_line(l):
    global current_src, current_all_files
    l = os.path.relpath(os.path.realpath(l.strip()))
    if l.endswith('cc'):
        current_src = [l]
        all_files[l] = current_all_files = set([l])
        return l
    else:
        dots, src = l.split(' ')
        current_all_files.add(src)
        level = len(dots)
        current_src = current_src[:level] + [src]
        #print current_src
        return current_src[-2:]

i, o = os.popen4(make_tree_cmd)

count = 0
tree = None
for l in o.xreadlines():
    p = parse_line(l)
    count += 1
    if type(p) is str:
        tree = {}
        trees[p] = tree
    #print >> sys.stderr, "\rread", count, "lines",
    #sys.stderr.flush()
    else:
        src, dest = p
        if src not in tree:
            tree[src] = set([dest])
        else:
            tree[src].add(dest)

#for k, v in tree.iteritems():
#    print k, v


def clean_path(entry):
    #print entry,
    path = entry.split('/')[:-1]
    #print path,
    if not path or path[0] == '.':
        ret = ('src',) + tuple(path[1:]) + (entry,)
    elif path[0] == '..':
        ret = tuple(path[1:]) + (entry,)
    else:
        ret = tuple(path) + (entry,)
    #print ret
    return ret


def add_in_clusters(clus, name, *a):
    if len(a) == 1:
        if not name in clus:
            clus[name] = {"files": list(a), "sub": {}}
        else:
            clus[name]["files"].append(*a)
    elif not name in clus:
        clus[name] = {"files": [], "sub": {}}
        add_in_clusters(clus[name]["sub"], *a)
    else:
        add_in_clusters(clus[name]["sub"], *a)


def print_cluster(out, name, contents):
    if not contents['files'] and len(contents['sub']) == 1:
        for k, v in contents['sub'].iteritems():
            print_cluster(out, name + '/' + k, v)
        return
    print >> out, 'subgraph "cluster-' + name + '" {'
    print >> out, 'label="' + name + '";'
    for f in contents['files']:
        print >> out, '"' + f + '" [label="' + os.path.basename(f) + '"];'
    for k, v in contents['sub'].iteritems():
        print_cluster(out, k, v)
    print >> out, '}'

for curfile in all_files:
    safe = curfile.replace('.', '').replace('/', '-')
    with open('include-graphs/includes-' + safe + '.dot', 'w') as out:
        print >> out, 'digraph "includes-' + safe + '" {'

        pathitems = [clean_path(f) for f in all_files[curfile]]

        clusters = {}

        for pi in pathitems:
            #print pi
            add_in_clusters(clusters, *pi)
            #print clusters
        #print clusters

        for k, v in clusters.iteritems():
            print_cluster(out, k, v)

        for src, v in trees[curfile].iteritems():
            for dest in v:
                print >> out, '"' + src + '"', "->", '"' + dest + '"'

        for k in trees[curfile]:
            print >> out, '"' + k + '" [label="' + os.path.basename(k) + '"];'

        #print "{rank=same;", '"' + '" "'.join(k
        #                                      for k in tree
        #                                      if k.endswith('cc')) + '"}'

        print >> out, "}"

# now invoke dot
os.system('for i in include-graphs/includes-*dot; do '
          + 'dot -Tpng $i -o ${i%%dot}png; done')


used_inc = set()
for k, v in all_files.iteritems():
    for f in v:
        if f.endswith('h'):
            used_inc.add(f)

#print used_inc
with open('include-graphs/unused.txt', 'w') as out:
    print >> out, "UNUSED INCLUDES"
    for unused in (all_include_files - set(used_inc)):
        print >> out, unused
