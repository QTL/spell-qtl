/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "map-likelihood/cli.h"
#include "data/chromosome.h"

#define PREDICATE CONSTRAINT_PREDICATE(mapqa_settings_t)
#define CALLBACK_ARGS ARGUMENT_CALLBACK_ARGS(mapqa_settings_t)


mapqa_settings_t* ensure(mapqa_settings_t*& t)
{
    if (t == NULL) {
        t = new mapqa_settings_t();
    }
    return t;
}


static
argument_section_list_t<mapqa_settings_t>
arguments = {
    {"Advanced (NOT INTENDED FOR USERS!)", "", true, {
        {{"--full-help", "--advanced-help"},
            {},
            "Display usage.",
            false,
            {false},
            [](CALLBACK_ARGS)
            {
                print_usage_impl(true, arguments);
                exit(0);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-D", "--debug"},
            {},
            "Display debug messages",
            false,
            {false},
            [](CALLBACK_ARGS)
            {
                msg_handler_t::debug_enabled() = true;
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-q", "--quiet"},
            {},
            "Suppress all output (including debug output)",
            false,
            {false},
            [](CALLBACK_ARGS)
            {
                msg_handler_t::quiet() = true;
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

    }},
    {"Miscellaneous", "", false, {
        {{"-h", "--help"},
            {},
            "Display usage.",
            false,
            {false},
            [](CALLBACK_ARGS)
            {
                print_usage_impl(false, arguments);
                exit(0);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-n", "--name"},
            {"population name"},
            "The name of this population (will also be used to create the output filename)",
            false,
            {true},
            [](CALLBACK_ARGS) {
                ensure(target)->name = *++ai;
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-wd", "--work-directory"},
            {"path"},
            "Path to directory for output files",
            false,
            {&mapqa_settings_t::work_directory},
            [](CALLBACK_ARGS) {
                ensure(target)->work_directory = *++ai;
                if (!check_file(ensure(target)->work_directory, true, true, false)) {
                    ensure_directories_exist(ensure(target)->work_directory);
                    check_file(ensure(target)->work_directory, true, true, true);
                }
                SAFE_IGNORE_CALLBACK_ARGS;
            }},
    }},
    {"Inputs", "Markers and distances", false, {
        {{"-m", "--map"},
            {"Marker [distance Marker]..."},
            "Compute the likelihood for this genetic map (single group)",
            false,
            {false},
            [](CALLBACK_ARGS)
            {
                bool on_marker = true;
                std::stringstream inp(*++ai);
                while (!inp.eof()) {
                    if (on_marker) {
                        std::string name;
                        inp >> name;
                        ensure(target)->group.raw.marker_name.push_back(name);
                    } else {
                        double dist;
                        inp >> dist;
                        ensure(target)->group.raw.marker_locus.push_back(dist * .01);
                    }
                    on_marker = !on_marker;
                }
                if (on_marker) {
                    MSG_ERROR("Map must start and end with a marker name!", "Input a well-formed map");
                }
                ensure(target)->group.compute_haplo_sizes();
                SAFE_IGNORE_CALLBACK_ARGS;
            }},
    }},
};


constraint_t<mapqa_settings_t> checks[] = {
        {
                "No map to work on.",
                "Specify a non-empty map",
                CONSTRAINT_PREDICATE(mapqa_settings_t) {
                    return s->group.raw.marker_locus.size() > 0 && s->group.raw.marker_locus.back() > 0;
                }
        },
};


argument_parser<mapqa_settings_t> arg_map(arguments);


mapqa_settings_t* mapqa_settings_t::from_args(int argc, const char** argv)
{
    std::vector<const char*> args(argv + 1, argv + argc);
    std::vector<const char*>::iterator ai = args.begin();
    std::vector<const char*>::iterator aj = args.end();
    ::prg_name(basename(argv[0]));

    mapqa_settings_t* ret = NULL;

    while (ai != aj && arg_map(ret, ai, aj)) {
        ++ai;
    }
    
    constraint_t<mapqa_settings_t>::check(ret);

    return ret;
}

void print_usage() { print_usage_impl(false, arguments); }

#include "io/output_impl.h"
