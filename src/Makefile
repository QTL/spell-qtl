include ../Makefile.conf
#TARGET=spell-qtl.experimental
TARGET=spell-qtl

SRC=input/read_mark.cc input/read_map.cc input/design.cc input/read_trait.cc input/read_settings.cc polynom.cc input/invoke_probapop.cc static_data.cc computations.cc

#INPUT_SRC=read_mark.cc read_map.cc design.cc read_trait.cc read_settings.cc invoke_probapop.cc pedigree.cc ld_matrices.cc xml/xml_design.cc xml/xml_format.cc xml/xml_settings.cc
#MAIN_SRC=static_data.cc probapop_dtd.cc main.cc
#INPUT_SRC=read_mark.cc read_map.cc design.cc read_trait.cc read_settings.cc pedigree.cc ld_matrices.cc xml/xml_design.cc xml/xml_format.cc xml/xml_settings.cc
INPUT_SRC=read_map.cc read_trait.cc read_settings.cc ld_matrices.cc xml/xml_format.cc xml/xml_settings.cc
MAIN_SRC=static_data.cc main.cc beta_gamma.cc
COMPUTATIONS_SRC=basic_data.cc probabilities.cc model.cc frontends.cc

#SRC=malloc.cc $(addprefix input/,$(INPUT_SRC)) $(addprefix computations/,$(COMPUTATIONS_SRC)) $(MAIN_SRC)
SRC=$(addprefix input/,$(INPUT_SRC)) $(addprefix computations/,$(COMPUTATIONS_SRC)) $(MAIN_SRC)

OBJ=$(subst .cc,.o,$(SRC))
DEP=$(subst .cc,.d,$(SRC)) test_script.d geno_matrix.d study_pedigree.d
COV_OBJ=$(subst .cc,.cov.o,$(SRC))

ALL_BUT_MAIN_OBJ=$(subst main.o ,,$(OBJ))


.header: .FORCE
	@echo "*********************************************************"
	@echo "**  MAKE  ***********************************************"
	@echo "*********************************************************"

-include .header

INFO_FILE=test-coverage.info


all: $(TARGET)


include .depend


test: $(OBJ)
	$C $(OBJ) -o $@ $(LIBS)

fast_polynom.o: fast_polynom.cc
	$C $(COV_OPTS) -c $< -o $@

test_fast_polynom: fast_polynom.o
	$C $(COV_OPTS) $< -o $@

test-coverage: test_fast_polynom
	lcov --zerocounters --directory .
	./$< | tail -3
	lcov --gcov-tool $(COV) --no-checksum --directory . --capture --output-file $(INFO_FILE)
	lcov --remove $(INFO_FILE) "/usr*" -o $(INFO_FILE)
	lcov --remove $(INFO_FILE) "catch.hpp" -o $(INFO_FILE)
	genhtml --highlight --legend --output-directory TestCodeCoverage $(INFO_FILE)


#.depend: $(SRC) Makefile
#	$C --depend $(SRC) > $@

.depend: $(DEP)
	cat $(DEP) > $@

study_pedigree.o probabilities.o test_selfing.o markov_population.o test_outbred.o outbred.o test_settings.o $(OBJ):%.o: %.cc
	$C -c $< -o $@

$(DEP):%.d: %.cc
	$C -MM $< -MT $@\ $(subst .d,.o,$@) -o $@

polynom.o: polynom.cc
	$C -DTEST_POLY -c $(DEBUG_OPTS) $< -o $@ -DNDEBUG -g

clean:
	rm -f $(DEP) .depend
	rm -f $(OBJ)
	rm -f $(TARGET)

.FORCE:

test.design: input/design.cc static_data.o input/xml.o input/read_map.o input/read_mark.o input/read_trait.o
	$C -DTEST_DESIGN $< static_data.o input/xml.o input/read_map.o input/read_mark.o input/read_trait.o -o $@ $(DEBUG_OPTS) $(LIBS)

test.proba: probabilities.cc input/read_mark.o input/read_map.o input/design.o input/read_trait.o input/read_settings.o 
	$C -DTEST_PROBA  $< input/read_mark.o input/read_map.o input/design.o input/read_trait.o input/read_settings.o -o $@ $(DEBUG_OPTS) $(LIBS)

test.poly: polynom.o input/read_mark.o input/read_map.o input/design.o input/read_trait.o input/read_settings.o 
	$C polynom.o input/read_mark.o input/read_map.o input/design.o input/read_trait.o input/read_settings.o -o $@ $(DEBUG_OPTS) $(LIBS)

test.cp: outbred.cc input/read_mark.o
	$C -DTEST_OUTBRED $< input/read_mark.o -o $@ $(DEBUG_OPTS) $(LIBS)

$(TARGET): $(OBJ)
	$L $(OBJ) $(LIBS) -o $@

test_selfing: test_selfing.o input/read_mark.o input/read_map.o input/design.o input/read_trait.o input/read_settings.o
	$C test_selfing.o input/read_mark.o input/read_map.o input/design.o input/read_trait.o input/read_settings.o $(LIBS) -o $@

test_mp: markov_population.o outbred.o input/design.o input/read_mark.o input/read_map.o probapop_dtd.o input/invoke_probapop.o static_data.o
	$C -ggdb $< outbred.o input/design.o input/read_mark.o input/read_map.o probapop_dtd.o input/invoke_probapop.o static_data.o -o $@ $(LIBS)

test_tensor: test_tensor.cc ../include/tensor.h
	$C -ggdb $< -o $@ $(LIBS)

test_cache: test_cache.cc
	$C -ggdb $< -o $@ $(LIBS)

test_outbred: test_outbred.o input/design.o input/read_mark.o input/read_map.o probapop_dtd.o input/invoke_probapop.o
	$C -ggdb $< input/design.o input/read_mark.o input/read_map.o probapop_dtd.o input/invoke_probapop.o -o $@ $(LIBS)

test_model_print: model_print.cc
	$C -ggdb -O0 $< -o $@ $(LIBS)

test_ev: test_matrix_cliques.cc
	$C -ggdb -O0 $< -o $@ $(LIBS)

test_script: test_script.cc test_script.d $(ALL_BUT_MAIN_OBJ)
	$C -ggdb -O0 $< $(ALL_BUT_MAIN_OBJ) -o $@ $(LIBS)

TEST_BAYES_OBJ=static_data.o input/xml/xml_format.o input/read_mark.o

test_bayes: bayes.cc bayes.d $(TEST_BAYES_OBJ)
	$C $< $(TEST_BAYES_OBJ) -o $@ $(LIBS)

test_bayes2: bayes.cc bayes.d $(TEST_BAYES_OBJ)
	$C $< $(TEST_BAYES_OBJ) -o $@ $(LIBS)

test_bayes_dai: ../sandbox/bayes.cc bayes.d $(ALL_BUT_MAIN_OBJ)
	$C -ggdb -O3 $< -o $@ -lgmp -lgmpxx -L../../ext/libdai/lib -ldai

test_sib: pedigree_analysis.cc static_data.o
	$C $< static_data.o -o $@

matrixexp: geno_matrix.cc ../include/lumping2.h ../include/geno_matrix.h ../include/pedigree.h ../include/permutation.h ../include/braille_plot.h Makefile ../include/pedigree_tree.h ../include/symmetry.h ../include/data/genoprob_computer2.h ../include/linear_combination.h ../include/bayes/factor_var3.h
	$C -DORDER=$(ORDER) $< -o $@ -DTEST_GENO_MATRIX

test_permutation: test_permutation.cc ../include/permutation.h ../include/braille_plot.h ../include/pedigree_tree.h Makefile
	$C -DORDER=$(ORDER) $< -o $@

test_sig: test_sig.cc ../include/error.h
	$C $< -o $@

study_pedigree.o: Makefile

study_pedigree: study_pedigree.o Makefile
	$C $< -o $@

test_map_print: test_map_print.cc input/read_map.o ../include/data/chromosome.h ../include/braille_plot.h
	$C $< input/read_map.o -o $@

test_diskhash: test_diskhash.cc ../include/disk_hashtable.h
	$C $< -o $@
