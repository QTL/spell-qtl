/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "eigen.h"
#include "input.h"
//#include "data/geno_matrix.h"
#include "cache/md5.h"
#include "computations/base.h"
#include "cache2.h"
#include "computations/basic_data.h"
#include "computations/probabilities.h"
//#include "io/output_impl.h"
#include <bitset>

static inline
size_t next_power_of_two(size_t n)
{
    --n;
    n|=n>>1;
    n|=n>>2;
    n|=n>>4;
    n|=n>>8;
    n|=n>>16;
    /*n|=n>>32;*/
    ++n;
    return n;
}

#if 0
static inline
size_t transform_index(size_t index, size_t shift_left_mask, size_t shift_right_mask, size_t parent_bits, size_t no_shift_mask)
{
    return ((index & shift_left_mask) << order)
         | ((index & shift_right_mask) >> parent_bits)
         | (index & no_shift_mask);
}
#endif

#define DUMPHEX(_x_) MSG_DEBUG(std::setw(30) << std::left << #_x_ << std::bitset<sizeof(_x_) * 8>(_x_))
#define DUMP(_x_) MSG_DEBUG(std::setw(30) << std::left << #_x_ << (_x_))
struct transform_index {
    /* the number of parents rounded to the next power of two */
    size_t n_parents_pow2;
    /* the number of bits required to encode the parent indices */
    size_t parent_bits;
    /* the number of columns in the resulting matrix (2^(n_loci * n_parents)) */
    size_t n_col;
    /* the amount of bits to shift the new locus left */
    size_t shift_left;
    /* mask for the bits to shift right */
    size_t shift_right_mask;
    /* mask for the bits to shift left */
    size_t shift_left_mask;
    /* mask for the bits to NOT shift */
    size_t no_shift_mask;

    transform_index(size_t n_parents, size_t n_loci, size_t n_order)
        : n_parents_pow2(next_power_of_two(n_parents))
        , parent_bits(__builtin_ctz(n_parents_pow2))
        , n_col(1 << (parent_bits * n_loci))
        , shift_left(parent_bits * n_order)
        , shift_right_mask((1 << (shift_left + parent_bits)) - 1)
        , shift_left_mask(n_parents_pow2 - 1)
        , no_shift_mask(~shift_right_mask)
    {
        /*DUMP(n_parents_pow2);*/
        /*DUMP(n_col);*/
        /*DUMP(parent_bits);*/
        /*DUMPHEX(shift_left);*/
        /*DUMPHEX(shift_left_mask);*/
        /*DUMPHEX(shift_right_mask);*/
        /*DUMPHEX(no_shift_mask);*/
    }

    size_t operator () (size_t index)
    {
        size_t loc_to_shift_left = index & shift_left_mask;
        size_t loc_shifted_left = loc_to_shift_left << shift_left;
        size_t loc_to_shift_right = index & shift_right_mask;
        size_t loc_shifted_right = loc_to_shift_right >> parent_bits;
        size_t loc_unchanged = index & no_shift_mask;
        /*DUMPHEX(index);*/
        /*DUMPHEX(loc_to_shift_left);*/
        /*DUMPHEX(loc_shifted_left);*/
        /*DUMPHEX(loc_to_shift_right);*/
        /*DUMPHEX(loc_shifted_right);*/
        /*DUMPHEX(loc_unchanged);*/
        size_t ret = loc_shifted_left | loc_shifted_right | loc_unchanged;
        /*DUMPHEX(ret);*/
        /*MSG_DEBUG("transform(" << index << ") = " << ret);*/
        return ret;
    }
};


MatrixXd permutation_matrix(size_t n_parents, size_t n_loci, size_t order)
{
    transform_index ti(n_parents, n_loci, order);
    MatrixXd ret = MatrixXd::Zero(ti.n_col, ti.n_col);
    for (size_t index = 0; index < ti.n_col; ++index) {
        ret(ti(index), index) = 1;
    }
    if (n_parents < ti.n_parents_pow2) {
        MatrixXd reduction_matrix_0 = MatrixXd::Identity(n_parents, ti.n_parents_pow2);
        MatrixXd reduction_matrix = reduction_matrix_0;
        for (size_t i = 1; i < n_loci; ++i) {
            MatrixXd tmp = kroneckerProduct(reduction_matrix, reduction_matrix_0);
            reduction_matrix = tmp;
        }
        /*MSG_DEBUG("reduction_matrix_0" << std::endl << reduction_matrix_0);*/
        /*MSG_DEBUG("reduction_matrix" << std::endl << reduction_matrix);*/
        ret = reduction_matrix * ret * reduction_matrix.transpose();
    }
    /*MSG_DEBUG("Permutation matrix (n_parents=" << n_parents << ", n_loci=" << n_loci << ", order=" << order << ')' << std::endl << ret);*/
    return ret;
}



#if 0
locus_probabilities_type
locus_prob(qtl_chromosome_value qtl_chr, double noise, generation_value gen,
           const std::vector<double>& loci, const MatrixXd& LV)
{
    segment_computer_t gc(*gen, qtl_chr, noise, loci);
	/*MSG_DEBUG("got gc " << gc);*/
    return gc.compute(LV);
}


locus_probabilities_type
locus_probabilities(qtl_chromosome_value qtl_chr, population_value pop,
                    generation_value gen,
                    const MatrixXd& mgo,
                    const selected_qtls_on_chromosome_type&,
                    const std::vector<double>& loci)
{
#if 0
    value<MatrixXb> LV = mgo.LV;
    return *make_value</*Disk|*/Sync>(locus_prob,
                                  value<qtl_chromosome_value>{qtl_chr},
                                  value<double>{pop->noise},
                                  value<generation_value>{gen},
                                  value<std::vector<double>>{loci},
                                  LV);
    /*MSG_DEBUG("LV" << std::endl << mgo.LV);*/
    /*locus_probabilities_type ret = gc.compute(mgo.LV);*/
    /*MSG_DEBUG("lp" << std::endl << ret);*/
    /*return ret;*/
#else
    generation_rs::segment_computer_t gc = gen->segment_computer(qtl_chr, pop->noise, loci);
	/*MSG_DEBUG("got gc " << gc);*/
    return gc.compute(mgo.LV);
#endif
}
#endif


optim_segment_computer_t
make_segment_computer(const chromosome* chr, population_value pop, const locus_key& lk, int ind, size_t)
{
    /*qtl_chromosome qc(ck->chr, lk);*/
    const geno_matrix* gen = pop->gen.get();
    return {*gen, chr, lk, 0/* FIXME: where is the noise parameter? */, pop->get_LV(chr->name, ind)};
}

/* FIXME! */
locus_probabilities_type
locus_probabilities(const context_key& ck, const locus_key& lk,
                    /*const MatrixXd& mgo,*/
                    int ind,
                    const std::vector<double>& loci)
{
    /*MSG_DEBUG("locus_probabilities(" << ck << ", " << lk << ", " << ind << ", " << loci << ")");*/
    /*MSG_QUEUE_FLUSH();*/
    /*const MatrixXd& LV = ck->pop->get_LV(ck->chr->name, ind);*/
    auto ret = make_value</*Mem|*/Sync>(make_segment_computer, as_value(ck->chr), as_value(ck->pop), as_value(lk), as_value(ind), as_value(ck->pop->indices[ind]))->compute(loci);
    /*MSG_DEBUG("locus_probabilities(" << ck << ", " << lk << ", " << ind << ", " << loci << ")" << std::endl << ret);*/
    return ret;
}




#if 0
state_to_parental_origin_matrix_type
state_to_parental_origin_matrix(generation_value gen,
                                int n_qtl)
{
    /*MSG_DEBUG("state_to_parental_origin_matrix " << ((void*)gen));*/
    return const_cast<generation_rs*>(gen)->get_state_to_parental_origin(n_qtl);
}

parental_origin_type
parental_origin(const locus_probabilities_type& lp, generation_value gen, const selected_qtls_on_chromosome_type& sqoc)
{
    auto& s = const_cast<generation_rs*>(gen)->get_state_to_parental_origin(sqoc.size());
    return s * lp;
}
#endif


template <typename T>
void dump_hash(const std::string& n, const T& x)
{
    std::hash<T> h;
    MSG_DEBUG("hash(" << n << ") = " << h(x));
}

#if 0
parental_origin_type
parental_origin2(qtl_chromosome_value qtl_chr,
			 	 population_value pop,
			 	 generation_value gen,
				 const state_to_parental_origin_matrix_type& stfopom,
		 		 const MatrixXd& mgo,
		 		 const selected_qtls_on_chromosome_type& sqoc,
                 const std::vector<double>& loci)
{
    /*auto& s = const_cast<generation_rs*>(gen)->compute_state_to_parental_origin(sqoc.size());*/
    /*MSG_DEBUG("stfopom: " << stfopom.innerSize() << "," << stfopom.outerSize());*/
    auto lp = l ocus_probabilities(qtl_chr, pop, gen, mgo, sqoc, loci);
    /*MSG_DEBUG("lp: " << lp.innerSize() << "," << lp.outerSize());*/
    if (0) {
        md5_digest md5;
        md5 << mgo;
        std::string m = md5;
        MSG_DEBUG("mgo: MD5 = " << m << std::endl << mgo.LV);
        dump_hash("stfopom", stfopom);
        dump_hash("mgo", mgo);
        dump_hash("sqoc", sqoc);
        dump_hash("loci", loci);
    }
    return stfopom * lp;
}

parental_origin_type
parental_origin2(const context_key& ck,
                 const locus_key& lk,
                 const MatrixXd& mgo)
{
    auto lp = make_value<Disk>(l ocus_probabilities, value<context_key>{ck}, value<locus_key>{lk}, value<MatrixXd>{mgo}, as_value(ck->loci));
    auto stfopom = make_value<Mem>(compute_state_to_parental_origin, value<context_key>{ck}, value<locus_key>{lk});
    return (*stfopom) * (*lp);
}
#endif


/* Compute the joint probability of selected loci UNION new locus */
geno_prob_type
joint_geno_prob_at_locus(const context_key& ck, const locus_key& lk)
{
//    DUMP_FILE_LINE();
//    MSG_DEBUG("* Computing geno prob in " << ck << " given " << lk);
//    MSG_DEBUG("=====================================================");
    auto it = ck->locus_indices.find(lk->locus);
    if (it == ck->locus_indices.end()) {
        /* FIXME: whine */
//        MSG_ERROR("THIS LOCUS DOESN'T EXIST IN THIS CONTEXT! ck.loci=[" << ck->loci << "] locus=" << lk->locus, "");
//        MSG_QUEUE_FLUSH();
        throw 0;
        return {};
    }
//    MSG_DEBUG_INDENT;
    size_t loc_idx = it->second;
    value<context_key> vck = ck;
    value<locus_key> vlk = lk->parent;
    /*collection<MatrixXd>*/
        /*vmgo = make_collection<Disk>(population_marker_obs,*/
                                     /*vck, range<int>(0, ck->pop->size(), 1));*/
    collection<locus_probabilities_type>
        /*alp = make_collection<Disk>(locus_probabilities, vck, vlk, vmgo, as_value((*vck)->loci));*/
        alp = make_collection<Disk>(locus_probabilities,
                                    vck, vlk, range<int>(0, ck->pop->size(), 1), as_value((*vck)->loci));
//     for (auto& x: alp) {
        /*MSG_DEBUG(x);*/
//         (void)*x;
//     }

    geno_prob_type ret;

    ret.row_labels = alp[0]->row_labels;
    ret.data.resize(alp[0]->innerSize(), alp.size());
//    DUMP_FILE_LINE();
    for (size_t ind = 0; ind < ck->pop->size(); ++ind) {
//        MSG_DEBUG("n.cols = " << ret.data.outerSize() << " vs " << alp[ind]->data.outerSize());
//        MSG_DEBUG("col.size = " << ret.data.innerSize() << " vs " << alp[ind]->data.innerSize());
//        MSG_QUEUE_FLUSH();
        ret.data.col(ind) = alp[ind]->data.col(loc_idx);
    }

//    DUMP_FILE_LINE();
//    MSG_DEBUG("computed " << ck << ' ' << lk);
//    MSG_DEBUG(MATRIX_SIZE(ret));
//    MSG_DEBUG("-----------------------------------------------------");

    /*if (lk && lk->locus != locus_key_struc::no_locus) {*/
    if (!(*vlk)->is_empty()) {
        /*std::vector<double> test_loci = {lk->locus};*/
        /*context_key ck2(new context_key_struc(ck->pop, ck->chr, test_loci));*/
        auto pop_pred = make_value<Disk|Sync>(joint_geno_prob_at_locus,
                                              as_value(ck), vlk);
        /*(void)*pop_pred;*/
//        DUMP_FILE_LINE();
//        MSG_DEBUG("previous geno_prob " << ck << ' ' << lk->parent << '+' << lk->locus);
//        MSG_DEBUG(MATRIX_SIZE((*pop_pred)));
//        MSG_DEBUG("-----------------------------------------------------");
        /*MatrixXd ones = MatrixXd::Ones(ck->gen->get_unique_labels().size(), 1);*/
        MatrixXd ones = MatrixXd::Ones(ret.data.rows() / pop_pred->data.rows(), 1);
        MatrixXd hada_mult = kroneckerProduct(pop_pred->data, ones);
//        MSG_DEBUG("Previous geno_prob after kronecker to prepare for the Hadamard product:" << std::endl << hada_mult);
//        MSG_DEBUG("-----------------------------------------------------");
        MatrixXd tmp = (ret.data.array() * hada_mult.array()).matrix();
        ret.data = tmp;
    }

//    DUMP_FILE_LINE();
//    MSG_DEBUG("joint probabilities " << ck << ' ' << lk);
//    MSG_DEBUG(MATRIX_SIZE(ret));
//    MSG_DEBUG("=====================================================");

//    DUMP_FILE_LINE();
//    MSG_DEBUG_DEDENT;
//    MSG_QUEUE_FLUSH();
    return ret;
}


/* Compute the joint probability of selected loci UNION new locus */
parental_origin_per_locus_type
joint_dominance_at_locus(const context_key& ck, const locus_key& lk, bool)
{
    value<context_key> vck{ck};
    value<locus_key> vlk{lk};
    value<locus_key> vlkp{lk->parent};
    auto pop = make_value<Disk>(joint_geno_prob_at_locus, vck, vlk);
    auto stdom = make_value<Mem>(compute_state_to_dominance, vck, vlkp);

    /*(void) *pop;*/
    /*(void) *stfopom;*/
    /*MSG_DEBUG("joint_dominance_at_locus");*/
    /*MSG_DEBUG("******* " << MATRIX_SIZE(*pop));*/
    /*MSG_DEBUG(pop);*/
    /*MSG_DEBUG("******* " << MATRIX_SIZE(*stdom));*/
    /*MSG_DEBUG(stdom);*/
    /*MSG_QUEUE_FLUSH();*/

    return ((*stdom) * (*pop)).transpose();
}


/* Compute the joint probability of selected loci UNION new locus */
parental_origin_per_locus_type
joint_parental_origin_at_locus(const context_key& ck, const locus_key& lk)
{
    value<context_key> vck{ck};
    value<locus_key> vlk{lk};
    value<locus_key> vlkp{lk->parent};
    auto pop = make_value<Disk>(joint_geno_prob_at_locus, vck, vlk);
    auto stfopom = make_value<Mem>(compute_state_to_parental_origin, vck, vlkp);

    /*(void) *pop;*/
    /*(void) *stfopom;*/
    /*MSG_DEBUG("joint_parental_origin_at_locus");*/
    /*MSG_DEBUG("******* " << MATRIX_SIZE(*pop));*/
    /*MSG_DEBUG(pop);*/
    /*MSG_DEBUG("******* " << MATRIX_SIZE(*stfopom));*/
    /*MSG_DEBUG(stfopom);*/
    /*MSG_QUEUE_FLUSH();*/

    return ((*stfopom) * (*pop)).transpose();
}




collection<parental_origin_per_locus_type>
parental_origin_per_locus(const collection<parental_origin_type>& apo)
{
    /* TODO :
     * - séparer la préparation (tout avant le premier for) (sync|mem)
     * - éliminer le for externe
     * - retourner un SEUL parental_origin_per_locus_type
     * - virer LD de la fin de la fonction, il faut que ça soit un calcul à part
     * - remplacer l'argument par context_key, locus_key, locus_index
     */

    /*DUMP_FILE_LINE();*/
    auto& tmp = apo[0];
    size_t n_ind = apo.size();
    size_t n_par = tmp->innerSize();
    size_t n_loc = tmp->outerSize();
    /*MSG_DEBUG("n_ind " << n_ind);*/
    /*MSG_DEBUG("n_par " << n_par);*/
    /*MSG_DEBUG("n_loc " << n_loc);*/
    /*MSG_DEBUG("apo" << std::endl << apo);*/

    /*DUMP_FILE_LINE();*/
    /* labels for individuals */
    std::vector<int> individuals;
    individuals.resize(n_ind);
    size_t index = 0;
    std::generate(individuals.begin(), individuals.end(), [&] () { return index++; });
    /*DUMP_FILE_LINE();*/
    /* labels for parents */
    std::vector<std::vector<char>> parents = tmp->row_labels;
    /*DUMP_FILE_LINE();*/
    /* loci */
    std::vector<double> loci = tmp->column_labels;
    /*DUMP_FILE_LINE();*/
    /* return value */
    collection<parental_origin_per_locus_type> ret(loci.size());

    /*DUMP_FILE_LINE();*/
    /* first round : init matrices */
    for (size_t l = 0; l < n_loc; ++l) {
        /*labelled_matrix<MatrixXd, int, std::vector<char>>& lm = ret[loci[l]];*/
    /*DUMP_FILE_LINE();*/
        ret[l] = new unique_value<parental_origin_per_locus_type>();
        labelled_matrix<MatrixXd, int, std::vector<char>>& lm = *ret[l];
        lm.row_labels = individuals;
        lm.column_labels = parents;
        lm.data.resize(n_ind, n_par);
    /*DUMP_FILE_LINE();*/
        for (int ind: individuals) {
            /*MSG_DEBUG("ind=" << ind << " l=" << l << " lm.data$size(" << lm.data.innerSize() << ',' << lm.data.outerSize() << ") apo[ind]->data$size(" << apo[ind]->data.innerSize() << ',' << apo[ind]->data.outerSize() << ')');*/
            lm.data.row(ind) = apo[ind]->data.col(l);
        }
    }
#ifdef NEED_TO_TRANSMIT_QTL_GENERATION_NAME_AND_CHROMOSOME_PTR_TO_HANDLE_LD_PROPERLY
    if (active_settings->ld_data.size()) {
        const LD_matrices& ld = active_settings->ld_data.begin()->second;
        /*assert(n_loc == ld.ld.size() && "WRONG LD SIZE");*/
        /*MSG_DEBUG("n_loc = " << n_loc << std::endl);*/
        /*MSG_DEBUG("ld.size = " << ld.ld.size() << std::endl);*/
        for (size_t l = 0; l < n_loc; ++l) {
            /*MSG_DEBUG("LD " << l << std::endl << ld.ld[l]);*/
            ret[l]->data = ret[l]->data * ld.ld[l].transpose();
        }
    }
#endif
    /*DUMP_FILE_LINE();*/
    return ret;
}



/*
 * TODO
 * in cache2:
 * - when caching is Disk only, have a registry record all CURRENTLY COMPUTING tasks:
 *   task registers upon creation, unregister upon completion, so that two asynchronous
 *   Disk tasks don't overwrite each other.
 */

#if 0
collection<parental_origin_per_locus_type>
compute_parental_origins(const value<population_value>& pop,
                         const value<chromosome_value>& chr,
                         const value<std::vector<double>>& loci)
{
    DUMP_FILE_LINE();
    value<qtl_chromosome_value> qtl_chr
        = qtl_chromosome_by_name((*chr)->name);

    DUMP_FILE_LINE();
     value<selected_qtls_on_chromosome_type> sqoc = selected_qtls_on_chromosome(*qtl_chr);

    DUMP_FILE_LINE();
    value<generation_value> qtl_gen = qtl_generation(*pop);

    /*DUMP_FILE_LINE();*/
    /*collection<pedigree_type> ap*/
        /*= make_collection(pedigree, pop, qtl_gen, individual_range(*pop));*/

    DUMP_FILE_LINE();
    value<const pop_mgo_data*> pmd = new pop_mgo_data(*pop);

    DUMP_FILE_LINE();
    collection<MatrixXd> apmo
        = make_collection<Sync|Disk>(population_marker_obs,
                                    pop, chr, range<int>(0, (*pop)->size(), 1), pmd);
    /*MSG_DEBUG("apmo" << std::endl << apmo);*/

    /*collection<locus_probabilities_type> alp*/
        /*= make_collection(l ocus_probabilities, qtl_chr, pop, gen, apmo, sqoc);*/

    DUMP_FILE_LINE();
	value<state_to_parental_origin_matrix_type>
		stfopom = make_value<Disk>(state_to_parental_origin_matrix, qtl_gen, value<int>(sqoc->size()));

    /*MSG_DEBUG("nqtl=" << sqoc->size());*/
    /*MSG_DEBUG("[cpo]stfopom: " << stfopom->innerSize() << "," << stfopom->outerSize());*/

    DUMP_FILE_LINE();
    collection<parental_origin_type> apo
        = make_collection<Disk>(parental_origin2, qtl_chr, pop, qtl_gen, stfopom, apmo, sqoc, loci);
    /*MSG_DEBUG("apo" << std::endl << apo);*/

    DUMP_FILE_LINE();
    return parental_origin_per_locus(apo);
}
#endif


#include "io/output_impl.h"
