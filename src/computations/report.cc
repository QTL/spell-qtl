/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "eigen.h"
#include "input.h"
//#include "data/geno_matrix.h"
#include "cache/md5.h"
#include "computations/base.h"
#include "cache2.h"
#include "computations/basic_data.h"
#include "computations/probabilities.h"
#include "io/model_print.h"
#include "model/model.h"
#include "model/tests.h"
#include "computations/model.h"
#include "computations/frontends4.h"
#include <unordered_set>
//#include "io/output_impl.h"

std::unique_ptr<analysis_report> ar_ptr;

region_of_interest_type all_roi;
point_of_interest_type all_poi;


void
init_analysis_report(model_manager& mm)
{
    ar_ptr.reset(new analysis_report(mm));
    ar_ptr->trait(mm.trait_name);
}

analysis_report&
ar_instance()
{
    return *ar_ptr;
}



analysis_report_computations::analysis_report_computations(model_manager& _)
    : mm(_)
    , poi()
    , roi()
    , X(mm.vMcurrent->X())
{}


void
analysis_report_computations::check_constraints()
{
    auto X = mm.vMcurrent->X();
    if (mm.vMcurrent->rank() < X.cols()) {
        MSG_ERROR("Insufficient rank for model matrix.", "Report to the developers");
    }
    int n_constraints = X.rows();
    for (const auto& p: mm.all_pops) {
        n_constraints -= (*p)->size();
    }
    auto res = mm.vMcurrent->residuals().bottomRows(n_constraints);
    if (res.lpNorm<1>() > CONSTRAINT_RESIDUAL_EPSILON) {
//         MSG_DEBUG(mm.vMcurrent->residuals().transpose());
//         MSG_DEBUG(res.transpose());
        double prout = res.lpNorm<1>();
        double eps = CONSTRAINT_RESIDUAL_EPSILON;
        MSG_ERROR("Inconsistent contraints for model matrix. " << prout << " > " << eps, "Report to the developers");
    }
}

void
analysis_report_computations::prepare_computations()
{
    TM = active_settings->trait_metadata[mm.trait_name];
    mm.vMcurrent->compute();
    Pt = TM.P.transpose();
    all_coefficients = mm.vMcurrent->coefficients() * Pt;
    /* FIXME use actual full Y instead of un-projecting the traits? */
    all_residuals = mm.vMcurrent->residuals() * Pt;
    all_rss = all_residuals.array().square().colwise().sum();
    pseudo_variances = (mm.vMcurrent->rss().transpose() * Pt.array().square().matrix()).transpose()
                        / (mm.vMcurrent->n_obs() - mm.vMcurrent->dof() - 1);
}

mws_result_by_block
analysis_report_computations::R2(int i)
{
    MatrixXd R2(1, mm.vMcurrent->m_blocks.size());
    auto bi = mm.vMcurrent->m_blocks.begin(), bj = mm.vMcurrent->m_blocks.end();
    int c = 0;
    for (; bi != bj; ++bi, ++c) {
        auto M0 = *mm.vMcurrent;
        M0.remove_block(bi->first);
        M0.compute();
        MatrixXd M0residuals = M0.residuals() * Pt;
        VectorXd M0rss = M0residuals.array().square().colwise().sum();
        double r1 = all_rss(i);
        double r0 = M0rss(i);
        R2(0, c) = fabs(r0) < 1.e-6 ? 0 : (r0 - r1) / r0;  /* FIXME use some real epsilon */
    }
    mws_result_by_block mws(R2);
    static std::vector<std::vector<char>> empty_label = {{' '}};
    bi = mm.vMcurrent->m_blocks.begin();
    for (/*++bi*/; bi != bj; ++bi) { mws.add_column_section(bi->first, empty_label); }
    mws.add_row_section("", 1);
    return mws;
}

int
analysis_report_computations::significance_level(double q)
{
    static const auto& L = significance_legend();
    return std::find_if(L.thresholds.begin(), L.thresholds.end(), [=] (double d) { return q < d; }) - 1 - L.thresholds.begin();
}

std::vector<qtl_description>
analysis_report_computations::prepare_qtls(std::vector<QTL> &qtls, ComputationType lod_test_type)
{
    std::vector<qtl_description> ret;
    for (auto& qtl: qtls) {
        ret.emplace_back(qtl_description{qtl.chromosome, qtl.locus, 0., 0., 0., 0.});
        auto& descr = ret.back();
        auto ci = qtl.confidence_interval();
        descr.outer_interval_start = ci[0];
        descr.outer_interval_end = ci[1];
        descr.inner_interval_start = ci[2];
        descr.inner_interval_end = ci[3];
//         std::tie(descr.inner_interval_start, descr.inner_interval_end,
//                     descr.outer_interval_start, descr.outer_interval_end) = qtl.confidence_interval();
        roi[qtl.chromosome][qtl.locus] = {descr.outer_interval_start, descr.outer_interval_end};
//         report_lod(qtl);
        std::string name = SPELL_STRING(mm.trait_name << " @ " << qtl.locus << " [" << descr.outer_interval_start << ':' << descr.outer_interval_end << ']');
        if (poi[qtl.chromosome][qtl.locus].size()) {
            poi[qtl.chromosome][qtl.locus] = SPELL_STRING(poi[qtl.chromosome][qtl.locus] << ',' << name);
        } else {
            poi[qtl.chromosome][qtl.locus] = name;
        }
        all_roi[qtl.chromosome][qtl.locus] = roi[qtl.chromosome][qtl.locus];
        all_poi[qtl.chromosome][qtl.locus] = poi[qtl.chromosome][qtl.locus];
    }
    return ret;
}




inline
std::pair<std::vector<size_t>, std::vector<std::pair<model_block_key, std::vector<std::vector<char>>>>>
analysis_report_computations::filter_columns(const MatrixXd& conrow, const model_block_collection& blocks)
{
    std::pair<std::vector<size_t>, std::vector<std::pair<model_block_key, std::vector<std::vector<char>>>>> ret;
    auto& columns = ret.first;
    auto& block_parts = ret.second;
    size_t j = 0;
    for (const auto& kv: blocks) {
        const auto& labels = kv.second->column_labels;
        for (size_t i = 0; i < labels.size(); ++i, ++j) {
            if (conrow(j)) {
                if (!block_parts.size() || block_parts.back().first != kv.first) {
//                     MSG_DEBUG("first used column in block " << kv.first);
                    block_parts.emplace_back();
                    block_parts.back().first = kv.first;
                    block_parts.back().second.push_back(labels[i]);
                } else {
                    block_parts.back().second.push_back(labels[i]);
                }
//                 MSG_DEBUG("added column #" << j);
//                 MSG_QUEUE_FLUSH();
                columns.push_back(j);
            }
        }
    }
    return ret;
}


// std::tuple<MatrixXd, MatrixXd, MatrixXi>
std::vector<contrast_description>
analysis_report_computations::contrasts(const MatrixXd& conmat, const MatrixXd& vcov, const MatrixXd& coef)
{
//     auto subset = filter_columns(conmat.row(r), mm.vMcurrent->m_blocks);
//     const auto& columns = subset.first;
//     const auto& blocks = subset.second;
    std::vector<contrast_description> ret;
    
    for (int r = 0; r < conmat.rows(); ++r) {
        auto subset = filter_columns(conmat.row(r), mm.vMcurrent->m_blocks);
        const auto& columns = subset.first;
        const auto& blocks = subset.second;
        
        for (const auto& block: blocks) {
            const auto& labels = mm.vMcurrent->labels_to_ancestor_names(block.second);
            const auto& key = block.first;

            std::stringstream contrast_group;
            contrast_group << std::string(labels[0].begin(), labels[0].end());
            for (int i1 = 1; i1 < (int) labels.size(); ++i1) {
                contrast_group << ", " << std::string(labels[i1].begin(), labels[i1].end());
            }

            for (int i1 = 0; i1 < (int) labels.size(); ++i1) {
                std::string l1(labels[i1].begin(), labels[i1].end());
                for (int i2 = i1 + 1; i2 < (int) labels.size(); ++i2) {
                    std::string l2(labels[i2].begin(), labels[i2].end());
                    std::string contrast_pair = SPELL_STRING(l1 << " - " << l2);

                    int a1 = columns[i1];
                    int a2 = columns[i2];
                    double sigma = vcov(a1, a1) + vcov(a2, a2) - 2 * vcov(a1, a2);
                    double delta = coef(a1) - coef(a2);
                    normal s(0, sigma > 0 ? sqrt(sigma) : 1.e-9);
                    double pvalue = 2 * cdf(complement(s, fabs(delta)));
                    ret.emplace_back(contrast_description{SPELL_STRING(key), contrast_group.str(), contrast_pair, delta, pvalue, significance_level(pvalue)});
                }
            }
        }
    }
    
    return ret;
/*
    std::tuple<MatrixXd, MatrixXd, MatrixXi> ret {
        MatrixXd::Zero(columns.size(), columns.size()),
        MatrixXd::Zero(columns.size(), columns.size()),
        MatrixXi::Zero(columns.size(), columns.size())
    };
    auto& pvmat = std::get<0>(ret);
    auto& ctrmat = std::get<1>(ret);
    auto& stymat = std::get<2>(ret);
    MatrixXd pvalue_mat = MatrixXd::Zero(pvmat.rows(), pvmat.cols());
    for (int i1 = 0; i1 < pvmat.cols(); ++i1) {
        stymat(i1, i1) = significance_level(1.);
        for (int i2 = i1 + 1; i2 < pvmat.cols(); ++i2) {
            int a1 = columns[i1];
            int a2 = columns[i2];
            double sigma = vcov(a1, a1) + vcov(a2, a2) - 2 * vcov(a1, a2);
            double delta = coef(a1) - coef(a2);
            normal s(0, sigma > 0 ? sqrt(sigma) : 1.e-9);
            double pvalue = 2 * cdf(complement(s, fabs(delta)));
            pvmat(i1, i2) = pvmat(i2, i1) = pvalue;
            ctrmat(i1, i2) = -delta;
            ctrmat(i2, i1) = delta;
            stymat(i1, i2) = stymat(i2, i1) = significance_level(pvalue);
        }
    }
    return ret;*/
}

std::vector<std::tuple<std::string, MatrixXd, MatrixXd, MatrixXi>>
analysis_report_computations::contrast_matrices(const std::vector<contrast_description>& descr)
{
//     std::tuple<MatrixXd, MatrixXd, MatrixXi> ret {
//         MatrixXd::Zero(columns.size(), columns.size()),
//         MatrixXd::Zero(columns.size(), columns.size()),
//         MatrixXi::Zero(columns.size(), columns.size())
//     };
//     auto& pvmat = std::get<0>(ret);
//     auto& ctrmat = std::get<1>(ret);
//     auto& stymat = std::get<2>(ret);
//     MatrixXd pvalue_mat = MatrixXd::Zero(pvmat.rows(), pvmat.cols());
//     for (int i1 = 0; i1 < pvmat.cols(); ++i1) {
//         stymat(i1, i1) = significance_level(1.);
//         for (int i2 = i1 + 1; i2 < pvmat.cols(); ++i2) {
//             int a1 = columns[i1];
//             int a2 = columns[i2];
//             double sigma = vcov(a1, a1) + vcov(a2, a2) - 2 * vcov(a1, a2);
//             double delta = coef(a1) - coef(a2);
//             normal s(0, sigma > 0 ? sqrt(sigma) : 1.e-9);
//             double pvalue = 2 * cdf(complement(s, fabs(delta)));
//             pvmat(i1, i2) = pvmat(i2, i1) = pvalue;
//             ctrmat(i1, i2) = -delta;
//             ctrmat(i2, i1) = delta;
//             stymat(i1, i2) = stymat(i2, i1) = significance_level(pvalue);
//         }
//     }
//     return ret;
    return {};
}



analysis_report&
analysis_report::report_computation(model& Mbase, std::string chrom, std::string trait_name, std::vector<double>& loci, const computation_along_chromosome& cac, const test_result& tr)
{
    report_algo_selection(Mbase, chrom, tr.test_value, tr.locus, mm.threshold);
    std::vector<std::string> colnames = {"locus"};
    MatrixXd values = MatrixXd::Zero(loci.size(),
                                     1
                                     + cac.chi2.cols() + cac.chi2_lod.cols() + cac.coefficients.cols()
                                     + cac.ftest_lod.rows() + cac.ftest_pvalue.rows() + cac.mahalanobis.cols()
                                     + cac.r2.cols() + cac.rank.cols() + cac.residuals.cols() + cac.rss.rows());
    values.col(0) = Eigen::Map<VectorXd>(loci.data(), loci.size());
    int c = 1;

    include_data<MatrixXd, &computation_along_chromosome::chi2, false>(c, colnames, values, "chi2.pvalue", cac);
    include_data<MatrixXd, &computation_along_chromosome::chi2_lod, false>(c, colnames, values, "chi2.lod", cac);
    include_data<MatrixXd, &computation_along_chromosome::coefficients, false>(c, colnames, values, "coef", cac);
    include_data<MatrixXd, &computation_along_chromosome::ftest_lod, true>(c, colnames, values, "ftest.lod", cac);
    include_data<MatrixXd, &computation_along_chromosome::ftest_pvalue, true>(c, colnames, values, "ftest.pvalue", cac);
    include_data<MatrixXd, &computation_along_chromosome::mahalanobis, false>(c, colnames, values, "mahalanobis", cac);
    include_data<MatrixXd, &computation_along_chromosome::r2, false>(c, colnames, values, "R2", cac);
    include_data<VectorXd, &computation_along_chromosome::rank, false>(c, colnames, values, "rank", cac);
    include_data<MatrixXd, &computation_along_chromosome::residuals, false>(c, colnames, values, "residuals", cac);
    include_data<MatrixXd, &computation_along_chromosome::rss, true>(c, colnames, values, "RSS", cac);

    m_stream.mode(ReportMode::LOD);
    select_book("computations").select_sheet(SPELL_STRING("computation " << m_selection_index));
    m_stream
    .matrix(values, colnames);
    select_previous_book();
    return *this;
}


void
analysis_report::init_selection_report()
{
    static bool must_init = true;
    if (must_init) {
        must_init = false;
        m_stream.mode(ReportMode::AlgoSel);
        select_book("algo_trace").select_sheet("Computations");
        m_stream.init_selection_report(std::vector<std::string> {"Index", "Group", "Selection", "Result.score", "Result.locus", "Result.threshold"});
    }
}


analysis_report&
analysis_report::report_algo_selection(model& Mbase, std::string chrom, double score, double locus, double threshold)
{
    init_selection_report();
    Mbase.compute();
    mm.vMcurrent->compute();
    MSG_DEBUG("[report_algo_selection] keys: " << mm.vMcurrent->keys());
    ++m_selection_index;
    m_stream.mode(ReportMode::AlgoSel);
    select_book("algo_trace").select_sheet("Computations");
    auto X = mm.vMcurrent->printable_X();
    m_stream
    .selection(m_selection_index, chrom, SPELL_STRING(Mbase.keys()), score, locus, threshold);
//         if (m_what & AR::Model) {
        select_book("algo_trace").select_sheet(SPELL_STRING(m_selection_index << " M0"));
        m_stream
        .matrix("", Mbase.printable_X());
//         }
    select_previous_book();
    return *this;
}


inline
analysis_report&
analysis_report::report_model(const model& Mcurrent)
{
    std::string modelx = SPELL_STRING("Final model X");
    std::string modelxtx = SPELL_STRING("Final model XtX");
    m_stream.mode(ReportMode::Model);
    select_book("models");
    select_sheet(modelx);
    m_stream
    .matrix("", Mcurrent.printable_X());
    select_sheet(modelxtx);
    m_stream
    .matrix("", model::xtx_printable(Mcurrent, true).mws);
    return *this;
}

analysis_report&
analysis_report::report_qtls()
{
    ComputationType ret = mm.vMcurrent->Y().cols() == 1 ? ComputationType::FTestLOD : ComputationType::Chi2LOD;
    auto vq = mm.QTLs();
    auto descr = prepare_qtls(vq, ret);
    MSG_DEBUG("Have " << descr.size() << " QTL(s) to report.");
    select_book("report").select_sheet("metadata");
    m_stream.qtls(descr);
    return *this;
}



void make_report(model_manager& mm, const std::vector<std::string>& cmdline)
{
    static std::string emptystr;
    analysis_report& report = ar_instance();
    std::string book_name("report");
    const trait_metadata_type& TM = report.trait_md();
    mm.vMcurrent->compute();
    MatrixXd X = mm.vMcurrent->X();
//     std::chrono::time_point<std::chrono::system_clock> tnow = std::chrono::system_clock::now();
//     std::time_t now = std::chrono::system_clock::to_time_t(tnow);
 
    std::stringstream cmd;
    auto ai = cmdline.begin(), aj = cmdline.end();
    cmd << (*ai++);
    for (; ai != aj; ++ai) { cmd << ' ' << *ai; }

    char mbstr[1024];
    std::time_t t = std::time(NULL);

    report
    .report_model(*mm.vMcurrent)
    .select_book(book_name)
    .select_sheet("metadata")
    .header("Spell-QTL")
    .line()
    .line(std::vector<std::string>{"Date", (std::strftime(mbstr, sizeof(mbstr), "%A %c", std::localtime(&t)) ? mbstr : "")})
    .line(std::vector<std::string>{"Command line", cmd.str()})
    .line(std::vector<std::string>{"Version", VERSION_MAJOR "." VERSION_MINOR "." VERSION_PATCH})
    .line(std::vector<std::string>{"Cores used", SPELL_STRING(active_settings->parallel)})
    .line()
    .header(SPELL_STRING("Report for " << (TM.dim_names.size() > 1 ? "pleiotropic" : "single") << " trait " << mm.trait_name))
    .line()
    .report_qtls()
    .line();
    mm.vMcurrent->compute();
    report.check_constraints();
    if (mm.vMcurrent->n_obs() <= mm.vMcurrent->dof() + 1) {
        report.line("* There are not enough observations to compute a variance/covariance matrix. Results not displayed.");
    } else {
        report.prepare_computations();
        for (int i = 0; i < (int) TM.dim_names.size(); ++i) {
            std::string title;
            if (TM.dim_names.size() > 1) {
//                 section_header(report_file, SPELL_STRING(mm.trait_name << " # " << TM.dim_names[i]));
                title = SPELL_STRING(mm.trait_name << " # " << TM.dim_names[i]);
            } else {
//                 section_header(report_file, mm.trait_name);
                title = mm.trait_name;
            }
            report.select_sheet(title);
            report.line(title).line();
            report.matrix("R2", report.R2(i));
            MatrixXd coef = report.coefs().col(i).transpose();
            /*MSG_DEBUG(MATRIX_SIZE(coef));*/
            {
                model_print::matrix_with_sections<std::string, void, std::string, std::vector<char>> mws(coef);
                mm.vMcurrent->set_column_sections(mws);
                mws.add_row_section(emptystr, 1);
//                 header(report_file, "Coefficients") << mws << std::endl;
                report.matrix("Coefficients", mws).line();
            }
            MatrixXd vcov = mm.vMcurrent->XtX_pseudo_inverse() * report.pseudo_var()(i); // mm.vMcurrent->vcov(i);
            model::xtx_printable vc(*mm.vMcurrent, vcov);
//             header(report_file, "Covariance matrix") << vc << std::endl;
            report.matrix("Covariance matrix", vc.mws).line();

            auto conmat = X.bottomRows(X.rows() - mm.vMcurrent->n_obs());

            report.report_contrasts(conmat, vcov, coef);
            
//             auto contrasts = report.contrasts(conmat, vcov, coef);
//             auto L = significance_legend();
//             for (const auto& descr: contrasts) {
//                 report.line(std::vector<std::string>{descr.key, descr.contrast_group, descr.contrast_pair, SPELL_STRING(descr.value), SPELL_STRING(descr.significance_value), L.labels[descr.significance_level]});
//             }
/*            
            for (int r = 0; r < conmat.rows(); ++r) {
                auto subset = report.filter_columns(conmat.row(r), mm.vMcurrent->m_blocks);
                const auto& columns = subset.first;
                const auto& blocks = subset.second;
                MatrixXd pvmat, ctrmat;
                MatrixXi stymat;
                std::tie(pvmat, ctrmat, stymat) = report.contrasts(conmat, vcov, coef);
                model_print::matrix_with_sections<void, std::vector<char>, model_block_key, std::vector<char>> ctrsig(pvmat);
                model_print::matrix_with_sections<void, std::vector<char>, model_block_key, std::vector<char>> ctr(ctrmat);
                for (const auto& kl: blocks) {
                    std::vector<std::vector<char>> names;
                    if (kl.first->type == mbk_CI) {
                        for (const auto& kp: mm.vMcurrent->m_all_pops) {
                            const auto& qgn = (*kp)->qtl_generation_name;
                            names.emplace_back(qgn.begin(), qgn.end());
                        }
                    } else {
                        names = mm.vMcurrent->labels_to_ancestor_names(kl.second);
                    }
                    ctr.add_row_section(names);
                    ctrsig.add_row_section(names);
                    for (auto& n: names) { n.push_back(' '); n.push_back(' '); n.push_back(' '); n.push_back(' '); }
                    ctr.add_column_section(kl.first, names);
                    ctrsig.add_column_section(kl.first, names);
                }
                report
                .matrix("Contrasts", ctr, stymat).line()
                .report_legend()
                .matrix("Contrast significance", ctrsig, stymat)
                .report_legend()
                ;
            }*/
        }
    }
//     model::xtx_printable xtx(*mm.vMcurrent, true);
//     report
//     .select_book(book_name)
//     .select_sheet("Final model X").matrix("Final model", mm.vMcurrent->printable_X())
//     .select_sheet("Final model XtX.inv").matrix("XtX^-1", xtx.mws)
//     .terminate();
}


void report_computation(model& Mbase, std::string trait_name, chromosome_value chr, std::vector<double>& loci, const computation_along_chromosome& cac, const test_result& tr)
{
    ar_instance().report_computation(Mbase, chr->name, trait_name, loci, cac, tr);
}



void
report_algo_phase(std::string descr)
{
    ar_instance().init_selection_report();
    ar_instance().report_algo_phase(descr);
}


void report_full_map()
{
    ar_instance().report_full_map(all_roi, all_poi);
}


void finish_report()
{
    ar_instance().terminate();
}


#include "io/output_impl.h"
