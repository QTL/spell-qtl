/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "eigen.h"
#include "input.h"
//#include "data/geno_matrix.h"
#include "cache/md5.h"
#include "computations/base.h"
#include "cache2.h"
#include "computations/basic_data.h"
#include "computations/probabilities.h"
#include "io/model_print.h"
#include "model/model.h"
#include "model/tests.h"
#include "computations/model.h"
#include "computations/frontends4.h"
#include <unordered_set>
#include "io/output_impl.h"

#if 0
MatrixXd
ftest_along_chromosome(chromosome_value chr, const locus_key& lk,
                       const model& M0, const std::vector<double> pos)
{
    collection<model_block_type>
        popl = compute_parental_origins_multipop(all_populations(), chr, lk, pos);
    
    computation_along_chromosome cac;
    /*compute_along_chromosome(cac, FTest, Test, M0, M0, popl);*/
    return cac.ftest_pvalue;
}





std::pair<bool, double>
detect_strongest_qtl(chromosome_value chr, const locus_key& lk, double threshold,
                     const model& M0, const std::vector<double> pos)
{
    active_settings->set_title(SPELL_STRING("QTL detection along chromosome " << chr->name << " given " << lk));
    /*MatrixXd ftac = ftest_along_chromosome(chr, lk, M0, pos);*/
    int max = max_col(ftac);
    return {ftac(0, max) >= threshold, pos[max]};
}
#endif


#if 0
locus_selection
forward(model_manager& mm,
        const std::map<std::string, std::vector<std::string>>& markers)
{
    mm.init_loci_by_marker_list(markers);
    locus_selection cofactors;
    while (true) {
        auto result = mm.find_max_over_all_chromosomes();
        if (result.over_threshold) {
            /*cofactors.push_back({result.chrom, result.locus});*/
            cofactors.add_locus(result.chrom->name, result.locus);
            locus_key lk(new locus_key_struc({result.locus}));
            model_block_key k({{result.chrom, lk}});
            mm.Mcurrent.add_block(k, result.block);
        }
    }
    return cofactors;
}

model_manager&
manual_skeleton(model_manager& mm,
                const std::map<std::string, std::vector<std::string>>& markers)
{
    mm.init_loci_by_marker_list(markers);
    return mm;
}


model_manager&
init_skeleton(model_manager& mm)
{
    std::map<std::string, std::vector<double>> skeleton;
    if (active_settings->skeleton_mode == "auto") {
        for (const chromosome& chr: active_settings->map) {
            double accept = -1.;
            for (double l: chr.condensed.marker_locus) {
                if (l > accept) {
                    skeleton[chr.name].push_back(l);
                    accept = l + active_settings->skeleton_interval;
                }
            }
        }
    } else if (active_settings->skeleton_mode == "manual") {
        for (const auto& name: active_settings->skeleton_markers) {
            bool found = false;
            for (const chromosome& chr: active_settings->map) {
                auto li = chr.condensed.marker_locus.begin();
                auto lj = chr.condensed.marker_locus.end();
                auto ni = chr.condensed.marker_name.begin();
                for (; li != lj; ++li, ++ni) {
                    if (*ni == name) {
                        skeleton[chr.name].push_back(*li);
                        found = true;
                        break;
                    }
                }
                if (found) {
                    break;
                }
            }
        }
    }
    mm.init_loci_by_hand(skeleton);

    return mm;
}
#endif



model_manager&
forward(model_manager& mm)
{
    report_algo_phase("Cofactor detection: forward");
    /*MSG_DEBUG("FORWARD");*/
    while (true) {
        const auto& result = mm.search_new_best();
        /*MSG_DEBUG(result);*/
        if (result.over_threshold) {
            result.select(mm);
            MSG_INFO("Select " << result.chrom->name << ':' << result.locus << " as cofactor");
        } else {
            break;
        }
    }
    MSG_INFO("Cofactors: " << mm.get_selection());
    return mm;
}


model_manager&
backward(model_manager& mm)
{
    report_algo_phase("Cofactor detection: backward");
    mm.select_all_loci();
    while (true) {
        auto worst_kb = mm.weakest_link();
        if (worst_kb.second >= mm.threshold) {
            break;
        }
        const model_block_key& mbk = worst_kb.first.first;
        assert(mbk->type == mbk_POP);
        for (double d: mbk->loci) {
            mm.deselect(mbk->chr, d);
        }
        /*MSG_DEBUG("Removed " << result.block_key << " which scored only " << result.test_value << " and now model has " << mm.Mcurrent.m_blocks.size() << " blocks");*/
    }
    return mm;
}



model_manager&
qtl_detect_cim(model_manager& mm)
{
    report_algo_phase("QTL Detection: CIM");

    return mm;
}


model_manager&
qtl_detect_cim_minus(model_manager& mm)
{
    report_algo_phase("QTL Detection: CIM-");
    auto chrom_best = mm.search_new_best_per_chromosome(true);
    mm.clear_selection();
    for (const auto& cb: chrom_best) {
        if (cb.second.over_threshold) {
            cb.second.select(mm);
        }
    }
    return mm;
}


struct loop_exception {};


bool
iqtlm_backward_one(model_manager& mm)
{
    std::vector<std::pair<bool, test_result>> results;
    size_t sz = 0;

    for (const auto& chr_sel: mm.get_selection()) { sz += chr_sel.second->depth(); }
    results.reserve(sz);

    bool not_modified = true;
    
    for (const auto& chr_sel: mm.get_selection()) {
        for (double d: chr_sel.second) {
            std::pair<bool, test_result> result = mm.challenge_qtl(chr_sel.first, d);
            MSG_DEBUG("Challenging QTL at " << chr_sel.first << ':' << d << " gives " << result.first << ' ' << result.second);
            results.emplace_back(result);
            not_modified &= results.back().first;
        }
    }

    {
        std::stringstream dbg;
        dbg << "backward";
        for (const auto& bt: results) {
            dbg << " {" << (bt.first ? "true" : "false") << ' ' << bt.second << '}';
        }
        MSG_DEBUG(dbg.str());
    }

    if (not_modified) {
        return false;
    }

    mm.clear_selection();

    for (const auto& good_result: results) {
        if (good_result.second.over_threshold) {
            good_result.second.select(mm);
        }
    }

    return true;
}


bool
in_history(std::set<std::map<chromosome_value, locus_key>>& history, const std::map<chromosome_value, locus_key>& selection)
{
    return !history.insert(selection).second;
}


bool
iqtlm_backward(std::string algo_suffix, model_manager& mm, std::set<std::map<chromosome_value, locus_key>>& history)
{
    report_algo_phase(SPELL_STRING("iQTLm" << algo_suffix << " backward"));
//     DUMP_FILE_LINE();
    while (iqtlm_backward_one(mm)) {
//     DUMP_FILE_LINE();
        if (in_history(history, mm.get_selection())) {
//     DUMP_FILE_LINE();
            return false;
        }
    }
//     DUMP_FILE_LINE();
    return true;
}


bool
iqtlm_forward(model_manager& mm, std::set<std::map<chromosome_value, locus_key>>& history)
{
    report_algo_phase("iQTLm forward");
//     DUMP_FILE_LINE();
    while (true) {
//     DUMP_FILE_LINE();
        auto results = mm.search_new_best_per_chromosome();
        bool modified = false;
        for (const auto& result: results) {
//     DUMP_FILE_LINE();
            modified |= result.second.over_threshold;
            if (result.second.over_threshold) {
//     DUMP_FILE_LINE();
                result.second.select(mm);
                MSG_DEBUG("added " << result);
            }
        }
//     DUMP_FILE_LINE();
        if (!modified) {
//     DUMP_FILE_LINE();
            return true;
        }
        if (in_history(history, mm.get_selection())) {
//     DUMP_FILE_LINE();
            return false;
        }
    }
}


bool
iqtlm_forward_gw(model_manager& mm, std::set<std::map<chromosome_value, locus_key>>& history)
{
    report_algo_phase("iQTLm-GW forward");
    while (true) {
        auto result = mm.search_new_best();
        bool modified = result.over_threshold;
        if (result.over_threshold) {
            result.select(mm);
        }
        if (!modified) {
            return true;
        }
        if (in_history(history, mm.get_selection())) {
            return false;
        }
    }
}


model_manager&
qtl_detect_iqtlm(model_manager& mm)
{
    report_algo_phase("QTL detection: iQTLm");
//     MSG_INFO("iQTLm starts with: " << mm.get_selection());
    std::set<std::map<chromosome_value, locus_key>> history;
    std::map<chromosome_value, locus_key> last_sel, new_sel;
    new_sel = mm.get_selection();
    do {
        if (iqtlm_backward("", mm, history) && iqtlm_forward(mm, history)) {
            last_sel = new_sel;
            new_sel = mm.get_selection();
        } else {
            break;
        }
    } while (new_sel != last_sel);
    return mm;
}


model_manager&
qtl_detect_iqtlm_gw(model_manager& mm)
{
    report_algo_phase("QTL detection: iQTLm-GW");
    std::set<std::map<chromosome_value, locus_key>> history;
    std::map<chromosome_value, locus_key> last_sel, new_sel;
    new_sel = mm.get_selection();
    while (new_sel != last_sel) {
        if (iqtlm_backward("-GW", mm, history) && iqtlm_forward_gw(mm, history)) {
            last_sel = new_sel;
            new_sel = mm.get_selection();
        } else {
            break;
        }
    }
    return mm;
}


#if 0
model_manager&
qtl_detect_iqtlm(model_manager& mm)
{
    bool go_on = true;
    model_manager::test_result result;
    mm.init_loci_by_step(active_settings->step);
    /*mm.set_joint_mode();*/
    /*mm.pmode = Joint;*/
    std::map<const chromosome*, std::unordered_set<locus_key>> previous_selections;
    std::map<const chromosome*, std::vector<std::pair<locus_key, MatrixXd>>> ordered_selections;
    std::map<const chromosome*, locus_key> previous;

    /*mm.output_test = true;*/
    /*mm.output_rank = true;*/
    /*mm.output_rss = true;*/
    /*mm.output_model = true;*/

    auto check_loops = [&] (const chromosome* chr)
    {
        auto& history = previous_selections[chr];
        locus_key current = mm.get_selection(chr);

        if (previous[chr] == current) {
            MSG_INFO("Selection is stable. Proceeding.");
            return false;
        }
        previous[chr] = current;

        MSG_INFO("[iQTLm] pass ended with selection " << mm.keys() << " (chromosome selection is " << current << ')');
        /*MSG_INFO("[iQTLm] sizeof(history) = " << history.size());*/
        auto i = history.begin(), j = history.end();
        if (i != j) {
            std::stringstream s;
            s << (*i);
            for (++i; i != j; ++i) {
                s << ", " << (*i);
            }
            MSG_DEBUG("History: " << s.str());
        }
        if (current->is_empty()) {
            return false;
        } else {
            auto ret = history.insert(current);
            if (!ret.second) {
                /*return false;*/
                throw loop_exception();
            }
            ordered_selections[chr].push_back({current, mm.Mcurrent.rss() / (mm.Mcurrent.X().innerSize() - mm.Mcurrent.rank())});
        }
        return true;
    };

    auto fix_loop = [&] (const chromosome* chr)
    {
        auto& history = ordered_selections[chr];
        locus_key current = mm.get_selection(chr);
        int i;
        for (i = history.size() - 1;
             i >= 0 && history[i].first != current;
             --i);
        if (i < 0) {
            MSG_ERROR("Couldn't find selection in history.", "");
        }
        int i_best = i;
        const int i_max = (int) history.size();
        for (; i < i_max; ++i) {
            if (history[i].second.lpNorm<1>() < history[i_best].second.lpNorm<1>()) {
                i_best = i;
            }
        }
        mm.set_selection(history[i_best].first);
    };

    for (const chromosome& chr: active_settings->map) {
        active_settings->set_title(SPELL_STRING("iQTLm on " << chr.name));
        try {
            /* setup */

            mm.select_chromosome(&chr);
            mm.cofactors_to_QTLs();
            previous[&chr] = mm.get_selection(&chr);
            do {
                /* step 1: backward */

                bool modified;

                MSG_INFO("[iQTLm] Backward");

                do {
                    /*locus_key loci = mm.Mcurrent.blocks_with_chromosome(&chr).begin()->first[&chr];*/
                    locus_key loci = mm.get_selection(&chr);
                    locus_key tmp_sel;

                    modified = false;

                    for (double l: *loci) {
                        auto tmp_removed = mm.remove(l);
                        /*mm.add_test_locus(&chr, l);*/
                        active_settings->set_title(SPELL_STRING("iQTLm on " << chr.name << " checking locus " << l << " against " << mm.keys()));
                        result = mm.test_along_chromosome(0, mm.max_testpos());
                        MSG_DEBUG(result);
                        if (result.over_threshold) {
                            model_block_key k1, k2;
                            k1 += std::make_pair(&chr, l);
                            k2 += std::make_pair(&chr, result.locus);
                            /*mm.add(result);*/
                            tmp_sel = tmp_sel + result.locus;
                            if (k1 != k2) {
                                MSG_DEBUG("[iQTLm] Replaced " << k1 << " with " << k2);
                            } else {
                                MSG_DEBUG("[iQTLm] Kept " << k1);
                            }
                            modified |= (result.locus != l);
                        } else {
                            model_block_key k1, k2;
                            k1 += std::make_pair(&chr, l);
                            MSG_DEBUG("[iQTLm] Removed " << k1);
                            modified = true;
                        }
                        mm.set_selection(tmp_removed);
                    }
                    if (modified) {
                        mm.set_selection(tmp_sel);
                    }
                } while (modified && check_loops(&chr));

                go_on = modified;

                /* step 2: forward */

                if (!check_loops(&chr)) {
                    active_settings->set_title(SPELL_STRING("iQTLm on " << chr.name << " forward"));
                    MSG_INFO("[iQTLm] Forward");
                    do {
                        result = mm.test_along_chromosome(0, mm.max_testpos());
                        MSG_DEBUG(result);
                        if (result.over_threshold) {
                            model_block_key k1;
                            k1 += std::make_pair(&chr, result.locus);
                            /*MSG_INFO("[iQTLm] Added " << (model_block_key() + std::make_pair(&chr, result.locus)));*/
                            mm.add(result);
                            MSG_DEBUG("[iQTLm] Added " << k1 << ", current selection is " << mm.keys());
                            go_on = true;
                        }
                    } while (result.over_threshold);
                    MSG_INFO("[iQTLm] pass ended with selection " << mm.keys() << " (chromosome selection is " << mm.get_selection(&chr) << ')');
                }

            } while (go_on && check_loops(&chr));

            /* teardown */
            /*mm.QTLs_to_cofactors();*/
        } catch (const loop_exception&) {
            fix_loop(&chr);
            MSG_WARNING("[iQTLm] Cycle detected while studying chromosome " << chr.name << ". Final selection is " << mm.get_selection(&chr));
        }
    }
    return mm;
}

#endif

