# Spell-QTL  Software suite for the QTL analysis of modern datasets.
# Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.0)
project(spell_qtl)

#LIST(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/CMake")
#INCLUDE(pandocology)

find_package(Boost 1.56.0 REQUIRED)
#set(CMAKE_CONFIGURATION_TYPES Debug Release CACHE INTERNAL FORCE)

# FIND_PACKAGE(PythonInterp 3)
# FIND_PACKAGE(PythonLibs 3)
# if (${PYTHON_VERSION_MAJOR} EQUAL 3)
#     FIND_PACKAGE(Boost COMPONENTS python3)
#     SET(Boost_PYTHON_LIBRARY boost_python-py35)
# else()
#     FIND_PACKAGE(Boost COMPONENTS python)
# endif()
# include_directories(AFTER ${PYTHON_INCLUDE_DIR})

set(CMAKE_VERBOSE_MAKEFILE ON)

set(BUILD_FOR_DEPLOYMENT FALSE CACHE BOOL "Link against static libc++ and use minimal symbol version where possible")

set(SANITIZER "" CACHE STRING "Select sanitizer (see compiler manpage for available options)")

MESSAGE(STATUS "CMAKE VERSION ${CMAKE_VERSION}")
MESSAGE(STATUS "${CMAKE_CURRENT_SOURCE_DIR}")

execute_process(
        COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/get_patch_number.sh
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        OUTPUT_VARIABLE VERSION_PATCH
        OUTPUT_STRIP_TRAILING_WHITESPACE
)

execute_process(
        COMMAND git tag
        COMMAND tail -1
        OUTPUT_VARIABLE LAST_TAG
        OUTPUT_STRIP_TRAILING_WHITESPACE
)

STRING(REGEX MATCHALL "[^.]+" VERSION "${LAST_TAG}")

MESSAGE(STATUS "LAST TAG ${LAST_TAG}")
MESSAGE(STATUS "VERSION ${VERSION}")

LIST(GET VERSION 0 VERSION_MAJOR)
LIST(GET VERSION 1 VERSION_MINOR)

set(CMAKE_CXX_STANDARD 14)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wextra -Wall -Wno-unused-parameter -pthread -fPIC")
SET(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} -O2 -DNDEBUG -DEIGEN_NO_DEBUG")
SET(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} -O0 -ggdb")

add_definitions(-DEIGEN_NO_DEPRECATED_WARNING -DVERSION_MAJOR=\"${VERSION_MAJOR}\" -DVERSION_MINOR=\"${VERSION_MINOR}\" -DVERSION_PATCH=\"${VERSION_PATCH}\")

MESSAGE(STATUS "CXX FLAGS ${CMAKE_CXX_FLAGS}")

find_library(EXPAT_LIBRARY_NAMES expat)
find_path(EXPAT_INCLUDE_DIR expat.h HINTS /usr/include /usr/local/include /usr/include/expat/ /usr/local/include/expat/)
# find_path(X2C_INCLUDE_DIR x2c/x2c.h HINTS /usr/include/ /usr/local/include/ /home/daleroux/include/)
find_path(EIGEN_INCLUDE_DIR Eigen/Eigen HINTS /usr/include /usr/local/include /usr/include/eigen3/ /usr/local/include/eigen3/ /home/daleroux/include/eigen3/)

find_library(XLNT_LIBRARY_NAMES xlnt)
find_path(XLNT_INCLUDE_DIR xlnt/xlnt.hpp HINTS /usr/include /usr/local/include)
include_directories(AFTER 3rd-party/ThreadPool)
include_directories(AFTER ${CMAKE_SOURCE_DIR}/include/ ${CMAKE_SOURCE_DIR}/include/input/ ${CMAKE_SOURCE_DIR}/include/bayes/ ${EIGEN_INCLUDE_DIR})
include_directories(SYSTEM /usr/include ${Boost_INCLUDE_DIRS} ${EXPAT_INCLUDE_DIR})
include_directories(AFTER /usr/include ${Boost_INCLUDE_DIRS} ${EXPAT_INCLUDE_DIR})

#if(${BUILD_FOR_DEPLOYMENT})
    #set(libstdcpp ${CMAKE_BINARY_DIR}/libstdc++.a)
#else(${BUILD_FOR_DEPLOYMENT})
    set(libstdcpp "")
#endif(${BUILD_FOR_DEPLOYMENT})

MESSAGE(STATUS "libstdcpp = ${libstdcpp}")

set(SPELL_PEDIGREE_SRC
        src/static_data.cc
        src/pedigree/main.cc
        src/pedigree/cli.cc
)

set(SPELL_MAP_SRC
        src/static_data.cc
        src/map-likelihood/main.cc
        src/map-likelihood/cli.cc
)

set(SPELL_MARKER_SRC
        src/input/read_mark.cc src/input/read_map.cc
        src/static_data.cc
        src/bayes/main.cc src/bayes/dispatch.cc src/bayes/jobs.cc src/bayes/cli.cc
)
# src/input/design.cc src/input/xml/xml_design.cc src/input/xml/xml_format.cc

set(SPELL_QTL_SRC
        src/input/read_map.cc src/input/read_trait.cc src/input/read_settings.cc src/input/ld_matrices.cc
        src/static_data.cc src/main.cc src/beta_gamma.cc
        src/computations/basic_data.cc src/computations/probabilities.cc src/computations/model.cc src/computations/frontends.cc
        src/computations/report.cc
)
#         src/input/xml/xml_format.cc src/input/xml/xml_settings.cc

MESSAGE(STATUS "spell-pedigree src = ${SPELL_PEDIGREE_SRC}")


#get_cmake_property(_variableNames VARIABLES)
#foreach (_variableName ${_variableNames})
#    message(STATUS "${_variableName}=${${_variableName}}")
#endforeach()

if ("${SANITIZER}" STREQUAL "")
    set(SANITIZER_OPT "")
else()
    set(SANITIZER_OPT "-fsanitize=${SANITIZER}")
endif()

if (${BUILD_FOR_DEPLOYMENT})
    add_executable(spell-pedigree ${SPELL_PEDIGREE_SRC} ${libstdcpp})
#     add_executable(spell-map ${SPELL_MAP_SRC} ${libstdcpp})
    add_executable(spell-marker ${SPELL_MARKER_SRC} ${libstdcpp})
    add_executable(spell-qtl ${SPELL_QTL_SRC} ${libstdcpp})
    add_custom_command(
        OUTPUT glibc.h libstdc++.a
        COMMAND ${CMAKE_SOURCE_DIR}/deploy/make_old_libc_header.sh
        WORKING_DIRECTORY ${CMAKE_BINARY_DIR})
    add_custom_command(
        OUTPUT libgmp.a
        COMMAND ${CMAKE_SOURCE_DIR}/deploy/compile_gmp.sh ${CMAKE_BINARY_DIR}/glibc.h ${CMAKE_BINARY_DIR}
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/deploy)
    add_custom_target(glibc.h)

    add_library(gmp INTERFACE IMPORTED)
    add_custom_target(pouet ALL DEPENDS ${CMAKE_BINARY_DIR}/libgmp.a)
    
    SET_SOURCE_FILES_PROPERTIES(${SPELL_PEDIGREE_SRC} ${SPELL_MARKER_SRC} ${SPELL_QTL_SRC}
            PROPERTIES
            OBJECT_DEPENDS glibc.h
            COMPILE_FLAGS "-include ${CMAKE_BINARY_DIR}/glibc.h -U_FORTIFY_SOURCE -D_FORTIFY_SOURCE=0 ${SANITIZER_OPT}")

    #add_dependencies(stdc++ libstdc++.a)
    #add_dependencies(gmp libgmp.a)
    add_dependencies(spell-pedigree glibc.h)
    add_dependencies(spell-marker glibc.h pouet)
    add_dependencies(spell-qtl glibc.h)

    set(CMAKE_EXE_LINKER_FLAGS "-include ${CMAKE_BINARY_DIR}/glibc.h -rdynamic -static-libgcc -static-libstdc++")

    target_link_libraries(spell-marker ${libstdcpp} ${CMAKE_BINARY_DIR}/libgmp.a)
    target_link_libraries(spell-qtl ${libstdcpp})
    target_link_libraries(spell-pedigree ${libstdcpp})
else()
    add_executable(spell-pedigree ${SPELL_PEDIGREE_SRC})
    add_executable(spell-marker ${SPELL_MARKER_SRC})
    add_executable(spell-qtl ${SPELL_QTL_SRC})
    # TODO add spell-map to BUILD_FOR_DEPLOYMENT
#     add_executable(spell-map ${SPELL_MAP_SRC})
    SET_SOURCE_FILES_PROPERTIES(${SPELL_PEDIGREE_SRC} ${SPELL_MARKER_SRC} ${SPELL_QTL_SRC}
            PROPERTIES
            COMPILE_FLAGS "-Wno-int-in-bool-context -U_FORTIFY_SOURCE -D_FORTIFY_SOURCE=0 ${SANITIZER_OPT}")
    set(CMAKE_EXE_LINKER_FLAGS "-rdynamic ${SANITIZER_OPT}")
endif()

target_compile_definitions(spell-qtl PUBLIC SPELL_USE_XLNT)

target_link_libraries(spell-marker dl gmp)
# target_link_libraries(spell-qtl ${Boost_PYTHON_LIBRARY} ${PYTHON_LIBRARY} xlnt dl rt)
target_link_libraries(spell-qtl xlnt dl rt)

SET(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin)

file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/doc)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/doc/man)

add_custom_target(manpages ALL)

function(add_manpage manpage)
    add_custom_command(
            TARGET manpages
            PRE_BUILD
            COMMAND pandoc --to man --standalone ${CMAKE_SOURCE_DIR}/doc/man/${manpage}.md -o ${manpage}
            WORKING_DIRECTORY doc/man
            COMMENT "Generating manpage ${manpage}")
    INSTALL(FILES ${CMAKE_CURRENT_BINARY_DIR}/doc/man/${manpage}
            DESTINATION share/man/man1)
endfunction()

add_manpage(spell-pedigree.1)
add_manpage(spell-marker.1)
add_manpage(spell-qtl.1)
add_manpage(spell-qtl-examples.1)

add_custom_target(user_manual.pdf ALL COMMAND ./build_manual.sh ${CMAKE_BINARY_DIR} ${CMAKE_SOURCE_DIR} WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/deploy)
add_dependencies(user_manual.pdf spell-pedigree spell-marker spell-qtl manpages)

#add_custom_command(OUTPUT spell-marker.1
#        COMMAND pandoc -t man ${CMAKE_SOURCE_DIR}/doc/man/spell-marker.1.md -o spell-marker.1
#        MAIN_DEPENDENCY ${CMAKE_SOURCE_DIR}/doc/man/spell-marker.1.md
#        WORKING_DIRECTORY doc/man)


#add_document(
#    spell-marker.1
#    SOURCES
#        ${CMAKE_SOURCE_DIR}/doc/man/spell-marker.1.md
#    PANDOC_DIRECTIVES
#        -t man
#    PRODUCT_DIRECTORY
#        doc/man
#)


# experimental binaries
#add_executable(test-json sandbox/test-json.cc)
                                

INSTALL(TARGETS spell-pedigree spell-marker spell-qtl DESTINATION bin)
#INSTALL(FILES
#        ${CMAKE_CURRENT_BINARY_DIR}/doc/man/spell-pedigree.1
#        ${CMAKE_CURRENT_BINARY_DIR}/doc/man/spell-marker.1
#        ${CMAKE_CURRENT_BINARY_DIR}/doc/man/spell-qtl.1
#        DESTINATION share/man/man1)

INSTALL(FILES ${CMAKE_SOURCE_DIR}/examples/read-POP.R
        DESTINATION share/spell-qtl/examples)

INSTALL(FILES ${CMAKE_BINARY_DIR}/doc/user_manual.pdf
        DESTINATION share/doc/spell-qtl)

INSTALL(FILES
        ${CMAKE_SOURCE_DIR}/examples/three_parents_F2/example1_F2C.gen
        ${CMAKE_SOURCE_DIR}/examples/three_parents_F2/example1_F2C.phen
        ${CMAKE_SOURCE_DIR}/examples/three_parents_F2/example1_F2.gen
        ${CMAKE_SOURCE_DIR}/examples/three_parents_F2/example1_F2.phen
        ${CMAKE_SOURCE_DIR}/examples/three_parents_F2/example1.map
        ${CMAKE_SOURCE_DIR}/examples/three_parents_F2/example1.ped
        ${CMAKE_SOURCE_DIR}/examples/three_parents_F2/README
        DESTINATION share/spell-qtl/examples/three_parents_F2)

INSTALL(FILES
        ${CMAKE_SOURCE_DIR}/examples/F3-outbred/F3-outbred_A.gen
        ${CMAKE_SOURCE_DIR}/examples/F3-outbred/F3-outbred_B.gen
        ${CMAKE_SOURCE_DIR}/examples/F3-outbred/F3-outbred_C.gen
        ${CMAKE_SOURCE_DIR}/examples/F3-outbred/F3-outbred_D.gen
        ${CMAKE_SOURCE_DIR}/examples/F3-outbred/F3-outbred_F3.gen
        ${CMAKE_SOURCE_DIR}/examples/F3-outbred/F3-outbred_F3.phen
        ${CMAKE_SOURCE_DIR}/examples/F3-outbred/F3-outbred.map
        ${CMAKE_SOURCE_DIR}/examples/F3-outbred/F3-outbred.ped
        ${CMAKE_SOURCE_DIR}/examples/F3-outbred/README
        DESTINATION share/spell-qtl/examples/F3-outbred)


INCLUDE(InstallRequiredSystemLibraries)

SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Software suite for the QTL analysis of modern datasets")
SET(CPACK_PACKAGE_VENDOR "Damien Leroux <damien.leroux@inra.fr>")
#SET(CPACK_PACKAGE_DESCRIPTION_FILE "${CMAKE_CURRENT_SOURCE_DIR}/ReadMe.txt")
SET(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/COPYING")
SET(CPACK_PACKAGE_VERSION_MAJOR ${VERSION_MAJOR})
SET(CPACK_PACKAGE_VERSION_MINOR ${VERSION_MINOR})
SET(CPACK_PACKAGE_VERSION_PATCH ${VERSION_PATCH})
SET(CPACK_GENERATOR "DEB;STGZ")
SET(CPACK_DEBIAN_PACKAGE_MAINTAINER "Damien Leroux <damien.leroux@inra.fr>")

INCLUDE(CPack)
