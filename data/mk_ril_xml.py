#!/usr/bin/env python

f = file('RIL.xml', 'w')

print >> f, """<design>
    <generation name="A">
        <ancestor haplotype="aa"/>
    </generation>
    <generation name="B">
        <ancestor haplotype="bb"/>
    </generation>
    <generation name="F1">
        <cross p1="A" p2="B"/>
    </generation>
"""

for i in xrange(2, 82):
    print >> f, '    <generation name="F%i">' % i
    print >> f, '        <self p1="F%i"/>' % (i - 1)
    print >> f, '    </generation>'

print >> f, '    <generation name="RIL">'
print >> f, '        <self p1="F81"/>'
print >> f, '    </generation>'
print >> f, '</design>'
