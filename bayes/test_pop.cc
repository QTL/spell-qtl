#include <dai/alldai.h>  // Include main libDAI header file
#include <dai/jtree.h>
#include <dai/bp.h>
#include <dai/decmap.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace dai;


size_t idx_to_ofs(const std::vector<size_t>& dim, const std::vector<size_t>& idx)
{
    size_t accum = 0;
    size_t stride = 1;
    if (dim.size() != idx.size()) { throw 0; }
    for (int i = dim.size() - 1; i >= 0; --i) {
        if (idx[i] >= dim[i]) { throw 1; }
        accum += stride * idx[i];
        stride *= dim[i];
    }
#if 0
    std::cout << "idx_to_ofs";
    for (int i = 0; i < dim.size(); ++i) {
        std::cout << ' ' << idx[i] << "[" << dim[i] << ']';
    }
    std::cout << " => " << accum << std::endl;
#endif
    return accum;
}


size_t size(const std::vector<size_t>& dim) {
    size_t stride = 1;
    for (auto d: dim) {
        stride *= d;
    }
    return stride;
}


std::pair<Var, size_t>
operator == (const Var& V, size_t conf)
{
    return {V, conf};
}


size_t factor_ofs(Factor& F, std::initializer_list<std::pair<Var, size_t>> configuration)
{
    VarSet vs;
    /* Stage 1: Paranoid */
    for (const auto& c: configuration) {
        if (vs.contains(c.first)) {
            throw std::runtime_error("Variable appears multiple times in configuration");
        }
        if (c.second >= c.first.states()) {
            throw std::runtime_error("State number greater than Variable cardinality");
        }
        if (!F.vars().contains(c.first)) {
            /* whine */
            throw std::runtime_error("Variable not in factor");
        }
        vs.insert(c.first);
    }
    if (vs.size() != F.vars().size()) {
        throw std::runtime_error("Not all variables of factor declared in configuration");
    }
    /* Stage 2: Iron Man */
    std::vector<size_t> dim;
    auto i = vs.rbegin();
    auto j = vs.rend();
    /*std::cout << "dim =";*/
    for (; i != j; ++i) {
        dim.push_back(i->states());
        /*std::cout << ' ' << dim.back();*/
    }
    /*std::cout << std::endl;*/
    /* Stage 3: War Pigs */
    std::vector<size_t> indices;
    i = vs.rbegin();
    /*std::cout << "idx =";*/
    for (; i != j; ++i) {
        for (const auto& c: configuration) {
            if (c.first.label() == i->label()) {
                indices.push_back(c.second);
                /*std::cout << ' ' << indices.back();*/
                break;
            }
        }
    }
    /*std::cout << std::endl;*/
    /* Stage 4: Megalomaniac architect */
    return idx_to_ofs(dim, indices);
}


void factor_set(Factor& F, double prob, std::initializer_list<std::pair<Var, size_t>> configuration)
{
    F.set(factor_ofs(F, configuration), prob);
}


void factor_add(Factor& F, double prob, std::initializer_list<std::pair<Var, size_t>> configuration)
{
    size_t ofs = factor_ofs(F, configuration);
    F.set(ofs, F.get(ofs) + prob);
}


void factor_zero(Factor& F)
{
    size_t size = 1;
    for (const auto& v: F.vars()) {
        size *= v.states();
    }
    for (size_t i = 0; i < size; ++i) {
        F.set(i, 0);
    }
}


void set_obs(Factor& F, size_t n_par, size_t n_al, double noise)
{
    if (F.vars().size() != 2) {
        throw std::runtime_error("Expecting exactly 2 variables in the var set of factor given to set_obs");
    }
    const Var& V = F.vars().front();
    const Var& O = F.vars().back();

    std::vector<size_t> dimtot = {n_al, n_al, n_par, n_al, n_par, n_al};
    std::vector<size_t> dimv = {n_par, n_al, n_par, n_al};
    std::vector<size_t> dimo = {n_al, n_al};

    for (size_t p1 = 0; p1 < n_par; ++p1) {
        for (size_t a1 = 0; a1 < n_al; ++a1) {
            for (size_t p2 = 0; p2 < n_par; ++p2) {
                for (size_t a2 = 0; a2 < n_al; ++a2) {
                    size_t idxv = idx_to_ofs(dimv, {p1, a1, p2, a2});
                    for (size_t im = 0; im < n_al; ++im) {
                        for (size_t jm = 0; jm < n_al; ++jm) {
                            size_t idxo = idx_to_ofs(dimo, {im, jm});
                            /*size_t idxtot = idx_to_ofs(dimtot, {p1, a1, p2, a2, im, jm});*/
                            size_t idxtot = idx_to_ofs(dimtot, {im, jm, p2, a2, p1, a1});
                            if (a1 == im && a2 == jm) {
                                factor_set(F, 1. - noise, {V == idxv, O == idxo});
                            } else {
                                factor_set(F, noise / (n_al * n_al - 1), {V == idxv, O == idxo});
                            }
                        }
                    }
                }
            }
        }
    }
}


void set_evidence(Factor& F, size_t n_al, size_t al_obs)
{
    int N = n_al * n_al;
    for (int i = 0; i < N; ++i) {
        F.set(i, 0);
    }
    double v = 1.;
    std::vector<size_t> dim = {n_al, n_al};
    for (size_t i = 0; i < n_al; ++i) {
        for (size_t j = 0; j < n_al; ++j) {
            F.set(idx_to_ofs(dim, {i, j}), i == al_obs && j == al_obs ? v : 0);
        }
    }
}


void set_ancestor_prob(Factor& F, size_t a, size_t n_par, size_t n_al)
{
    size_t N = n_par * n_al * n_par * n_al;
    for (size_t i = 0; i < N; ++i) {
        F.set(i, 0);
    }
    double v = 1.;
    std::vector<size_t> dim = {n_par, n_al, n_par, n_al};
    for (size_t i = 0; i < n_al; ++i) {
        F.set(idx_to_ofs(dim, {a, i, a, i}), v);
    }
}


void set_cross_prob(Factor& F, size_t n_par, size_t n_al)
{
    Var M = F.vars().elements().at(0),
        P = F.vars().elements().at(1),
        D = F.vars().elements().at(2);

    std::vector<size_t> dim = {n_par, n_al, n_par, n_al};

    factor_zero(F);

    for (size_t mp1 = 0; mp1 < n_par; ++mp1) {
        for (size_t ma1 = 0; ma1 < n_al; ++ma1) {
            for (size_t mp2 = 0; mp2 < n_par; ++mp2) {
                for (size_t ma2 = 0; ma2 < n_al; ++ma2) {
                    size_t idxm = idx_to_ofs(dim, {mp1, ma1, mp2, ma2});
                    for (size_t pp1 = 0; pp1 < n_par; ++pp1) {
                        for (size_t pa1 = 0; pa1 < n_al; ++pa1) {
                            for (size_t pp2 = 0; pp2 < n_par; ++pp2) {
                                for (size_t pa2 = 0; pa2 < n_al; ++pa2) {
                                    size_t idxp = idx_to_ofs(dim, {pp1, pa1, pp2, pa2});
                                    size_t idxd1 = idx_to_ofs(dim, {mp1, ma1, pp1, pa1});
                                    size_t idxd2 = idx_to_ofs(dim, {mp1, ma1, pp2, pa2});
                                    size_t idxd3 = idx_to_ofs(dim, {mp2, ma2, pp1, pa1});
                                    size_t idxd4 = idx_to_ofs(dim, {mp2, ma2, pp2, pa2});
                                    factor_add(F, .25, {M == idxm, P == idxp, D == idxd1});
                                    factor_add(F, .25, {M == idxm, P == idxp, D == idxd2});
                                    factor_add(F, .25, {M == idxm, P == idxp, D == idxd3});
                                    factor_add(F, .25, {M == idxm, P == idxp, D == idxd4});
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}


void set_dh_prob(Factor& F, size_t n_par, size_t n_al)
{
    Var M = F.vars().elements().at(0),
        D = F.vars().elements().at(1);

    std::vector<size_t> dim = {n_par, n_al, n_par, n_al};

    factor_zero(F);

    for (size_t mp1 = 0; mp1 < n_par; ++mp1) {
        for (size_t ma1 = 0; ma1 < n_al; ++ma1) {
            for (size_t mp2 = 0; mp2 < n_par; ++mp2) {
                for (size_t ma2 = 0; ma2 < n_al; ++ma2) {
                    size_t idxm = idx_to_ofs(dim, {mp1, ma1, mp2, ma2});
                    size_t idxd1 = idx_to_ofs(dim, {mp1, ma1, mp1, ma1});
                    size_t idxd2 = idx_to_ofs(dim, {mp2, ma2, mp2, ma2});
                    factor_add(F, .5, {M == idxm, D == idxd1});
                    factor_add(F, .5, {M == idxm, D == idxd2});
                }
            }
        }
    }
}


void set_self_prob(Factor& F, size_t n_par, size_t n_al)
{
    std::vector<size_t> dim = {n_par, n_al, n_par, n_al, n_par, n_al, n_par, n_al};
#define gpa(_g_, _i_) g##_g_##p##_i_, g##_g_##a##_i_
    for (size_t g1p1 = 0; g1p1 < n_par; ++g1p1) {
        for (size_t g1a1 = 0; g1a1 < n_al; ++g1a1) {
            for (size_t g1p2 = 0; g1p2 < n_par; ++g1p2) {
                for (size_t g1a2 = 0; g1a2 < n_al; ++g1a2) {
                    for (size_t g2p1 = 0; g2p1 < n_par; ++g2p1) {
                        for (size_t g2a1 = 0; g2a1 < n_al; ++g2a1) {
                            for (size_t g2p2 = 0; g2p2 < n_par; ++g2p2) {
                                for (size_t g2a2 = 0; g2a2 < n_al; ++g2a2) { /* sic */
                                    size_t i;
                                    i = idx_to_ofs(dim, {gpa(2, 2), gpa(2, 1), gpa(1, 2), gpa(1, 1)});
                                    if (g1p1 == g1p2 && g1a1 == g1a2 && g2p1 == g2p2 && g2a1 == g2a2 && g2p1 == g1p1 && g2a1 == g1a1) {
                                        F.set(i, 1);
                                    } else if (g1p1 == g2p1 && g1a1 == g2a1 && g1p2 == g2p2 && g1a2 == g2a2) {
                                        F.set(i, .25);
                                    } else if (g1p2 == g2p1 && g1a2 == g2a1 && g1p2 == g2p2 && g1a2 == g2a2) {
                                        F.set(i, .25);
                                    } else if (g1p2 == g2p1 && g1a2 == g2a1 && g1p1 == g2p2 && g1a1 == g2a2) {
                                        F.set(i, .25);
                                    } else if (g1p1 == g2p1 && g1a1 == g2a1 && g1p1 == g2p2 && g1a1 == g2a2) {
                                        F.set(i, .25);
                                    } else {
                                        F.set(i, 0);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
#undef gpa
}




struct __AutoVar {
    static int& __index() { static int idx = -1; return idx; }
    static int __next() { return ++__index(); }
};


#define AUTOVAR(__name, __card) Var __name(__AutoVar::__next(), __card)



void dump_mat(const std::vector<double>& v) {
    size_t N2 = v.size();
    size_t N = 1;
    for (; (N * N) < N2; ++N);
    auto i = v.begin();
    auto j = v.end();
    for (; i != j;) {
        std::cout << (*i++);
        for (size_t n = 1; n < N && i != j; ++n) {
            std::cout << '\t' << (*i++);
        }
        std::cout << std::endl;
    }
}



struct choco_bn {
    InfAlg* alg;
    std::vector<Var> vars;
    std::vector<Factor> factors;
    std::vector<bool> observed;
    FactorGraph* network;
    FactorGraph* current_network;
    size_t n_par, n_al;
    double observation_noise;

    choco_bn(size_t n_parents, size_t n_alleles)
        : alg(0), vars(), factors()
        , network(0), current_network(0)
        , n_par(n_parents), n_al(n_alleles)
        , observation_noise(0)
    {}
    ~choco_bn()
    {
        if (alg) { delete alg; }
        if (network) { delete network; }
    }

    Var decl_obs(const Var& Observed, double noise)
    {
        observed[Observed.label()] = true;
        Var o = decl_var(n_al * n_al);
        factors.emplace_back(VarSet(Observed, o));
        set_obs(factors.back(), n_par, n_al, noise);
        return o;
    }

    Var ancestor(size_t parent_number, bool observed=false)
    {
        Var a = decl_var(n_par * n_par * n_al * n_al);
        factors.emplace_back(a);
        set_ancestor_prob(factors.back(), parent_number, n_par, n_al);
        if (observed) { decl_obs(a, observation_noise); }
        return a;
    }

    Var crossing(const Var& M, const Var& P, bool observed=false)
    {
        Var g = decl_var(n_par * n_par * n_al * n_al);
        VarSet vs(M, P);
        std::cout << vs << std::endl;
        vs |= g;
        std::cout << vs << std::endl;
        factors.emplace_back(vs);
        set_cross_prob(factors.back(), n_par, n_al);
        if (observed) { decl_obs(g, observation_noise); }
        return g;
    }

    Var selfing(const Var& A, bool observed=false)
    {
        Var g = decl_var(n_par * n_par * n_al * n_al);
        factors.emplace_back(VarSet(A, g));
        set_self_prob(factors.back(), n_par, n_al);
        if (observed) { decl_obs(g, observation_noise); }
        return g;
    }

    Var dhing(const Var& A, bool observed=false)
    {
        Var g = decl_var(n_par * n_par * n_al * n_al);
        factors.emplace_back(VarSet(A, g));
        set_dh_prob(factors.back(), n_par, n_al);
        if (observed) { decl_obs(g, observation_noise); }
        return g;
    }

    std::vector<Var> decl_gen_cross(const Var& M, const Var& P, size_t n, bool observed=false)
    {
        std::vector<Var> ret;
        ret.reserve(n);
        for (; n > 0; --n) {
            ret.emplace_back(crossing(M, P, observed));
        }
        return ret;
    }

    std::vector<Var> decl_gen_self(const Var& A, size_t n, bool observed=false)
    {
        std::vector<Var> ret;
        ret.reserve(n);
        for (; n > 0; --n) {
            ret.emplace_back(selfing(A, observed));
        }
        return ret;
    }

    std::vector<Var> decl_gen_dh(const Var& A, size_t n, bool observed=false)
    {
        std::vector<Var> ret;
        ret.reserve(n);
        for (; n > 0; --n) {
            ret.emplace_back(dhing(A, observed));
        }
        return ret;
    }

    /* FIXME: can be merged with reset() */
    void build_network()
    {
        if (network) { delete network; }
        network = new FactorGraph(factors);
    }

    void reset()
    {
        if (current_network) { delete current_network; }
        current_network = network->clone();
    }

    void add_evidence(const Var& v, const std::vector<size_t> states)
    {
        if (!observed[v.label()]) {
            throw std::runtime_error("Variable is not observed.");
        }
        /* FIXME: La variable observation SI ELLE EXISTE est TOUJOURS variable+1. À nettoyer. */
        current_network->clampVar(v.label() + 1, states);
    }

    void add_evidence(const std::vector<Var>& vv, const std::vector<std::vector<size_t>> vstates)
    {
        if (vv.size() != vstates.size()) {
            throw std::runtime_error("Vectors of variables and variable states don't have the same length");
        }
        for (size_t i = 0; i < vv.size(); ++i) {
            add_evidence(vv[i], vstates[i]);
        }
    }

    void compute()
    {
        PropertySet opts;
        opts.set("tol", 1.e-20);
        /*opts.set("updates", std::string("LINEAR"));*/
        opts.set("verbose", 0ul);
        /*opts.set("doubleloop", false);*/
        /*opts.set("logdomain",false);*/
        //opts.set("clusters", std::string("DELTA")); /* MIN BETHE DELTA LOOP */
        if (!current_network) {
            reset();
        }
        /*alg = new JTree(*current_network, opts);*/
        opts.set("maxiter", 10000ul);
        /*opts.set("inits", std::string("EXACT"));*/
        /*opts.set("ianame", std::string(""));*/
        /*opts.set("iaopts", std::string(""));*/
        alg = new JTree(*current_network, opts("updates", std::string("HUGIN"))("inference", std::string("MAXPROD")));
        alg->init();
        alg->run();
    }

    Factor belief(const Var& v)
    {
        if (!alg) { return {}; }
        return alg->belief(v);
    }

private:
    Var decl_var(size_t n_states)
    {
        vars.emplace_back(vars.size(), n_states);
        observed.push_back(false);
        return vars.back();
    }

    Var decl_gen()
    {
        return decl_var(n_par * n_par * n_al * n_al);
    }
};


std::vector<double> reduce_alleles(std::vector<double> p, size_t n_par, size_t n_al)
{
    std::vector<double> ret;
    ret.resize(n_par * n_par, 0.);
    std::vector<size_t> dimA = {n_par, n_al, n_par, n_al};
    std::vector<size_t> dim = {n_par, n_par};
    for (size_t g1p1 = 0; g1p1 < n_par; ++g1p1) {
        for (size_t g1a1 = 0; g1a1 < n_al; ++g1a1) {
            for (size_t g1p2 = 0; g1p2 < n_par; ++g1p2) {
                for (size_t g1a2 = 0; g1a2 < n_al; ++g1a2) {
                    size_t idxA = idx_to_ofs(dimA, {g1p1, g1a1, g1p2, g1a2});
                    size_t idx = idx_to_ofs(dim, {g1p1, g1p2});
                    ret[idx] += p[idxA];
                }
            }
        }
    }
    return ret;
}


int main()
{
    size_t n_p = 4;
    size_t n_a = 2;

    choco_bn bn(n_p, n_a);

    bn.observation_noise = .01;

    Var A = bn.ancestor(0, true);
    Var B = bn.ancestor(1, true);
    Var F1 = bn.crossing(A, B);
    Var F2 = bn.selfing(F1);
    Var F3 = bn.selfing(F2);
    Var F4 = bn.selfing(F3);
    Var F5 = bn.selfing(F4);
    Var DH = bn.dhing(F2);
    std::vector<Var> FObs = bn.decl_gen_self(F5, 100, true);

#define DUMP_VAR(__x) std::cout<< #__x ": " << std::endl; dump_mat(reduce_alleles(bn.belief(__x).p().p(), n_p, n_a)); std::cout <<std::endl;

    bn.build_network();
    bn.compute();
    /*DUMP_VAR(F2);*/
    /*DUMP_VAR(FObs[0]);*/
    /*DUMP_VAR(FObs[1]);*/

    std::vector<std::vector<size_t>> f3obs;
    f3obs.resize(FObs.size());
    size_t i;
    for (size_t hcount = 1; hcount < FObs.size(); ++hcount) {
        i = 0;
        std::generate(f3obs.begin(), f3obs.end(), [&] {
            if (!(i++ >= hcount)) {
                return std::vector<size_t>{1, 2};
            }
            return std::vector<size_t>{0};
        });
        bn.reset();
        /* insert evidence here */
        bn.add_evidence(A, {0});
        bn.add_evidence(B, {3});
        bn.add_evidence(FObs, f3obs);
        bn.compute();
        /*std::cout << "H-INTERVAL " << interval << std::endl;*/
        /*DUMP_VAR(A);*/
        /*DUMP_VAR(B);*/
        /*DUMP_VAR(F1);*/
        /*DUMP_VAR(F2);*/
        /*DUMP_VAR(DH);*/
        /*DUMP_VAR(FObs[0]);*/
        std::cout << "pop.size " << FObs.size() << " H-RATIO " << std::setw(5) << (100. * hcount / (double) FObs.size()) << "%\tF2:A=" << bn.belief(F2).p().p()[0];
        std::cout << "\tF3:A=" << bn.belief(F3).p().p()[0];
        std::cout << "\tF4:A=" << bn.belief(F4).p().p()[0];
        std::cout << "\tF5:A=" << bn.belief(F5).p().p()[0];
        std::cout << "\tFObs-H:A=" << bn.belief(FObs[0]).p().p()[0] << std::endl;
        /*DUMP_VAR(FObs[1]);*/
        /*DUMP_VAR(FObs[2]);*/
        /*DUMP_VAR(FObs[3]);*/
    }

    return 0;
}
#undef DUMP_VAR



int main_old_2()
{
    size_t n_p = 2;
    size_t n_a = 4;
    choco_bn bn(n_p, n_a);

    Var A = bn.ancestor(0);
    Var B = bn.ancestor(1);
    Var OA = bn.decl_obs(A, 0);
    Var OB = bn.decl_obs(B, 0);
    Var F1 = bn.crossing(A, B);
    Var F2 = bn.selfing(F1);
    Var F31 = bn.selfing(F2);
    Var F32 = bn.selfing(F2);
    Var F33 = bn.selfing(F2);
    Var F34 = bn.selfing(F2);
    Var O31 = bn.decl_obs(F31, 0.);
    Var O32 = bn.decl_obs(F32, 0.);
    Var O33 = bn.decl_obs(F33, 0.);
    Var O34 = bn.decl_obs(F34, 0.);

    bn.build_network();

#define DUMP_VAR(__x) std::cout<< #__x ": " << std::endl; dump_mat(reduce_alleles(bn.belief(__x).p().p(), n_p, n_a)); std::cout <<std::endl;

    {
        bn.reset();
        /* insert evidence here */
        bn.add_evidence(OA, {0});
        bn.add_evidence(OB, {10});
        bn.add_evidence(O31, {0});
        bn.add_evidence(O32, {0});
        bn.add_evidence(O33, {0});
        bn.add_evidence(O34, {8});
        bn.compute();
        DUMP_VAR(A);
        DUMP_VAR(B);
        DUMP_VAR(F1);
        DUMP_VAR(F2);
        DUMP_VAR(F31);
        DUMP_VAR(F32);
        DUMP_VAR(F33);
        DUMP_VAR(F34);
    }

    {
        bn.reset();
        /* insert evidence here */
        bn.add_evidence(OA, {0});
        bn.add_evidence(OB, {10});
        bn.add_evidence(O31, {0});
        bn.add_evidence(O32, {0});
        bn.add_evidence(O33, {0});
        bn.add_evidence(O34, {8});
        bn.compute();
        DUMP_VAR(A);
        DUMP_VAR(B);
        DUMP_VAR(F1);
        DUMP_VAR(F2);
        DUMP_VAR(F31);
        DUMP_VAR(F32);
        DUMP_VAR(F33);
        DUMP_VAR(F34);
    }

    return 0;
}
#undef DUMP_VAR




int old_main() {
    size_t n_allele = 2;
    size_t n_par = 4;
    size_t N = n_par * n_allele * n_par * n_allele;

    /*std::cout << "PROUT 10x10x10" << size({10, 10, 10}) << std::endl;*/

    /* F0 */
    AUTOVAR(A, N);
    AUTOVAR(OA, n_allele * n_allele);
    AUTOVAR(B, N);
    AUTOVAR(OB, n_allele * n_allele);
    AUTOVAR(C, N);
    AUTOVAR(OC, n_allele * n_allele);
    AUTOVAR(D, N);
    AUTOVAR(OD, n_allele * n_allele);

    /* F1 */
    AUTOVAR(AB, N);
    AUTOVAR(CD, N);

    /* CP */
    AUTOVAR(CP, N);

    /* F2 */
    AUTOVAR(F2, N);

    /* F3's */
    AUTOVAR(F31, N);
    AUTOVAR(F32, N);
    AUTOVAR(F33, N);

    /* Obs */

    AUTOVAR(O31, n_allele * n_allele);
    AUTOVAR(O32, n_allele * n_allele);
    AUTOVAR(O33, n_allele * n_allele);

    /* Probas */

    Factor P_A(A);
    set_ancestor_prob(P_A, 0, n_par, n_allele);
    Factor P_B(B);
    set_ancestor_prob(P_B, 1, n_par, n_allele);
    Factor P_C(C);
    set_ancestor_prob(P_C, 2, n_par, n_allele);
    Factor P_D(D);
    set_ancestor_prob(P_D, 3, n_par, n_allele);

    Factor P_OA(OA);
    /*set_evidence(P_OA, n_allele, 0);*/
    Factor P_OB(OB);
    /*set_evidence(P_OB, n_allele, 1);*/
    Factor P_OC(OC);
    /*set_evidence(P_OC, n_allele, 0);*/
    Factor P_OD(OD);
    /*set_evidence(P_OD, n_allele, 1);*/

    /* Facteurs */
    /* Muiiiiiiihihihihihihihihhhhhh pflflflflflflflp */

    Factor AB_given_A_B(VarSet(A, B) | AB);
    set_cross_prob(AB_given_A_B, n_par, n_allele);
    Factor CD_given_C_D(VarSet(C, D) | CD);
    set_cross_prob(CD_given_C_D, n_par, n_allele);
    Factor CP_given_AB_CD(VarSet(AB, CD) | CP);
    set_cross_prob(CP_given_AB_CD, n_par, n_allele);
    Factor F2_given_CP(VarSet(CP, F2));
    set_self_prob(F2_given_CP, n_par, n_allele);
    Factor F31_given_F2(VarSet(F31, F2));
    set_self_prob(F31_given_F2, n_par, n_allele);
    Factor F32_given_F2(VarSet(F32, F2));
    set_self_prob(F32_given_F2, n_par, n_allele);
    Factor F33_given_F2(VarSet(F33, F2));
    set_self_prob(F33_given_F2, n_par, n_allele);

    /* Observations */

    Factor OA_given_A(VarSet(A, OA));
    set_obs(OA_given_A, n_par, n_allele, 0);
    Factor OB_given_B(VarSet(B, OB));
    set_obs(OB_given_B, n_par, n_allele, 0);
    Factor OC_given_C(VarSet(C, OC));
    set_obs(OC_given_C, n_par, n_allele, 0);
    Factor OD_given_D(VarSet(D, OD));
    set_obs(OD_given_D, n_par, n_allele, 0);

    Factor O31_given_F31(VarSet(F31, O31));
    set_obs(O31_given_F31, n_par, n_allele, 0);
    Factor O32_given_F32(VarSet(F32, O32));
    set_obs(O32_given_F32, n_par, n_allele, 0);
    Factor O33_given_F33(VarSet(F33, O33));
    set_obs(O32_given_F32, n_par, n_allele, 0);

    std::vector<Factor> PedigreeFactors;
    PedigreeFactors.push_back(P_A);
    PedigreeFactors.push_back(P_B);
    PedigreeFactors.push_back(P_C);
    PedigreeFactors.push_back(P_D);
    PedigreeFactors.push_back(P_OA);
    PedigreeFactors.push_back(P_OB);
    PedigreeFactors.push_back(P_OC);
    PedigreeFactors.push_back(P_OD);
    PedigreeFactors.push_back(AB_given_A_B);
    PedigreeFactors.push_back(CD_given_C_D);
    PedigreeFactors.push_back(CP_given_AB_CD);
    PedigreeFactors.push_back(OA_given_A);
    PedigreeFactors.push_back(OB_given_B);
    PedigreeFactors.push_back(OC_given_C);
    PedigreeFactors.push_back(OD_given_D);
    PedigreeFactors.push_back(F2_given_CP);

    PedigreeFactors.push_back(F31_given_F2);
    PedigreeFactors.push_back(F32_given_F2);
    PedigreeFactors.push_back(F33_given_F2);
    PedigreeFactors.push_back(O31_given_F31);
    PedigreeFactors.push_back(O32_given_F32);
    PedigreeFactors.push_back(O33_given_F33);
    FactorGraph PedigreeNetwork( PedigreeFactors );
    PedigreeNetwork.clamp(OA.label(), 0);
    PedigreeNetwork.clamp(OB.label(), 3);
    /*PedigreeNetwork.clamp(OC.label(), 3);*/
    /*PedigreeNetwork.clamp(OD.label(), 3);*/
    PedigreeNetwork.WriteToFile( "pedigree.fg" );

    {
        InfAlg* alg;

        std::cout << "INFERENCE..." << std::endl;

        {
            PropertySet opts;
            opts.set("tol", 1.e-9);
            /*opts.set("logdomain", false);*/
            /*opts.set("updates", std::string("FULL"));*/
            opts.set("updates", std::string("HUGIN")); /* or SHSH */
            //opts.set("updates", std::string("SHSH")); /* or HUGIN */
            opts.set("verbose", 1ul);
            /*opts.set("maxiter", 100ul);*/
            /*opts.set("inits", std::string("CLAMPING"));*/
            opts.set("doubleloop", false);
            opts.set("clusters", std::string("DELTA")); /* MIN BETHE DELTA LOOP */
            /*opts.set("type", std::string("ALT"));*/
            /*alg = new HAK(PedigreeNetwork, opts);*/
            alg = new JTree(PedigreeNetwork, opts);
            alg->init();
            alg->run();
        }
        
#define DUMP_VAR(__x) std::cout<< #__x ": " << std::endl; dump_mat(alg->belief(__x).p().p()); std::cout <<std::endl;
        DUMP_VAR(AB);
        DUMP_VAR(A);
        DUMP_VAR(OA);
        DUMP_VAR(B);
        DUMP_VAR(OB);
        DUMP_VAR(C);
        DUMP_VAR(OC);
        DUMP_VAR(D);
        DUMP_VAR(OD);
        DUMP_VAR(CD);
        DUMP_VAR(CP);
        DUMP_VAR(F2);
        delete alg;
    }

    if (0) {
        std::cout << "######################## EXACT ########################" << std::endl;
        Factor P; 
        for (int I = 0; I < PedigreeNetwork.nrFactors(); I++)
        {
            P *= PedigreeNetwork.factor( I );
        }
#undef DUMP_VAR
#define DUMP_VAR(__x) std::cout<< #__x ": " << std::endl; dump_mat(P.marginal(__x).p().p()); std::cout <<std::endl;
        DUMP_VAR(AB);
        DUMP_VAR(A);
        DUMP_VAR(OA);
        DUMP_VAR(B);
        DUMP_VAR(OB);
        /*std::cout<<"C : "<< P.marginal( C )<<std::endl;*/
        /*std::cout<<"OC : "<< P.marginal( OC )<<std::endl;*/
        /*std::cout<<"D : "<< P.marginal( D )<<std::endl;*/
        /*std::cout<<"OD : "<< P.marginal( OD )<<std::endl;*/
        /*std::cout<<"F31 : "<< P.marginal( F31 )<<std::endl<<std::endl;*/
    }

    return 0;
}
