#ifdef _SPELL_TEST_SETTINGS_FIXTURE_H_
#error "_SPELL_TEST_SETTINGS_FIXTURE_H_ already defined!"
#else
#define _SPELL_TEST_SETTINGS_FIXTURE_H_

extern const char* bc_conf[];

struct settings_fixture {
    settings_fixture(const char** argv)
    {
        int argc = 0;
        for (; argv[argc]; ++argc);
        active_settings = settings_t::from_args(argc, argv);
        /*REQUIRE(active_settings != NULL);*/
        if (active_settings) {
            active_settings->finalize();
        } else {
            /*throw bad_settings_exception("Couldn't initialize settings.");*/
            MSG_ERROR("Couldn't initialize settings.", "");
        }
    }

    ~settings_fixture() { delete active_settings; active_settings = NULL; }
};

#endif

