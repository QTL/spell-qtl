#include "fast_polynom.h"
#include <iostream>
#include "catch.hpp"




TEST_CASE("degree_and_valuation", "Check some wrappers")
{
    impl::polynom_tables::reset();
    CHECK(fast_polynom::one.degree() == 0);
    CHECK(fast_polynom::zero.degree() == 0);
    CHECK(fast_polynom::r.degree() == 1);
    CHECK(fast_polynom::s.degree() == 1);
    CHECK(fast_polynom::one.valuation() == 0);
    CHECK(fast_polynom::zero.valuation() == 0);
    CHECK(fast_polynom::r.valuation() == 1);
    CHECK(fast_polynom::s.valuation() == 0);
    fast_polynom p = {1, 0, 1};
    CHECK(p.degree() == 2);
    CHECK(p.valuation() == 0);
    fast_polynom q = {0, 0, 0, 1};
    CHECK(q.degree() == 3);
    CHECK(q.valuation() == 3);
    fast_polynom fs = (const impl::f_polynom&) fast_polynom::s;
    fast_polynom fr = (const impl::f_polynom&) fast_polynom::r;
    fast_polynom fone = (const impl::f_polynom&) fast_polynom::one;
    fast_polynom fzero = (const impl::f_polynom&) fast_polynom::zero;
    CHECK(fone.degree() == 0);
    CHECK(fzero.degree() == 0);
    CHECK(fr.degree() == 1);
    CHECK(fs.degree() == 1);
    CHECK(fone.valuation() == 0);
    CHECK(fzero.valuation() == 0);
    CHECK(fr.valuation() == 1);
    CHECK(fs.valuation() == 0);
    /*std::cout << p << std::endl;*/
}









TEST_CASE("basic.reg", "Check all four axiomatic polynoms are correctly registered")
{
    impl::polynom_tables::reset();
    CHECK(fast_polynom::zero != fast_polynom::one);
    CHECK(fast_polynom::zero != fast_polynom::r);
    CHECK(fast_polynom::zero != fast_polynom::s);
    CHECK(fast_polynom::one != fast_polynom::r);
    CHECK(fast_polynom::one != fast_polynom::s);
    CHECK(fast_polynom::r != fast_polynom::s);
}


TEST_CASE("adv.reg", "Check new polynoms are correctly registered")
{
    impl::polynom_tables::reset();
    fast_polynom a = {1, 2, 1};
    fast_polynom b = {1, 1, 1};
    fast_polynom _s = {1, -1};
    CHECK(_s == fast_polynom::s);
    CHECK(a != fast_polynom::zero);
    CHECK(a != fast_polynom::one);
    CHECK(a != fast_polynom::r);
    CHECK(a != fast_polynom::s);
    CHECK(b != fast_polynom::zero);
    CHECK(b != fast_polynom::one);
    CHECK(b != fast_polynom::r);
    CHECK(b != fast_polynom::s);
    CHECK(a != b);
}

TEST_CASE("basic.add", "Check simple additions of axiomatic polynoms")
{
    impl::polynom_tables::reset();
    /*std::cout << "Add table" << std::endl << impl::polynom_tables::add_table() << std::endl;*/
    fast_polynom _2s = {2, -2};
    /*std::cout << ((const impl::d_polynom&) _2s) << std::endl;*/
    /*std::cout << ((const impl::f_polynom&) _2s) << std::endl;*/
    /*std::cout << "Add table" << std::endl << impl::polynom_tables::add_table() << std::endl;*/
    fast_polynom _2r = {0, 2};
    /*std::cout << "Add table" << std::endl << impl::polynom_tables::add_table() << std::endl;*/
    fast_polynom a = fast_polynom::r + fast_polynom::r;
    /*std::cout << "Add table" << std::endl << impl::polynom_tables::add_table() << std::endl;*/
    fast_polynom b = fast_polynom::s + fast_polynom::s;
    /*std::cout << "Add table" << std::endl << impl::polynom_tables::add_table() << std::endl;*/
    /*std::cout << "fb " << ((const impl::f_polynom&) b) << std::endl;*/
    /*std::cout << "db " << ((const impl::d_polynom&) b) << std::endl;*/
    CHECK((fast_polynom::r + fast_polynom::s) == fast_polynom::one);
    CHECK((fast_polynom::zero + fast_polynom::zero) == fast_polynom::zero);
    CHECK((fast_polynom::one + fast_polynom::zero) == fast_polynom::one);
    CHECK((fast_polynom::r + fast_polynom::zero) == fast_polynom::r);
    CHECK((fast_polynom::s + fast_polynom::zero) == fast_polynom::s);
    CHECK(_2r == a);
    CHECK(_2s == b);
    fast_polynom _s = {1, -1};
    CHECK(_s == fast_polynom::s);
    fast_polynom __2s = _s + _s;
    CHECK(__2s == _2s);
    CHECK((fast_polynom::r + fast_polynom::s) == (fast_polynom::s + fast_polynom::r));
}

TEST_CASE("more.add", "Check more additions")
{
    fast_polynom p;
    p = fast_polynom::s + fast_polynom::s;
    p += fast_polynom::s;
    CHECK((p == fast_polynom{3, -3}));
    /*std::cout << p << "   " << ((const impl::f_polynom&)p) << std::endl;*/
    p = fast_polynom::s + fast_polynom::s;
    p += fast_polynom::r;
    CHECK((p == fast_polynom{2, -1}));
    /*std::cout << p << "   " << ((const impl::f_polynom&)p) << std::endl;*/
}


TEST_CASE("adv.ops", "Check more combinations")
{
    fast_polynom p;
    p = fast_polynom::s * fast_polynom::s + fast_polynom::r.pow(3);
    /*std::cout << p << "   " << ((const impl::f_polynom&)p) << std::endl;*/
    CHECK((p == fast_polynom{1 , -2, 1, 1}));
    p = fast_polynom::s * fast_polynom::s + fast_polynom::r.pow(3) - fast_polynom::s;
    /*std::cout << p << "   " << ((const impl::f_polynom&)p) << std::endl;*/
    CHECK((p == fast_polynom{0 , -1, 1, 1}));
}


TEST_CASE("side.effects", "Check against some side-effects")
{
    impl::polynom_tables::reset();
    fast_polynom _c = {1, -2, 1};
    fast_polynom _a = {1, 2, 1};
    fast_polynom _b = {1, 1, 1};
    fast_polynom a = fast_polynom::r + fast_polynom::r;
    fast_polynom b = fast_polynom::s + fast_polynom::s;
    /*std::cout << "_a " << _a << std::endl;*/
    /*std::cout << "_b " << _b << std::endl;*/
    /*std::cout << "_c " << _c << std::endl;*/
    REQUIRE_FALSE(_a == _c);
    CHECK(a != _a);
    CHECK(a != _b);
    CHECK(b != _a);
    CHECK(b != _b);
}

TEST_CASE("side.effects.2", "Check against some side-effects")
{
    impl::polynom_tables::reset();
    fast_polynom _a = {1, 2, 1};
    fast_polynom _c = {1, -2, 1};
    /*std::cout << "_a " << _a << std::endl;*/
    /*std::cout << "_c " << _c << std::endl;*/
    REQUIRE_FALSE(_a == _c);
}

TEST_CASE("d_polynom", "Basic checks for d_polynom implementation")
{
    impl::polynom_tables::reset();
    using dpoly=impl::d_polynom;
    using poly=impl::polynom;
    using fpoly=impl::f_polynom;
    dpoly a = {1, -1};
    dpoly b = {2, -2};
    CHECK(a.add(a) == b);
    fpoly fb = b;
    dpoly db = fb;
    CHECK(db == b);
    poly c = a;
    dpoly dc = c.add(c).to_d();
    CHECK(dc == b);
    dpoly s2 = {1, -2, 1};
    dpoly s2plus4r2 = {1, 2, 1};
    CHECK_FALSE(s2 == s2plus4r2);
    fpoly fs2 = s2;
    fpoly fs2plus4r2 = s2plus4r2;
    CHECK_FALSE(fs2 == fs2plus4r2);
    dpoly ds2 = fs2;
    dpoly ds2plus4r2 = fs2plus4r2;
    CHECK_FALSE(ds2 == ds2plus4r2);
}

TEST_CASE("basic.conv", "Check conversions of some simple polynoms")
{
    impl::polynom_tables::reset();
    fast_polynom x = {1, -2, 1};
    /*std::cout << "x = " << x << std::endl;*/
    CHECK(x == fast_polynom::s.pow(2));
    CHECK(x == (fast_polynom::s * fast_polynom::s));
    impl::d_polynom d({1, -1});
    impl::d_polynom e = d.o(d);
    impl::d_polynom check({0, 1});
    /*std::cout << d << std::endl;*/
    /*std::cout << e << std::endl;*/
    /*std::cout << check << std::endl;*/
    CHECK(e == check);
}

TEST_CASE("basic.pow", "Check basic power operations")
{
    impl::polynom_tables::reset();
    CHECK(fast_polynom::s.pow(1) == fast_polynom::s);
    CHECK(fast_polynom::s.pow(0) == fast_polynom::one);
    int n = 0;
    CHECK(fast_polynom::s.pow(n) == fast_polynom::one);
}

TEST_CASE("check.roots", "Check some polynom root-finding")
{
    fast_polynom x = {0, 1, -3, 3, -1};
    impl::f_polynom f = x;
    CHECK(f.r_exp == 1);
    CHECK(f.s_exp == 3);
    CHECK(f.P.size() == 1);
    CHECK(f.P[0] == 1);

    fast_polynom a = {0, 0, 0, 1};
    fast_polynom b = {1, -1};
    fast_polynom c = a * b;
    /*std::cout << "c = " << c << std::endl;*/
    impl::f_polynom fc = c;
    /*std::cout << "fc = " << fc << std::endl;*/
    CHECK(fc.r_exp == 3);
    CHECK(fc.s_exp == 1);
    CHECK(fc.P.size() == 1);
    CHECK(fc.P[0] == 1);
}


double accum_poly(const std::vector<double>& coefs, double x_val)
{
    double accum = 0;
    double x = 1.;
    for (double c: coefs) {
        accum += c * x;
        x *= x_val;
    }
    return accum;
}


void check_poly(const std::vector<double>& coefs)
{
    fast_polynom fp;
    fp = coefs;
    double x = 0;
    double step = .01;
    while (x <= .5) {
        double delta = fabs(accum_poly(coefs, x) - fp(x));
        CHECK(delta < 1.e-10);
        if (delta >= 1.e-10) {
            std::cerr << "P = " << ((impl::f_polynom)fp) << std::endl;
            std::cerr << "x = " << x << std::endl << std::endl;
        }
        x += step;
    }
}


TEST_CASE("check.eval", "Check some polynom evaluations")
{
    check_poly({1., 1., 1.});
    check_poly({1., 2., 1.});
    check_poly({1., -2., 1.});
    check_poly({1., -3., 3., -1.});
    check_poly({1., -3., -2., -5.});
    check_poly({1., 0., 0., 0., 123., 4., 42., 23., 7., 15., 32.});
    check_poly({0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 32.});
    std::vector<double> s_17_decr = {-1,17,-136,680,-2380,6188,-12376,19448,-24310,24310,-19448,12376,-6188,2380,-680,136,-17,1};
    std::vector<double> s_17(s_17_decr.rbegin(), s_17_decr.rend());
    check_poly(s_17);
    
}

