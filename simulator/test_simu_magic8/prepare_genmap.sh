#!/bin/bash
#INGEN=$1
#INMAP=$2
#OUTGEN=$3
#OUTMAP=$4

#if [ "x$INGEN" == "x" -o "x$INMAP" == "x" -o "x$OUTGEN" == "x" -o "x$OUTMAP" == "x" ]; then
#    echo "Usage: $0 INGEN INMAP OUTGEN OUTMAP"
#    exit 1
#fi

MARK=$1
DIST=$2
NIND3=$3

if [ "x$MARK" == "x" -o "x$DIST" == "x" -o "x$NIND3" == "x" ]; then
    echo "Usage: $0 N_MARK TOT_DIST N_IND"
    exit 1
fi

NIND2=$(($NIND3 * 2))
NIND1=$(($NIND2 * 2))

sed -e "s/%MARK/$MARK/" -e "s/%DIST/$DIST/" -e "s/%NIND3/$NIND3/" -e "s/%NIND2/$NIND2/" -e "s/%NIND1/$NIND1/" test_ril_template.txt > test_ril.txt

../simu.py -i test_ril.txt -o test_ril

INGEN=test_ril_MAGIC8.gen
INMAP=test_ril.map
OUTGEN=toto.txt
OUTMAP=totomap.txt

head -3 $INGEN|tail -1|sed 's/[ABCDEFGH]/-/g' > $OUTGEN
tail -$((`wc -l $INGEN | cut -d \  -f 1` - 2)) $INGEN >> $OUTGEN

sed -i -e 's/.* //' -e 's/./"&"\t/g' -e 's/\t$//' $OUTGEN
sed -e 's/[^ ]* [^ ]*//' -e 's/ M[^ ]*//g' $INMAP > $OUTMAP

R CMD BATCH test_broman.R
