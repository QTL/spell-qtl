from simu import *
import sys
import scipy
from itertools import izip

noise_range = xrange(2, 3)
n_runs = 1


d = Design()


def gen_trait(noise_gain):
    global d
    d += """
map,11,150
line,A,a
line,B,b
cross,F1,A,B,1,None
cross,BC,F1,A,10,aa:A ba:H
qtls,1
    """.split('\n')

    d += "trait,trait1,BC,1,aa:1.7321 ba:0,0,%f,0,1" % noise_gain

table = open('results_micro_tmp.txt.experimental', 'w')

print >> table, " run chrom pos rss thres ftest r2 ftest_at_qtl M0rss",
print >> table, "qtl_pos noise tmean tvar tss"

i = 0
lineno = 0

for noise in noise_range:
    #noise = noise
    for run in xrange(n_runs):
    #for run in xrange(1):
        gen_trait(noise)
        s = SpellQtl("debug", d, parallel=9, step=20)
        so = s.output
        i += 1
        if not so:
            print "isofoirate de foirure foirique !"
            continue
        trait = d.traits["trait1"]
        mean = scipy.mean(trait.values)
        var = scipy.var(trait.values)
        tss = var * len(trait.values)
        for l in so:
            lineno += 1
            print >> table, lineno, i, l.strip(), noise, mean, var, tss
        table.flush()
        for i, tfm in izip(s.testpos, s.tpftac, s.maha):
            tpftac = open('debug_testpos_ftac_maha.' + str(lineno) + '.txt',
                          'w')
            print >> tpftac, '\n'.join(tfm)
