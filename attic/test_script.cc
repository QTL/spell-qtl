/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "script_parser.h"
#include "../src/malloc.h"
#include "computations.h"
#include "chrono.h"


double settings_t::get_threshold(const std::string& trait)
{
    double& thres = qtl_thresholds[trait];
    if (thres == 0) {
        set_title(SPELL_STRING("Computing threshold for single_trait '" << trait << "' with " << n_permutations << " permutations and quantile " << qtl_threshold_quantile));
        thres = *make_value<Disk|Sync>(qtl_threshold_all_chromosomes,
                                       value<std::string>(trait),
                                       value<double>(qtl_threshold_quantile),
                                       value<int>(n_permutations));
    }
    return thres;
}


extern "C" void delete_settings() { if (active_settings) { msg_handler_t::instance().queue.m_stop = true; delete active_settings; } }



int main(int argc, const char** argv)
{
    (void)msg_handler_t::instance();

    atexit(delete_settings);

    int i0;
    for (i0 = 0; i0 < argc && strcmp(argv[i0], "--"); ++i0);

    active_settings = settings_t::from_args(i0, argv);
    ++i0;

    if (!active_settings) {
        exit(0);
    }

    msg_handler_t::check(true);

    active_settings->finalize();

    active_settings->set_title("Checking the validity of the configuration");
    if (!active_settings->sanity_check()) {
        exit(-1);
    }

    msg_handler_t::check(true);


    script::script_parser parser;

    /* Build lists of populations per observed single_trait */
    std::map<std::string, collection<population_value>> pops_by_trait;
    for (const auto& kv_pop: active_settings->populations) {
        for (const auto& trait: kv_pop.second.observed_traits) {
            pops_by_trait[trait.name].emplace_back(&kv_pop.second);
        }
    }

    const auto& trait_pops = *pops_by_trait.begin();

    for (int i = i0; i < argc; ++i) {
        model_manager mm(trait_pops.second,
                         trait_matrix(trait_pops.first, trait_pops.second),
                         active_settings->get_threshold(trait_pops.first) * .9,
                         ComputationType::FTest,
                         SolverType::QR);
        mm.Mcurrent.compute();
        mm.threshold = active_settings->get_threshold(trait_pops.first);
        MSG_DEBUG("threshold is " << mm.threshold);
        mm.init_loci_by_step(active_settings->step);

        MSG_DEBUG("Parsing script <<");
        MSG_DEBUG(argv[i]);
        MSG_DEBUG(">>");
        auto script = parser.parse(argv[i], &mm);
        script->exec();
        /*model::xtx_printable xtx(mm.Mcurrent, true);*/
        /*MSG_DEBUG("XtX^-1" << std::endl << xtx);*/
        /*MSG_DEBUG("Coefficients " << mm.Mcurrent.coefficients().transpose());*/
        /*MSG_DEBUG("Residuals " << mm.Mcurrent.residuals().transpose());*/
        /*MSG_DEBUG("RSS " << mm.Mcurrent.rss());*/
    }

    return 0;
}

