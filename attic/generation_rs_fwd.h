#ifndef _SPEL_GENERATION_RS_FWD_H_
#define _SPEL_GENERATION_RS_FWD_H_

#define USE_RIL_SERIES

#include <mutex>
#include "data/chromosome.h"
/*#include "markov_population.h"*/
/*#include "algebraic_genotype.h"*/
#include "data/genoprob_computer.h"
#include "data/qtl_chromosome.h"
/*#include "lazy.h"*/
#include "lumping.h"
#include "input/read_mark.h"
#include "input/read_trait.h"
/*#include <Eigen/SparseCore>*/
/*#include <unsupported/Eigen/MatrixFunctions>*/
/*#include <unsupported/Eigen/KroneckerProduct>*/
#include "eigen.h"
#include <unordered_map>
#include <atomic>
#include <map>
#include "chrono.h"
#include "linear_combination.h"
#include "input/observations.h"


/*#define RIL_ORDER 10*/
#define MAX_EXACT_LUMP_SIZE 2048
#define FUZZY_COMPARE

#define LOCUS_EPSILON (1.e-5)

/*typedef labelled_matrix<GenoMatrix, allele_pair> GenerationRS;*/
typedef geno_matrix GenerationRS;

typedef Eigen::Matrix<bool, Eigen::Dynamic, 1> VectorXb;
typedef Eigen::Matrix<bool, Eigen::Dynamic, Eigen::Dynamic> MatrixXb;
typedef Eigen::Array<bool, Eigen::Dynamic, Eigen::Dynamic> ArrayXb;
namespace impl { struct generation_rs; }

typedef std::pair<const impl::generation_rs*, int> mgo_key_type;

typedef std::pair<size_t, size_t> pedigree_node;
typedef std::map<const impl::generation_rs*, std::vector<pedigree_node>> pedigree_type;

/*typedef std::vector<std::pair<char, std::vector<std::pair<char, char>>>> marker_observation_spec;*/

struct marker_observation_spec {
    ObservationDomain domain;
    size_t domain_size;
    std::vector<std::pair<char, std::vector<std::pair<char, char>>>> scores;
    std::map<char, VectorXd> compile() const;
};


struct population_marker_observation {
    std::string filename;
    std::string format_name;
    size_t process_size;
    marker_data observations;

    std::map<char, VectorXd> observation_vectors;

    void compute_observation_vectors(
            const impl::generation_rs* g,
            const marker_observation_spec& obs_spec);

    MatrixXd raw_observations(const std::vector<char>& observations) const;

    operator marker_data& () { return observations; }
    operator std::string () { return filename; }
};


#if 0
struct population {
    std::string name;
    std::string qtl_generation_name;
    std::string observed_traits_filename;
    std::vector<trait> observed_traits;
    std::map<std::string, population_marker_observation> observed_mark;
    /* TODO: pedigree... */
    std::string pedigree_filename;
    pedigree_type pedigree;

    double noise;

    population()
        : name(), qtl_generation_name()
        , observed_traits_filename()
        , observed_traits(), observed_mark()
        , pedigree_filename()
        , pedigree()
        , noise(0)
        /*, ld_data()*/
    {}

    inline size_t size() const { return observed_traits.front().values.size(); }

    inline
    const population_marker_observation& get_observed_mark(const std::string& gen) const
    {
        auto it = observed_mark.find(gen);
        if (it == observed_mark.end()) {
            static population_marker_observation empty;
            return empty;
        }
        return it->second;
    }

    bool is_observed(const impl::generation_rs* g, int i) const;

    std::vector<char> get_observations(const chromosome* chr, const impl::generation_rs* g, int i) const;

    inline
    void init_observations(MatrixXd& ret,
                           const chromosome* chr, const impl::generation_rs* g, int i) const;

    pedigree_node pedigree_get_parents(const impl::generation_rs* g, int i) const;

};
#endif

namespace impl {
struct generation_rs {
    /* semi-optimistic yet paranoid attempt at handling locus vectors
     * - store the partition used to lump the matrix
     * - for each individual at each generation, check that the corresponding LV is coherent (paranoia)
     * it should be. (optimism)
     */
    /* TODO: fix constructors and crossing selfing to_ril etc to take into account name and design */
    /* TODO: make sure the generations survive (use some auto_ptr ?) */

/*
 * Big assumption: all processes have exactly the same size, labels, AND lumping partition.
 */
    typedef enum { DAncestor, DSelf, DRA, DCross, DSib, DBroman, DRIL, DDH, DAdhoc } design_type;

    struct generation_design_base {
        const generation_rs* g;
        bool is_ril;
        bool is_ancestor;
        generation_design_base(const generation_rs* my_gen, bool _is_anc=false);
        virtual ~generation_design_base();
        generation_design_base* clone(const generation_rs* my_gen) const;
        virtual generation_design_base* _clone(const generation_rs* my_gen) const = 0;

        friend
        std::ostream& operator << (std::ostream& os, const impl::generation_rs::generation_design_base& d);
        virtual void to_str(std::ostream&) const = 0;
        virtual std::pair<const generation_rs*, const generation_rs*> get_parents() const = 0;
        virtual design_type type() const = 0;
        virtual VectorLC raw_lincomb() const = 0;
    };

    struct generation_design_adhoc : public generation_design_base {
        generation_design_adhoc(const generation_rs* my_gen);
        generation_design_base* _clone(const generation_rs* my_gen) const;
        void to_str(std::ostream& os) const;
        virtual std::pair<const generation_rs*, const generation_rs*> get_parents() const;
        virtual design_type type() const { return design_type::DAdhoc; }
        virtual VectorLC raw_lincomb() const;
    };


    struct generation_design_ancestor : public generation_design_base {
        generation_design_ancestor(const generation_rs* my_gen);
        generation_design_base* _clone(const generation_rs* my_gen) const;
        void to_str(std::ostream& os) const;
        virtual std::pair<const generation_rs*, const generation_rs*> get_parents() const;
        virtual design_type type() const { return design_type::DAncestor; }
        virtual VectorLC raw_lincomb() const;
    };


    struct generation_design_self : public generation_design_base {
        const generation_rs* parent;
        generation_design_self(const generation_rs* my_gen, const generation_rs* p);
        generation_design_base* _clone(const generation_rs* my_gen) const;
        void to_str(std::ostream& os) const;
        virtual std::pair<const generation_rs*, const generation_rs*> get_parents() const;
        virtual design_type type() const { return design_type::DSelf; }
        virtual VectorLC raw_lincomb() const;
    };


    struct generation_design_reduce_alleles : public generation_design_base {
        const generation_rs* parent;
        generation_design_reduce_alleles(const generation_rs* my_gen, const generation_rs* p);
        generation_design_base* _clone(const generation_rs* my_gen) const;
        void to_str(std::ostream& os) const;
        virtual std::pair<const generation_rs*, const generation_rs*> get_parents() const;
        virtual design_type type() const { return design_type::DRA; }
        virtual VectorLC raw_lincomb() const;
    };


    struct generation_design_cross : public generation_design_base {
        const generation_rs* parent1;
        const generation_rs* parent2;
        generation_design_cross(const generation_rs* my_gen, const generation_rs* p1, const generation_rs* p2);
        generation_design_base* _clone(const generation_rs* my_gen) const;
        void to_str(std::ostream& os) const;
        virtual std::pair<const generation_rs*, const generation_rs*> get_parents() const;
        virtual design_type type() const { return design_type::DCross; }
        virtual VectorLC raw_lincomb() const;
    };


    struct generation_design_sib : public generation_design_base {
        const generation_rs* parent1;
        generation_design_sib(const generation_rs* my_gen, const generation_rs* p);
        generation_design_base* _clone(const generation_rs* my_gen) const;
        void to_str(std::ostream& os) const;
        virtual std::pair<const generation_rs*, const generation_rs*> get_parents() const;
        virtual design_type type() const { return design_type::DSib; }
        virtual VectorLC raw_lincomb() const;
    };


    struct generation_design_broman : public generation_design_base {
        const generation_rs* parent1;
        const generation_rs* parent2;
        const generation_rs* parent3;
        const generation_rs* parent4;
        const generation_rs* parent5;
        const generation_rs* parent6;
        const generation_rs* parent7;
        const generation_rs* parent8;
        generation_design_broman(const generation_rs* my_gen,
                const generation_rs* p1, const generation_rs* p2,
                const generation_rs* p3, const generation_rs* p4,
                const generation_rs* p5, const generation_rs* p6,
                const generation_rs* p7, const generation_rs* p8);
        generation_design_base* _clone(const generation_rs* my_gen) const;
        void to_str(std::ostream& os) const;
        virtual std::pair<const generation_rs*, const generation_rs*> get_parents() const;
        virtual design_type type() const { return design_type::DBroman; }
        virtual VectorLC raw_lincomb() const;
    };


    struct generation_design_double_haploid : public generation_design_base {
        const generation_rs* parent;
        generation_design_double_haploid(const generation_rs* my_gen, const generation_rs* p);
        generation_design_base* _clone(const generation_rs* my_gen) const;
        void to_str(std::ostream& os) const;
        virtual std::pair<const generation_rs*, const generation_rs*> get_parents() const;
        virtual design_type type() const { return design_type::DDH; }
        virtual VectorLC raw_lincomb() const;
    };

    struct generation_design_ril : public generation_design_base {
        const generation_rs* parent;
        generation_design_ril(const generation_rs* my_gen, const generation_rs* p);
        generation_design_base* _clone(const generation_rs* my_gen) const;
        void to_str(std::ostream& os) const;
        virtual std::pair<const generation_rs*, const generation_rs*> get_parents() const;
        virtual design_type type() const { return design_type::DRIL; }
        virtual VectorLC raw_lincomb() const;
    };

    struct process {
        GenerationRS G;
        double weight;

        /* should handle LV for {xy} with some kind of combination of
         * (ref_to_mother).is_possible_allele(x)&&(ref_to_father).is_possible_allele(y)
         * with an OR of clauses from lumped states I guess.
         */

        process(const GenerationRS& g, double w) : G(g), weight(w) {}
        process() : G(), weight(0) {}

        friend std::ostream& operator << (std::ostream& os, const process& p)
        {
            return os << "* " << p.weight << std::endl << p.G;
        }
    };

    bool process_equiv(const GenerationRS& g1, const GenerationRS& g2);

    void reduce_processes();

    void precompute_redux();

    const MatrixXd& get_redux() const;

    void precompute_unique_labels();

    const std::vector<allele_pair>& get_unique_labels() const;

    const labelled_matrix<MatrixXd, std::vector<char>, allele_pair>&
        get_state_to_parental_origin(int n_selected_qtl) const;

    const labelled_matrix<MatrixXd, std::vector<char>, allele_pair>&
        compute_state_to_parental_origin(int n_selected_qtl);

    generation_rs& operator = (const generation_rs& grs);

    const GenerationRS& main_process() const;

    void compute_lumping_partition();

    void lump();

    MatrixXd noisy_LV(const MatrixXd& LV, double noise) const;

    struct segment_computer_t {
        const generation_rs* g_this;
        const qtl_chromosome* chr;
        geno_prob_computer<impl::generation_rs> gpc_vec;
        std::vector<double> steps;
        std::vector<double> loci;
        double noise;

        bool operator == (const segment_computer_t& sc) const;

        segment_computer_t();
        /*segment_computer_t(const segment_computer_t& sc)*/
            /*: g_this(sc.g_this), chr(sc.chr), gpc_vec(sc.gpc_vec), steps(sc.steps), loci(sc.loci), noise(sc.noise)*/
        /*{*/
            /*gpc_vec.locus = &loci;*/
        /*}*/
        segment_computer_t(segment_computer_t&& sc);

        segment_computer_t(const generation_rs& g, const qtl_chromosome* c, double step, double nz);

        segment_computer_t(const generation_rs& g, const qtl_chromosome* c, double nz, const std::vector<double>& loc_vec);

        void init();

        segment_computer_t& operator = (segment_computer_t&& sc);

        labelled_matrix<MatrixXd, allele_pair, double>
            compute(const MatrixXd& LV);

        labelled_matrix<MatrixXd, allele_pair, double>
            compute();
    };

    segment_computer_t segment_computer(
            const qtl_chromosome* chr,
            double step,
            double noise) const;

    segment_computer_t segment_computer(
            const qtl_chromosome* chr,
            double noise,
            const std::vector<double>& loci) const;

    void init_epo();

    generation_rs(const std::string& n, char haplo, const std::vector<std::pair<char, char>>& tagpairs);
    generation_rs(const std::string& n, generation_design_base* gdb);
    generation_rs(const std::string& n);

    virtual ~generation_rs();

    static bool __allele_pairs_equal(const allele_pair& a, const allele_pair& b);

	/*spin_lock ulv_lock;*/

    void precompute_uulv();

    /* NOT CONST! only for the sake of lazy evaluation */
    const std::vector<VectorXd>& unphased_LV() const;

    labelled_matrix<MatrixXd, std::string, allele_pair>
    genotype_probabilities() const;

    void precompute_lincomb();
    void precompute();

    VectorLC raw_lincomb(size_t i) const;

    std::string name;
    std::set<subset> lumping_partition;
    MatrixXd lumper;
    std::vector<const impl::generation_rs*> ancestor_subtree;
    labelled_matrix<MatrixXd, allele_pair, allele_pair> consistent_states_redux;
    VectorLC this_lincomb;

    std::vector<process> P;
    generation_design_base* design;

    std::vector<VectorXd> unique_unphased_LV;

    std::vector<eval_poly_optim> epo;

    MatrixXd redux_matrix;
	/*spin_lock redux_lock;*/

    std::vector<allele_pair> unique_labels;
	/*spin_lock ul_lock;*/

	/*spin_lock stpo_lock;*/
	std::recursive_mutex stpo_lock;

private:
    std::atomic_int stpo_max;
    std::atomic_bool ul_computed;
    std::atomic_bool ulv_computed;
};

} /* namespace impl */


struct generation_rs : public impl::generation_rs {
private:
    struct gen_map: std::map<std::string, generation_rs*> {
        ~gen_map();
    };

    static gen_map& dict();
    static generation_rs* fetch_impl(const std::string& n);
    static generation_rs* fetch_impl(const std::string& n, char haplo, const std::vector<std::pair<char, char>>& tagpairs);
    generation_rs(const std::string& n);
    generation_rs(const std::string& n, char haplo, const std::vector<std::pair<char, char>>& tagpairs);

public:
    static void clear_dict();
    static generation_rs* from_matrix(const std::string& new_name, const GenoMatrix& gm);
    static generation_rs* from_matrix(const std::string& new_name, const GenoMatrix& gm, const VectorLC& lc);
    static generation_rs* ancestor(const std::string& new_name, char haplo, const std::vector<std::pair<char, char>>& tagpairs);
    static generation_rs* blank(const std::string& new_name);
    generation_rs* selfing(const std::string& new_name) const;
    generation_rs* sibling(const std::string& new_name) const;
    generation_rs* to_doubled_haploid(const std::string& new_name) const;
    generation_rs* clone_and_lump() const;
    generation_rs* crossing(const std::string& new_name, const generation_rs* sylvester) const; /* parce que l'étalon */
    /* Attention
     * TODO: pour RIL sibling (R=4r/(1+6r)), le poids est -2^(-2*i-1)*3^(i-1).
     * TODO: vérifier le reste pour RIL sibling.
     * TODO: traiter les autosomes à part etc etc etc blabla.
     */
    generation_rs* to_ril(const std::string& ril_name) const;
    static
        generation_rs* broman8(const std::string& name,
                               generation_rs* A, generation_rs* B, generation_rs* C, generation_rs* D,
                               generation_rs* E, generation_rs* F, generation_rs* G, generation_rs* H);
    static
    generation_rs* get(const std::string& name);
    std::pair<const generation_rs*, const generation_rs*>
        get_parents() const;
};



struct pow_cache {
    double root;
    std::unordered_map<int, double> cache;
    pow_cache(double r) : root(r), cache() {}
    double operator () (int p)
    {
        double& v = cache[p];
        if (v == 0) {
            v = pow(root, (double) p);
        }
        return v;
    }
};


struct LD_matrices {
    std::vector<double> loci;
    std::vector<MatrixXd> ld;
};

#endif

