/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "input.h"
#include "x2c.h"

#if 0
static inline
std::istream& operator >> (std::istream& is, ancestor_allele& aa)
{
    is >> aa.ancestor;
    if (!is.eof()) {
        char c = is.peek();
        if (c >= '0' && c <= '9') {
            int i;
            is >> i;
            if (i > 255) {
                MSG_ERROR("Allele number too big : must be inferior to 255.", "Fix the observation format specification");
            }
            aa.tag = (char) i;
        }
    }
    return is;
}


static inline
std::istream& operator >> (std::istream& is, allele_pair& ap)
{
    return is >> ap.first >> ap.second;
}
#endif


typedef std::pair<std::string, marker_observation_spec> format_specification_keyvalue;


struct copy_manip {
    std::string generation;
    std::string from;

    void operator () (format_specification_t& fs)
    {
        fs.map[generation] = fs.map[from];
    }
};


struct allele_pair_tag_manip {
    char a, b;

    void operator () (decltype(marker_observation_spec::scores)::value_type&)
    {
        /*kv.second.emplace_back(allele_pair {{a}, {b}});*/
        /*MSG_DEBUG("added tags " << a << '/' << b << " to observation '" << kv.first << "'");*/
    }
};


struct unknown_relative_obs_manip {
    char unk;
    void operator () (format_specification_keyvalue& kv)
    {
        kv.second.scores.push_back({0, {}});
        kv.second.scores.push_back({unk, {}});
    }
};

bool set_domain(const std::string* attr_value, format_specification_keyvalue* fs)
{
    if (*attr_value == "allele") {
        fs->second.domain = ODAllele;
    } else if (*attr_value == "ancestor") {
        fs->second.domain = ODAncestor;
    } else {
        return false;
    }
    return true;
}


struct domain_manip {
    std::string type;
    static std::vector<char>& alphabet() { static std::vector<char> _; return _; }

    static size_t index_of(char x)
    {
        auto pos = std::find(alphabet().begin(), alphabet().end(), x);
        if (pos == alphabet().end()) {
            MSG_ERROR("Character " << x << " is not in alphabet of observables", "");
            return (size_t) -1;
        }
        return pos - alphabet().begin();
    }

    static
    bool __alphabet_manip (const std::string* value, domain_manip*)
    {
        alphabet().assign(value->begin(), value->end());
        return true;
    }

    void operator () (format_specification_keyvalue& kv)
    {
        if (type == "allele") {
            kv.second.domain = ODAllele;
        } else if (type == "ancestor") {
            kv.second.domain = ODAncestor;
        }
        kv.second.domain_size = alphabet().size();
    }

    static std::function<bool(const std::string*, domain_manip*)> alphabet_manip;
};

std::function<bool(const std::string*, domain_manip*)> domain_manip::alphabet_manip = &domain_manip::__alphabet_manip;

struct scores_manip {
    std::vector<std::pair<char, std::vector<std::pair<char, char>>>> contents;

    void operator () (format_specification_keyvalue& kv)
    {
        kv.second.scores.swap(contents);
        MSG_DEBUG("FORMAT SPEC " << kv.first << " (domain " << kv.second.domain << ", size " << kv.second.domain_size << "):");
        for (const auto& score_kv: kv.second.scores) {
            MSG_DEBUG(" * " << score_kv.first);
            for (const auto& s: score_kv.second) {
                MSG_DEBUG("      " << ((int)s.first) << ((int)s.second));
            }
        }
    }
};


struct from_manip {
    char a, b;

    void operator () (decltype(marker_observation_spec::scores)::value_type& kv)
    {
        /*kv.second.emplace_back(domain_manip::index_of(a), domain_manip::index_of(b));*/
        kv.second.emplace_back(a, b);
    }
};


DTD_START_WITH_ROOT_NAME(format_dtd, format_spec, "format-specification", format_specification_t)
    ELEMENT_WITH_NAME(obs_spec, "format", format_specification_keyvalue);
    ELEMENT(domain, domain_manip);
    ELEMENT(scores, scores_manip);
    ELEMENT(score, decltype(scores_manip::contents)::value_type);
    ELEMENT(from, from_manip);
    ELEMENT(copy, copy_manip);
    /*ELEMENT(unknown, unknown_relative_obs_manip);*/

    format_spec
        = A("file", make_loader(*this))
        | (E(obs_spec, &format_specification_t::map) , M(E(copy)));
    obs_spec
        = A("name", &format_specification_keyvalue::first)
        & E(domain)
        & E(scores);
    domain
        = (A("type", &domain_manip::type), A("alphabet-from", domain_manip::alphabet_manip));
    scores
        = E(score, &scores_manip::contents);
    score
        = A("symbol", &decltype(scores_manip::contents)::value_type::first)
        & M(E(from));
    from
        = A("mother", &from_manip::a)
        & A("father", &from_manip::b);
    /*copy*/
        /*= (A("generation", &copy_manip::generation),*/
           /*A("from", &copy_manip::from));*/
DTD_END(format_dtd);


void read_format(std::map<std::string, marker_observation_spec>& settings, const std::string& filename, file& is)
{
    if (!is.good()) {
        throw input_exception(is, SPELL_STRING("couldn't read from file '" << filename << '\''));
    }
    try {
        auto* ret = format_dtd.parse(is.m_impl);
        (*ret)(settings);
        delete ret;
    } catch (x2c::xml_exception& xe) {
        std::stringstream workaround;
        workaround << "Fix the XML in file " << filename;
        MSG_ERROR("Reading " << filename << ": " << xe.what(), workaround.str());
    }
    /*return NULL;*/
}

DTD_ACCESSOR(format_dtd);

