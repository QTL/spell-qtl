/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "input.h"
/*#include "generation_rs.h"*/
#include "x2c.h"

#if 0
typedef std::pair<std::string, generation_rs*> generation_keyvalue;

std::ostream& operator << (std::ostream& os, const std::pair<char, char>& pcc)
{
    return os << '(' << ((int)pcc.first) << '/' << ((int)pcc.second) << ')';
}



struct ancestor_manip {
    char haplo;
    std::string profile;

    void operator () (generation_keyvalue & g)
    {
        if (profile != "") {
            const ancestor_profile& ap = design_type::__current()->profiles[profile];
            /*std::cout << ap.genotype_tags << std::endl;*/
            g.second = generation_rs::ancestor(g.first, haplo, ap.genotype_tags);
        } else {
            std::vector<std::pair<char, char>> alleles = {{0, 0}};
            g.second = generation_rs::ancestor(g.first, haplo, alleles);
        }
    }
};

struct cross_manip {
    std::string p1;
    std::string p2;

    void operator () (generation_keyvalue & g)
    {
        generation_rs* g1 = generation_rs::get(p1);
        generation_rs* g2 = generation_rs::get(p2);
        g.second = g1->crossing(g.first, g2);
    }
};

struct self_manip {
    std::string p1;

    void operator () (generation_keyvalue & g)
    {
        generation_rs* g1 = generation_rs::get(p1);
        g.second = g1->selfing(g.first);
    }
};

struct sib_manip {
    std::string p1;

    void operator () (generation_keyvalue & g)
    {
        generation_rs* g1 = generation_rs::get(p1);
        g.second = g1->sibling(g.first);
    }
};

/*
struct reduce_alleles_manip {
    std::string p1;

    void operator () (generation_keyvalue & g)
    {
        generation_rs* g1 = generation_rs::get(p1);
        g.second = g1->reduce_alleles(g.first);
    }
};
*/

struct ril_manip {
    std::string p1;

    void operator () (generation_keyvalue & g)
    {
        generation_rs* g1 = generation_rs::get(p1);
        /*std::stringstream s;*/
        /*s << "_" << g.first << "_dh";*/
        /*g.second = g1->to_doubled_haploid(s.str())->to_ril(g.first);*/
        g.second = g1->to_ril(g.first);
    }
};

struct dh_manip {
    std::string p1;

    void operator () (generation_keyvalue & g)
    {
        generation_rs* g1 = generation_rs::get(p1);
        g.second = g1->to_doubled_haploid(g.first);
    }
};

struct bro_manip {
    std::string p1;
    std::string p2;
    std::string p3;
    std::string p4;
    std::string p5;
    std::string p6;
    std::string p7;
    std::string p8;

    void operator () (generation_keyvalue & g)
    {
        generation_rs* g1 = generation_rs::get(p1);
        generation_rs* g2 = generation_rs::get(p2);
        generation_rs* g3 = generation_rs::get(p3);
        generation_rs* g4 = generation_rs::get(p4);
        generation_rs* g5 = generation_rs::get(p5);
        generation_rs* g6 = generation_rs::get(p6);
        generation_rs* g7 = generation_rs::get(p7);
        generation_rs* g8 = generation_rs::get(p8);
        g.second = generation_rs::broman8(g.first, g1, g2, g3, g4, g5, g6, g7, g8);
    }
};
#endif


DTD_START_WITH_ROOT_NAME(design_dtd, design, "breeding-design", design_type)
#if 0
    typedef std::pair<std::string, ancestor_profile> ancestor_profile_keyvalue;
    typedef std::pair<char, char> ancestor_tagpair;
    ELEMENT_WITH_NAME(ancestor_prof, "ancestor-profile", ancestor_profile_keyvalue);
    ELEMENT(genotypes, ancestor_profile);
    ELEMENT(genotype, ancestor_tagpair);
    ELEMENT(ancestor, ancestor_manip);
    ELEMENT(generation, generation_keyvalue);
    ELEMENT(cross, cross_manip);
    ELEMENT(self, self_manip);
    ELEMENT(sib, sib_manip);
    ELEMENT(ril, ril_manip);
    /*ELEMENT_WITH_NAME(reduce_alleles, "reduce-alleles", reduce_alleles_manip);*/
    ELEMENT(dh, dh_manip);
    ELEMENT(broman8, bro_manip);
    ancestor_prof
        = A("name", &ancestor_profile_keyvalue::first)
        & E(genotypes, &ancestor_profile_keyvalue::second);
    genotypes
        = E(genotype, &ancestor_profile::genotype_tags);
    genotype
        = (A("mater", &ancestor_tagpair::first), 
           A("pater", &ancestor_tagpair::second));
    ancestor
        = (A("haplotype", &ancestor_manip::haplo),
           x2c::make_optional(A("profile", &ancestor_manip::profile)));
    cross
        = (A("p1", &cross_manip::p1), A("p2", &cross_manip::p2));
    self
        = A("p1", &self_manip::p1);
    sib
        = A("p1", &sib_manip::p1);
    /*reduce_alleles*/
        /*= A("p1", &reduce_alleles_manip::p1);*/
    ril
        = A("p1", &ril_manip::p1);
    dh
        = A("p1", &dh_manip::p1);
    broman8
        = (A("p1", &bro_manip::p1), A("p2", &bro_manip::p2), A("p3", &bro_manip::p3), A("p4", &bro_manip::p4),
          A("p5", &bro_manip::p5), A("p6", &bro_manip::p6), A("p7", &bro_manip::p7), A("p8", &bro_manip::p8));
    generation
        = A("name", &generation_keyvalue::first)
        & (E(ancestor) | E(cross) | E(self) | E(sib) | E(ril) | E(dh)
                /*| E(reduce_alleles)*/
                | E(broman8));
    design
        = A("file", make_loader(*this))
        | (E(generation, &design_type::generation), E(ancestor_prof, &design_type::profiles));
#endif
DTD_END(design_dtd);


design_type* read_design(const std::string& filename, file& is)
{
    if (!is.good()) {
        throw input_exception(is, "couldn't read from file");
    }
    try {
        return design_dtd.parse(is.m_impl);
    } catch (x2c::xml_exception& xe) {
        MSG_ERROR("Reading " << filename << ": " << xe.what(), SPELL_STRING("Fix the XML in file " << filename));
    }
    return NULL;
}

DTD_ACCESSOR(design_dtd);

