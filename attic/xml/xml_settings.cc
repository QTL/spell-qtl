/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "input.h"
/*#include "generation_rs.h"*/
#include "x2c.h"

std::function<bool(const std::string*, qtl_pop_type*)>
xml_read_traits = [] (const std::string* filename, qtl_pop_type* pop)
{
    pop->observed_traits_filename = *filename;
    bool ret = check_file(*filename, false, false);
    if (ret) {
        ifile ifs(*filename);
        pop->observed_traits = read_data::read_trait(ifs, {});
    }
    return ret;
};

typedef std::pair<std::string, std::shared_ptr<qtl_pop_type>> pop_keyvalue;
typedef std::pair<std::string, population_marker_observation> pmo_keyvalue;


struct map_loader_manip {
    std::string filename;

    void operator () (settings_t& s)
    {
        s.map_filename = filename;
        check_file(filename, false, false);
        ifile ifs(filename);
        s.map = read_data::read_map(ifs);
    }
};

struct marker_loader_manip {
    std::string generation;
    std::string filename;

    void operator () (qtl_pop_type& p)
    {
#if 0
        auto& om = p.observed_mark[generation];
        check_file(filename, false, false);
        ifile ifs(filename);
        om.filename = filename;
        om.observations = read_data::read_marker(ifs);
#else
        (void) p;
#endif
    }
};


DTD_START(analysis_dtd, analysis, settings_t)
    ELEMENT(notes, std::string);
    ELEMENT_WITH_NAME(map, "consensus-map", map_loader_manip);
    /* format-specification */
    /*ELEMENT(dataset, pop_keyvalue);*/
    ELEMENT_WITH_NAME(pop, "population", qtl_pop_type);
    ELEMENT(markers, marker_loader_manip);
    ELEMENT(noise, double);
    ELEMENT(settings, settings_t);
    ELEMENT(step, double);
    ELEMENT(tolerance, double);
    ELEMENT(parallel, int);
    ELEMENT(threshold, settings_t);
    ELEMENT(permutations, int);
    ELEMENT(quantile, double);
    typedef std::map<std::string, double> threshold_map_type;
    /*typedef std::pair<std::string, double> threshold_map_value_type;*/
    ELEMENT(values, settings_t);
    ELEMENT(trait, threshold_map_type);
    ELEMENT_WITH_NAME(wd, "work-directory", std::string);
    ELEMENT(model, settings_t);
    ELEMENT(connected, bool);
    ELEMENT(epistasis, bool);
    ELEMENT(pleiotropy, settings_t);
    ELEMENT_WITH_NAME(sets, "working-sets", settings_t);
    ELEMENT_WITH_NAME(chromsel, "chromosome-selection", settings_t);
    ELEMENT(chromosome, std::string);
    ELEMENT_WITH_NAME(cofactorsel, "cofactor-marker-selection", settings_t);
    ELEMENT_WITH_NAME(epistasissel, "epistasis-initial-qtl-selection", std::string);

    analysis
        = A("name", &settings_t::name)
        & (E(notes, &settings_t::notes),
           /*E(design_dtd.root(), &settings_t::design),*/
           /*E(format_dtd.root(), &settings_t::marker_observation_specs),*/
           /*E(design_dtd_root(), &settings_t::design),*/
           /*E(format_dtd_root(), &settings_t::marker_observation_specs),*/
           /*M(E(format_dtd_root())),*/
           E(map),
           /*E(dataset, &settings_t::populations),*/
           E(settings),
           E(sets));
    notes
        = chardata();
    map
        = (A("file", &map_loader_manip::filename),
           A("format", &ignore::entity) / check_mapmaker);
    /*dataset*/
        /*= A("name", &pop_keyvalue::first)*/
        /*& E(pop, &pop_keyvalue::second);*/
    // FIXME
#if 0
    pop
        = (A("generation", &population::qtl_generation_name),
           /*A("traits", &population::observed_traits_filename))*/
           A("traits", xml_read_traits))
        & (M(E(markers)),
           E(noise, &population::noise));
    markers
        = (A("generation", &marker_loader_manip::generation),
           A("file", &marker_loader_manip::filename));
    noise
        = A("value");
#endif
    settings
        = (E(step, &settings_t::step),
           E(parallel, &settings_t::parallel),
           E(wd, &settings_t::work_directory),
           E(model),
           E(sets));
    step
        = A("value");
    parallel
        = A("value");
    wd
        = A("path");
	tolerance
		= A("value");
    quantile
        = A("value");
    permutations
        = A("value");
    threshold
        = (E(quantile, &settings_t::qtl_threshold_quantile),
           E(permutations, &settings_t::n_permutations)
           /*,*/
           /*E(values)*/
           );
    /*values*/
        /*= E(single_trait, &settings_t::qtl_thresholds);*/
    /*single_trait*/
        /*= (A("name", &std::pair<std::string, double>::first),*/
           /*A("value", &std::pair<std::string, double>::second));*/
    model
        = (E(connected, &settings_t::connected),
           E(epistasis, &settings_t::epistasis),
           E(pleiotropy),
           E(threshold),
		   E(tolerance, &settings_t::tolerance));
    connected
        /*= A("enabled", convert_bool);*/
        = A("enabled");
    epistasis
        = A("enabled");
    pleiotropy
        /*= (A("enabled", set_pleiotropy),*/
        = (A("enabled", &settings_t::pleiotropy),
        /*= (A("enabled", &ignore::entity),*/
           A("tolerance", &settings_t::pleiotropy_tolerance));
    sets
        = (E(chromsel),
           E(cofactorsel),
           E(epistasissel, &settings_t::epistasis_qtl_selection_filename));
    chromsel
        = (A("all", &ignore::entity) / check_true)
        | ((A("all", &ignore::entity) / check_false)
           & E(chromosome, &settings_t::chromosome_selection));
    chromosome
        = A("name");
    cofactorsel
        = (A("automatic", &ignore::entity) / check_true
          & A("distance-threshold", &settings_t::cofactor_marker_selection_distance))
        | (A("automatic", &ignore::entity) / check_false
          & A("file", &settings_t::cofactor_marker_selection_filename));
    epistasissel
        = A("file");
DTD_END(analysis_dtd);


settings_t* read_settings(const std::string& filename, ifile& is)
{
    if (!is.good()) {
        throw input_exception(is, "couldn't read from file");
    }
    try {
        return analysis_dtd.parse(is);
    } catch (x2c::xml_exception& xe) {
        std::stringstream workaround;
        workaround << "Fix the XML in file " << filename;
        MSG_ERROR("Reading " << filename << ": " << xe.what(), workaround.str());
    }
    return NULL;
}

DTD_ACCESSOR(analysis_dtd);

