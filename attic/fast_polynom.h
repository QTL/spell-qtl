#ifndef _SPEL_FAST_POLY_H_
#define _SPEL_FAST_POLY_H_

#include <vector>
#include <unordered_map>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <cstdint>
#include <cmath>

namespace detail {
    struct debug_scope {
        std::string msg;
        int& indent() { static int lvl = 0; return lvl; }
        debug_scope(std::string f, int l, std::string n)
        {
            std::stringstream s;
            /*s << std::setw(indent() * 3) << "" << f << ':' << l << ' ' << n;*/
            s << std::setw(indent() * 3) << "" << l << ':' << n;
            msg = s.str();
            ++indent();
            std::cout << msg << " -> ENTER" << std::endl;
            (void)f;
        }
        ~debug_scope() { std::cout << msg << " <- LEAVE" << std::endl; --indent(); }
    };
}

#ifdef VERBOSE_DEBUG
#define DEBUG_SCOPE ::detail::debug_scope __debug__(__FILE__, __LINE__, __func__)
#else
#define DEBUG_SCOPE
#endif

struct fast_polynom;

#ifdef POLYNOM_COEF_INT
typedef int64_t coef_t;
#else
typedef double coef_t;
#endif

namespace impl {
    struct d_polynom;
    struct f_polynom;

    struct d_polynom : std::vector<coef_t> {
        int valuation;
        int degree;

        d_polynom();
        d_polynom(const d_polynom& d);
        d_polynom(d_polynom&& d);
        d_polynom(size_t n, int v);
        d_polynom(size_t n);
        d_polynom(std::initializer_list<coef_t> x);
        d_polynom(const std::vector<coef_t>& x);

        bool operator == (const d_polynom& d) const;

        void cleanup();
        operator f_polynom () const;
        d_polynom add(const d_polynom& d) const;
        d_polynom sub(const d_polynom& d) const;
        d_polynom mul(coef_t k) const;
        d_polynom mul(const d_polynom& d) const;
        d_polynom& operator = (d_polynom&& d);
        d_polynom o(const fast_polynom& d) const;
        double norm() const;

        bool inf(const d_polynom& d) const;
    };

    struct f_polynom {
        int r_exp;
        int s_exp;
        std::vector<coef_t> P;

        f_polynom(const f_polynom& f);
        f_polynom(f_polynom&& f);
        f_polynom(int r, int s);
        f_polynom(int r, int s, size_t n);
        f_polynom(int r, int s, std::initializer_list<coef_t> x);
        f_polynom(int r, int s, const std::vector<coef_t>& x);

        bool operator == (const f_polynom& f) const;

        f_polynom mul(const f_polynom& d) const;
        f_polynom add(const f_polynom& d) const;
        f_polynom div(coef_t scalar) const;
        f_polynom& operator = (f_polynom&& d);
        int degree() const;
        operator d_polynom () const;

        bool inf(const f_polynom& f) const;

        std::ostream& serialize(std::ostream&) const;
        static f_polynom unserialize(std::istream&);
    };

    struct polynom {
        bool f_init;
        bool d_init;
        f_polynom f;
        d_polynom d;

        polynom();

        polynom(d_polynom&& dp, f_polynom&& fp);
        polynom(const polynom& p);
        polynom(polynom&& p);
        polynom(const f_polynom& fp);
        polynom(const d_polynom& dp);
        polynom(f_polynom&& fp);
        polynom(d_polynom&& dp);
        bool operator == (const polynom& p) const;
        const f_polynom& to_f() const;
        const d_polynom& to_d() const;
        polynom mul(const polynom& p) const;
        polynom div(coef_t scalar) const;
        polynom add(const polynom& p) const;
        int degree() const;
        int valuation() const;
        double norm() const;
        polynom o(const fast_polynom& f) const;
        bool inf(const polynom& p) const;
    };

    typedef std::unordered_map<polynom, fast_polynom> p2i_t;
    typedef std::vector<const polynom*> i2p_t;
    typedef std::pair<int, int> operands_t;
    typedef std::unordered_map<operands_t, fast_polynom> operation_table_t;
}

struct fast_polynom {
    int value;

    static const int not_initialized = -1;

    fast_polynom();
    fast_polynom(const fast_polynom& fp);
    fast_polynom(int v);
    fast_polynom(std::initializer_list<coef_t> ild);
    fast_polynom(const impl::d_polynom&);
    fast_polynom(const impl::f_polynom&);

    operator const impl::polynom& () const;
    operator const impl::d_polynom& () const;
    operator const impl::f_polynom& () const;

    operator int () const {DEBUG_SCOPE; return value; }

    fast_polynom operator + (const fast_polynom&) const;
    fast_polynom operator - (const fast_polynom&) const;
    fast_polynom operator * (const fast_polynom&) const;
    fast_polynom operator / (coef_t scalar) const;
    fast_polynom& operator += (const fast_polynom&);
    fast_polynom& operator *= (const fast_polynom&);
    /*fast_polynom operator ^ (int);*/
    fast_polynom pow(int) const;
    fast_polynom o(const fast_polynom& f) const;

    fast_polynom& operator = (const impl::f_polynom&);
    fast_polynom& operator = (const impl::d_polynom&);
    fast_polynom& operator = (std::initializer_list<coef_t>);
    fast_polynom& operator = (const std::vector<coef_t>&);

    bool operator < (const fast_polynom& f) const;
    bool operator == (const fast_polynom& f) const;
    bool operator == (int f) const;
    bool inf(const fast_polynom& p) const;

    static fast_polynom zero;
    static fast_polynom one;
    static fast_polynom r;
    static fast_polynom s;
    static fast_polynom infinity;

    int degree() const;
    int valuation() const;

    double norm() const;

    double operator () (double x) const;

    std::ostream& serialize(std::ostream&) const;
    static fast_polynom unserialize(std::istream&);
};

std::ostream& operator << (std::ostream&, const fast_polynom&);
std::ostream& operator << (std::ostream&, const impl::f_polynom&);
std::ostream& operator << (std::ostream&, const impl::d_polynom&);
std::ostream& operator << (std::ostream& os, const impl::polynom& p);

#include "fast_polynom/hash.h"
#include "fast_polynom/tables.h"
#include "fast_polynom/frontend.h"
#include "fast_polynom/output.h"
#include "fast_polynom/d_polynom.h"
#include "fast_polynom/f_polynom.h"
#include "fast_polynom/polynom.h"


#endif

