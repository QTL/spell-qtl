/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "file.h"
#include "geno_matrix.h"
#include "data/chromosome.h"
#include "input/read_map.h"


int main(int argc, char** argv)
{
    ifile ifs(argv[1]);
    std::vector<chromosome> map = read_data::read_map(ifs);
    for (auto& chr: map) {
        chr.compute_haplo_sizes();
    }
    std::map<double, std::string> poi = {
        {5, "a bit too long"},
        {100., "toto"},
        {120., "pouet"},
    };

    std::map<double, std::pair<double, double>> roi = {
        {5, {0, 50}},
        {100., {70., 130.}},
        {120., {110., 140.}},
    };
    for (const auto& chr: map) {
        MSG_DEBUG(chr.name << std::endl << chr.pretty_print(msg_handler_t::termcols(), poi, roi));
    }
    for (const auto& chr: map) {
        MSG_DEBUG(chr.name << std::endl << chr.pretty_print(msg_handler_t::termcols(), poi, {}));
    }
    return 0;
}
