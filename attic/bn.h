/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPELL_BAYES_BN_H_
#define _SPELL_BAYES_BN_H_

#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include "file.h"

#include "error.h"
#include "chrono.h"
#include "generation_rs_fwd.h"
#include "input/read_mark.h"
#include "pedigree.h"

#include "bayes.h"

#include "bayes/factor_var2.h"

#include "input/observations.h"

struct pedigree_bayesian_network {
    bayesian_network bn;
    std::map<std::string, std::vector<size_t>> evidence_by_gen_name;
    std::map<std::string, generation_rs*> generations;
    std::map<const generation_rs*, SparseMatrix<double>> geno_to_state;
    std::map<std::string, std::vector<int>> families;
    std::map<size_t, const generation_rs*> gen_by_id;
    std::vector<pedigree_item> pedigree;
    std::map<int, size_t> vars;
    struct vtsh_item {
        size_t var;
        std::vector<size_t> parents;
        /*size_t p1;*/
        /*size_t p2;*/
        const generation_rs* gen;
        vtsh_item(size_t v, const std::vector<size_t>& p, const generation_rs* g) : var(v), parents(p), gen(g) {}
    };
    std::vector<vtsh_item> var_to_state_helper;
    double tol;
    pedigree_bayesian_network(size_t n_parents, size_t n_alleles, double noise, double tolerance)
        : bn(n_parents, n_alleles, noise)
        , evidence_by_gen_name()
        , generations()
        , geno_to_state()
        , families()
        , pedigree()
        , var_to_state_helper()
        , tol(tolerance)
    {}

    std::map<std::string, std::vector<VectorXd>>
        run(std::function<VectorXd(const std::string&, size_t)> get_obs, size_t verbosity=0) const
        {
            auto comp = bn.instance();
            for (const auto& gv: evidence_by_gen_name) {
                for (size_t i = 0; i < gv.second.size(); ++i) {
                    comp.evidence(gv.second[i]) = get_obs(gv.first, i);
                }
            }
            chrono::start("LoopyBP");
            comp.run(tol, verbosity);
            chrono::stop("LoopyBP");
            std::map<std::string, std::vector<VectorXd>> ret;
#if 1
            chrono::start("Geno->State");
            std::map<size_t, VectorXd> state_vectors;
            /*std::map<const generation_rs*, SparseMatrix<double>> lincombs;*/
            /*for (const auto& kv: generations) {*/
                /*VectorLC lc = kv.second->design->lincomb();*/
                /*SparseMatrix<double> mat(*/
                /*lincombs*/
            /*}*/
            std::map<const generation_rs*, VectorLC> lincombs;
            for (const auto& kv: generations) {
                lincombs[kv.second] = kv.second->this_lincomb;
            }
            state_vectors[0] = VectorXd::Ones(1);
            for (const auto& vts: var_to_state_helper) {
                /*VectorLC lc = vts.gen->design->lincomb();*/
                /*auto par = vts.gen->design->get_parents();*/
                const auto& lc = lincombs[vts.gen];
                VectorXd v1(lc.size());
                VectorXd v2(lc.size());
                /*VectorXd p1 = state_vectors[vts.p1], p2 = state_vectors[vts.p2];*/
                std::map<size_t, VectorXd> pmap;
                /* FIXME: not always 2 parents */
                if (lc.size() == 1 && vts.parents[0] == 0 && vts.parents[1] == 0) {
                    /* ancestor! */
                    v1(0) = 1.;
                } else {
                    for (size_t i = 0; i < vts.parents.size(); ++i) {
                        pmap[i] = state_vectors[vts.parents[i]];
                        /*MSG_DEBUG("PMAP[" << i << "] = #" << vts.parents[i] << " : " << pmap[i]);*/
                    }
                    for (int i = 0; i < lc.size(); ++i) {
                        /* FIXME the parent key should be {gen, #id} or #id. NOT gen only. */
                        /*v1(i) = lc(i, 0).apply({{par.first, p1}, {par.second, p2}});*/
                        v1(i) = lc(i, 0).apply(pmap);
                    }
                }
                /*v1 = (v1.array() == 0).select(VectorXd::Zero(v1.size()), VectorXd::Ones(v1.size()));*/
                const SparseMatrix<double>& g2s = geno_to_state.find(vts.gen)->second;
                VectorXd b = comp.parental_origin_belief(vts.var);
                VectorXd bp = g2s.transpose() * v1;
                VectorXd norm2 = (b.array() / (bp.array() == 0).select(VectorXd::Ones(bp.size()), bp).array()).matrix();
                v2 = g2s * norm2;
                /*v2 = g2s * b;*/
                /*VectorXd b01 = (b.array() == 0).select(VectorXd::Zero(b.size()), VectorXd::Ones(b.size()));*/
                /*VectorXd bs = (g2s.array().rowwise() * b01.array().transpose()).array().colwise().sum();*/
                /*VectorXd norm2 = g2s * bs;*/
                /*v2.array() /= (norm2.array() == 0).select(VectorXd::Ones(norm2.size()), norm2).array();*/
                VectorXd tmp = (v1.array() * v2.array()).matrix();
                double s = tmp.sum();
                state_vectors[vts.var] = tmp / (s ? s : 1.);
                /*MSG_DEBUG(vts.gen->name << ':' << vts.var);*/
                /*MSG_DEBUG("p1 " << p1.transpose());*/
                /*MSG_DEBUG("p2 " << p2.transpose());*/
                /*MSG_DEBUG("v1 " << v1.transpose());*/
                /*MSG_DEBUG("b " << b.transpose());*/
                /*MSG_DEBUG("bp " << bp.transpose());*/
                /*MSG_DEBUG("norm2 " << norm2.transpose());*/
                /*MSG_DEBUG("v2 " << v2.transpose());*/
                MSG_DEBUG("STATE_VEC " << vts.var << "   " << state_vectors[vts.var].transpose());
            }
            std::map<std::string, size_t> sizes;
            for (const auto& pi: pedigree) {
                ++sizes[pi.gen_name];
            }
            for (const auto& kv: sizes) {
                ret[kv.first].reserve(kv.second);
            }
            for (const auto& pi: pedigree) {
                ret[pi.gen_name].emplace_back(state_vectors[vars.find(pi.id)->second]);
            }
            chrono::stop("Geno->State");
#endif
            return ret;
        }
};


pedigree_bayesian_network
make_bn(const std::vector<pedigree_item>& pedigree, const std::map<std::string, ObservationDomain>& obs_gen, /*const std::string& query_gen,*/ size_t n_alleles=1, double obs_noise=0, double tolerance=1.e-10);




#endif

