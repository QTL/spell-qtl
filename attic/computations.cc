/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "computations.h"
#include "settings.h"


std::vector<std::pair<const chromosome*, double>>
selected_qtls()
{
    std::vector<std::pair<const chromosome*, double>> ret;
    for (auto& qc: active_settings->working_set) {
        for (auto& l: qc.qtl) {
            ret.push_back({qc.chr, l});
        }
    }
    return ret;
}


population_value
population_by_name(const std::string& name)
{
    for (auto& kv: active_settings->populations) {
        if (kv.first == name) {
            return &kv.second;
        }
    }
    return NULL;
}


std::vector<double>
selected_qtls_on_chromosome(qtl_chromosome_value chr)
{
    return chr->qtl;
}


generation_value
generation_by_name(const std::string& name)
{
    return active_settings->design->generation[name];
}


generation_value
qtl_generation(const population_value& pop)
{
    return generation_by_name(pop->qtl_generation_name);
}


chromosome_value
chromosome_by_name(const std::string& name)
{
    for (auto& chr: active_settings->map) {
        if (chr.name == name) {
            return &chr;
        }
    }
    return NULL;
}


qtl_chromosome_value
qtl_chromosome_by_name(const std::string& name)
{
    for (auto& chr: active_settings->working_set) {
        if (chr.chr->name == name) {
            return &chr;
        }
    }
    return NULL;
}


collection<std::string>
all_observed_generation_names(population_value pop)
{
    collection<std::string> ret;
    for (auto& kv: pop->observed_mark) {
        ret.emplace_back(kv.first);
    }
    return ret;
}


std::vector<char>
marker_observations(population_value pop, chromosome_value chr, generation_value gen, int ind)
{
    return pop->get_observed_mark(gen->name)
            .observations
            .get_obs(chr->marker_name.begin(), chr->marker_name.end(), ind);
}


collection<double>
test_loci(chromosome_value chr)
{
    std::vector<double> steps = compute_steps(chr->marker_locus, active_settings->step);
    return collection<double>(steps.begin(), steps.end());
}


generation_rs::segment_computer_t
genoprob_computer(population_value pop, generation_value gen, qtl_chromosome_value qtl_chr)
{
    return gen->segment_computer(qtl_chr, active_settings->step, pop->noise);
}


marker_observation_spec
marker_observation_specifications(generation_value gen)
{
    return active_settings->marker_observation_specs->map[gen->name];
}


observation_vectors_type
get_observation_vectors(generation_value gen)
{
        return gen->observation_vectors(marker_observation_specifications(gen));
}


pedigree_type
pedigree(population_value pop, generation_value gen, int ind)
{
    return pop->get_pedigree(gen, ind);
}


range<int>
individual_range(population_value pop)
{
    auto it = pop->observed_mark.find(pop->qtl_generation_name);
    if (it == pop->observed_mark.end()) {
        return {0, 0, 0};
    }
    return {0, (int)it->second.observations.n_obs, 1};
}


multi_generation_observations
population_marker_obs(population_value pop, chromosome_value chr, const pedigree_type& ped)
{
    generation_value qtl_gen = generation_by_name(pop->qtl_generation_name);
    collection<std::string> aogn = all_observed_generation_names(pop);
    collection<generation_value> aog = make_collection(generation_by_name, aogn);
    collection<observation_vectors_type> aov = make_collection(get_observation_vectors, aog);
    multi_generation_observations mgo(chr->marker_locus.size(), qtl_gen);
    for (size_t i = 0; i < aogn.size(); ++i) {
        mgo[*aog[i]] = (*aog[i])->raw_observations(*aov[i], marker_observations(pop, chr, *aog[i], ped[i]));
    }
    qtl_gen->update_locus_vectors(mgo);
    return mgo;
}


locus_probabilities_type
locus_probabilities(qtl_chromosome_value qtl_chr, population_value pop,
                    generation_value gen,
                    const multi_generation_observations& mgo,
                    const selected_qtls_on_chromosome_type&)
{
    auto it = mgo.find(gen);
    if (it == mgo.end()) {
        return {};
    }
    auto gc = gen->segment_computer(qtl_chr, active_settings->step, pop->noise);
    return gc.compute(it->second);
}


state_to_parental_origin_matrix_type
state_to_parental_origin_matrix(generation_value gen,
                                qtl_chromosome_value qtl_chr,
                                const selected_qtls_on_chromosome_type&)
{
    return const_cast<generation_rs*>(gen)->compute_state_to_parental_origin(qtl_chr->qtl.size());
}


parental_origin_type
parental_origin(const locus_probabilities_type& lp, generation_value gen, const selected_qtls_on_chromosome_type& sqoc)
{
    auto& s = const_cast<generation_rs*>(gen)->compute_state_to_parental_origin(sqoc.size());
    return s * lp;
}


collection<parental_origin_per_locus_type>
parental_origin_per_locus(const collection<parental_origin_type>& apo)
{
    auto& tmp = apo[0];
    size_t n_ind = apo.size();
    size_t n_par = tmp->innerSize();
    size_t n_loc = tmp->outerSize();
    /*MSG_DEBUG("n_ind " << n_ind);*/
    /*MSG_DEBUG("n_par " << n_par);*/
    /*MSG_DEBUG("n_loc " << n_loc);*/

    /* labels for individuals */
    std::vector<int> individuals;
    individuals.resize(n_ind);
    size_t index = 0;
    std::generate(individuals.begin(), individuals.end(), [&] () { return index++; });
    /* labels for parents */
    std::vector<std::vector<char>> parents = tmp->row_labels;
    /* loci */
    std::vector<double> loci = tmp->column_labels;
    /* return value */
    collection<parental_origin_per_locus_type> ret(loci.size());

    /* first round : init matrices */
    for (size_t l = 0; l < n_loc; ++l) {
        /*labelled_matrix<MatrixXd, int, std::vector<char>>& lm = ret[loci[l]];*/
        ret[l] = new unique_value<parental_origin_per_locus_type>();
        labelled_matrix<MatrixXd, int, std::vector<char>>& lm = *ret[l];
        lm.row_labels = individuals;
        lm.column_labels = parents;
        lm.data.resize(n_ind, n_par);
        for (int ind: individuals) {
            lm.data.row(ind) = apo[ind]->data.col(l);
        }
    }
    return ret;
}




MatrixXd traits_to_matrix()
{
    std::vector<MatrixXd> tmp;
    tmp.reserve(active_settings->populations.size());
    for (auto& pop: active_settings->populations) {
        auto& observed_traits = pop.second.observed_traits;
        /*MSG_DEBUG("ot.size " << observed_traits.size());*/
        /*MSG_DEBUG("ot.front.name " << observed_traits.front().name);*/
        /*MSG_DEBUG("ot.front.size " << observed_traits.front().values.size());*/
        /*Eigen::MatrixXd Y(observed_traits.front().values.size(), observed_traits.size());*/
        tmp.emplace_back(observed_traits.front().values.size(), observed_traits.size());
        for (size_t i = 0; i < observed_traits.front().values.size(); ++i) {
            for (size_t j = 0; j < observed_traits.size(); ++j) {
                tmp.back()(i, j) = observed_traits[j].values[i];
            }
        }
    }
    return concat_down(tmp);
}


VectorXd f_tester(const model& M, const model& M0, const parental_origin_per_locus_type& popl)
{
    model M1 = extend(M0, popl);
    return f_test(*const_cast<model*>(&M), *const_cast<model*>(&M1));
}

MatrixXd cross_indicator()
{
    int n_cols = active_settings->populations.size();
    int n_rows = 0;
    for (auto& pop: active_settings->populations) {
        n_rows += pop.second.observed_traits.front().values.size();
    }
    MatrixXd val = MatrixXd::Zero(n_rows, n_cols);
    n_rows = 0;
    int p = 0;
    for (auto& pop: active_settings->populations) {
        int sz = pop.second.observed_traits.front().values.size();
        val.block(n_rows, p, sz, 1) = VectorXd::Ones(sz);
        n_rows += sz;
        ++p;
    }
    return val;
}


model basic_model()
{
    model M(traits_to_matrix());
    M.add_bloc(cross_indicator());
    const char* decomp_method = getenv("DECOMP_METHOD");
    std::string s(decomp_method ? decomp_method : "");
    if (s == "SVD") {
        M.use_SVD();
    } else {
        M.use_QR();
    }
    /*std::cout << "Xt" << std::endl << M.X().transpose() << std::endl;*/
    /*std::cout << "Yt" << std::endl << M.Y().transpose() << std::endl;*/
    /*std::cout << "XtX^-1" << std::endl << M.XtX_pseudo_inverse() << std::endl;*/
    /*std::cout << "coefs" << std::endl << M.coefficients() << std::endl;*/
    /*auto r = M.residuals().array();*/
    /*std::cout << "residuals" << std::endl << (r * r).colwise().sum() << std::endl;*/
    return M;
}


collection<parental_origin_per_locus_type>
compute_parental_origins(const value<population_value>& pop,
                         const value<chromosome_value>& chr,
                         const value<generation_value>& gen)
{
    value<qtl_chromosome_value> qtl_chr
        = qtl_chromosome_by_name((*chr)->name);

     value<selected_qtls_on_chromosome_type> sqoc = selected_qtls_on_chromosome(*qtl_chr);

    value<generation_value> qtl_gen = qtl_generation(*pop);

    collection<pedigree_type> ap
        = make_collection(pedigree, pop, gen, individual_range(*pop));

    collection<multi_generation_observations> apmo
        = make_collection(population_marker_obs,
                          pop, chr, ap);

    collection<locus_probabilities_type> alp
        = make_collection(locus_probabilities, qtl_chr, pop, gen, apmo, sqoc);

    collection<parental_origin_type> apo
        = make_collection(parental_origin, alp, gen, sqoc);

    return parental_origin_per_locus(apo);
}


MatrixXd f_test_along_chromosome(const value<model>& Mcurrent, const value<model>& M0,
                                 const collection<parental_origin_per_locus_type>& popl)
{
    collection<VectorXd> aft
        = make_collection(f_tester, Mcurrent, M0, popl);

    MatrixXd ret(aft[0]->innerSize(), aft.size());

    for (size_t i = 0; i < aft.size(); ++i) {
        ret.col(i) = *aft[i];
    }
    return ret;
}


model
init_model(const MatrixXd& Y, const MatrixXd& indicator)
{
    model M(Y);
    const char* decomp_method = getenv("DECOMP_METHOD");
    std::string s(decomp_method ? decomp_method : "");
    if (s == "SVD") {
        M.use_SVD();
    } else {
        M.use_QR();
    }
    M.add_bloc(indicator);
    std::cout << "Xt" << std::endl << M.X().transpose() << std::endl;
    std::cout << "Yt" << std::endl << M.Y().transpose() << std::endl;
    std::cout << "XtX^-1" << std::endl << M.XtX_pseudo_inverse() << std::endl;
    std::cout << "coefs" << std::endl << M.coefficients() << std::endl;
    auto r = M.residuals().array();
    std::cout << "residuals" << std::endl << (r * r).colwise().sum() << std::endl;
    return M;
}


