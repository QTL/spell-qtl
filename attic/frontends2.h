/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_FRONTENDS_H_
#define _SPEL_FRONTENDS_H_

#include "../include/cache2.h"
#include "../include/computations/basic_data.h"
#include "../include/computations/model.h"
/*#include "model/tests.h"*/

#include "../../../../../usr/include/boost/math/distributions/normal.hpp"
  using boost::math::normal; // typedef provides default type is double.
  using boost::math::cdf;
  using boost::math::mean;
  using boost::math::variance;
  using boost::math::quantile;
  using boost::math::complement;


typedef std::pair<const chromosome*, double> selected_locus;

inline bool operator < (const selected_locus& sl1, const selected_locus& sl2) { return sl1.first < sl2.first || (sl1.first == sl2.first && sl1.second < sl2.second); }

inline std::ostream& operator << (std::ostream& os, const selected_locus& sl) { return os << sl.first->name << ':' << sl.second; }


struct chromosome_search_domain {
    const chromosome* chrom;
    std::vector<double> loci;

    chromosome_search_domain(const chromosome* c, const std::vector<double>& l) : chrom(c), loci(l) {}

    struct const_iterator {
        const chromosome_search_domain* this_csd;
        std::vector<double>::const_iterator i;
        const_iterator() : this_csd(NULL), i(__()) {}
        const_iterator(const chromosome_search_domain* t, const std::vector<double>::const_iterator& i_) : this_csd(t), i(i_) {}
        bool operator == (const const_iterator& other) const { return i == other.i; }
        bool operator != (const const_iterator& other) const { return i != other.i; }
        /*bool operator < (const const_iterator& other) const { return i < other.i; }*/
        /*size_t operator - (const const_iterator& other) const { return i - other.i; }*/
        std::pair<const chromosome*, double> operator * () const { return {this_csd->chrom, *i}; }
        const_iterator& operator ++ () { ++i; return *this; }
        const_iterator& operator -- () { --i; return *this; }
        static std::vector<double>::const_iterator __() { static std::vector<double> _; return _.end(); }
    };

    const_iterator begin() const { return {this, loci.begin()}; }
    const_iterator end() const { return {this, loci.end()}; }
    const_iterator cbegin() const { return {this, loci.begin()}; }
    const_iterator cend() const { return {this, loci.end()}; }
};


typedef std::vector<chromosome_search_domain> genome_search_domain;

struct gsd_iterator {
    std::vector<chromosome_search_domain>::const_iterator csd_i, csd_j;
    chromosome_search_domain::const_iterator i, j;

    bool operator == (const gsd_iterator& other) const { return csd_i == other.csd_i && i == other.i; }
    bool operator != (const gsd_iterator& other) const { return csd_i != other.csd_i || i != other.i; }

    gsd_iterator&
        operator ++ ()
        {
            ++i;
            if (i == j) {
                MSG_DEBUG("at end of chromosome!");
                ++csd_i;
                if (csd_i != csd_j) {
                    i = csd_i->begin();
                    j = csd_i->end();
                } else {
                    i = {};
                    j = {};
                }
            }
            return *this;
        }

    std::pair<const chromosome*, double> operator * () const { return *i; }
};

namespace std {
    inline gsd_iterator begin(const genome_search_domain& gsd) { return {gsd.begin(), gsd.end(), gsd.begin()->begin(), gsd.begin()->end()}; }
    inline gsd_iterator end(const genome_search_domain& gsd) { return {gsd.end(), gsd.end(), {}, {}}; }
}


typedef std::vector<selected_locus> locus_set;

typedef std::vector<locus_set> model_descriptor;

std::pair<bool, double>
detect_strongest_qtl(chromosome_value chr, const locus_key& lk,
                     const model& M0, const std::vector<double> pos);

MatrixXd
ftest_along_chromosome(chromosome_value chr, const locus_key& lk,
                       const model& M0, const std::vector<double> pos);


/* Definitions:
 * - cofactor: isolated POP (single chromosome, single locus)
 * - QTLs: joint POP (single chromosome, single or multiple loc(i)us)
 *
 *
 * Configurations:
 * - with/without Dominance                          D
 * - with/without Constraints (can/can't estimate)   WC
 * - Joint/Single POP computation mode               JS
 *
 *
 * Steps:                           D   JS  WC
 * - establish skeleton                      
 *   - manual (marker list)                  
 *   - by step                               
 * - discover cofactors                 S    
 *   - manual                           S    
 *   - forward                          S    
 *   - backward                         S    
 * - detect QTLs                    ?   J   Y
 *   - CIM-                         ?   J   Y
 *   - iQTLm                        ?   J   Y
 *   - iQTLm++                      ?   J   Y
 * - OPTIONALLY analyze epistasis   ?   J   Y
 * - estimate parameters            
 *
 *
 * Operations:
 * - select chromosome
 * - cofactors to QTLs for the current chromosome
 * - QTLs to cofactors for the current chromosome
 * - test along the chromosome
 * - test along all chromosomes
 * - add cofactor (if current chromosome in product probability mode)
 * - add QTL (if current chromosome in joint probability mode)
 * - remove cofactor/QTL
 */


struct signal_display {
#ifdef SIGNAL_DISPLAY_ONELINER
    static const char* tick(double x)
    {
        static const char* ticks[9] = { " ", "\u2581", "\u2582", "\u2583", "\u2584", "\u2585", "\u2586", "\u2587", "\u2588" };
        return ticks[x < 0. ? 0
                            : x >= 1. ? 8
                                      : ((int) floor(x * 9))];
    }

    VectorXd values;
    int imax_;
    bool above_;

    signal_display(const VectorXd& v, int imax, bool above)
        : values(v.innerSize()), imax_(imax), above_(above)
    {
        values = v;
#if 0
        int sig_cols = msg_handler_t::termcols() - 3;
        MSG_DEBUG("values.innerSize = " << values.innerSize());
        MSG_QUEUE_FLUSH();
        while (values.innerSize() >= sig_cols) {
            if (values.innerSize() & 1) {
                int sz = values.innerSize();
                values.conservativeResize(sz + 1);
                values(sz) = values(sz - 1);
            }
            int i = values.innerSize() >> 1;
            values = values.transpose() * kroneckerProduct(MatrixXd::Identity(i, i), MatrixXd::Constant(1, 2, .5));
            MSG_DEBUG("values.innerSize = " << values.innerSize());
            MSG_QUEUE_FLUSH();
        }
#endif
        double vmin = values.minCoeff();
        double vmax = values.maxCoeff();
        if (vmin == vmax) {
            values = (values.array() - vmin).matrix();
        } else {
            values = ((values.array() - vmin) / (vmax - vmin)).matrix();
        }
    }

    friend std::ostream& operator << (std::ostream& os, const signal_display& sd)
    {
        os << _WHITE << '[';
        for (int i = 0; i < sd.values.innerSize(); ++i) {
            if (i == sd.imax_) {
                os << (sd.above_ ? _GREEN : _RED);
            }
            os << tick(sd.values(i));
            if (i == sd.imax_) {
                os << _WHITE;
            }
        }
        return os << ']' << _NORMAL;
    }
#else
    braille_grid grid;

    signal_display(const chromosome& chr, const std::vector<double>& X, const VectorXd& y, int imax, double threshold)
        : grid(build(chr, X, y, imax, threshold))
    {}

    braille_grid
        build(const chromosome& chr, const std::vector<double>& X, const VectorXd& y, int imax, double threshold)
        {
            std::vector<double> Y(y.data(), y.data() + y.size());
            int padding_left = 0;
            int W = (int) (msg_handler_t::termcols() * .8);
            if (W > 1000) {
                W = 80;
            }
            braille_grid chr_map = chr.pretty_print(W, {}, {}, padding_left, false);

            braille_plot plot(W - padding_left, 5, 0, X.back(), 0, std::max(threshold, y(imax)));
            plot.plot(X, Y);
            plot.hline(threshold, 1, 1, 0, 255, 0);
            bool above = y(imax) > threshold;
            plot.vline(X[imax], 1, 0, above ? 0 : 255, above ? 255 : 0, 0);
            return plot.compose_vert(true, chr_map, false);
        }

    friend
        std::ostream&
        operator << (std::ostream& os, const signal_display& sd)
        {
            return os << sd.grid;
        }
#endif
};


struct QTL {
    std::string chromosome;
    double locus;
    std::vector<double> LOD_loci;
    std::vector<double> LOD;

    QTL(const std::string& n, double l, const std::vector<double>& x, const MatrixXd& y)
        : chromosome(n), locus(l), LOD_loci(x), LOD(y.data(), y.data() + y.size())
    {
        /*MSG_DEBUG("QTL at " << chromosome << ':' << locus);*/
        /*MSG_DEBUG(y);*/
        /*MSG_DEBUG(MATRIX_SIZE(y));*/
        /*MSG_DEBUG("" << LOD);*/
    }

    static
        double
        interpolate(double x0, double y0, double x1, double y1, double yT)
        {
            double delta_x = x1 - x0;
            double delta_y = y1 - y0;
            return delta_x * (yT - y0) / delta_y + x0;
        }

    std::pair<double, double>
        confidence_interval(const std::string &trait, const std::vector<QTL> &selection, ComputationType lod_test_type);
#if 0
        {
            /*MSG_DEBUG_INDENT_EXPR("[Confidence interval] ");*/
            /*MSG_DEBUG("LOD: " << LOD);*/
            double maxLOD = *std::max_element(LOD.begin(), LOD.end());
            double lod_cap = maxLOD - 1.5;
            /*MSG_DEBUG("max=" << maxLOD << " threshold=" << lod_cap);*/
            int i;
            for (i = 0; i < (int) LOD_loci.size() && LOD_loci[i] < locus && LOD[i] < lod_cap; ++i);
            /*MSG_DEBUG("LEFT i=" << i);*/
            double left;
            if (i > 0) {
                left = interpolate(LOD_loci[i - 1], LOD[i - 1], LOD_loci[i], LOD[i], lod_cap);
            } else {
                left = LOD_loci[i];
            }
            for (i = LOD_loci.size() - 1; i >= 0 && LOD_loci[i] > locus && LOD[i] < lod_cap; --i);
            /*MSG_DEBUG("RIGHT i=" << i);*/
            double right;
            if (i < (int) (LOD_loci.size() - 1)) {
                right = interpolate(LOD_loci[i], LOD[i], LOD_loci[i + 1], LOD[i + 1], lod_cap);
            } else {
                right = LOD_loci[i];
            }
            /*MSG_INFO("Confidence interval for " << chromosome << ':' << locus << " {" << left << ':' << right << '}');*/
            /*MSG_DEBUG_DEDENT;*/
            return {left, right};
        }
#endif
};


struct model_manager;

enum class AR { RSS=1, Rank=2, Test=4, Model=8, All=0xFF };

constexpr bool operator & (AR a, AR b) { return !!(((int) a) & ((int) b)); }


enum probability_mode { Joint, Single };


struct analysis_report {

    bool output_rss;
    bool output_rank;
    bool output_test;
    bool output_model;

    std::string report_path;

    std::string trait_name;

    std::string full_path;

    file report_file;

    std::map<std::string, std::map<double, std::string>> poi;
    std::map<std::string, std::map<double, std::pair<double, double>>> roi;

    analysis_report(const std::string& path, AR what)
        : output_rss(what & AR::RSS), output_rank(what & AR::Rank), output_test(what & AR::Test), output_model(what & AR::Model)
        , report_path(path)
        , trait_name()
        , full_path()
        , report_file()
        , poi(), roi()
    {
        ensure_directories_exist(report_path);
    }

    ~analysis_report()
    {
        report_file.close();
        report_file.open(MESSAGE(report_path << "/full_map.txt"), std::fstream::out);
        for (const auto& chr: active_settings->map) {
            report_file << chr.pretty_print(200, poi[chr.name], roi[chr.name]) << std::endl;
        }
    }

    void attach_model_manager(model_manager& mm);
    void detach_model_manager(model_manager& mm);

    void report_trait(const std::string& /*name*/, const MatrixXd& values)
    {
        static Eigen::IOFormat trait_format(Eigen::FullPrecision, Eigen::DontAlignCols, "\t", "\n", "", "", "", "");
        std::string filename = MESSAGE(full_path << '/' << "trait_values.txt");
        ofile of(filename);
        of << values.format(trait_format);
        of.close();
    }

    void report_lod(const QTL& qtl)
    {
        std::string filename = MESSAGE(full_path << '/' << qtl.chromosome << ':' << qtl.locus << "_LOD.txt");
        ofile of(filename);
        for (size_t i = 0; i < qtl.LOD.size(); ++i) {
            of << qtl.LOD_loci[i] << '\t' << qtl.LOD[i] << std::endl;
        }
        of.close();
    }

    void report_model(const model& Mcurrent)
    {
        if (output_model) {
            Mcurrent.output_X_to_file(full_path);
            Mcurrent.output_XtX_inv_to_file(full_path);
        }
    }

    /*enum ComputationType { NoTest=0, FTest=1, FTestLOD=2, R2=4, Chi2=8, Mahalanobis=16 };*/
    std::string
        computation_type_to_string(ComputationType ct)
        {
            std::stringstream ret;
            if (ct & FTest) { ret << "_FTest"; }
            if (ct & FTestLOD) { ret << "_FTestLOD"; }
            if (ct & R2) { ret << "_R2"; }
            if (ct & Chi2) { ret << "_Chi2"; }
            if (ct & Mahalanobis) { ret << "_Mahalanobis"; }
            return ret.str();
        }

    void report_computation(const model& Mcurrent, const chromosome* chrom_under_study, const computation_along_chromosome& cac, ComputationType ct, ComputationResults /*cr*/, const std::vector<double>& testpos, probability_mode pmode)
    {
        if (output_test | output_rss | output_rank) {
            /*MSG_DEBUG(MATRIX_SIZE(cac.ftest_pvalue));*/
            /*MSG_DEBUG(MATRIX_SIZE(cac.rss));*/
            std::string path = MESSAGE(full_path << '/' << chrom_under_study->name);
            ensure_directories_exist(path);
            std::string filename
                = MESSAGE(path << '/' << Mcurrent.keys()
                        << (output_test ? computation_type_to_string(ct) : "")
                        << (output_rss ? "_RSS" : "")
                        << (output_rank ? "_Rank" : "")
                        << (pmode == Joint ? "_Joint" : "")
                        << ".txt"
                        );
            ofile f(filename);
            if (output_test) { f << '\t' << "Test"; }
            if (output_rss) { for (int i = 0; i < cac.rss.innerSize(); ++i) { f << '\t' << "RSS"; } }
            if (output_rank) { f << '\t' << "Rank"; }
            f << std::endl;
            for (size_t i = 0; i < testpos.size(); ++i) {
                f << testpos[i];
                if (output_test) {
                    switch(ct) {
                        case ComputationType::FTest:
                            f << '\t' << cac.ftest_pvalue(0, i);
                            break;
                        case ComputationType::FTestLOD:
                            f << '\t' << cac.ftest_lod(0, i);
                            break;
                        case ComputationType::Chi2:
                            f << '\t' << cac.chi2(0, i);
                            break;
                        default:
                            /*last_computation = NULL;*/
                            ;
                    };
                }
                if (output_rss) {
                    for (int j = 0; j < cac.rss.innerSize(); ++j) {
                        f << '\t' << cac.rss(j, i);
                    }
                }
                if (output_rank) {
                    f << '\t' << cac.rank(i);
                }
                f << std::endl;
            }
            f.close();
        }
    }

    void report_final_model(model_manager& mm);

    void report_qtls(std::vector<QTL> &qtls, ComputationType lod_test_type);
};



struct model_manager {
    /* name of the studied single_trait */
    std::string trait_name;
    /* populations used in this model */
    collection<population_value> all_pops;
    /* POP matrices per locus per population (in the order of active_settings->populations) */
    std::vector<collection<parental_origin_per_locus_type>> pop_blocks;
    /* number of parents (for locus_key reduction of POP matrices) per population (same order) */
    std::vector<size_t> n_par;
    /* thy reference model */
    model Mcurrent;
    /* thy cofactor/qtl acceptance threshold */
    double threshold;
    /* the test to perform (FTest, Chi2...) */
    ComputationType test_type;
    /* the lists of loci behind the expression "compute X along the chromosome" per chromosome */
    std::map<const chromosome*, value<std::vector<double>>> all_loci;
    /* the chrrently selected chromosome */
    const chromosome* chrom_under_study;
    /* the list of loci for the chromosome currently under study */
    value<std::vector<double>> loci;
    /* the model blocks along the chromosome currently under study */
    collection<model_block_type> locus_blocks;
    /* the keys for each model block in locus_blocks */
    /*std::vector<model_block_key> locus_keys;*/
    model_block_key locus_base_key;
    /* structure to cache the results of the last computation along the chromosome */
    computation_along_chromosome cac;
    /* a direct access to the result of the last computation (&cac.TEST_TYPE) */
    MatrixXd* last_computation;
    /* the current probability mode */
    probability_mode pmode;

    /* slight hack. */
    std::vector<double> testpos;

    /* additiona, colpops.front()->ancestor_namesl data for output and postprocessing */
    std::map<std::string, std::map<double, std::pair<double, double>>> qtl_confidence_interval;

    analysis_report* reporter;

/* Construction
 */
    model_manager(const std::string& trait, const collection<population_value>& colpops,
                  const value<MatrixXd>& y, double th,
                  ComputationType ct = ComputationType::FTest,
                  SolverType st = SolverType::QR)
        : trait_name(trait)
        , all_pops(colpops)
        , pop_blocks()
        , n_par()
        , Mcurrent(y, colpops, (*colpops.front())->ancestor_names, st)
        , threshold(th)
        , test_type(ct)
        , all_loci()
        , chrom_under_study(NULL)
        , loci()
        , locus_blocks()
        , cac()
        , last_computation(NULL)
        , pmode(Single)
        , testpos()
        , qtl_confidence_interval()
        , reporter(NULL)
    {
        Mcurrent.add_block({}, cross_indicator(trait));
        Mcurrent.compute();
        /*MSG_DEBUG("INITIAL MODEL SIZE: " << MATRIX_SIZE(Mcurrent.X()));*/
        /*MSG_QUEUE_FLUSH();*/
        /*for (const auto& kpop: active_settings->populations) {*/
        for (const auto& kpop: all_pops) {
            context_key ck(new context_key_struc(*kpop,
                                                 &active_settings->map[0],
                                                 std::vector<double>()));
            n_par.push_back(
                    make_value<Mem>(compute_state_to_parental_origin,
                                    as_value(ck),
                                    as_value(locus_key()))->innerSize());
        }
        pop_blocks.resize(active_settings->populations.size());
    }


    model_manager(const model_manager& mm)
        : trait_name(mm.trait_name)
        , all_pops(mm.all_pops)
        , pop_blocks(mm.pop_blocks)
        , n_par(mm.n_par)
        , Mcurrent(mm.Mcurrent)
        , threshold(mm.threshold)
        , test_type(mm.test_type)
        , all_loci(mm.all_loci)
        , chrom_under_study(mm.chrom_under_study)
        , loci(mm.loci)
        , locus_blocks(mm.locus_blocks)
        , cac()
        , last_computation(NULL)
        , pmode(Single)
        , testpos(mm.testpos)
        , reporter(NULL)
    {}


/* Initializing test positions (TODO: optional exclusion window)
 */

    void init_loci_by_step(double step)
    {
        for (const chromosome& c: active_settings->map) {
            all_loci[&c]
                = compute_steps(
                    c.condensed.marker_locus,
                    step);
            /*MSG_DEBUG("test loci for chromosome " << c << std::endl << all_loci[&c]);*/
        }
        pop_blocks.clear();
    }

    void init_loci_by_hand(const std::map<std::string, std::vector<double>>& locmap)
    {
        for (auto mark_list: locmap) {
            const chromosome* c = active_settings->find_chromosome(mark_list.first);
            value<std::vector<double>>& vml = all_loci[c];
            vml = std::vector<double>();
            *vml = mark_list.second;
            std::sort(vml->begin(), vml->end());
        }
        pop_blocks.clear();
    }

    void init_loci_by_marker_list(const std::map<std::string, std::vector<std::string>>& markers)
    {
        for (auto mark_list: markers) {
            const chromosome* c = active_settings->find_chromosome(mark_list.first);
            value<std::vector<double>>& vml = all_loci[c];
            vml = std::vector<double>();
            vml->reserve(mark_list.second.size());
            for (const auto& mn: mark_list.second) {
                vml->push_back(c->raw.locus_by_name(mn));
            }
            std::sort(vml->begin(), vml->end());
        }
        pop_blocks.clear();
    }

    void init_loci_by_step_with_exclusion(double step, double exclusion_window = 0)
    {
        /* exclude all loci already in the model block keys */
        std::map<const chromosome*, std::vector<std::pair<double, double>>> excl;
        for (const auto& kb: Mcurrent.m_blocks) {
            for (const auto& ch_loc: kb.first.selection) {
                auto& exvec = excl[ch_loc.first];
                for (double l: ch_loc.second->to_vec()) {
                    exvec.emplace_back(l - exclusion_window, l + exclusion_window);
                }
            }
        }
        /* sort all exclusion windows */
        for (auto& cv: excl) {
            std::sort(cv.second.begin(), cv.second.end(),
                      [] (const std::pair<double, double>& a, const std::pair<double, double>& b)
                      {
                          return a.first < b.first;
                      });
        }
        /* initialize all_loci */
        for (const chromosome& c: active_settings->map) {
            all_loci[&c]
                = compute_steps(
                    c.condensed.marker_locus,
                    step,
                    excl[&c]);
        }
        pop_blocks.clear();
    }

/* Chromosome selection
 */
    void select_chromosome(const std::string& name)
    {
        select_chromosome(active_settings->find_chromosome(name));
    }

    void select_chromosome(const chromosome* c)
    {
        if (chrom_under_study != c) {
            chrom_under_study = c;
            pop_blocks.clear();
        }
        loci = all_loci[chrom_under_study];
    }

    const chromosome* selected_chromosome() const
    {
        return chrom_under_study;
    }

    void
        set_joint_mode()
        {
            /*MSG_DEBUG("[Model Manager] JOINT MODE");*/
            pmode = Joint;
        }

    void
        set_product_mode()
        {
            /*MSG_DEBUG("[Model Manager] PRODUCT MODE");*/
            pmode = Single;
        }

/* cofactors <-> QTLs for selected chromosome
 */
    locus_key cofactors_to_QTLs()
    {
        set_joint_mode();
        model_block_collection mbc = Mcurrent.extract_blocks_with_chromosome(chrom_under_study);
        model_block_key qtl_key = model_block_key::join(mbc);
        if (qtl_key.empty()) {
            return {};
        }
        const locus_key& lk = qtl_key[chrom_under_study];
        std::vector<double> loc{lk->locus};
        /*MSG_DEBUG(__LINE__ <<":chromosome " << chrom_under_study->name << " loci " << (*loci)); MSG_QUEUE_FLUSH();*/
        std::vector<double> allpos = lk->to_vec();
        std::vector<double> testpos = {lk->locus};
        Mcurrent.add_block(
            qtl_key,
            compute_parental_origins_multipop(
                all_pops,
                as_value(chrom_under_study),
                as_value(lk->parent),
                as_value(allpos),
                testpos)[0]);
        Mcurrent.compute();
        locus_base_key = qtl_key;
        return qtl_key.selection.begin()->second;
    }

    void QTLs_to_cofactors()
    {
        set_product_mode();
        model_block_collection mbc = Mcurrent.extract_blocks_with_chromosome(chrom_under_study);
        /* There shall be only ONE key in mbc. */
        if (mbc.size() == 0) {
            return;
        }
        std::vector<model_block_key> qtl_keys = mbc.begin()->first.split_loci();
        for (const auto& mbk: qtl_keys) {
            const locus_key& lk = mbk[chrom_under_study];
            std::vector<double> loc = lk->to_vec();
            loc.push_back(lk->locus);
            std::sort(loc.begin(), loc.end());
            /*MSG_DEBUG(__LINE__ <<":chromosome " << chrom_under_study->name << " loci " << loc); MSG_QUEUE_FLUSH();*/
            Mcurrent.add_block(
                mbk,
                compute_parental_origins_multipop(
                    all_pops,
                    as_value(chrom_under_study),
                    as_value(lk->parent),
                    as_value(loc),
                    loc)[0]);
            Mcurrent.compute();
        }
        locus_base_key.selection.clear();
    }

/* Test along the currently selected chromosome
 * TODO
 */
    struct test_result {
        const chromosome* chrom;
        double locus;
        double test_value;
        int index;
        bool over_threshold;
        model_block_key block_key;
        value<model_block_type> block;

        test_result()
            : chrom(NULL), locus(0), test_value(0), index(0), over_threshold(false), block_key(), block()
        {}

        test_result(const chromosome* c, double l, double tv, int i, bool ot, const model_block_key& mbk, const value<model_block_type>& mb)
            : chrom(c), locus(l), test_value(tv), index(i), over_threshold(ot), block_key(mbk), block(mb)
        {}

        test_result(const test_result& tr)
            : chrom(tr.chrom), locus(tr.locus), test_value(tr.test_value),
              index(tr.index), over_threshold(tr.over_threshold),
              block_key(tr.block_key), block(tr.block)
        {}

        test_result&
            operator = (const test_result& tr)
            {
                chrom = tr.chrom;
                locus = tr.locus;
                test_value = tr.test_value;
                index = tr.index;
                over_threshold = tr.over_threshold;
                block_key = tr.block_key;
                block = tr.block;
                return *this;
            }

        void reset()
        {
                chrom = NULL;
                locus = 0;
                test_value = 0;
                index = 0;
                over_threshold = false;
                block_key.selection.clear();
                block = value<model_block_type>();
        }

        friend
            std::ostream& operator << (std::ostream& os, const test_result& tr)
            {
                os << "<result chrom=" << (tr.chrom ? tr.chrom->name : "nil")
                    << " locus=" << tr.locus
                    << " test=" << tr.test_value
                    << " at=" << tr.index
                    << " over?=" << tr.over_threshold
                    << " block_key=" << tr.block_key
                    << '>';
                return os;
            }
    };

    /* todo: study_maha(locus)
     * creates base model by removing locus from proper block and computes maha distance cleanly
     */

    MatrixXd study_maha(const chromosome* chr, double locus, double minpos, double maxpos)
    {
        model_manager tmp(*this);
        tmp.select_chromosome(chr);
        tmp.remove(locus);
        value<model> Mbase = tmp.Mcurrent;
        return custom_test_along_chromosome(ComputationType::Mahalanobis, ComputationResults::Test, Mbase, minpos, maxpos).mahalanobis;
    }

    double max_testpos() const { return max_testpos(chrom_under_study); }
    double max_testpos(const chromosome* c) const { return all_loci.find(c)->second->back(); }

    const computation_along_chromosome&
        custom_test_along_chromosome(ComputationType ct, ComputationResults cr, value<model> Mbase, double minpos, double maxpos)
        {
            Mcurrent.compute();
            if (reporter) { reporter->report_model(Mcurrent); }

            if (!loci->size()) {
                static computation_along_chromosome dummy_cac;  // = {{}, {}, {}, {}, {}, {}, {}, {}, {}};
                return dummy_cac;
            }
            testpos.clear();
            _recompute(testpos, minpos, maxpos);
            /*MSG_DEBUG("Rank(Mcurrent " << Mcurrent.keys() << ") = " << Mcurrent.rank());*/
            /*MSG_DEBUG("Rank(Mbase " << Mbase->keys() << ") = " << Mbase->rank());*/
            compute_along_chromosome<>(cac, ct, cr,
                                     Mcurrent, Mbase,
                                     /*Mcurrent, Mcurrent,*/
                                     locus_base_key,
                                     chrom_under_study,
                                     testpos,
                                     locus_blocks);

            if (reporter) { reporter->report_computation(Mcurrent, chrom_under_study, cac, ct, cr, testpos, pmode); }

            return cac;
        }

    test_result
        test_along_chromosome(double min_pos, double max_pos)
        {
            if (!loci->size()) {
                last_computation = NULL;
                return test_result();
            }
            ComputationResults cr = Test;
            if (reporter && reporter->output_rss) { cr = cr | RSS; }
            if (reporter && reporter->output_rank) { cr = cr | Rank; }

            value<model> Mbase;
            if (pmode == Joint) {
                Mbase = Mcurrent.reduce(chrom_under_study);
                Mbase->compute();
            } else {
                Mbase = Mcurrent.clone();
            }

            custom_test_along_chromosome(test_type, cr, Mbase, min_pos, max_pos);

            switch(test_type) {
                case ComputationType::FTest:
                    last_computation = &cac.ftest_pvalue;
                    break;
                case ComputationType::FTestLOD:
                    last_computation = &cac.ftest_lod;
                    break;
                case ComputationType::Chi2:
                    last_computation = &cac.chi2;
                    break;
                default:
                    last_computation = NULL;
            };

            return find_max();
        }

#if 0
    double is_ghost_qtl(double locus)
    {
        locus_key selection = get_selection(chrom_under_study);
        double highest_test = 0;
        double highest_locus = -1;
        auto pmode_backup = pmode;
        set_product_mode();
        model m0 = Mcurrent;
        m0.add(chrom_under_study, locus);

        for (double l: selection) {
            model mA = m0;
            mA.add(chrom_under_study, l);
            model mB = mA;
            mB.ghost_constraint(chrom_under_study, locus, l);
            MSG_DEBUG("GHOST CHECK");
            MSG_DEBUG(mA);
            MSG_DEBUG(mB);
            /* FIXME: which test? */
            double test = ftest(mA, mB);
            if (test > highest_test) {
                highest_test = test;
            }
        }
        pmode = pmode_backup;
        return highest_test;
    }
#endif

    test_result
        find_max()
        {
            if (last_computation == NULL) {
                return {};
            }
            if (testpos.size() != (size_t)(last_computation->outerSize())) {
                MSG_ERROR("LOCI INCONSISTENT WITH COMPUTATION RESULT (" << loci->size() << " vs " << last_computation->outerSize() << ")", "");
            }
            int i_max = -1;
            double max = -1;
            for (int i = 0; i < last_computation->outerSize(); ++i) {
                if ((*last_computation)(0, i) >= max) {
                    max = (*last_computation)(0, i);
                    i_max = i;
                }
            }
            if (i_max == -1) {
                return {};
            }
            /*model_block_key k = locus_base_key;*/
            model_block_key mbk = locus_base_key;
            mbk += std::make_pair(chrom_under_study, (*loci)[i_max]);
            /*MSG_DEBUG("locus_base_key " << locus_base_key << " mbk " << mbk);*/
            /*MSG_DEBUG("last_computation@" << last_computation);*/
            /*MSG_QUEUE_FLUSH();*/
            /*MSG_DEBUG((*last_computation));*/
            /*MSG_QUEUE_FLUSH();*/

#ifdef SIGNAL_DISPLAY_ONELINER
            /*signal_display sd(last_computation->transpose(), i_max, max > threshold);*/
            /*MSG_DEBUG("[COMPUTATION] " << loci->front() << sd << loci->back() << " max=" << max << " at " << (*loci)[i_max]);*/
#else
            /*signal_display sd(*chrom_under_study, testpos, last_computation->transpose(), i_max, threshold);*/
            /*MSG_DEBUG("[COMPUTATION] " << loci->front() << " ... " << loci->back() << " max=" << max << " at " << (*loci)[i_max] << std::endl << sd);*/
#endif

            return {chrom_under_study,
                testpos[i_max], max, i_max, max > threshold,
                mbk,
                locus_blocks[i_max]};
        }

    test_result
        find_max_over_all_chromosomes()
        {
            test_result ret;
            for (const chromosome& c: active_settings->map) {
                select_chromosome(&c);
                if (!loci->size()) {
                    continue;
                }
                auto tmp = test_along_chromosome(0, max_testpos());
                if (tmp.test_value > ret.test_value) {
                    ret = tmp;
                }
            }
            return ret;
        }

/* Adding/Removing cofactors and QTLs
 */
    void add(const test_result& tr)
    {
        add(tr.block_key, tr.block);
        /*remove_test_locus(tr.chrom, tr.locus);*/
        if (pmode == Joint) {
            locus_base_key = tr.block_key;
        }
    }

    void remove_test_locus(const chromosome* chr, double loc)
    {
        std::vector<double>& llist = *all_loci[chr];
        auto it = std::find(llist.begin(), llist.end(), loc);
        if (it != llist.end()) {
            llist.erase(it);
            /*MSG_DEBUG("after remove_locus: " << llist);*/
        } else {
            MSG_ERROR("Locus " << loc << " doesn't exist in the list for chromosome " << chr->name, "");
            /*MSG_INFO("Available loci are " << llist);*/
        }
    }

    void add_test_locus(const chromosome* chr, double loc)
    {
        std::vector<double>& llist = *all_loci[chr];
        llist.push_back(loc);
        std::sort(llist.begin(), llist.end());
    }

    void add(const model_block_key& mbk, const value<model_block_type>& mb)
    {
        /*MSG_DEBUG("ADD " << mbk << " columns " << mb->column_labels);*/
        if (pmode == Joint) {
            /* replace previous block with the new one */
            Mcurrent.remove_blocks_with_chromosome(chrom_under_study);
        }
        /* add new block */
        Mcurrent.add_block(mbk, mb);
        Mcurrent.compute();
        /*MSG_DEBUG(Mcurrent.X());*/
    }

#if 1
    locus_key get_selection(const chromosome* chr)
    {
        if (pmode == Joint) {
            auto bwc = Mcurrent.blocks_with_chromosome(chr);
            if (bwc.begin() != bwc.end()) {
                return bwc.begin()->first[chr];
            }
        }
        return {};
    }

    double
        get_threshold()
        {
            return threshold;
#if 0
            if (pmode == Joint) {
                /*model Mperm(trait_permutations(trait_name, active_settings->n_permutations), Mcurrent);*/
                /*Mperm.compute();*/
                std::string old_title = active_settings->set_title(MESSAGE("Computing threshold for single_trait " << trait_name << " given selection " << Mcurrent.keys() << " using " << active_settings->n_permutations << " and quantile " << active_settings->qtl_threshold_quantile));
                double ret = *make_value<Disk|Sync>(qtl_threshold_all_chromosomes_for_model,
                                                    as_value(trait_name),
                                                    as_value(active_settings->qtl_threshold_quantile),
                                                    as_value(active_settings->n_permutations),
                                                    as_value(Mcurrent));
                MSG_INFO("New threshold computed given " << Mcurrent.keys() << ": " << ret);
                active_settings->set_title(old_title);
                return ret;
            } else {
                return threshold;
            }
#endif
        }

    void set_selection(const model_block_collection& mbk) {
        /*MSG_DEBUG(__LINE__ << ": set_selection pmode=" << pmode << " selection=" << Mcurrent.keys());*/
        if (pmode == Joint) {
            /* replace previous block with the new one */
            Mcurrent.remove_blocks_with_chromosome(chrom_under_study);
            locus_base_key = mbk.begin()->first;
        }
        /*MSG_DEBUG(__LINE__ << ": set_selection pmode=" << pmode << " selection=" << Mcurrent.keys());*/
        Mcurrent.add_blocks(mbk);
        /*MSG_DEBUG(__LINE__ << ": set_selection pmode=" << pmode << " selection=" << Mcurrent.keys());*/
        Mcurrent.compute();
    }

    model_descriptor
        get_model_descriptor(bool joined) const
        {
            model_descriptor ret;
            if (joined) {
                ret.emplace_back();
                for (const auto& chr_lk: locus_base_key.selection) {
                    for (double l: chr_lk.second) {
                        ret.back().emplace_back(chr_lk.first, l);
                    }
                }
            } else {
               for (const auto& chr_lk: locus_base_key.selection) {
                   for (double l: chr_lk.second) {
                       ret.emplace_back(std::vector<selected_locus>{{chr_lk.first, l}});
                   }
               }
            }
            return ret;
        }

    void set_selection(const locus_key& lk)
    {
        /*MSG_DEBUG("POUET " << pmode);*/
        /*MSG_DEBUG(__LINE__ << ": set_selection pmode=" << pmode << " selection=" << Mcurrent.keys());*/
        Mcurrent.remove_blocks_with_chromosome(chrom_under_study);
        if (pmode == Joint) {
            /* replace previous block with the new one */
            /*MSG_DEBUG(__LINE__ << ": set_selection pmode=" << pmode << " selection=" << Mcurrent.keys());*/
            if (!lk->is_empty()) {
                locus_base_key.selection.clear();
                locus_base_key.selection[chrom_under_study] = lk;
                /*std::vector<double> l = {lk->locus};*/
                std::vector<double> tmp_all = lk->to_vec();
                std::vector<double> tmp_test = {lk->locus};
                auto vmb = compute_parental_origins_multipop(all_pops,
                                                             as_value(chrom_under_study),
                                                             as_value(lk->parent),
                                                             as_value(tmp_all),
                                                             tmp_test)[0];
                /* add new block */
                /*MSG_DEBUG("ADD " << locus_base_key << " lk=" << lk << std::endl << vmb);*/
                Mcurrent.add_block(locus_base_key, vmb);
            }
        }
        /*MSG_DEBUG(__LINE__ << ": set_selection pmode=" << pmode << " selection=" << Mcurrent.keys());*/
        Mcurrent.compute();
    }

    void add(const chromosome* chr, double loc)
    {
        locus_key sel = get_selection(chr);
        model_block_key mbk;
        mbk.selection[chr] = sel + loc;
        std::vector<double> l = {loc};
        /*MSG_DEBUG(__LINE__ <<":chromosome " << chrom_under_study->name << " loci " << l); MSG_QUEUE_FLUSH();*/
        auto vmb = compute_parental_origins_multipop(all_pops,
                                                     as_value(chr),
                                                     as_value(sel),
                                                     as_value(l),
                                                     l)[0];
        /* add new block */
        if (pmode == Joint) {
            Mcurrent.remove_blocks_with_chromosome(chr);
        }
        Mcurrent.add_block(mbk, vmb);
    }

    void
        add(const chromosome* chr, const locus_key& sel)
        {
            model_block_key mbk;
            std::vector<double> loci = sel->to_vec();
            std::vector<double> l = {loci.back()};
            loci.pop_back();
            mbk.selection[chr] = sel;
            locus_key presel = sel - l.front();
            /*MSG_DEBUG("Compute parental origins chr=" << chr->name << " sel " << sel << " presel " << presel << " locus " << l);*/
            auto vmb = compute_parental_origins_multipop(all_pops,
                                                         as_value(chr),
                                                         as_value(presel),
                                                         as_value(l),
                                                         l)[0];
            Mcurrent.add_block(mbk, vmb);
        }

    void
        add_sub_blocks(const chromosome* chr, const locus_key& lk)
        {
            if (!lk || lk->is_empty()) {
                return;
            }
            /*MSG_DEBUG("ADDING SUB-BLOCKS FOR " << chr->name << " " << lk);*/
            std::vector<double> vsel = lk->to_vec();
            if (vsel.size() > 1) {
                for (double l: vsel) {
                    locus_key k = lk - l;
                    /*MSG_DEBUG("sub key " << k);*/
                    add_sub_blocks(chr, k);
                    add(chr, k);
                }
            }
        }

    model_manager
        model_for_estimation()
        {
            model_manager ret(*this);
            for (const auto& kb: Mcurrent.m_blocks) {
                for (const auto& chr_sel: kb.first.selection) {
                    if (chr_sel.second->is_empty()) { continue; }
                    ret.add_sub_blocks(chr_sel.first, chr_sel.second);
                }
            }
            /*MSG_DEBUG("New keys: " << ret.Mcurrent.keys());*/
            ret.Mcurrent.compute();
            return ret;
        }

    void add_all_loci()
    {
        for (const auto& kv: all_loci) {
            if (!kv.second) {
                continue;
            }
            for (double loc: *kv.second) {
                add(kv.first, loc);
            }
        }
    }
#endif

    /* create the reduction matrix to remove a locus from a model block. work across all chromosomes in selection. */
    MatrixXd reduce(int n_ancestors, const model_block_key& mbk, const chromosome* chr, double locus)
    {
        MatrixXd ret = MatrixXd::Ones(1, 1);

        /* TODO: use LD data if available */

        for (const auto& chr_lk: mbk.selection) {
            MatrixXd tmp = kroneckerProduct(
                                ret,
                                chr_lk.second->reduce([=] (double) -> int { return n_ancestors; },
                                                      chr_lk.first == chr ? locus : -1));
            ret.swap(tmp);
        }

        return ret;
    }

    model_block_collection remove(double loc)
    {
        model_block_collection
            mbc = Mcurrent.extract_blocks_with_chromosome_and_locus(
                    chrom_under_study, loc);
        /* FIXME: there should be a special treatment for epistasis blocks on multiple chromosomes */
        for (auto& kv: mbc) {
            model_block_key k = kv.first - std::make_pair(chrom_under_study, loc);
            if (!k.empty()) {
                /* If this block still has any locus in it, perform reduction of the joint probabilities */
                locus_key lk = kv.first[chrom_under_study];
                locus_key lk2 = k[chrom_under_study];
                auto pop_it = all_pops.begin();
                auto npar_it = n_par.begin();
                /* disassemble population blocks */
                auto pb = disassemble_parental_origins_multipop(chrom_under_study, lk->parent, *kv.second, all_pops);
                std::vector<collection<parental_origin_per_locus_type>> all_popl;
                all_popl.reserve(pb.size());
                for (auto& vmat: pb) {
                    /* reduce each population block */
                    const qtl_pop_type* pop = **pop_it;
                    context_key ck(new context_key_struc(pop, chrom_under_study, std::vector<double>()));
                    MatrixXd red = lk->reduce([&](double) -> int { return *npar_it; }, loc);
                    ++npar_it;
                    /*MSG_DEBUG(MATRIX_SIZE(vmat->data));*/
                    /*MSG_DEBUG(MATRIX_SIZE(red));*/
                    /*MSG_DEBUG("vmat BEFORE red" << std::endl << vmat);*/
                    MatrixXd data = vmat->data * red;
                    vmat->data = data;
                    vmat->column_labels = get_stpom_data(ck, lk2->parent)->row_labels;
                    /*MSG_DEBUG("vmat AFTER red" << std::endl << vmat);*/
                    all_popl.emplace_back();
                    all_popl.back().push_back(vmat);
                }
                /* reassemble model block */
                auto new_block = assemble_parental_origins_multipop(as_value(chrom_under_study),
                                                                    as_value(lk2->parent),
                                                                    all_popl,
                                                                    all_pops)[0];
                /*MSG_DEBUG("Resulting block" << std::endl << new_block);*/
                Mcurrent.add_block(k, new_block);
            }
        }
        if (pmode == Joint) {
            locus_base_key -= std::make_pair(chrom_under_study, loc);
        }
        Mcurrent.compute();
        return mbc;
    }

    void add(const model_block_collection& mbc)
    {
        Mcurrent.add_blocks(mbc);
    }

    model::printable_keys keys() const { return Mcurrent.keys(); }

    std::vector<QTL>
        QTLs()
        {
            std::set<model_block_key> uniq_loci;
            for (const auto& mb: Mcurrent.m_blocks) {
                for (const auto& k: mb.first.split_loci()) {
                    uniq_loci.insert(k);
                }
            }
            std::vector<QTL> ret;
            for (const auto& ul: uniq_loci) {
                /*chrom_under_study = ul.selection.begin()->first;*/
                double loc = *std::begin(ul.selection.begin()->second);
                select_chromosome(ul.selection.begin()->first);
                model_manager sub(*this);
                /*sub.output_model = sub.output_rank = sub.output_rss = sub.output_test = false;*/
                sub.remove(loc);
                /*testpos = *all_loci[chrom_under_study];*/
                value<model> Mbase = sub.Mcurrent;
                active_settings->set_title(MESSAGE("Computing confidence interval for QTL " << chrom_under_study->name << ':' << loc));
                auto lod = sub.custom_test_along_chromosome(ComputationType::FTestLOD, ComputationResults::Test, Mbase, 0, sub.max_testpos()).ftest_lod;
                /*MSG_DEBUG("ALL_LOCI " << testpos);*/
                /*MSG_DEBUG("LOD " << lod);*/
                ret.emplace_back(sub.chrom_under_study->name, loc, testpos, lod);
            }
            return ret;
        }

private:
    void _recompute(std::vector<double>& testpos, double minpos, double maxpos)
    {
        locus_key lk;
        if (pmode == Joint) {
            /*lk = locus_base_key[chrom_under_study];*/
            lk = get_selection(chrom_under_study);
        }
        _recompute(lk, testpos, minpos, maxpos);
    }

    void _recompute(const locus_key& lk, std::vector<double>& testpos, double minpos, double maxpos)
    {
        if (pmode == Joint) {
            threshold = get_threshold();
        }
        /*testpos = compute_effective_loci(lk, *loci);*/
        std::vector<double>::const_iterator mini, maxi;
        std::vector<double>::const_iterator i = loci->begin(), j = loci->end();
        while (i != j && (*i) < minpos) { ++i; }
        mini = i;
        while (i != j && (*i) <= maxpos) { ++i; }
        maxi = i;
        /*testpos = *loci;*/
        testpos.clear();
        testpos.reserve(maxi - mini);
        if (lk) {
            for (; mini != maxi; ++mini) {
                if (!lk->has(*mini)) {
                    testpos.push_back(*mini);
                }
            }
        } else {
            testpos.assign(mini, maxi);
        }
        /*MSG_DEBUG(__LINE__ << ":chromosome " << chrom_under_study->name << std::endl*/
                  /*<< " loci " << (*loci) << std::endl*/
                  /*<< " testpos " << testpos); MSG_QUEUE_FLUSH();*/
        locus_blocks
            = compute_parental_origins_multipop(
                    all_pops,
                    as_value(chrom_under_study),
                    as_value(lk),
                    loci,
                    testpos);
    }
};


inline
void analysis_report::attach_model_manager(model_manager& mm)
{
    trait_name = mm.trait_name;
    mm.reporter = this;
    full_path = MESSAGE(report_path << '/' << trait_name);
    ensure_directories_exist(full_path);
    report_trait(trait_name, mm.Mcurrent.Y());
    report_file.close();
    report_file.open(MESSAGE(full_path << '/' << trait_name << "_report.txt"), std::fstream::out);
}

inline
void analysis_report::detach_model_manager(model_manager& mm)
{
    mm.reporter = NULL;
}


inline
const std::string&
significance_string(double pvalue)
{
    static std::string
        threestar = "***",
        twostar = "**",
        onestar = "*",
        zerostar = ".",
        bad = " ";
    if (pvalue <= .001) {
        return threestar;
    } else if (pvalue <= .01) {
        return twostar;
    } else if (pvalue <= .05) {
        return onestar;
    } else if (pvalue <= .1) {
        return zerostar;
    } else {
        return bad;
    }
}
/* FIXME: define some kind of global array so significance strings and legend are always consistent. */
inline
const std::string&
significance_legend() { static std::string _ = "Significance codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1"; return _; }

inline
std::string
format_pvalue(double pvalue)
{
    std::stringstream pv;
    double d = 100 * pvalue;
    pv << std::setw(7);
    if (d < .1) {
        pv.setf(std::ios::scientific, std::ios::floatfield);
        pv.precision(1);
    } else {
        pv.setf(std::ios::fixed, std::ios::floatfield);
        pv.precision(2);
    }
    pv << d << "% " << std::setw(3) << std::left << significance_string(pvalue);
    return pv.str();
}


inline std::string empty_cell() { return "____________"; }


inline
std::pair<std::vector<size_t>, std::vector<std::pair<model_block_key, std::vector<std::vector<char>>>>>
filter_columns(const MatrixXd& conrow, const model_block_collection& blocks)
{
    std::pair<std::vector<size_t>, std::vector<std::pair<model_block_key, std::vector<std::vector<char>>>>> ret;
    auto& columns = ret.first;
    auto& block_parts = ret.second;
    size_t j = 0;
    for (const auto& kv: blocks) {
        const auto& labels = kv.second->column_labels;
        for (size_t i = 0; i < labels.size(); ++i, ++j) {
            if (conrow(j)) {
                if (!block_parts.size() || block_parts.back().first != kv.first) {
                    MSG_DEBUG("first used column in block " << kv.first);
                    block_parts.emplace_back();
                    block_parts.back().first = kv.first;
                    block_parts.back().second.push_back(labels[i]);
                } else {
                    block_parts.back().second.push_back(labels[i]);
                }
                MSG_DEBUG("added column #" << j);
                MSG_QUEUE_FLUSH();
                columns.push_back(j);
            }
        }
    }
    return ret;
}


inline
file& section_header(file& f, const std::string& str)
{
    if (f.tellp() != 0) { f << std::endl; }
    f << "=================================================================================================================" << std::endl
      << ' ' << str << std::endl
      << "-----------------------------------------------------------------------------------------------------------------" << std::endl << std::endl;
    return f;
}


inline
file& header(file& f, const std::string& str)
{
    size_t w = str.size() + 1;
    auto fill = [&] (char c) -> file& { char prev = f.m_impl.fill(c); f << std::setw(w) << ""; f.m_impl.fill(prev); return f; };
    f << std::endl;
    fill(' ') << '|' << std::endl
        << str << " |" << std::endl;
    fill('_') << '|' << std::endl << std::endl;
    return f;
}


inline
void analysis_report::report_final_model(model_manager& mm)
{
    static std::string emptystr;

    mm.Mcurrent.output_X_to_file(full_path);
    mm.Mcurrent.output_XtX_inv_to_file(full_path);
    model::xtx_printable xtx(mm.Mcurrent, true);
    const auto& rss = mm.Mcurrent.rss();
    /*const auto& XtX_inv = mm.Mcurrent.XtX_pseudo_inverse();*/
    const auto& X = mm.Mcurrent.X();
    /*const auto& XtX = mm.Mcurrent.XtX();*/
    /*size_t dof = mm.Mcurrent.rank();*/
    /*size_t n = mm.Mcurrent.Y().rows();*/
    auto qtls = mm.QTLs();
    section_header(report_file, MESSAGE("Report for single_trait " << mm.trait_name));
    report_qtls(qtls, mm.lod_test_type());
    //*

    /*header(report_file, "RSS ") << mm.Mcurrent.rss() << std::endl;*/
    //*/

    /* TODO ajouter R2 global */

    if (mm.Mcurrent.n_obs() <= mm.Mcurrent.dof() - 1) {
        report_file << "* There are not enough observations to compute a variance/covariance matrix. Results not displayed." << std::endl;
    } else {
        for (int i = 0; i < rss.size(); ++i) {
            section_header(report_file, MESSAGE(mm.trait_name << '#' << (i + 1)));
            {
                MatrixXd R2(1, mm.Mcurrent.m_blocks.size() - 1);
                auto bi = mm.Mcurrent.m_blocks.begin(), bj = mm.Mcurrent.m_blocks.end();
                int c = 0;
                ++bi;  // skip Cross Indicators block
                for (; bi != bj; ++bi, ++c) {
                    auto M0 = mm.Mcurrent;
                    M0.remove_block(bi->first);
                    M0.compute();
                    double r1 = mm.Mcurrent.rss()(i);
                    double r0 = M0.rss()(i);
                    R2(0, c) = (r0 - r1) / r0;
                }
                model_print::matrix_with_sections<std::string, void, model_block_key, std::vector<char>> mws(R2);
                static std::vector<std::vector<char>> empty_label = {{' '}};
                bi = mm.Mcurrent.m_blocks.begin();
                for (++bi; bi != bj; ++bi) { mws.add_column_section(bi->first, empty_label); }
                /*mm.Mcurrent.set_column_sections(mws, true);*/
                mws.add_row_section(emptystr, 1);
                header(report_file, "R2") << mws << std::endl;
            }
            MatrixXd coef = mm.Mcurrent.coefficients().col(i).transpose();
            /*MSG_DEBUG(MATRIX_SIZE(coef));*/
            {
                model_print::matrix_with_sections<std::string, void, model_block_key, std::vector<char>> mws(coef);
                mm.Mcurrent.set_column_sections(mws);
                /*for (const auto& kv: mm.Mcurrent.m_blocks) {*/
                    /*mws.add_column_section(kv.first, mm.Mcurrent.labels_to_ancestor_names(*kv.second));*/
                /*}*/
                mws.add_row_section(emptystr, 1);
                header(report_file, "Coefficients") << mws << std::endl;
            }
            /*report_file*/
                /*<< "Coefficients " << std::endl << mws << std::endl;*/
            /*double var_resid_est = rss(i) / (n - dof - 1);*/
            MatrixXd vcov = mm.Mcurrent.vcov(i);
            model::xtx_printable vc(mm.Mcurrent, vcov);
            header(report_file, "Covariance matrix") << vc << std::endl;
            auto conmat = X.bottomRows(X.rows() - mm.Mcurrent.n_obs());

            for (int r = 0; r < conmat.rows(); ++r) {
                auto subset = filter_columns(conmat.row(r), mm.Mcurrent.m_blocks);
                const auto& columns = subset.first;
                const auto& blocks = subset.second;
                /*Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic> pvmat = Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic>::Constant(columns.size(), columns.size(), empty_cell());*/
                Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic> pvmat(columns.size(), columns.size());
                Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic> ctrmat(columns.size(), columns.size());
                /*MatrixXd ctrmat = MatrixXd::Constant(columns.size(), columns.size(), std::numeric_limits<double>::quiet_NaN());*/
                for (int i1 = 0; i1 < pvmat.cols(); ++i1) {
                    for (int i2 = i1 + 1; i2 < pvmat.cols(); ++i2) {
                        int a1 = columns[i1];
                        int a2 = columns[i2];
                        double sigma = vcov(a1, a1) + vcov(a2, a2) - 2 * vcov(a1, a2);
                        double delta = coef(a1) - coef(a2);
                        normal s(0, sqrt(sigma));
                        double pvalue = 2 * cdf(complement(s, fabs(delta)));
                        ctrmat(i1, i2) = MESSAGE(std::setprecision(3) << -delta << ' ' << std::setw(3) << std::left << significance_string(pvalue));
                        ctrmat(i2, i1) = MESSAGE(std::setprecision(3) << delta << ' ' << std::setw(3) << std::left << significance_string(pvalue));
                        pvmat(i1, i2) = pvmat(i2, i1) = format_pvalue(pvalue);
                    }
                }
                model_print::matrix_with_sections<void, std::vector<char>, model_block_key, std::vector<char>, Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic>> ctrsig(pvmat);
                model_print::matrix_with_sections<void, std::vector<char>, model_block_key, std::vector<char>, Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic>> ctr(ctrmat);
                for (const auto& kl: blocks) {
                    std::vector<std::vector<char>> names;
                    if (kl.first.empty()) {
                        for (const auto& kp: mm.Mcurrent.m_all_pops) {
                            const auto& qgn = (*kp)->qtl_generation_name;
                            names.emplace_back(qgn.begin(), qgn.end());
                        }
                    } else {
                        names = mm.Mcurrent.labels_to_ancestor_names(kl.second);
                    }
                    ctr.add_row_section(names);
                    ctrsig.add_row_section(names);
                    for (auto& n: names) { n.push_back(' '); n.push_back(' '); n.push_back(' '); n.push_back(' '); }
                    ctr.add_column_section(kl.first, names);
                    ctrsig.add_column_section(kl.first, names);
                }
                header(report_file, "Contrasts") << ctr << std::endl << significance_legend() << std::endl;
                header(report_file, "Contrast significance") << ctrsig << std::endl << significance_legend() << std::endl;
            }

#if 0
            std::vector<std::vector<int>> groups(conmat.rows());
            for (int i = 0; i < conmat.rows(); ++i) {
                for (int j = 0; j < conmat.cols(); ++j) {
                    if (conmat(i, j)) {
                        groups[i].push_back(j);
                    }
                }
            }
            std::vector<std::string> colnames;
            colnames.reserve(X.cols());

            for (const auto& k_blk: mm.Mcurrent.m_blocks) {
                for (const auto& v: mm.Mcurrent.labels_to_ancestor_names(*k_blk.second)) {
                    colnames.emplace_back(v.begin(), v.end());
                }
            }

            for (const auto& grp: groups) {
                report_file << "Contrast group";
                for (int j: grp) {
                    report_file << ' ' << mm.Mcurrent.m_ancestor_names['a' + j];
                }
                report_file << std::endl;

                for (size_t i1 = 0; i1 < grp.size(); ++i1) {
                    for (size_t i2 = i1 + 1; i2 < grp.size(); ++i2) {
                        int a1 = grp[i1];
                        int a2 = grp[i2];
                        std::string name1 = colnames[a1];
                        std::string name2 = colnames[a2];
                        double sigma = vcov(a1, a1) + vcov(a2, a2) - 2 * vcov(a1, a2);
                        double delta = coef(a1) - coef(a2);
                        normal s(0, sqrt(sigma));
                        /*MSG_DEBUG("Normal distribution, mean = " << s.mean() << ", variance = " << variance(s));*/
                        double q = quantile(s, .975);
                        bool out = fabs(delta) > q;
                        double pvalue = 2 * cdf(complement(s, fabs(delta)));
                        /*MSG_DEBUG("quantile(cdf(.975)) == x ? " << (fabs(quantile(s, cdf(s, .975)) - .975) < 1.e-5));*/
                        /*report_file << "** " << name1 << " -- " << name2 << "   µ=" << mean(s) << " sigma=" << variance(s) << std::endl;*/
                        /*report_file << name1 << " -- " << name2 << ": " << (out ? "true" : "false") << " quantile(.975)=" << q << " delta=" << delta << " cdf(" << fabs(delta) << ")=" << cdf(s, fabs(delta)) << std::endl;*/
                        /*report_file << name1 << " -- " << name2 << ": " << pvalue << " (-log10 = " << (-log10(pvalue)) << ") " << (out ? "true" : "false") << std::endl;*/
                        std::string pvstr = format_pvalue(pvalue);
                        report_file << name1 << " -- " << name2 << ": " << pvstr << std::endl;
                    }
                }
            }
#endif
        }
    }
    section_header(report_file, "Final model") << std::endl << mm.Mcurrent << std::endl;
    section_header(report_file, "XtX^-1") << std::endl << xtx << std::endl;
}

inline
void analysis_report::report_qtls(std::vector<QTL> &qtls, ComputationType lod_test_type)
{
    for (auto& qtl: qtls) {
        const auto& ci = roi[qtl.chromosome][qtl.locus] = qtl.confidence_interval(trait_name, qtls, lod_test_type);
        report_lod(qtl);
        std::string name = MESSAGE(trait_name << " @ " << qtl.locus << " [" << ci.first << ':' << ci.second << ']');
        if (poi[qtl.chromosome][qtl.locus].size()) {
            poi[qtl.chromosome][qtl.locus] = MESSAGE(poi[qtl.chromosome][qtl.locus] << ',' << name);
        } else {
            poi[qtl.chromosome][qtl.locus] = name;
        }
        report_file << "QTL detected on chromosome " << qtl.chromosome << " at " << qtl.locus << "cM with confidence interval {" << roi[qtl.chromosome][qtl.locus].first << ':' << roi[qtl.chromosome][qtl.locus].second << '}' << std::endl;
    }
}

model_manager&
init_skeleton(model_manager& mm);

model_manager&
forward(model_manager& mm);

model_manager&
backward(model_manager& mm);

model_manager&
qtl_detect_cim_minus(model_manager& mm);

model_manager&
qtl_detect_cim(model_manager& mm);

model_manager&
qtl_detect_iqtlm(model_manager& mm);

model_manager&
qtl_detect_iqtlm_gw(model_manager& mm);






struct test_manager;


struct test_domain_search_method {
    virtual void init(const test_manager*, const std::vector<genome_search_domain>&, locus_set& point) = 0;
    virtual bool next(locus_set& point) = 0;
};


inline
std::ostream&
operator << (std::ostream& os, const qtl_pop_type* p) { return os << p->name << '/' << p->qtl_generation_name; }

inline
std::ostream&
operator << (std::ostream& os, const chromosome* c) { return os << c->name; }

inline
collection<population_value>
flatten_pops(const std::string& trait_name)
{
    auto lpi = active_settings->linked_pops.find(trait_name);
    if (lpi == active_settings->linked_pops.end()) {
        return {};
    }
    collection<population_value> pops;
    for (const auto& pvec: lpi->second) {
        for (const auto& pptr: pvec) {
            pops.emplace_back(pptr.get());
        }
    }
    return pops;
}


struct test_manager {
    std::string trait_name;
    collection<population_value> all_pops;
    std::map<locus_set, double> results;

    model_descriptor M0_descr;

    size_t epistasis_max;
    size_t dominance_depth;
    bool with_constraints;

    model M0;

    ComputationType test_type;

    test_manager(const std::string& trait, const collection<population_value>& colpops,
                  const value<MatrixXd>& y,
                  size_t epis_max,
                  size_t dom_depth,
                  bool constr,
                  ComputationType ct = ComputationType::FTest,
                  SolverType st = SolverType::QR)
        : trait_name(trait)
        , all_pops(colpops)
        , results()
        , M0_descr({{}})
        , epistasis_max(epis_max)
        , dominance_depth(dom_depth)
        , with_constraints(constr)
        , M0(y, colpops, st)
        , test_type(ct)
    {}

    test_manager(const std::string& trait, size_t epis_max, size_t dom_depth, bool constr, ComputationType ct = ComputationType::FTest)
        : trait_name(trait)
        , all_pops(flatten_pops(trait))
        , results()
        , M0_descr({{}})
        , epistasis_max(epis_max)
        , dominance_depth(dom_depth)
        , with_constraints(constr)
        , M0(trait_matrix(trait, all_pops), all_pops, SolverType::QR)
        , test_type(ct)
    {}


    std::pair<bool, double>
        get_result(const locus_set& point) const
        {
            auto it = results.find(point);
            if (it == results.end()) {
                return {false, 0};
            }
            return {true, it->second};
        }


    bool
        next_subset(size_t n, std::vector<size_t>& sub)
        {
            if (sub.back() == n - sub.size()) {
                return false;
            }
            size_t i = sub.size() - 1;
            ++sub[i];
            while (i > 0 && sub[i] == sub[i - 1]) {
                sub[i] = i == sub.size() - 1 ? 0 : sub[i + 1] + 1;
                ++sub[i - 1];
                --i;
            }
            return true;
        }


    std::vector<size_t>
        first_subset(size_t m)
        {
            std::vector<size_t> ret(m);
            for (size_t i = 0; i < m; ++i) {
                ret[i] = m - i - 1;
            }
            return ret;
        }


    model_block_type
        compute_block(const model_block_key& mbk)
        {
            std::vector<model_block_type> parts;
            /* compute independent blocks */
            /*MSG_DEBUG("COMPUTE BLOCK " << mbk);*/
            /*MSG_DEBUG("all_pops");*/
            /*for (const auto& p: all_pops) {*/
                /*MSG_DEBUG(" * " << (*p)->name << '/' << (*p)->qtl_generation_name);*/
            /*}*/
            for (const auto& kv: mbk.selection) {
                const locus_key& lk = kv.second;
                std::vector<double> allpos = lk->to_vec();
                std::vector<double> testpos = {lk->locus};
                /*MSG_DEBUG("lk " << lk);*/
                /*MSG_DEBUG("allpos " << allpos);*/
                /*MSG_DEBUG("testpos " << testpos);*/
                auto blocks = compute_parental_origins_multipop(
                            all_pops,             /* populations */
                            as_value(kv.first),   /* chromosome */
                            as_value(lk->parent), /* context */
                            as_value(allpos),     /* context + this pos */
                            testpos);             /* this pos */
                /*MSG_DEBUG("Computed block parts: ");*/
                /*MSG_DEBUG("" << blocks);*/
                parts.emplace_back(*blocks[0]);
                /*MSG_DEBUG("" << parts.back());*/
            }
            /* row-kronecker them */
            model_block_type ret = parts.front();
            for (size_t i = 1; i < parts.size(); ++i) {
                std::vector<std::vector<char>> new_labels;
                new_labels.reserve(ret.column_labels.size() * parts[i].column_labels.size());
                for (const auto& l1: ret.column_labels) {
                    for (const auto& l2: parts[i].column_labels) {
                        new_labels.emplace_back(l1);
                        new_labels.back().insert(new_labels.back().end(), l2.begin(), l2.end());
                    }
                }
                ret.column_labels = new_labels;
                for (int row = 0; row < ret.data.rows(); ++row) {
                    ret.row(row) = kroneckerProduct(ret.row(row), parts[i].row(row));
                }
            }
            return ret;
        }

    void
        add_blocks(model& M, const locus_set& selection)
        {
            if (selection.size() == 0) {
                /* cross indicator */
                M.add_block({}, cross_indicator(trait_name));
            } else {
                size_t max_subset_size = std::min(selection.size(), epistasis_max);
                for (size_t subset_size = 1; subset_size <= max_subset_size; ++subset_size) {
                    std::vector<size_t> subset = first_subset(subset_size);
                    do {
                        model_block_key mbk;
                        for (size_t i: subset) {
                            mbk += selection[i];
                        }
                        M.add_block(mbk, compute_block(mbk));
                    } while (next_subset(selection.size(), subset));
                }
            }
        }

    void
        add_blocks(model& M, const std::vector<locus_set>& selection)
        {
            for (const auto& s: selection) {
                add_blocks(M, s);
            }
        }

    void
        build_M0()
        {
            M0.m_blocks.clear();
            M0.m_computed = false;
            for (const auto& s: M0_descr) {
                add_blocks(M0, s);
            }
            M0.compute();
        }

    model
        build_M1(const locus_set& delta_M1_descr)
        {
            model M1 = M0;
            add_blocks(M1, delta_M1_descr);
            M1.compute();
            return M1;
        }

    /* test M1 = M0 + (expansion from search domain) vs M0 */
    void
        compute(const std::vector<genome_search_domain>& domain, test_domain_search_method& tds, bool link_to_search=false)
        {
            static MatrixXd tmp(1, 1);
            locus_set point;
            build_M0();
            /*MSG_DEBUG("M0");*/
            /*MSG_DEBUG("" << M0);*/
            results.clear();
            tds.init(this, domain, point);
            do {
                model M1 = build_M1(point);
                /*MSG_DEBUG("at " << point);*/
                /*MSG_DEBUG("M1");*/
                /*MSG_DEBUG("" << M1);*/
                results[point] = test(M1);
            } while (tds.next(point));
        }

    double
        test(const model& M1)
        {
            static MatrixXd tmp(1, 1);
            switch (test_type) {
                case FTest:
                    f_test(M0, M1, 0, &tmp, NULL);
                    break;
                case FTestLOD:
                    f_test(M0, M1, 0, NULL, &tmp);
                    break;
                default:;
                        /* TODO */
            };
            return tmp(0, 0);
        }

    const std::map<locus_set, double>::value_type*
        best_result() const
        {
            const std::map<locus_set, double>::value_type* ret = NULL;
            double best = -std::numeric_limits<double>::infinity();
            for (const auto& kv: results) {
                if (kv.second > best) {
                    ret = &kv;
                    best = kv.second;
                }
            }
            return ret;
        }
};


struct test_all_domain : public test_domain_search_method {
    std::vector<gsd_iterator> sd_begin, sd_end, sd_cur;
    size_t counter;

    test_all_domain() : sd_begin(), sd_end(), sd_cur() {}

    void
        init(const test_manager*, const std::vector<genome_search_domain>& dom, locus_set& point)
        {
            sd_begin.clear();
            sd_cur.clear();
            sd_end.clear();
            sd_begin.reserve(dom.size());
            sd_cur.reserve(dom.size());
            sd_end.reserve(dom.size());
            for (const auto& gsd: dom) {
                gsd_iterator b = std::begin(gsd);
                gsd_iterator e = std::end(gsd);
                sd_begin.emplace_back(b);
                sd_cur.emplace_back(b);
                sd_end.emplace_back(e);
            }
            counter = 0;
            for (const auto& cur: sd_cur) {
                point.push_back(*cur);
            }
        }

    bool
        next(locus_set& point)
        {
            int i = sd_cur.size() -  1;
            point.clear();
            point.reserve(sd_cur.size());
            bool carry;
            do {
                /*MSG_DEBUG('#' << counter);*/
                carry = false;
                ++sd_cur[i];
                if (sd_cur[i] == sd_end[i]) {
                    sd_cur[i] = sd_begin[i];
                    --i;
                    carry = true;
                }
            } while (carry && i >= 0);
            if (i < 0) {
                return false;
            }
            ++counter;
            for (const auto& cur: sd_cur) {
                point.push_back(*cur);
            }
            return true;
        }
};


inline
std::pair<double, double>
QTL::confidence_interval(const std::string &trait, const std::vector<QTL> &selection, ComputationType lod_test_type)
{
    test_all_domain tad;
    test_manager tm(trait, 1, 0, true);
    tm.test_type = ComputationType::FTestLOD;
    std::vector<genome_search_domain> vgsd(1);
    const ::chromosome* chr = active_settings->find_chromosome(chromosome);
    auto steps = compute_steps(chr->condensed.marker_locus, active_settings->step);
    vgsd.back().emplace_back(chr, steps);
    tm.compute(vgsd, tad);
    size_t first = 0, last = steps.size() - 1;
    double step_min, step_max;

    auto lod = [&] (double l) { locus_set point = {{chr, l}}; return tm.results[point]; };

    double threshold = lod(locus) - 1;

    {
        LOD.clear();
        LOD.reserve(tm.results.size());
        LOD_loci = steps;
        for (const auto& r: tm.results) {
            LOD.push_back(r.second);
        }
    }

    {
        auto i = tm.results.begin();
        while (i->second < threshold) { ++i; ++first; }
        if (first == 0) {
            step_min = 0;
        } else {
            step_min = interpolate(steps[first - 1], lod(steps[first - 1]), steps[first], lod(steps[first]), threshold);
        }
    }

    {
        /* TODO add selection \ {qtl} */
        (void) selection;
        auto i = tm.results.rbegin();
        while (i->second < threshold) { ++i; --last; }
        if (last == steps.size() - 1) {
            step_max = steps.back();
        } else {
            step_max = interpolate(steps[last], lod(steps[last]), steps[last + 1], lod(steps[last + 1]), threshold);
        }
    }

    return {step_min, step_max};
}

#endif

