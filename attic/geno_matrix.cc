/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "geno_matrix.h"
#include "pedigree.h"
#include "pedigree_tree.h"
#include "data/genoprob_computer2.h"
#include "cache2.h"
#include "bayes/factor_var4.h"


inline
std::vector<size_t> operator | (const std::vector<size_t>& s1, const std::vector<size_t>& s2)
{
    std::vector<size_t> ret(s1.size() + s2.size());
    auto it = std::set_union(s1.begin(), s1.end(), s2.begin(), s2.end(), ret.begin());
    ret.resize(it - ret.begin());
    return ret;
}


inline
std::vector<size_t> operator ^ (const std::vector<size_t>& s1, const std::vector<size_t>& s2)
{
    std::vector<size_t> ret(s1.size() + s2.size());
    auto it = std::set_intersection(s1.begin(), s1.end(), s2.begin(), s2.end(), ret.begin());
    ret.resize(it - ret.begin());
    return ret;
}


inline
std::vector<size_t> operator | (const std::vector<size_t>& s1, size_t p)
{
    std::vector<size_t> ret(s1.size() + 1);
    auto i = s1.begin();
    auto j = s1.end();
    auto o = ret.begin();
    while (i != j && *i < p) {
        *o++ = *i++;
    }
    if (i != j && *i == p) {
        ++i;
    }
    *o++ = p;
    while (i != j) {
        *o++ = *i++;
    }
    ret.resize(o - ret.begin());
    return ret;
}


inline
std::vector<size_t> operator / (const std::vector<size_t>& s1, const std::vector<size_t>& s2)
{
    std::vector<size_t> ret(s1.size());
    auto it = std::set_difference(s1.begin(), s1.end(), s2.begin(), s2.end(), ret.begin());
    ret.resize(it - ret.begin());
    return ret;
}




label_type empty = {GAMETE_EMPTY, GAMETE_EMPTY};


geno_matrix
    gamete = {
        "generic_gamete",
        Gamete,
        {{GAMETE_L, GAMETE_EMPTY}, {GAMETE_R, GAMETE_EMPTY}},
        (MatrixXd(2, 2) <<
         -1,  1,
          1, -1).finished(),
        /* BEWARE these matrices (p and p_inv) SHOULD be * 1/sqrt(2) */
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         P_NORM_FACTOR, -P_NORM_FACTOR).finished(),
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         P_NORM_FACTOR, -P_NORM_FACTOR).finished(),
        (MatrixXd(2, 1) << 0, -2).finished(),
        (VectorXd(2) << .5, .5).finished(),
        (MatrixXd(2, 2) <<
         1, 0,
         0, 1).finished(),
        (MatrixXd(2, 2) <<
         1, 0,
         0, 1).finished(),
#ifdef WITH_OVERLUMPING
        {},
            /*{{{GAMETE_L, GAMETE_L}, {GAMETE_R, GAMETE_R}}, {{0, 0}, {1, 1}}},*/
            /*{{{GAMETE_L, GAMETE_R}, {GAMETE_R, GAMETE_L}}, {{0, 1}, {1, 0}}}*/
            /*symmetry_group_type().insert({permutation_type::anti_identity(2), letter_permutation_type{}})*/
        /*},*/
        {}
#endif

    },
    doubling_gamete = {
        "doubling_gamete",
        DoublingGamete,
        {{GAMETE_L, GAMETE_L}, {GAMETE_R, GAMETE_R}},
        (MatrixXd(2, 2) <<
         -1,  1,
          1, -1).finished(),
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         P_NORM_FACTOR, -P_NORM_FACTOR).finished(),
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         P_NORM_FACTOR, -P_NORM_FACTOR).finished(),
        (MatrixXd(2, 1) << 0, -2).finished(),
        (VectorXd(2) << .5, .5).finished(),
        (MatrixXd(2, 2) <<
         1, 0,
         0, 1).finished(),
        (MatrixXd(2, 2) <<
         1, 0,
         0, 1).finished(),
#ifdef WITH_OVERLUMPING
        {},
            /*{{{GAMETE_L, GAMETE_L}, {GAMETE_R, GAMETE_R}}, {{0, 1}, {1, 0}}}*/
            /*{{{0, 1}, {1, 0}}}*/
            /*symmetry_group_type().insert({permutation_type::identity(2), letter_permutation_type{}})*/
        /*},*/
        {}
#endif
    };

geno_matrix
    selfing_gamete = kronecker(gamete, gamete);



#if 0
geno_matrix_variant_type geno_matrix_variant_table[_GMVT_size][_GMVT_size] =
{                  /*            Gamete, DoublingGamete, SelfingGamete, Haplo, Geno*/
/*Gamete*/         {Nonsense, SelfingGamete, Nonsense, Nonsense, Nonsense,    Haplo},
/*DoublingGamete*/ {Nonsense,      Nonsense, Nonsense, Nonsense,     Geno, Nonsense},
/*SelfingGamete*/  {Nonsense,      Nonsense, Nonsense, Nonsense, Nonsense,     Geno},
/*Haplo*/          {Nonsense,      Nonsense, Nonsense, Nonsense,     Geno, Nonsense},
/*Geno*/           {Nonsense,      Nonsense, Nonsense, Nonsense, Nonsense, Nonsense}
};
#endif



void test_group(const MatrixXd& inf_mat, const std::vector<label_type>& labels)
{
    symmetry_group_type group(labels);
    symmetry_table_type tmp;
    bool ok;
    std::vector<int> perm(group.m_symmetries[0].table.map());
    std::tie(ok, tmp) = symmetry_table_type::build(permutation_type{perm}, labels, inf_mat, false);
    MSG_DEBUG("built identity? " << ok);
    MSG_DEBUG("identity: " << tmp);
    std::tie(ok, tmp) = symmetry_table_type::build(permutation_type{perm}, labels, inf_mat, true);
    if (ok) {
        MSG_DEBUG("");
        MSG_DEBUG("FOUND LATENT SYMMETRY!");
        MSG_DEBUG(tmp);
    }
    while (std::next_permutation(perm.begin(), perm.end())) {
        std::tie(ok, tmp) = symmetry_table_type::build(permutation_type{perm}, labels, inf_mat, false);
        if (ok) {
            MSG_DEBUG("");
            MSG_DEBUG("GOOD " << tmp);
            group.insert(tmp);
        }
        std::tie(ok, tmp) = symmetry_table_type::build(permutation_type{perm}, labels, inf_mat, true);
        if (ok) {
            MSG_DEBUG("");
            MSG_DEBUG("FOUND LATENT SYMMETRY!");
            MSG_DEBUG(tmp);
        }
    }
    MSG_DEBUG("Group size: " << group.size());
    MSG_DEBUG(group);
}




labelled_matrix<MatrixXd, label_type, double>
run_gpc(const geno_matrix* gen, const std::vector<double>& LV_loci, const MatrixXd& LV, const std::vector<double>& test_loci)
{
    geno_prob_computer gpc;
    gpc.LV = &LV;
    gpc.locus = &LV_loci;
    gpc.init(gen);
    return gpc.fast_compute_over_segment(test_loci);
}


labelled_matrix<MatrixXd, label_type, double>
run_on_segment(const geno_matrix* gen, double delta_M2, double delta_M3,
               const std::vector<label_type>& obs_M1, const std::vector<label_type>& obs_M2, const std::vector<label_type>& obs_M3,
               std::vector<double> steps, const std::set<subset>& P)
{
    auto obs2LV = gen->LV_map();

    MatrixXd LV = MatrixXd::Zero(gen->rows(), 3);

    for (auto l: obs_M1) { LV.col(0) += obs2LV[l]; }
    for (auto l: obs_M2) { LV.col(1) += obs2LV[l]; }
    for (auto l: obs_M3) { LV.col(2) += obs2LV[l]; }

    double dist_M3 = delta_M2 + delta_M3;

    std::vector<double> LV_loci = {0, delta_M2, dist_M3};
    MSG_DEBUG("steps " << steps);
    MSG_DEBUG("LV_loci " << LV_loci);
    MSG_DEBUG("LV");
    MSG_DEBUG(LV);

    auto ret = run_gpc(gen, LV_loci, LV, steps);
    auto collect = experimental_lumper::make_collect(P, ret.data.rows());
    ret.data = collect * ret.data;
    auto pi = P.begin();
    auto pj = P.end();
    for (size_t i = 0; pi != pj; ++i, ++pi) {
        ret.row_labels[i] = ret.row_labels[pi->front()];
    }
    ret.row_labels.resize(P.size());
    return ret;
}


/*
labelled_matrix<MatrixXd, label_type, double>
run_on_segment(const geno_matrix* gen, double delta_M2, double delta_M3, const std::vector<label_type>& obs_M1, const std::vector<label_type>& obs_M2, const std::vector<label_type>& obs_M3, double step)
{
    std::vector<double> LV_loci = {0, delta_M2, delta_M3 + delta_M3};
    auto steps = compute_steps(LV_loci, step, {});
    return run_on_segment(gen, delta_M2, delta_M3, obs_M1, obs_M2, obs_M3, steps);
}
*/


void proba_plot(braille_plot& p, const labelled_matrix<MatrixXd, label_type, double>& LM)
{
    std::vector<double> y;
    struct col { int r, g, b; };
    std::vector<col> palette = {
        {255, 0, 0},
        {0, 255, 0},
        {0, 255, 0},
        {0, 0, 255}
    };
    for (int r = 0; r < LM.data.rows(); ++r) {
        y.clear();
        y.reserve(LM.data.cols());
        for (int c = 0; c < LM.data.cols(); ++c) {
            y.push_back(LM.data(r, c));
        }
        p.plot(LM.column_labels, y, palette[r].r, palette[r].g, palette[r].b);
    }
}


void proba_plot(const labelled_matrix<MatrixXd, label_type, double>& LM)
{
    braille_plot p(300, 50, LM.column_labels.front(), LM.column_labels.back(), -.02, 1.02);
    proba_plot(p, LM);
    p.haxis(0, 0, .5, .1);
    p.vaxis(0, 0, .5, .25);
    MSG_DEBUG("" << p);
}


template <typename T>
    T to(const std::string& s) { std::stringstream ss(s); T x; ss >> x; return x; }

template <typename T>
    std::string to_string(T&& x) { std::stringstream ss; ss << x; return ss.str(); }

struct sim_data_table {
    std::vector<std::string> colnames;
    std::vector<std::string> rownames;
    std::vector<std::vector<double>> data;

    sim_data_table(const std::string& filename)
        : colnames(), rownames(), data()
    {
        std::string line, word;
        ifile in(filename);
        std::getline(in, line);
        std::stringstream header(line);
        /*MSG_DEBUG("HEADER: " << line);*/
        header >> word;
        while (!header.eof()) {
            colnames.emplace_back();
            header >> colnames.back();
            data.emplace_back();
        }
        while (!in.eof()) {
            std::getline(in, line);
            if (line.size() == 0) {
                continue;
            }
            /*MSG_DEBUG("row: " << line);*/
            std::stringstream inl(line);
            rownames.emplace_back();
            inl >> rownames.back();
            for (int i = 0; !inl.eof(); ++i) {
                data[i].emplace_back();
                inl >> data[i].back();
            }
        }
    }

    sim_data_table(const labelled_matrix<MatrixXd, label_type, double>& result)
        : colnames(), rownames(), data()
    {
        for (const auto& l: result.column_labels) { rownames.push_back(to_string(l)); }
        for (const auto& l: result.row_labels) { colnames.push_back(to_string(l)); }
        data.resize(result.data.rows(), std::vector<double>(result.data.cols(), 0));
        for (int c = 0; c < result.data.cols(); ++c) {
            for (int r = 0; r < result.data.rows(); ++r) {
                data[r][c] = result.data(r, c);
            }
        }
    }

    sim_data_table() : colnames(), rownames(), data() {}

    size_t
        find_col(const std::string& name) const
        {
            return std::find(colnames.begin(), colnames.end(), name) - colnames.begin();
        }

    size_t
        find_row(const std::string& name) const
        {
            return std::find(rownames.begin(), rownames.end(), name) - rownames.begin();
        }

    const std::vector<double>&
        col(size_t n) const { return data[n]; }

    std::vector<double>
        row(size_t n) const { std::vector<double> ret; ret.reserve(data.size()); for (const auto& col: data) { ret.push_back(col[n]); } return ret; }

    double
        sample_count() const { auto x = row(0); double accum = 0; for (double i: x) { accum += i; } return accum; }

    size_t rows() const { return data[0].size(); }
    size_t cols() const { return data.size(); }

    sim_data_table
        operator + (const sim_data_table& other) const
        {
            /*MSG_DEBUG("ADDING");*/
            /*MSG_DEBUG("" << other);*/
            /*MSG_DEBUG("TO");*/
            /*MSG_DEBUG("" << (*this));*/
            sim_data_table ret;
            ret.colnames = colnames;
            ret.rownames = rownames;
            ret.data = data;
            for (size_t c = 0; c < data.size(); ++c) {
                for (size_t r = 0; r < data[c].size(); ++r) {
                    ret.data[c][r] += other.data[c][r];
                }
            }
            /*MSG_DEBUG("=");*/
            /*MSG_DEBUG("" << ret);*/
            return ret;
        }

    sim_data_table
        operator - (const sim_data_table& other) const
        {
            /*MSG_DEBUG("ADDING");*/
            /*MSG_DEBUG("" << other);*/
            /*MSG_DEBUG("TO");*/
            /*MSG_DEBUG("" << (*this));*/
            sim_data_table ret;
            ret.colnames = colnames;
            ret.rownames = rownames;
            ret.data = data;
            for (size_t c = 0; c < data.size(); ++c) {
                for (size_t r = 0; r < data[c].size(); ++r) {
                    ret.data[c][r] -= other.data[c][r];
                }
            }
            /*MSG_DEBUG("=");*/
            /*MSG_DEBUG("" << ret);*/
            return ret;
        }

    sim_data_table
        normalized() const
        {
            double factor = 1. / sample_count();
            sim_data_table ret = *this;
            for (auto& col: ret.data) {
                for (auto& d: col) {
                    d *= factor;
                }
            }
            return ret;
        }

    sim_data_table
        operator | (const sim_data_table& other) const
        {
            sim_data_table ret(*this);
            ret.colnames.insert(ret.colnames.end(), other.colnames.begin(), other.colnames.end());
            ret.data.insert(ret.data.end(), other.data.begin(), other.data.end());
            return ret;
        }

    friend
        std::ostream&
        operator << (std::ostream& os, const sim_data_table& sd)
        {
            os << "marker";
            for (const auto& c: sd.colnames) { os << '\t' << c; }
            os << std::endl;
            for (size_t i = 0; i < sd.rownames.size(); ++i) {
                os << sd.rownames[i];
                for (size_t c = 0; c < sd.data.size(); ++c) {
                    os << '\t' << sd.data[c][i];
                }
                os << std::endl;
            }
            return os;
        }
};



#include <glob.h>
#include <regex>

/* taken from http://stackoverflow.com/a/24703135 */
std::vector<std::string>
globVector(const std::string& pattern){
    glob_t glob_result;
    glob(pattern.c_str(),GLOB_TILDE,NULL,&glob_result);
    std::vector<std::string> files;
    for(unsigned int i=0;i<glob_result.gl_pathc;++i){
        files.push_back(std::string(glob_result.gl_pathv[i]));
    }
    globfree(&glob_result);
    return files;
}


struct sim_data_map {
    std::map<int, std::map<int, std::map<std::string, sim_data_table>>> data;
    size_t counts;
    std::set<char> letters;

    sim_data_map(const std::string& base_dir)
        : data(), counts(0)
    {
        auto filelist = globVector(SPELL_STRING(base_dir << "/*/*/*/*.txt"));
        std::regex path_info(".*/counts_([0-9]+)/([0-9]+)/([0-9]+)/([a-zA-Z]{3}).txt", std::regex_constants::extended);
        MSG_DEBUG("Reading " << filelist.size() << " files...");
        for (const auto& path: filelist) {
            std::smatch match;
            if (std::regex_match(path.begin(), path.end(), match, path_info)) {
                /*MSG_DEBUG("MATCH FOUND! " << match[0] << ", " << match[1] << ", " << match[2] << ", " << match[3] << ", " << match[4]);*/
                if (counts == 0) {
                    counts = to<int>(match[1]);
                }
                int dm12 = to<int>(match[2]);
                int dm23 = to<int>(match[3]);
                std::string m123 = match[4];
                data[dm12][dm23].emplace(m123, sim_data_table{path});
                for (char c: m123) { letters.insert(c); }
            }
        }
        MSG_DEBUG("Done reading " << filelist.size() << " files.");
    }

    const sim_data_table&
        get(int m12, int m23, char M1, char M2, char M3) const
        {
            return data.find(m12)->second.find(m23)->second.find(std::string{M1, M2, M3})->second;
        }

    sim_data_table
        get_sums_over_M3(int m12, int m23, char M1, char M2) const
        {
            auto i = letters.begin();
            auto j = letters.end();
            sim_data_table ret = get(m12, m23, M1, M2, *i);
            for (++i; i != j; ++i) {
                ret = ret + get(m12, m23, M1, M2, *i);
            }
            return ret;
        }

    double
        index_to_distance(int i) const
        {
            return r_to_d(.5 * i / counts);
        }

private:
    
    /* ENSURE THAT THIS CODE IS EXACTLY COMPATIBLE WITH THE SIMULATOR CODE */

    inline
        static double r_to_d(double r)
        {
            if (r == .5) {
                r = .4999;
            }
            return -.5 * log(1. - 2. * r);
        }

};


std::vector<double>
simu_steps(size_t n_rows, double d12, double d23)
{
    std::vector<double> X(n_rows + 1);
    for (size_t i = 0; i < n_rows; ++i) {
        X[i] = i * d12 / (n_rows - 1);
    }
    X.back() = X[n_rows] + d23;
    MSG_DEBUG("COMPUTED X WITH n_rows=" << n_rows << " = size(" << X.size() << ") " << X);
    return X;
}

braille_plot
proba_plot(const sim_data_table& table, double d12)
{
    auto X = simu_steps(table.rows() - 1, d12, 0);
    X.pop_back();
    /*MSG_DEBUG("X " << X);*/
    /*MSG_DEBUG("X.size() = " << X.size());*/
    struct rgb { int r, g, b; };
    std::vector<rgb> palette = {
        {255, 0, 0},
        {0, 255, 0},
        {0, 0, 255},
        {255, 0, 0},
        {0, 255, 0},
        {0, 0, 255},
        {255, 128, 128},
        {128, 255, 128},
        {128, 128, 255},
        {255, 255, 0},
        {0, 255, 255},
        {255, 0, 255},
        {255, 255, 128},
        {128, 255, 255},
        {255, 128, 255},
        {255, 255, 255}
    };
    std::vector<std::pair<int, int>> styles = {
        {1, 0},
        {1, 0},
        {1, 0},
        {1, 1},
        {1, 1},
        {1, 1}
    };
    double max = -std::numeric_limits<double>::infinity(), min = std::numeric_limits<double>::infinity();
    for (size_t c = 0; c < table.cols(); ++c) {
        for (size_t r = 0; r < X.size(); ++r) {
            double x = table.col(c)[r];
            if (x > max) {
                max = x;
            }
            if (x < min) {
                min = x;
            }
        }
    }
    double pad_x = (X.back() - X.front()) * .05;
    double pad_y = (max - min) * .05;
    double min_x = X.front() - pad_x;
    double max_x = X.back() + pad_x;
    double min_y = min - pad_y;
    double max_y = max + pad_y;
    MSG_DEBUG("USING TABLE(" << table.rows() << ", " << table.cols() << ')');
    MSG_DEBUG("" << table);
    MSG_DEBUG("xlim=(" << min_x << ", " << max_x << ") ylim=(" << min_y << ", " << max_y << ')');
    braille_plot p(200, 75, X.front() - pad_x, X.back() + pad_x, min - pad_y, max + pad_y);
    for (size_t c = 0; c < table.cols(); ++c) {
        /*MSG_DEBUG("C " << table.col(c));*/
        auto e = table.col(c).end();
        --e;
        MSG_DEBUG("PLOTTING COLUMN #" << c);
        p.plot(X, std::vector<double>(table.col(c).begin(), e), palette[c].r, palette[c].g, palette[c].b, styles[c].first, styles[c].second);
    }
    double xtick = 1;
    while (xtick > X.back()) { xtick *= .1; }
    double ytick = 1;
    while (ytick > max) { ytick *= .1; }
    p.haxis(0, 0, xtick, xtick * .25);
    p.vaxis(0, 0, ytick, ytick * .25);
    MSG_DEBUG("" << p);
    return p;
}




#ifdef TEST_GENO_MATRIX

#define ENABLE if (1)
#define DISABLE if (0)

#if 0
typedef std::map<bn_label_type, double> bn_message_type;
typedef std::map<size_t, bn_message_type> bn_message_buffer_type;


inline
double compute_geno_comb(
        const genotype_comb_type& comb,
        const bn_message_buffer_type& message_buffers)
{
    double accum = 0;
    for (const auto& elem: comb) {
        double partial = elem.coef;
        for (const auto& k: elem.keys) {
            partial *= message_buffers.find(k.parent)->second.find(k.state)->second;
        }
        accum += partial;
    }
    return accum;
}


inline
void compute_factor_message(
        const std::map<bn_label_type, genotype_comb_type>& factor,
        const bn_message_buffer_type& message_buffers, 
        bn_message_type& output)
{
    for (const auto& kv: factor) {
        output[kv.first] = compute_geno_comb(kv.second, message_buffers);
    }
}



typedef std::map<size_t, std::map<bn_label_type, genotype_comb_type>> bn_factor_type;


inline
bn_factor_type
make_factor(const genotype_comb_type& comb)
{
    std::vector<size_t> targets;
    for (const auto & k: comb.begin()->keys) { targets.push_back(k.parent); }
    bn_factor_type ret;
    for (size_t target: targets) {
        /*for (const auto& kv: sum_over_and_normalize(comb, {target})) {*/
        for (const auto& kv: sum_over(comb, {target})) {
            ret[target][kv.first.keys[0].state] = kv.second;
        }
    }
    return ret;
}


double normalize_message(bn_message_type& msg)
{
    double accum = 0;
    for (const auto& kv: msg) { accum += kv.second; }
    double inv = 1. / accum;
    for (auto& kv: msg) { kv.second *= inv; }
    return accum;
}
#endif


void
apply_evidence(factor_graph::instance_type& fgi, const std::vector<std::pair<size_t, std::vector<std::pair<std::string, double>>>>& obs)
{
    for (const auto& kv: obs) {
        size_t ind = kv.first;
        for (const auto& lab_prob: kv.second) {
            auto l = lab_prob.first;
            bn_label_type lab(l[0], l[2], l[1] - '0', l[3] - '0');
            fgi.evidence(ind, lab, lab_prob.second);
        }
    }
}



int main(int argc, char** argv)
{
#if 0
    label_type g = {'G', 'H'};
    label_type h = {'h', GAMETE_EMPTY};
    label_type gam1 = {GAMETE_L, GAMETE_EMPTY};
    label_type gam2 = {GAMETE_R, GAMETE_EMPTY};
    label_type d1 = {GAMETE_L, GAMETE_L};
    label_type d2 = {GAMETE_R, GAMETE_R};

    MSG_DEBUG("" << (h * h));
    MSG_DEBUG("" << (g * gam1));
    MSG_DEBUG("" << (g * gam2));
    MSG_DEBUG("" << (g * d1));
    MSG_DEBUG("" << (g * d2));
    MSG_DEBUG("" << (g * g));

    /*return 0;*/
#endif

    int arg;
    {
        std::stringstream iss(argv[1]);
        iss >> arg;
    }

    auto test_case = [&] () { bool ret = arg & 1; arg >>= 1; return ret; };

    /* 1 */
    if (test_case())
    {
        pedigree_type ped;
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("A");
        size_t A = ped.ancestor("A");
        MSG_DEBUG((*ped.get_gen(A)));
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("B");
        size_t B = ped.ancestor("B");
        MSG_DEBUG((*ped.get_gen(B)));
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("F1");
        size_t F1 = ped.crossing("F1", A, B);
        MSG_DEBUG((*ped.get_gen(F1)));
        size_t BCR = ped.crossing("BCR", F1, A); (void) BCR;
        size_t BCL = ped.crossing("BCL", A, F1); (void) BCL;

        factor_graph fg(ped, 2);
        auto I = fg.instance();
        MSG_DEBUG("" << fg);
        bn_message_type marginals;
        double delta = I.run(marginals);
        MSG_DEBUG("Marginal probabilities:");
        MSG_DEBUG("" << marginals.dump());
    }

    /* 2 */
    if (test_case())
    {
        /* TODO lumper
         *
         * - il faut sélectionner les splits à faire au lieu d'essayer de
         *   faire des joins une fois qu'il est trop tard
         * - peut-être que l'intérêt d'un split est mesuré par la distance de la / des colonne(s)
         *   au reste de son/leur (futur ex) groupe
         */
        pedigree_type ped;
        /*ped.n_alleles = 2;*/
        /*ped.max_states = 4;*/
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("A");
        size_t A = ped.ancestor("A");
        MSG_DEBUG((*ped.get_gen(A)));
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("B");
        size_t B = ped.ancestor("B");
        MSG_DEBUG((*ped.get_gen(B)));
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("F1");
        size_t F1 = ped.crossing("F1", A, B);
        /*MSG_DEBUG((*ped.get_gen(F1)));*/

        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("F2");
        size_t F2 = ped.selfing("F2", F1);
        /*MSG_DEBUG((*ped.get_gen(F2)));*/

        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("F3");
        size_t F3 = ped.selfing("F3", F2); (void) F3;
        /*MSG_DEBUG((*ped.get_gen(F3)));*/
 
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("F4");
        size_t F4 = ped.selfing("F4", F3);
        /*MSG_DEBUG((*ped.get_gen(F4)));*/
 
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("F5");
        size_t F5 = ped.selfing("F5", F4);
 
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("F6");
        size_t F6 = ped.selfing("F6", F5); (void) F6;
        size_t F6_2 = ped.selfing("F6", F5); (void) F6_2;
 
        {
            factor_graph fg(ped, 2);
            auto I = fg.instance();
            MSG_DEBUG("" << fg);
            bn_message_type marginals;
            I.clear_evidence();
            /*apply_evidence(I, {*/
            /*{ped.tree.ind2node(F4), {*/
            /*{"a0a0", 1},*/
            /*{"b0a0", 0},*/
            /*{"a0b0", 0},*/
            /*{"b0b0", 0},*/
            /*{"b1a0", 0},*/
            /*{"a1b0", 0},*/
            /*{"b0a1", 0},*/
            /*{"a0b1", 0},*/
            /*{"a1a1", 0},*/
            /*{"b1a1", 0},*/
            /*{"a1b1", 0},*/
            /*{"b1b1", 0}*/
            /*}*/
            /*}});*/
            double delta = I.run(marginals);
            MSG_DEBUG("Marginal probabilities:");
            MSG_DEBUG("" << marginals.dump());
        }

#if 1
        {
            /*factor_graph fg(ped, 2);*/
            factor_graph fg(ped, 1);
            MSG_DEBUG("" << fg);
            bn_message_type marginals;
            /*fg.clear_evidence()*/
            /*.evidence(ped.tree.ind2node(F6), {'a', 'a', 0, 0}, 0)*/
            /*.evidence(ped.tree.ind2node(F6), {'a', 'b', 0, 0}, .5)*/
            /*.evidence(ped.tree.ind2node(F6), {'b', 'a', 0, 0}, .5)*/
            /*.evidence(ped.tree.ind2node(F6), {'b', 'b', 0, 0}, 0);*/
            /*double delta = fg.run(marginals); (void) delta;*/
            /*MSG_DEBUG("Marginal probabilities:");*/
            /*MSG_DEBUG("" << marginals);*/
            auto I = fg.instance();
            I.clear_evidence();
            apply_evidence(I, {
                    {ped.tree.ind2node(F4), {
                    {"a0a0", 1},
                    {"b0a0", 0},
                    {"a0b0", 0},
                    {"b0b0", 0},
                    /*{"b1a0", 0},*/
                    /*{"a1b0", 0},*/
                    /*{"b0a1", 0},*/
                    /*{"a0b1", 0},*/
                    /*{"a1a1", 0},*/
                    /*{"b1a1", 0},*/
                    /*{"a1b1", 0},*/
                    /*{"b1b1", 0}*/
                    }
                    }});
            I.run(marginals);
            MSG_DEBUG("I Marginal probabilities:");
            MSG_DEBUG("" << marginals.dump());
            auto Ii = fg.instance();
            Ii.clear_evidence();
            marginals.clear();
            Ii.run(marginals);
            MSG_DEBUG("Ii Marginal probabilities:");
            MSG_DEBUG("" << marginals.dump());

            factor_graph fg2;
            fg.save("test.factorgraph-n2");
            fg2.load("test.factorgraph-n2");
            MSG_DEBUG("" << fg2);
            marginals.clear();
            auto I2 = fg2.instance();
            I2.clear_evidence();
            I2.run(marginals);
            MSG_DEBUG("I2 Marginal probabilities:");
            MSG_DEBUG("" << marginals.dump());
        }
#endif
#if 0
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("F4");
        size_t F4 = ped.selfing("F4", F3);
        /*MSG_DEBUG((*ped.get_gen(F4)));*/
 
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("F5");
        size_t F5 = ped.selfing("F5", F4);
 
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("F6");
        size_t F6 = ped.selfing("F6", F5);
 
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("F7");
        size_t F7 = ped.selfing("F7", F6);
 
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("F8");
        size_t F8 = ped.selfing("F8", F7);
 
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("F9");
        size_t F9 = ped.selfing("F9", F8); (void) F9;
        int D12 = 10;
        int D23 = 1;
        /*MSG_DEBUG("test F3 vs old impl");*/
        /*geno_matrix old_F3 = lump(~~(*ped.get_gen(A) | *ped.get_gen(B)));*/
        /*MSG_DEBUG((old_F3.inf_mat - ped.get_gen(F3)->inf_mat));*/

        sim_data_map simu("simulation/gen_RIL");
        /*MSG_DEBUG("" << simu.get_sums_over_M3(1, 5, 'A', 'B'));*/
        /*MSG_DEBUG("" << simu.get_sums_over_M3(1, 5, 'B', 'A'));*/
        /*MSG_DEBUG("" << simu.get_sums_over_M3(1, 5, 'A', 'A'));*/
        /*MSG_DEBUG("" << simu.get_sums_over_M3(1, 5, 'B', 'B'));*/
        /*MSG_DEBUG("" << simu.get_sums_over_M3(1, 5, 'A', 'H'));*/
        auto AB_15 = simu.get_sums_over_M3(D12, D23, 'A', 'B').normalized();
        MSG_DEBUG("AB_15" << std::endl << AB_15);
        auto ABA_15 = simu.get(D12, D23, 'A', 'B', 'A').normalized() - AB_15;
        MSG_DEBUG("ABA_15" << std::endl << ABA_15);
        /*auto plot = proba_plot(ABA_15, simu.index_to_distance(D12));*/

        auto X = simu_steps(ABA_15.rows() - 1, simu.index_to_distance(D12), simu.index_to_distance(D23));
        std::vector<label_type>
            obsA = {{'a', 'a'}},
            obsB = {{'b', 'b'}},
            obsH = {{'a', 'b'}, {'b', 'a'}},
            obsU = {{'a', 'a'}, {'a', 'b'}, {'b', 'a'}, {'b', 'b'}};
        auto result = run_on_segment(ped.get_gen(F9).get(), simu.index_to_distance(D12), simu.index_to_distance(D23), obsA, obsB, obsA, X, std::set<subset>{{0}, {1, 2}, {3}});
        MSG_DEBUG(result);
        sim_data_table result_table(result);
        MSG_DEBUG(result_table);
        /*proba_plot(plot, result);*/
        auto full_comp = ABA_15 | (result_table - AB_15);
        MSG_DEBUG(full_comp);
        proba_plot(full_comp, simu.index_to_distance(D12));
        proba_plot(simu.get(D12, D23, 'A', 'B', 'A').normalized() | result_table, simu.index_to_distance(D12));
        /*proba_plot(result_table, simu.index_to_distance(D12));*/
        /*MSG_DEBUG(plot);*/
#endif
    }

    /* 4 */
    if (test_case())
    {
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("                            OUTBRED");
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("#####################################################################");
        pedigree_type ped;
        /*ped.n_alleles = 2;*/
        MSG_DEBUG("A");
        size_t A = ped.ancestor("A");
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("B");
        size_t B = ped.ancestor("B");
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("C");
        size_t C = ped.ancestor("C");
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("D");
        size_t D = ped.ancestor("D");
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("F1_AB");
        size_t F1_AB = ped.crossing("F1_AB", A, B);
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("F1_CD");
        size_t F1_CD = ped.crossing("F1_CD", C, D);

        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("CP");
        size_t CP = ped.crossing("CP", F1_AB, F1_CD);

        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("F2_CP");
        size_t F2_CP = ped.selfing("F2_CP", CP);
        (void) F2_CP;

#if 0
        ped.save("test_pedigree_save.dat");
        pedigree_type ped2;
        ped2.load("test_pedigree_save.dat");
        MSG_DEBUG("tree eq: " << (ped2.tree == ped.tree));
        MSG_DEBUG("node_generations eq: " << (ped2.node_generations == ped.node_generations));
        for (size_t i = 1; i < ped2.generations.size(); ++i) {
            MSG_DEBUG("generation#" << i << ".name eq: " << (ped2.generations[i]->name == ped.generations[i]->name));
            MSG_DEBUG("generation#" << i << ".inf_mat eq: " << (ped2.generations[i]->inf_mat == ped.generations[i]->inf_mat));
            MSG_DEBUG("generation#" << i << ".p eq: " << (ped2.generations[i]->p == ped.generations[i]->p));
            MSG_DEBUG("generation#" << i << ".p_inv eq: " << (ped2.generations[i]->p_inv == ped.generations[i]->p_inv));
            MSG_DEBUG("generation#" << i << ".diag eq: " << (ped2.generations[i]->diag == ped.generations[i]->diag));
            MSG_DEBUG("generation#" << i << ".stat_dist eq: " << (ped2.generations[i]->stat_dist == ped.generations[i]->stat_dist));
        }
        MSG_DEBUG("ancestor_letters eq: " << (ped2.ancestor_letters == ped.ancestor_letters));
        MSG_DEBUG("generation_names eq: " << (ped2.generation_names == ped.generation_names));
        MSG_DEBUG("individuals_by_generation_name eq: " << (ped2.individuals_by_generation_name == ped.individuals_by_generation_name));
        MSG_DEBUG("max_states eq: " << (ped2.max_states == ped.max_states));
        MSG_DEBUG("n_alleles eq: " << (ped2.n_alleles == ped.n_alleles));
        MSG_DEBUG("filename eq: " << (ped2.filename == ped.filename));
        MSG_DEBUG("LC eq: " << (ped2.LC == ped.LC));
        MSG_DEBUG("factor_messages eq: " << (ped2.factor_messages == ped.factor_messages));
        MSG_QUEUE_FLUSH();
#endif

        factor_graph fg(ped, 2);
        MSG_DEBUG("" << fg);
        bn_message_type marginals;
        /*fg.evidence(ped.tree.ind2node(CP), {'a', 'c', 0, 0}, .5);*/
        /*fg.evidence(ped.tree.ind2node(CP), {'b', 'c', 0, 0}, .5);*/
        /*fg.evidence(ped.tree.ind2node(CP), {'a', 'd', 0, 0}, 0);*/
        /*fg.evidence(ped.tree.ind2node(CP), {'b', 'd', 0, 0}, 0);*/
        double delta = fg.instance().run(marginals); (void) delta;
        MSG_DEBUG("Marginal probabilities:");
        MSG_DEBUG("" << marginals.dump());
    }

    /* 8 */
    if (test_case())
    {
        MSG_DEBUG("");
        MSG_DEBUG("*");
        MSG_DEBUG("**");
        MSG_DEBUG("*");
        MSG_DEBUG("");

        pedigree_type ped;
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("A");
        size_t A = ped.ancestor("A");
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("B");
        size_t B = ped.ancestor("B");
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("C");
        size_t C = ped.ancestor("C");
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("D");
        size_t D = ped.crossing("D", A, B);
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("E");
        size_t E = ped.crossing("E", B, C);
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("F");
        size_t F = ped.crossing("F", D, E);
        MSG_DEBUG("#####################################################################");
        MSG_DEBUG("G");
        size_t G = ped.crossing("G", F, E);

        /*MSG_DEBUG("prout");*/
        /*MSG_DEBUG(ped.get_gen(G)->lim_inf());*/

        factor_graph fg(ped, 1);
        MSG_DEBUG("" << fg);
        bn_message_type marginals;
        double delta = fg.instance().run(marginals); (void) delta;
        MSG_DEBUG("Marginal probabilities:");
        MSG_DEBUG("" << marginals.dump());
    }

    /* 16 */
    if (test_case())
    {
        pedigree_type ped;
        size_t A = ped.ancestor("A");
        size_t B = ped.ancestor("B");
        size_t C = ped.ancestor("C");
        size_t AB = ped.crossing("AB", A, B);
        size_t BC = ped.crossing("BC", B, C);
        size_t F1 = ped.crossing("F1", AB, BC);
        size_t F2 = ped.selfing("F2", F1); (void) F2;
    }

    /* 32 */
    if (test_case())
    {
        pedigree_type ped;
        /*ped.max_states = 32;*/
        /*ped.max_states = 4;*/
        /*ped.n_alleles = 2;*/
        size_t A = ped.ancestor("A");
        size_t B = ped.ancestor("B");
        size_t F1_1 = ped.crossing("F1", A, B);
        size_t F1_2 = ped.crossing("F1", A, B);
        size_t F2_1 = ped.crossing("F2", F1_1, F1_2);
        size_t F2_2 = ped.crossing("F2", F1_1, F1_2);
        MSG_DEBUG("F3_1");
        size_t F3_1 = ped.crossing("F3", F2_1, F2_2); (void) F3_1;
        size_t F3_2 = ped.crossing("F3", F2_1, F2_2);
        MSG_DEBUG("F4s");
        size_t F4_1 = ped.crossing("F4", F3_1, F3_2); (void) F4_1;
        size_t F4_2 = ped.crossing("F4", F3_1, F3_2); (void) F4_2;
        /*MSG_DEBUG("F5s");*/
        /*size_t F5_1 = ped.crossing("F5", F4_1, F4_2); (void) F5_1;*/
        /*size_t F5_2 = ped.crossing("F5", F4_1, F4_2); (void) F5_2;*/
        /*size_t F5_3 = ped.crossing("F5", F4_1, F4_2); (void) F5_3;*/

        factor_graph fg(ped, 2);
        MSG_DEBUG("" << fg);
        bn_message_type marginals;
        double delta = fg.instance().run(marginals);
        MSG_DEBUG("Marginal probabilities:");
        MSG_DEBUG("" << marginals.dump());
    }

    /* 64 */
    if (test_case())
    {
        pedigree_type ped;
        size_t A = ped.ancestor("A");
        size_t B = ped.ancestor("B");
        size_t C = ped.ancestor("C");
        size_t D = ped.ancestor("D");
        size_t E = ped.ancestor("E");
        size_t F = ped.ancestor("F");
        size_t AB = ped.crossing("AB", A, B);
        size_t EF = ped.crossing("EF", E, F);
        size_t ABC = ped.crossing("ABC", AB, C);
        size_t DEF = ped.crossing("DEF", D, EF);
        size_t X = ped.crossing("X", ABC, DEF);
        size_t S = ped.selfing("S", X); (void) S;
        size_t S1 = ped.selfing("S1", S); (void) S1;
    }

    /* 128 */
    if (test_case())
    {
        pedigree_type ped;
        size_t A = ped.ancestor("A");
        size_t B = ped.ancestor("B");
        size_t C = ped.ancestor("C");
        size_t D = ped.ancestor("D");
        size_t E = ped.ancestor("E");
        size_t F = ped.ancestor("F");
        size_t G = ped.ancestor("G");
        size_t H = ped.ancestor("H");
        size_t AB = ped.crossing("AB", A, B);
        size_t CD = ped.crossing("CD", C, D);
        size_t EF = ped.crossing("EF", E, F);
        size_t GH = ped.crossing("GH", G, H);
        size_t L = ped.crossing("L", AB, CD);
        size_t R = ped.crossing("R", EF, GH);
        size_t X = ped.crossing("X", L, R); (void) X;
        size_t S = ped.selfing("S", X); (void) S;
    }

    /* 256 */
    if (test_case())
    {
        pedigree_type ped;
        ped.n_alleles = 2;
        size_t A = ped.ancestor("A");
        size_t B = ped.ancestor("B");
        size_t C = ped.ancestor("C");
        size_t D = ped.ancestor("D");
        size_t E = ped.ancestor("E");
        size_t F = ped.ancestor("F");
        size_t G = ped.ancestor("G");
        size_t AB = ped.crossing("AB", A, B);
        size_t CD = ped.crossing("CD", C, D);
        size_t EF = ped.crossing("EF", E, F);
        size_t GH = ped.crossing("GH", G, A);
        size_t L = ped.crossing("L", AB, CD);
        size_t R = ped.crossing("R", EF, GH);
        size_t X = ped.crossing("X", L, R); (void) X;
        /*size_t S = ped.selfing("S", X); (void) S;*/
    }

    /* 512 */
    if (test_case())
    {
        pedigree_type ped;
        size_t A = ped.ancestor("A");
        size_t B = ped.ancestor("B");
        size_t C = ped.ancestor("C");
        size_t D = ped.ancestor("D");
        size_t E = ped.ancestor("E");
        size_t F = ped.ancestor("F");
        size_t G = ped.ancestor("G");
        size_t AB = ped.crossing("AB", A, B);
        size_t CD = ped.crossing("CD", C, D);
        size_t EF = ped.crossing("EF", A, G);
        size_t GH = ped.crossing("GH", F, E);
        size_t L = ped.crossing("L", AB, CD);
        size_t R = ped.crossing("R", EF, GH);
        size_t X = ped.crossing("X", L, R); (void) X;
        size_t S = ped.selfing("S", X); (void) S;
    }

    /* 1024 */
    if (test_case())
    {
        pedigree_type ped;
        ped.n_alleles = 2;
        size_t A = ped.ancestor("A");
        size_t B = ped.ancestor("B");
        size_t C = ped.ancestor("C");
        size_t D = ped.ancestor("D");
        size_t AB = ped.crossing("AB", A, B);
        /*size_t CD = ped.crossing("CD", C, A);*/
        size_t CD = ped.crossing("CD", C, D);
        size_t X = ped.crossing("X", AB, CD); (void) X;
        size_t S = ped.selfing("S", X); (void) S;
    }

    /* 2048 */
    if (test_case())
    {
        pedigree_type ped;
        size_t A = ped.ancestor("A");
        size_t B = ped.ancestor("B");
        size_t C = ped.ancestor("C");
        size_t D = ped.ancestor("D");
        size_t AB = ped.crossing("AB", A, B);
        size_t CD = ped.crossing("CD", C, D);
        size_t BCa = ped.crossing("BCa", A, AB);
        /*size_t BCd = ped.crossing("BCd", CD, D);*/
        size_t BCd = ped.crossing("BCd", D, CD);
        size_t X = ped.crossing("X", BCa, BCd); (void) X;
        size_t S = ped.selfing("S", X); (void) S;
    }

    /* 4096 */
    if (test_case())
    {
        pedigree_type ped;
        size_t A = ped.ancestor("A");
        size_t B = ped.ancestor("B");
        /*size_t C = ped.ancestor("C");*/
        /*size_t D = ped.ancestor("D");*/
        size_t AB = ped.crossing("AB", A, B);
        /*size_t CD = ped.crossing("CD", C, D);*/
        size_t BCa = ped.crossing("BCa", A, AB);
        size_t BCd = ped.crossing("BCd", AB, B);
        size_t X = ped.crossing("X", BCa, BCd); (void) X;
        size_t S = ped.selfing("S", X); (void) S;
    }

    /* 8192 */
    if (test_case())
    {
        auto A = ancestor_matrix("A", 'a');
        auto B = ancestor_matrix("B", 'b');
        auto F1 = A | B;
        auto F2 = ~F1;
        MSG_DEBUG("= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =");
        MSG_DEBUG(F1);
        MSG_DEBUG("= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =");
        MSG_DEBUG(F2);
        MSG_DEBUG("= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =");
        MSG_DEBUG("GAMETES");
        MSG_DEBUG("= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =");
        MSG_DEBUG(kronecker(A, gamete));
        MSG_DEBUG("= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =");
        MSG_DEBUG(kronecker(F1, gamete));
    }

    /* 16384 */
    if (test_case())
    {
        pedigree_type ped;
        size_t A = ped.ancestor("A");
        size_t B = ped.ancestor("B");
        size_t C = ped.ancestor("C");
        size_t D = ped.ancestor("D");
        size_t E = ped.ancestor("E");
        size_t F = ped.ancestor("F");
        size_t G = ped.ancestor("G");
        size_t H = ped.ancestor("H");

        size_t AB = ped.crossing("AB", A, B);
        size_t ABC = ped.crossing("ABC", AB, C);
        size_t ABCD = ped.crossing("ABCD", ABC, D);
        size_t EF = ped.crossing("EF", E, F);
        size_t EFG = ped.crossing("EFG", G, EF);
        size_t EFGH = ped.crossing("EFGH", H, EFG);
        size_t X = ped.crossing("X", ABCD, EFGH); (void) X;
        size_t Y = ped.selfing("Y", X); (void) Y;
    }

    /* 32768 */
    if (test_case())
    {
        pedigree_type ped;
        size_t A = ped.ancestor("A");
        size_t B = ped.ancestor("B");
        size_t C = ped.ancestor("C");
        size_t D = ped.ancestor("D");
        size_t D2 = ped.ancestor("D2");
        size_t E = ped.ancestor("E");
        size_t F = ped.ancestor("F");
        size_t G = ped.ancestor("G");
        size_t H = ped.ancestor("H");
        size_t H2 = ped.ancestor("H2");

        size_t AB = ped.crossing("AB", A, B);
        size_t ABC = ped.crossing("ABC", AB, C);
        size_t ABCD = ped.crossing("ABCD", ABC, D);
        ABCD = ped.crossing("ABCD", ABCD, D2);
        size_t EF = ped.crossing("EF", E, F);
        size_t EFG = ped.crossing("EFG", G, EF);
        size_t EFGH = ped.crossing("EFGH", H, EFG);
        EFGH = ped.crossing("EFGH", H2, EFGH);
        size_t X = ped.crossing("X", ABCD, EFGH); (void) X;
        size_t Y = ped.selfing("Y", X); (void) Y;
    }

    /* 65536 */
    if (test_case())
    {
        pedigree_type ped;
        size_t A = ped.ancestor("A");
        size_t B = ped.ancestor("B");
        size_t F1 = ped.crossing("F1", A, B);
        size_t BC1 = ped.crossing("BC", A, F1);
        size_t BC2 = ped.crossing("BC2", A, BC1);
        size_t BC3_1 = ped.crossing("BC3", A, BC2);
        size_t BC3_2 = ped.crossing("BC3", A, BC2);
        size_t BC3_3 = ped.crossing("BC3", A, BC2);

        factor_graph fg(ped, 1);
        MSG_DEBUG("" << fg);
        bn_message_type marginals;
        auto I = fg.instance();
        I.clear_evidence()
         .evidence(ped.tree.ind2node(BC3_1), {'a', 'a', 0, 0}, 0)
         .evidence(ped.tree.ind2node(BC3_1), {'a', 'b', 0, 0}, 1)
         ;
        double delta = I.run(marginals); (void) delta;
        MSG_DEBUG("Marginal probabilities:");
        MSG_DEBUG("" << marginals.dump());
#if 0
        geno_matrix A = ancestor_matrix("A", 'a');
        geno_matrix B = ancestor_matrix("B", 'b');
        geno_matrix F1 = lump(A|B);
        geno_matrix F2_1 = lump(F1|F1);
        geno_matrix F2_2 = F2_1;
        geno_matrix ones_4;
        ones_4.variant = Geno;
        ones_4.stat_dist = (VectorXd(4) << .25, .25, .25, .25).finished();
        ones_4.inf_mat = MatrixXd::Ones(4, 4);
        ones_4.labels = F2_1.labels;
        ones_4.collect = ones_4.dispatch = MatrixXd::Identity(4, 4);
        ones_4 = lump_using_partition_weighted(ones_4, {{0}, {1}, {2}, {3}});
        MSG_DEBUG("" << ones_4);
        geno_matrix F3_1 = lump(F2_1 | ones_4);
        geno_matrix F3_2 = lump(F2_2 | ones_4);
#endif
    }

    /* 131072 */
    if (test_case())
    {
        pedigree_type ped;
        size_t A = ped.ancestor("A");
        size_t B = ped.ancestor("B");
        size_t C = ped.ancestor("C");
        size_t D = ped.ancestor("C");
        size_t E = ped.ancestor("C");

        size_t AB = ped.crossing("AB", A, B);
        size_t BC = ped.crossing("BC", B, C);
        size_t ABD = ped.crossing("ABD", D, AB);
        size_t BCE = ped.crossing("BCE", BC, E);
        size_t F1 = ped.crossing("F", ABD, BCE);
        size_t F2 = ped.crossing("F", ABD, BCE);

        factor_graph fg(ped, 2);
        auto I = fg.instance();
        MSG_DEBUG("" << fg);
        I.clear_evidence();
        apply_evidence(I, {
            {ped.tree.ind2node(F1), {
                {"a0b0", 0},
                {"b0b0", 1},
                {"d0b0", 0},
                {"a0c0", 0},
                {"b0c0", 0},
                {"d0c0", 0},
                {"a0e0", 0},
                {"b0e0", 0},
                {"d0e0", 0},
                {"a1b0", 0},
                {"d1b0", 0},
                {"a1c0", 0},
                {"b1c0", 0},
                {"d1c0", 0},
                {"a1e0", 0},
                {"b1e0", 0},
                {"d1e0", 0},
                {"a0b1", 0},
                {"d0b1", 0},
                {"a0c1", 0},
                {"b0c1", 0},
                {"d0c1", 0},
                {"a0e1", 0},
                {"b0e1", 0},
                {"d0e1", 0},
                {"a1b1", 0},
                {"b1b1", 0},
                {"d1b1", 0},
                {"a1c1", 0},
                {"b1c1", 0},
                {"d1c1", 0},
                {"a1e1", 0},
                {"b1e1", 0},
                {"d1e1", 0}
            }}
        });
        bn_message_type marginals;
        double delta = I.run(marginals); (void) delta;
        MSG_DEBUG("Marginal probabilities (converged to " << delta << "):");
        MSG_DEBUG(marginals.dump());
#if 0
        size_t D = ped.ancestor("D");
        size_t AB = ped.crossing("AB", A, B);
        size_t CD = ped.crossing("CD", C, D);
        size_t F2_1 = ped.crossing("F2_1", AB, CD);
        size_t F2_2 = ped.crossing("F2_2", AB, CD);
        size_t F2_rev = ped.crossing("F2_rev", CD, AB);
        size_t F3_1 = ped.crossing("F3_1", F2_1, F2_2);
        size_t F3_rev = ped.crossing("F3_rev", F2_1, F2_rev);
        auto g1 = ped.get_gen("g1", F3_1);
        auto g2 = ped.get_gen("g2", F3_rev);
        /*size_t sz = ped.get_gen(F2_1)->cols("sz") * 2;*/
        MatrixXb prout_1 = MatrixXb::Identity(8, 8);
        MatrixXb prout_2 = generate_lozenge(2, 2);
        MatrixXb prout_3 = MatrixXb::Ones(2, 2) - MatrixXb::Identity(2, 2);
        MatrixXb sub = kroneckerProduct(prout_1, prout_2);
        MatrixXb lz = kroneckerProduct(sub, prout_3);
        MSG_DEBUG("--");
        MSG_DEBUG("Compare g1->inf_mat and g2->inf_mat: " << std::boolalpha << (g1->inf_mat - g2->inf_mat).isZero());
        /*MSG_DEBUG((g1->inf_mat - lz * g2->inf_mat * lz.transpose()));*/
        MSG_DEBUG("Compare g1->collect and g2->collect: " << std::boolalpha << (g1->collect - g2->collect).isZero());
        lz = g1->collect.cast<bool>() * lz * g1->collect.cast<bool>().transpose();
        MatrixXd im = lz.cast<double>().transpose() * g2->inf_mat * lz.cast<double>();
        MSG_DEBUG("Compare g1->inf_mat and lz o g2->inf_mat: " << std::boolalpha << (g1->inf_mat - im).isZero());
        MSG_DEBUG(('.' + (('@' - '.') * lz.array().cast<char>())));
        std::vector<label_type> g2l(g2->labels.size());
        for (int i = 0; i < lz.cols(); ++i) {
            int r;
            lz.col(i).maxCoeff(&r);
            g2l[r] = g2->labels[i];
        }
        MSG_DEBUG("Compare labels: " << std::boolalpha << (g2l == g1->labels));
        MSG_DEBUG("Compare (wrong) labels: " << std::boolalpha << (g2->labels == g1->labels));
#endif
    }

    /* 262144 */
    if (test_case())
    {
        pedigree_type ped;
        size_t A = ped.ancestor("A");
        size_t B = ped.ancestor("B");
        size_t C = ped.ancestor("C");
        size_t D = ped.ancestor("D");
        size_t E = ped.ancestor("E");
        size_t F = ped.ancestor("F");
        size_t AB = ped.crossing("AB", A, B);
        size_t CD = ped.crossing("CD", C, D);
        size_t EF = ped.crossing("EF", E, F);
        size_t BCa = ped.crossing("BCa", A, AB);
        size_t BCc = ped.crossing("BCc", C, CD);
        size_t BCf = ped.crossing("BCf", EF, F);
        size_t F1_1 = ped.crossing("F1_1", BCa, BCc);
        size_t F1_2 = ped.crossing("F1_2", BCc, BCf);
        size_t F2 = ped.crossing("F2", F1_1, F1_2); (void) F2;

        factor_graph fg(ped, 1);
        auto I = fg.instance();
        MSG_DEBUG("" << fg);
        bn_message_type marginals;
        double delta = I.run(marginals);
        MSG_DEBUG("Marginal probabilities:");
        MSG_DEBUG("" << marginals);
    }

    if (0)
    {
        /*MatrixXd inf_mat(4, 4);*/
        /*inf_mat << */
            /*-2,  1,  1,  0,*/
             /*1, -2,  0,  1,*/
             /*1,  0, -2,  1,*/
             /*0,  1,  1, -2;*/
        /*std::vector<label_type> labels = {{{'a'}, {'c'}}, {{'a'}, {'d'}}, {{'b'}, {'c'}}, {{'b'}, {'d'}}};*/
        MSG_DEBUG("A");
        auto A = ancestor_matrix("A", 'a');
        MSG_DEBUG("B");
        auto B = ancestor_matrix("B", 'b');
        MSG_DEBUG("F1");
        auto F1 = A | B;
        MSG_DEBUG("F2");
        auto F2 = ~F1;
        MSG_DEBUG("TEST GROUP F1");
        MSG_DEBUG(F1);
        test_group(F1.inf_mat, F1.labels);
        MSG_DEBUG("TEST GROUP F2");
        MSG_DEBUG(F2);
        test_group(F2.inf_mat, F2.labels);

        auto C = ancestor_matrix("C", 'c');
        auto D = ancestor_matrix("D", 'd');
        auto CD = C | D;
        auto CP = F1 | CD;
        MSG_DEBUG("TEST GROUP CP");
        test_group(CP.inf_mat, CP.labels);
    }

    if (0)
    {
        permutation_type p{0, 2, 1};
        symmetry_mask_type s;
        MSG_DEBUG("");
        MSG_DEBUG(symmetry_mask_type::build(1, 1, p).as_matrix());
        MSG_DEBUG("");
        MSG_DEBUG(symmetry_mask_type::build(1, 2, p).as_matrix());
        MSG_DEBUG("");
        MSG_DEBUG(symmetry_mask_type::build(2, 1, p).as_matrix());
        MSG_DEBUG("");
        MSG_DEBUG(symmetry_mask_type::build(2, 2, p).as_matrix());
    }

    if (0)
    {
        permutation_type p1{1, 2, 0, 3};
        permutation_type p2{1, 0};
        symmetry_mask_type sm1 = symmetry_mask_type::build(1, 2, p1);
        MSG_DEBUG("sm1");
        MSG_DEBUG(sm1.as_matrix());
        MSG_DEBUG(sm1);
        symmetry_mask_type sm2 = symmetry_mask_type::build(4, 1, p2);
        MSG_DEBUG("sm2");
        MSG_DEBUG(sm2.as_matrix());
        MSG_DEBUG(sm2);
        MSG_DEBUG("sm1 & sm2");
        symmetry_mask_type result = sm1 & sm2;
        MSG_DEBUG(result.as_matrix());
        MSG_DEBUG(result);
    }

    if (0)
    {
        pedigree_type ped;
        /*ped.max_states = 4;*/
        /*ped.n_alleles = 2;*/
        size_t A = ped.ancestor("A");
        size_t B = ped.ancestor("B");
        size_t F1_1 = ped.crossing("F1_1", A, B);
        size_t F1_2 = ped.crossing("F1_2", A, B);
        size_t F2_1 = ped.crossing("F2_1", F1_1, F1_2);
        size_t F2_2 = ped.crossing("F2_2", F1_1, F1_2);
        MSG_DEBUG("F3_1");
        size_t F3_1 = ped.crossing("F3_1", F2_1, F2_2);
        size_t F3_2 = ped.crossing("F3_2", F2_1, F2_2);
        MSG_DEBUG("F4s");
        size_t F4_1 = ped.crossing("F4_1", F3_1, F3_2); (void) F4_1;
        /*size_t F4_2 = ped.crossing("F4_2", F3_1, F3_2); (void) F4_2;*/
        /*MSG_DEBUG("F5s");*/
        /*size_t F5_1 = ped.crossing("F5_1", F4_1, F4_2); (void) F5_1;*/
        std::vector<size_t> sizes1 = {4, 2, 4, 2, 2, 4, 2, 4, 2, 2};
        std::vector<bool> reent1 =   {0, 0, 0, 0, 0, 1, 0, 1, 0, 0};
        std::vector<size_t> sizes2 = {2, 2, 2, 2, 2, 2, 2, 2};
        std::vector<bool> reent2 =   {0, 0, 0, 0, 0, 0, 0, 0};
        /*symmetry_group_type sF2(ped.get_gen(F2_1)->labels), sF4(ped.get_gen(F4_1)->labels);*/
        symmetry_group_type sF2, sF4(ped.get_gen(F4_1)->labels);
        /*sF2.insert({{{'a', 'b'}, {'b', 'a'}}, {3, 2, 1, 0}});*/
        sF2.insert(symmetry_table_type::build(permutation_type{3, 2, 1, 0}, ped.get_gen(F2_1)->labels, ped.get_gen(F2_1)->inf_mat, false).second);
        MSG_DEBUG("sF2");
        MSG_DEBUG(sF2);
        auto g = symmetry_group_type::gamete();
        std::vector<symmetry_group_type> grplist1 = {sF2, g, sF2, g, g, sF2, g, sF2, g, g};
        std::vector<symmetry_group_type> grplist2 = {sF2, g, sF2, g, g, g, g, g};
        /*std::function<const symmetry_group_type&(size_t)> get_grp1 = [=] (size_t i) { return grplist1[i]; };*/
        /*std::function<const symmetry_group_type&(size_t)> get_grp2 = [=] (size_t i) { return grplist2[i]; };*/
        auto LR = compute_size_factors(sizes1, reent1);
        auto grp1 = combine_groups(LR, grplist1, ped.get_gen(F4_1)->labels, ped.get_gen(F4_1)->inf_mat);
        LR = compute_size_factors(sizes2, reent2);
        /*auto grp2 = combine_groups(LR, grplist2, ped.get_gen(F4_1)->labels, ped.get_gen(F4_1)->inf_mat);*/
    }

    if (0)
    {
        pedigree_type ped;
        size_t A = ped.ancestor("A");
        size_t B = ped.ancestor("B");
        size_t F1 = ped.crossing("F1", A, B);
        size_t F2 = ped.selfing("F2", F1);
        size_t F3 = ped.selfing("F3", F2);
        permutation_type g = permutation_type::identity(2);
        permutation_type p1{0};
        permutation_type p2{3,2,1,0};
        permutation_type p3 = kronecker(p2, kronecker(g, g));
        MSG_DEBUG("p3");
        MSG_DEBUG(p3);
        MSG_DEBUG("p3 lumpé");
        MSG_DEBUG(p3.lump(ped.get_gen(F3)->collect.cast<bool>()));
    }

    if (0)
    {
        pedigree_type ped;
        size_t A = ped.ancestor("A");
        size_t B = ped.ancestor("B");
        size_t F1_1 = ped.crossing("F1_1", A, B);
        size_t F1_2 = ped.crossing("F1_2", A, B);
        size_t F2_1 = ped.crossing("F2_1", F1_1, F1_2);
        size_t F2_2 = ped.crossing("F2_2", F1_1, F1_2);
        MSG_DEBUG("F3_1");
        size_t F3_1 = ped.crossing("F3_1", F2_1, F2_2);
        size_t F3_2 = ped.crossing("F3_2", F2_1, F2_2);
        MSG_DEBUG("F4s");
        size_t F4_1 = ped.crossing("F4_1", F3_1, F3_2); (void) F4_1;
        /*symmetry_group_type sF2(ped.get_gen(F2_1)->labels), sF4(ped.get_gen(F4_1)->labels);*/
        symmetry_group_type sF4(ped.get_gen(F4_1)->labels);
        auto s2 = symmetry_table_type::build(permutation_type{3, 2, 1, 0}, ped.get_gen(F2_1)->labels, ped.get_gen(F2_1)->inf_mat, false).second;
        auto g = *symmetry_group_type::gamete().begin();
        permutation_type gi = permutation_type::identity(2);
        permutation_type ga = permutation_type::anti_identity(2);
        MSG_DEBUG("s2");
        MSG_DEBUG(s2);
        MSG_DEBUG(s2.table);
        permutation_type p4 = 
                kronecker(s2.table,
                kronecker(gi,
                kronecker(s2.table,
                kronecker(gi,
                kronecker(gi,
                kronecker(gi,
                kronecker(gi, gi)))))));

        /*MSG_DEBUG("--");*/
        /*MSG_DEBUG(p4);*/
        /*MSG_DEBUG("--");*/
        p4 = p4.lump(ped.get_gen(F4_1)->collect.cast<bool>());

        auto b = symmetry_table_type::build(p4, ped.get_gen(F4_1)->labels, ped.get_gen(F4_1)->inf_mat, false);

        MSG_DEBUG(p4);
        if (b.first) {
            MSG_DEBUG("SYMMETRY BUILT OK!");
            sF4.insert(b.second);
        } else {
            MSG_DEBUG("BUILD FAILED LAMENTABLEMENT.");
        }

        if (0)
        {
            /*permutation_type s2({0, 2, 1, 3});*/
            permutation_type s2({3, 2, 1, 0});
            std::vector<size_t> sizes1 = {4, 2, 4, 2, 2, 4, 2, 4, 2, 2};
            std::vector<bool> reent1 =   {0, 0, 0, 0, 0, 1, 0, 1, 0, 0};
            MSG_DEBUG("s2");
            MSG_DEBUG(s2);
            MSG_DEBUG("----------------");
            /*std::vector<permutation_type> pvec = {s2, gi, s2, gi, gi, s2, gi, s2, gi, gi};*/
            std::vector<permutation_type> pvec = {s2, gi, s2, gi, gi, s2, gi, s2, gi, gi};
            /*std::vector<permutation_type> pvec = {s2, ga, s2, ga, ga, s2, ga, s2, ga, ga};*/
            auto LR = compute_size_factors(sizes1, reent1);
            {
                std::stringstream lr;
                lr << "L:";
                for (auto l: LR.first) { lr << '\t' << l; }
                lr << std::endl;
                lr << "R:";
                for (auto r: LR.second) { lr << '\t' << r; }
                MSG_DEBUG(lr.str());
            }
            auto ph = combine_permutations(LR, pvec).lump(ped.get_gen(F4_1)->collect.cast<bool>());

            if (ph.size()) {
                auto bh = symmetry_table_type::build(ph, ped.get_gen(F4_1)->labels, ped.get_gen(F4_1)->inf_mat, false);

                MSG_DEBUG(ph);
                if (bh.first) {
                    MSG_DEBUG("SYMMETRY BUILT OK!");
                    sF4.insert(bh.second);
                } else {
                    MSG_DEBUG("BUILD FAILED LAMENTABLEMENT (sym).");
                }
            } else {
                MSG_DEBUG("BUILD FAILED LAMENTABLEMENT (perm).");
            }
        }
    }

    if (0)
    {
        pedigree_type ped;
        /*ped.max_states = 4;*/
        /*ped.n_alleles = 2;*/
        size_t A = ped.ancestor("A");
        size_t B = ped.ancestor("B");
        size_t F1 = ped.crossing("F1", A, B);
        size_t AB = ped.dh("AB", F1); (void) AB;
        /*size_t C = ped.ancestor("C");*/
        /*size_t F2_1 = ped.crossing("F2_1", AB, C);*/
        /*size_t F2_2 = ped.crossing("F2_2", AB, C);*/
        /*MSG_DEBUG("F3_1");*/
        /*size_t F3_1 = ped.crossing("F3_1", F2_1, F2_2);*/
        /*size_t F3_2 = ped.crossing("F3_2", F2_1, F2_2);*/
        /*MSG_DEBUG("F4s");*/
        /*size_t F4_1 = ped.crossing("F4_1", F3_1, F3_2); (void) F4_1;*/
    }

    if (0)
    {
        permutation_type lz1 = permutation_type::lozenge(3, 2);
        permutation_type lz2 = permutation_type::lozenge(2, 3);
        MatrixXd two(2, 2);
        two << 1, 2,
               3, 4;
        MatrixXd three(3, 3);
        three << 1, 2, 3,
                 4, 5, 6,
                 7, 8, 9;
        MatrixXd twothree = kroneckerProduct(two, three);
        MatrixXd threetwo = kroneckerProduct(three, two);
        MSG_DEBUG("lz(3, 2)");
        MSG_DEBUG(lz1);
        MSG_DEBUG("lz(2, 3)");
        MSG_DEBUG(lz2);
        MSG_DEBUG("?");
        MSG_DEBUG((lz1 * lz2));
        MSG_DEBUG("two ⊗ three");
        MSG_DEBUG(twothree);
        MSG_DEBUG("three ⊗ two");
        MSG_DEBUG(threetwo);
        MSG_DEBUG("lz(3, 2) % (two ⊗ three)");
        MSG_DEBUG((threetwo - (lz1 % twothree)));
        MSG_DEBUG("lz(2, 3) % (two ⊗ three)");
        MSG_DEBUG((threetwo - (lz2 % twothree)));

        MSG_DEBUG("avec transpose !");
        MSG_DEBUG((lz1.matrix<double>() * twothree * lz1.matrix<double>().transpose() - threetwo));
        MSG_DEBUG("");
        MSG_DEBUG((lz2.matrix<double>() * twothree * lz2.matrix<double>().transpose() - threetwo));
    }

    if (0)
    {
        pedigree_tree_type tree;

        auto g = [&] (int p) { return tree.add_node(p); };
        auto c = [&] (int p1, int p2) { return tree.add_node(g(p1), g(p2)); };
        auto a = [&] () { return tree.add_node(); };
        auto s = [&] (int p) { return c(p, p); };

        /*int A = tree.add_node();*/
        /*int B = tree.add_node();*/
        /*int gA = tree.add_node(A);*/
        /*int gB = tree.add_node(B);*/
        /*int F1 = tree.add_node(gA, gB);*/
        /*int gF1a = tree.add_node(F1);*/
        /*int gF1b = tree.add_node(F1);*/
        /*int F2 = tree.add_node(gF1a, gF1b);*/
        int A = a();
        int B = a();
        int F1 = c(A, B);
        int F2 = s(F1);
        std::vector<int> in, out;
        MSG_DEBUG(tree);
        auto tree_F1 = tree.extract_expression(F1, in, out);
        MSG_DEBUG(tree_F1);
        MSG_DEBUG("inputs: " << in);
        MSG_DEBUG("outputs: " << out);
        auto descr = tree_F1.compute_descr(*tree_F1.m_roots.begin());
        auto tree_F2 = tree.extract_expression(F2, in, out);
        MSG_DEBUG(tree_F2);
        MSG_DEBUG("inputs: " << in);
        MSG_DEBUG("outputs: " << out);
        descr = tree_F2.compute_descr(*tree_F2.m_roots.begin());
    }

    if (0)
    {
        pedigree_tree_type tree;

        int A = tree.ancestor();
        int B = tree.ancestor();
        int F1_1 = tree.crossing(A, B);
        int F1_2 = tree.crossing(A, B);
        int F2_1 = tree.crossing(F1_1, F1_2);
        int F2_2 = tree.crossing(F1_1, F1_2);
        int F3_1 = tree.crossing(F2_1, F2_2);
        int F3_2 = tree.crossing(F2_1, F2_2);
        int F4_1 = tree.crossing(F3_2, F3_1);

        std::vector<int> in, out;

        MSG_DEBUG(tree);

        auto tree_F2 = tree.extract_expression(F2_1, in, out);
        MSG_DEBUG(tree_F2);
        MSG_DEBUG("inputs: " << in);
        MSG_DEBUG("outputs: " << out);
        auto descr = tree_F2.root_descr();

        auto tree_F3 = tree.extract_expression(F3_1, in, out);
        MSG_DEBUG(tree_F3);
        MSG_DEBUG("inputs: " << in);
        MSG_DEBUG("outputs: " << out);
        descr = tree_F3.root_descr();

        auto tree_F4 = tree.extract_expression(F4_1, in, out);
        MSG_DEBUG(tree_F4);
        MSG_DEBUG("inputs: " << in);
        MSG_DEBUG("outputs: " << out);
        /*descr = tree_F4.compute_descr(tree_F4.count_ancestors(*tree_F4.m_roots.begin()));*/

        /*MSG_DEBUG("tree_F2 == tree_F3 ? " << (tree_F2 == tree_F3));*/
        /*MSG_DEBUG("tree_F2 == tree_F4 ? " << (tree_F2 == tree_F4));*/
        /*MSG_DEBUG("tree_F3 == tree_F4 ? " << (tree_F3 == tree_F4));*/
        auto f2_order = tree_F2.configuration_iterator().node_order();
        MSG_DEBUG("f2_order F F " << tree_F2.configuration_iterator().node_order());
        MSG_DEBUG("f2_order F T " << tree_F2.configuration_iterator().node_order(false, true));
        MSG_DEBUG("f2_order T F " << tree_F2.configuration_iterator().node_order(true, false));
        MSG_DEBUG("f2_order T T " << tree_F2.configuration_iterator().node_order(true, true));
        auto spi = tree_F4.configuration_iterator();
        MSG_DEBUG("f2_order " << f2_order);
        MSG_DEBUG("PERMUTATINO COUNT " << spi.size());
        MSG_DEBUG("leaf order " << spi.leaf_order());
        MSG_DEBUG("node order " << spi.node_order());
        MSG_DEBUG("== f2_order? " << (spi.node_order() == f2_order));
        MSG_DEBUG("mapping: " << create_mapping(tree_F2, tree_F4).mapping);
        auto exp_sym = expression_symmetries_type(tree_F4);
        MSG_DEBUG("Number of internal symmetries: " << exp_sym.symmetry_cursors.size() << " actual and " << exp_sym.latent_symmetry_cursors.size() << " latent");
        
        if (1)
        {
            extreme_lozenge_generator lz(tree_F4, exp_sym.symmetry_cursors[0]);
            MSG_DEBUG("lz.ref_order " << lz.ref_order);
            MSG_DEBUG("lz.disorder " << lz.disorder);
            MSG_DEBUG("lz.sizes " << lz.sizes);
            MSG_DEBUG("lz.rotations " << lz.rotations);
            MSG_DEBUG(lz([] (int i) { MSG_DEBUG("called permutation_getter on #" << i); return permutation_type::anti_identity(4); },
                         [] (int) { return false; }));
        }
        if (1)
        {
            extreme_lozenge_generator lz(tree_F4, exp_sym.symmetry_cursors[1]);
            MSG_DEBUG("lz.ref_order " << lz.ref_order);
            MSG_DEBUG("lz.disorder " << lz.disorder);
            MSG_DEBUG("lz.sizes " << lz.sizes);
            MSG_DEBUG("lz.rotations " << lz.rotations);
            MSG_DEBUG(lz([] (int) { return permutation_type::anti_identity(4); },
                         [] (int) { return false; }));
        }
    }

    if (0)
    {
        pedigree_tree_type tree;

        int A = tree.ancestor();
        int B = tree.ancestor();
        int F1 = tree.crossing(A, B);
        int F2 = tree.selfing(F1);
        int F3 = tree.selfing(F1);

        std::vector<int> in, out;

        MSG_DEBUG(tree);

        auto tree_F2 = tree.extract_expression(F2, in, out);
        MSG_DEBUG(tree_F2);
        MSG_DEBUG("inputs: " << in);
        MSG_DEBUG("outputs: " << out);
        auto descr = tree_F2.root_descr();
        expression_symmetries_type exp_sym(tree_F2);
        MSG_DEBUG("Number of internal symmetries: " << exp_sym.symmetry_cursors.size() << " actual and " << exp_sym.latent_symmetry_cursors.size() << " latent");
        MSG_QUEUE_FLUSH();
        if (0)
        {
            extreme_lozenge_generator lz(tree_F2, exp_sym.symmetry_cursors[0]);
            MSG_DEBUG("lz.ref_order " << lz.ref_order);
            MSG_DEBUG("lz.disorder " << lz.disorder);
            MSG_DEBUG("lz.sizes " << lz.sizes);
            MSG_DEBUG("lz.rotations " << lz.rotations);
            MSG_DEBUG("");
            extreme_lozenge_generator lz2(tree_F2, exp_sym.latent_symmetry_cursors[0]);
            MSG_DEBUG("lz.ref_order " << lz.ref_order);
            MSG_DEBUG("lz.disorder " << lz.disorder);
            MSG_DEBUG("lz.sizes " << lz.sizes);
            MSG_DEBUG("lz.rotations " << lz.rotations);
            MSG_DEBUG("");
            MSG_DEBUG(lz([] (int i) { MSG_DEBUG("called permutation_getter on #" << i); return permutation_type::identity(1); },
                         [] (int) { return false; }));
            MSG_DEBUG("");
            MSG_DEBUG(lz([] (int) { return permutation_type::identity(1); },
                         [] (int) { return true; }));
            MSG_DEBUG("");
            MSG_DEBUG(lz([] (int) { return permutation_type::identity(4); },
                         [] (int) { return false; }));
            MSG_DEBUG("");
            MatrixXb collect_f3(8, 16);
            collect_f3 <<
                1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0;
            MSG_DEBUG(collect_f3);

            MSG_DEBUG("");
            MSG_DEBUG(lz([] (int) { return permutation_type::anti_identity(4); },
                         [] (int) { return false; }).lump(collect_f3));
            MSG_DEBUG("");
            MSG_DEBUG(lz([] (int) { return permutation_type{0, 2, 1, 3}; },
                         [] (int) { return true; }).lump(collect_f3));
            MSG_DEBUG("LZ2");
            MSG_DEBUG(lz([] (int) { return permutation_type::identity(4); },
                         [] (int) { return false; }).lump(collect_f3));
            MSG_DEBUG("LZ2");
            MSG_DEBUG(lz([] (int) { return permutation_type::anti_identity(4); },
                         [] (int) { return false; }).lump(collect_f3));

            symmetry_propagator sp(tree_F2);
            std::vector<label_type> f1_labels = {{'a', 'b'}};
            std::vector<label_type> f2_labels = {{'a', 'a'}, {'a', 'b'}, {'b', 'a'}, {'b', 'b'}};
            MatrixXd f2_mat(4, 4);
            f2_mat << -2,  1,  1,  0,
                       1, -2,  0,  1,
                       1,  0, -2,  1,
                       0,  1,  1, -2;
            MSG_DEBUG("F2 symmetries:");
            pedigree_tree_type f1 = tree.extract_subtree(F1);
            MSG_DEBUG("LEAF:");
            MSG_DEBUG(f1);
            symmetry_group_type f1_s, f1_ls;
            f1_s.insert(symmetry_table_type(permutation_type{0}, {{'a', 'a'}, {'b', 'b'}}));
            f1_ls.insert(symmetry_table_type(permutation_type{0}, {{'a', 'b'}, {'b', 'a'}}));
            MatrixXb lump_f1 = MatrixXb::Ones(1, 2);
            MSG_DEBUG(sp.compute_propagated_symmetries(
                            [&] (int) -> const pedigree_tree_type& { return f1; },
                            [&] (int) -> const symmetry_group_type& { return f1_s; },
                            [&] (int) -> const symmetry_group_type& { return f1_ls; },
                            f2_labels, f2_mat, [&] (int i) -> const MatrixXb& { MSG_DEBUG("GET LUMPER " << i); return lump_f1; }));
            MSG_DEBUG(sp.compute_propagated_latent_symmetries(
                            [&] (int) -> const pedigree_tree_type& { return f1; },
                            [&] (int) -> const symmetry_group_type& { return f1_s; },
                            [&] (int) -> const symmetry_group_type& { return f1_ls; },
                            f2_labels, f2_mat, [&] (int i) -> const MatrixXb& { MSG_DEBUG("GET LUMPER " << i); return lump_f1; }));
        }
        {
            std::vector<label_type> A_labels = {{'a', 'a'}};
            std::vector<label_type> B_labels = {{'b', 'b'}};
            std::vector<label_type> f1_labels = {{'a', 'b'}};
            symmetry_group_type SA(A_labels);
            symmetry_group_type SB(B_labels);
            symmetry_group_type empty;
            MatrixXb oneone = MatrixXb::Ones(1, 1);
            pedigree_tree_type tA = tree.extract_subtree(A);
            pedigree_tree_type tB = tree.extract_subtree(B);

            auto expr_f1 = tree.extract_expression(F1, in, out);
            symmetry_propagator sp(expr_f1);
            MSG_DEBUG("F1 SYMMETRIES");
            MSG_DEBUG(sp.compute_propagated_symmetries(
                        [&] (int i) -> const pedigree_tree_type& { MSG_DEBUG("get tree #" << i); return i ? tB : tA; },
                        [&] (int i) -> const symmetry_group_type& { return i ? SB : SA; },
                        [&] (int) -> const symmetry_group_type& { return empty; },
                        f1_labels, oneone.cast<double>(),
                        [&] (int) -> const MatrixXb& { return oneone; }));
            MSG_DEBUG("F1 LATENT SYMMETRIES");
            MSG_DEBUG(sp.compute_propagated_latent_symmetries(
                        [&] (int i) -> const pedigree_tree_type& { MSG_DEBUG("get tree #" << i); return i ? tB : tA; },
                        [&] (int i) -> const symmetry_group_type& { return i ? SB : SA; },
                        [&] (int) -> const symmetry_group_type& { return empty; },
                        f1_labels, oneone.cast<double>(),
                        [&] (int) -> const MatrixXb& { return oneone; }));
        }
        {
            std::vector<label_type> f2_labels = {{'a', 'a'}, {'a', 'b'}, {'b', 'a'}, {'b', 'b'}};
            std::vector<label_type> f3_labels = {{'a', 'a'}, {'a', 'a'}, {'a', 'a'}, {'a', 'b'}, {'b', 'a'}, {'b', 'b'}, {'b', 'b'}, {'b', 'b'}};
            symmetry_group_type Sf2(f2_labels);
            MatrixXd F3_mat(8, 8);
            F3_mat <<
                -4,  2,  1,  0,  0,  1,  0,  0,
                 2, -4,  0,  1,  1,  0,  0,  0,
                 1,  0, -4,  1,  1,  0,  1,  0,
                 0,  1,  1, -4,  0,  1,  0,  1,
                 0,  1,  1,  0, -4,  1,  0,  1,
                 1,  0,  0,  1,  1, -4,  1,  0,
                 0,  0,  1,  0,  0,  1, -4,  2,
                 0,  0,  0,  1,  1,  0,  2, -4;
            MatrixXb collect_f3(8, 16);
            collect_f3 <<
                1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0;

            symmetry_group_type LSf2;
            Sf2.insert(symmetry_table_type(permutation_type{3, 2, 1, 0}, {{'a', 'b'}, {'b', 'a'}}));
            MatrixXb oneone = MatrixXb::Ones(1, 1);
            pedigree_tree_type tF2 = tree.extract_subtree(F2);
            MSG_DEBUG("subtree for F2:");
            MSG_DEBUG(tF2);
            MatrixXb onetwo = MatrixXb::Ones(1, 2);
            MatrixXb onefour = MatrixXb::Ones(1, 4);
            MatrixXb twotwo = MatrixXb::Identity(2, 2);
            MatrixXb lump_F2 = MatrixXb::Identity(4, 4);
            MatrixXb empty;

            auto get_lumper =
                [&] (int i) -> const MatrixXb&
                {
                    switch (i) {
                        case 0:                 // A
                        case 1: return oneone;  // B
                        case 2:                 // gA
                        case 3: return twotwo;  // gB
                        case 4: return onefour;  // F1
                        case 5:                 // gF1 (1)
                        case 6: return twotwo;  // gF1 (2)
                        case 7: return lump_F2; // F2
                        case 8:
                        case 9: return empty;
                        case 10: return collect_f3;
                        default:;
                                return empty;
                    };
                };

            auto expr_f3 = tree.extract_expression(F3, in, out);
            symmetry_propagator sp(expr_f3);
            MSG_DEBUG("F3 SYMMETRIES");
            MSG_DEBUG(sp.compute_propagated_symmetries(
                        [&] (int) -> const pedigree_tree_type& { return tF2; },
                        [&] (int) -> const symmetry_group_type& { return Sf2; },
                        [&] (int) -> const symmetry_group_type& { return LSf2; },
                        f3_labels, F3_mat,
                        get_lumper));
            MSG_DEBUG("F3 LATENT SYMMETRIES");
            MSG_DEBUG(sp.compute_propagated_latent_symmetries(
                        [&] (int) -> const pedigree_tree_type& { return tF2; },
                        [&] (int) -> const symmetry_group_type& { return Sf2; },
                        [&] (int) -> const symmetry_group_type& { return LSf2; },
                        f3_labels, F3_mat,
                        get_lumper));
        }
    }

    if (0)
    {
        auto A = ancestor_matrix("A", 'a');
        auto B = ancestor_matrix("B", 'b');
        auto C = ancestor_matrix("C", 'c');
        auto D = ancestor_matrix("D", 'd');
        auto AB = A | B;
        auto CD = C | D;
        auto F2 = AB | AB;
        MSG_DEBUG(F2);
        MSG_DEBUG((-F2 * A));
        MSG_DEBUG((-F2 * A * gamete));
        MSG_DEBUG((-F2 * A));
        MSG_DEBUG((-F2 * A * gamete));
        MSG_DEBUG(AB);
        MSG_DEBUG((-AB * CD));
        MSG_DEBUG((-AB * CD * gamete));
    }

    if (0)
    {
        pedigree_type ped;
        size_t A = ped.ancestor("A");
        size_t B = ped.ancestor("B");
        size_t F1_1 = ped.crossing("F1", A, B);
        size_t F1_2 = ped.crossing("F1", A, B);
        size_t F2_1 = ped.crossing("F2", F1_1, F1_2);
        auto g2 = *ped.get_gen(F2_1);
        MSG_DEBUG((-g2 * g2));
        MSG_DEBUG((-(g2 | g2)));
    }

    if (0)
    {
        auto A = ancestor_matrix("A", 'a');
        auto B = ancestor_matrix("B", 'b');
        auto F1 = A | B;
        auto RF1 = recall_matrix(1);
        auto Rg = recall_matrix(2);
        geno_matrix F2 = lump(kronecker_expr("F2").add(F1).add(gamete).add(RF1).add(gamete));
        MSG_DEBUG("");
        /*geno_matrix F3 = lump(kronecker_expr().add(F2).add(gamete).add(RF1).add(gamete));*/
        geno_matrix F3 = lump(kronecker_expr("F3") << F2 << gamete << RF1 << gamete);
        MSG_DEBUG(F3);
    }

    if (0)
    {
        auto A = ancestor_matrix("A", 'a');
        auto B = ancestor_matrix("B", 'b');
        auto F1 = A | B;
        geno_matrix DH = (kronecker_expr("DH") << F1 << gamete << recall_matrix(1) << recall_matrix(2));
        MSG_DEBUG(DH);
    }

    if (0)
    {
        auto A = ancestor_matrix("A", 'a');
        auto B = ancestor_matrix("B", 'b');
        auto F1 = A | B;
        auto R1 = recall_matrix(1);
        auto R3 = recall_matrix(3);
        auto F2 = (kronecker_expr("F2") << F1 << gamete << R1 << gamete);
        geno_matrix LUMP;
        MSG_DEBUG("============== F3 ==============");
        geno_matrix F3
            = (kronecker_expr("F3")
                << F1 << gamete << F1 << gamete << gamete << R1 << gamete << R3 << gamete << gamete << LUMP
            );
        MSG_DEBUG(F3);
        geno_matrix F3l
            = (kronecker_expr("F3l")
                << F1 << gamete << F1 << gamete << LUMP << gamete << R1 << gamete << R3 << gamete << gamete << LUMP
            );
        MSG_DEBUG(F3l);

        MSG_DEBUG("DIFF F3");
        MSG_DEBUG((F3l.inf_mat - F3.inf_mat));

        geno_matrix F4
            = (kronecker_expr("F4")
                << F2 << gamete << F2 << gamete << gamete << R1 << gamete << R3 << gamete << gamete << LUMP
            );
        MSG_DEBUG(F4);
        geno_matrix F4l
            = (kronecker_expr("F4l")
                << F2 << gamete << F2 << gamete << LUMP << gamete << R1 << gamete << R3 << gamete << gamete << LUMP
            );
        MSG_DEBUG(F4l);

        MSG_DEBUG("DIFF F4");
        MSG_DEBUG((F4l.inf_mat - F4.inf_mat));
    }

    if (1)
    {
        gencomb_type c1(0, 4);
        gencomb_type c2(1, 2);
        gencomb_type c3(3, 2);

        gencomb_type c = gencomb_type(.125) * kronecker(c1, c2);
        gencomb_type d = gencomb_type(.5) * hadamard(c2, c3);
        MSG_DEBUG("c1 " << c1);
        MSG_DEBUG("c2 " << c2);
        MSG_DEBUG("c3 " << c3);
        MSG_DEBUG("c " << c);
        MSG_DEBUG("d " << d);
        auto cd = kronecker(c, d);
        MSG_DEBUG("c x d " << cd);
        auto cd0 = sum_over(cd, {0});
        for (const auto& kv: cd0) {
            MSG_DEBUG("" << kv.first << " => " << kv.second);
        }
    }

#if 0
    if (1)
    {
        std::vector<bn_label_type> label_f2 = {{'a', 'a', 0, 0}, {'a', 'b', 0, 0}, {'b', 'a', 0, 0}, {'b', 'b', 0, 0}};
        std::vector<bn_label_type> label_g = {{'<', 0, 0, 0}, {'>', 0, 0, 0}};
        pedigree_tree_type tree;

        struct compute_labels {
            bn_label_type
                find_label(size_t n, const genotype_comb_type::element_type& labels)
                {
                    return std::find_if(labels.keys.begin(), labels.keys.end(),
                                        [=] (const genotype_comb_type::key_type& k) { return k.parent == n; }
                           )->state;
                }

            bn_label_type
                operator () (const pedigree_tree_type& tree, size_t n, const genotype_comb_type::element_type& labels)
                {
                    if (tree.get_p2(n) == NONE) {
                        /* gamete or ancestor */
                        if (tree.get_p1(n) == NONE) {
                            /* ancestor */
                            return find_label(n, labels);
                        } else {
                            auto gl = find_label(n, labels);
                            auto sub = operator () (tree, tree.get_p1(n), labels);
                            if (gl.first == GAMETE_L) {
                                return {sub.first, 0, sub.first_allele, 0};
                            } else {
                                return {sub.second, 0, sub.second_allele, 0};
                            }
                        }
                    } else {
                        auto subl = operator () (tree, tree.get_p1(n), labels);
                        auto subr = operator () (tree, tree.get_p2(n), labels);
                        return {subl.first, subr.first, subl.first_allele, subr.first_allele};
                    }
                }

            std::vector<bn_label_type>
                operator () (const pedigree_tree_type& tree, size_t n, const genotype_comb_type& comb)
                {
                    std::vector<bn_label_type> ret;
                    ret.reserve(comb.m_combination.size());
                    for (const auto& e: comb) {
                        ret.emplace_back(operator () (tree, n, e));
                    }
                    return ret;
                }

            static
                genotype_comb_type
                make_comb(const pedigree_tree_type& tree, size_t n, const genotype_comb_type& comb)
                {
                    return state_to_combination(n, compute_labels()(tree, n, comb));
                }

            static
                genotype_comb_type
                add_labels(const pedigree_tree_type& tree, size_t n, const genotype_comb_type& comb)
                {
                    auto labcomb = make_comb(tree, n, comb);
                    return hadamard(labcomb, comb);
                }
        };

        size_t F2 = tree.add_node();
        size_t M2 = tree.add_node();
        size_t G1 = tree.add_node(F2);
        size_t G2 = tree.add_node(M2);
        size_t F3 = tree.add_node(G1, G2);
        size_t G3 = tree.add_node(F2);
        size_t G4 = tree.add_node(M2);
        size_t M3 = tree.add_node(G3, G4);
        size_t G5 = tree.add_node(F3);
        size_t G6 = tree.add_node(M3);
        size_t M4 = tree.add_node(G5, G6);
        genotype_comb_type
            f2 = state_to_combination(F2, label_f2),
            m2 = state_to_combination(M2, label_f2),
            g1 = state_to_combination(G1, label_g) * .5,
            g2 = state_to_combination(G2, label_g) * .5,
            g3 = state_to_combination(G3, label_g) * .5,
            g4 = state_to_combination(G4, label_g) * .5,
            g5 = state_to_combination(G5, label_g) * .5,
            g6 = state_to_combination(G6, label_g) * .5;
        auto f3 = compute_labels::add_labels(tree, F3, kronecker(kronecker(kronecker(f2, g1), m2), g2));
        auto m3 = compute_labels::add_labels(tree, M3, kronecker(kronecker(kronecker(f2, g3), m2), g4));
        auto m4 = compute_labels::add_labels(tree, M4, kronecker(kronecker(kronecker(f3, g5), m3), g6));
        MSG_DEBUG("size(m3): " << m3.size());
        MSG_DEBUG("size(f3): " << f3.size());
        MSG_DEBUG("size(m4): " << m4.size());
        auto m4ng = fold(sum_over(m4, {G1, G2, G3, G4, G5, G6}));
        MSG_DEBUG("m4 without gametes: " << m4ng);

        bn_factor_type factor = make_factor(m4ng);

        {
            auto x7 = sum_over(m4ng, {7});
            MSG_DEBUG_INDENT_EXPR("[factoring out 7] ");
            for (const auto& x: x7) {
                MSG_DEBUG("" << x.first << " => " << x.second);
            }
            MSG_DEBUG_DEDENT;
        }
        for (const auto& tm: factor) {
            MSG_DEBUG("MESSAGE TO " << tm.first);
            for (const auto& kv: tm.second) {
                MSG_DEBUG(" * " << kv.first);
                MSG_DEBUG("" << kv.second);
            }
        }

        auto test_factor =
            [&] (size_t target,
                 size_t i1, double p11, double p12, double p13, double p14,
                 size_t i2, double p21, double p22, double p23, double p24,
                 size_t i3, double p31, double p32, double p33, double p34,
                 size_t i4, double p41, double p42, double p43, double p44)
            {
                bn_message_type prob;
                MSG_DEBUG("COMPUTING MESSAGE TOWARDS " << target << " GIVEN "
                          << i1 << "={" << p11 << ' ' << p12 << ' ' << p13 << ' ' << p14 << "} "
                          << i2 << "={" << p21 << ' ' << p22 << ' ' << p23 << ' ' << p24 << "} "
                          << i3 << "={" << p31 << ' ' << p32 << ' ' << p33 << ' ' << p34 << "} "
                          << i4 << "={" << p41 << ' ' << p42 << ' ' << p43 << ' ' << p44 << "}");
                bn_message_buffer_type inputs = {
                    {i1, {{{'a', 'a', 0, 0}, p11},
                          {{'a', 'b', 0, 0}, p12},
                          {{'b', 'a', 0, 0}, p13},
                          {{'b', 'b', 0, 0}, p14}}},
                    {i2, {{{'a', 'a', 0, 0}, p21},
                          {{'a', 'b', 0, 0}, p22},
                          {{'b', 'a', 0, 0}, p23},
                          {{'b', 'b', 0, 0}, p24}}},
                    {i3, {{{'a', 'a', 0, 0}, p31},
                          {{'a', 'b', 0, 0}, p32},
                          {{'b', 'a', 0, 0}, p33},
                          {{'b', 'b', 0, 0}, p34}}},
                    {i4, {{{'a', 'a', 0, 0}, p41},
                          {{'a', 'b', 0, 0}, p42},
                          {{'b', 'a', 0, 0}, p43},
                          {{'b', 'b', 0, 0}, p44}}},
                };
                compute_factor_message(factor[target], inputs, prob);
                double norm = normalize_message(prob);
                MSG_DEBUG("message norm = " << norm);
                for (const auto& kv: prob) {
                    MSG_DEBUG(target << ":" << kv.first << " => " << kv.second);
                }
            };

        test_factor(M4, F2,   1,   0,   0,   0, M2,   0,   0,   0,   1, F3,   1,   1,   1,   1, M3,   1,   1,   1,   1);
        /*MSG_DEBUG("USING ABUNDANCES");*/
        /*test_factor(F2, M4, .3125, .1875, .1875, .3125, M2, .25, .25, .25, .25, F3,   0,  .5,  .5,   0, M3, .25, .25, .25, .25);*/
        /*test_factor(M2, M4, .3125, .1875, .1875, .3125, F2, .25, .25, .25, .25, F3,   0,  .5,  .5,   0, M3, .25, .25, .25, .25);*/
        /*test_factor(F2, M4, .3125, .1875, .1875, .3125, M2, .25, .25, .25, .25, F3,   1,   0,   0,   0, M3, .25, .25, .25, .25);*/

        MSG_DEBUG("USING UNIFORM");
        test_factor(F2, M4, .25, .25, .25, .25, M2, .25, .25, .25, .25, F3,   0,  .5,  .5,   0, M3, .25, .25, .25, .25);
        test_factor(M2, M4, .25, .25, .25, .25, F2, .25, .25, .25, .25, F3,   0,  .5,  .5,   0, M3, .25, .25, .25, .25);
        test_factor(F2, M4, .25, .25, .25, .25, M2, .25, .25, .25, .25, F3,   1,   0,   0,   0, M3, .25, .25, .25, .25);
        test_factor(F2, M4, .25, .25, .25, .25, M2, .25, .25, .25, .25, F3,   1,   0,   0,   0, M3, .25, .25, .25, .25);

        /*MSG_DEBUG("TESTING ALL UNKNOWNS (abun)");*/
        /*test_factor(F2, M4, .3125, .1875, .1875, .3125, M2, .25, .25, .25, .25, F3, .25, .25, .25, .25, M3, .25, .25, .25, .25);*/
        /*test_factor(M2, M4, .3125, .1875, .1875, .3125, F2, .25, .25, .25, .25, F3, .25, .25, .25, .25, M3, .25, .25, .25, .25);*/
        /*test_factor(F3, M4, .3125, .1875, .1875, .3125, M2, .25, .25, .25, .25, F2, .25, .25, .25, .25, M3, .25, .25, .25, .25);*/
        /*test_factor(M3, M4, .3125, .1875, .1875, .3125, M2, .25, .25, .25, .25, F2, .25, .25, .25, .25, F3, .25, .25, .25, .25);*/
        /*test_factor(M4, M3, .25, .25, .25, .25, M2, .25, .25, .25, .25, F2, .25, .25, .25, .25, F3, .25, .25, .25, .25);*/
        MSG_DEBUG("TESTING ALL UNKNOWNS (unif)");
        test_factor(F2, M4, .25, .25, .25, .25, M2, .25, .25, .25, .25, F3, .25, .25, .25, .25, M3, .25, .25, .25, .25);
        test_factor(M2, M4, .25, .25, .25, .25, F2, .25, .25, .25, .25, F3, .25, .25, .25, .25, M3, .25, .25, .25, .25);
        test_factor(F3, M4, .25, .25, .25, .25, M2, .25, .25, .25, .25, F2, .25, .25, .25, .25, M3, .25, .25, .25, .25);
        test_factor(M3, M4, .25, .25, .25, .25, M2, .25, .25, .25, .25, F2, .25, .25, .25, .25, F3, .25, .25, .25, .25);
        test_factor(M4, M3, .25, .25, .25, .25, M2, .25, .25, .25, .25, F2, .25, .25, .25, .25, F3, .25, .25, .25, .25);
    }
#endif



    return 0;
    (void) argc; (void) argv;
}

#endif

