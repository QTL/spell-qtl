/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPELL_TOKENIZER_H_
#define _SPELL_TOKENIZER_H_

#include <string>
#include <regex>
#include <vector>
#include "error.h"

namespace script {

template <typename TokClass>
struct tokenizer {
    std::vector<std::pair<TokClass, std::regex>> token_classes;
    struct token_type {
        TokClass cls;
        std::string text;
        size_t ofs;
        friend std::ostream& operator << (std::ostream& os, const token_type& t)
        {
            os << t.cls;
            if (t.text.size()) {
                os << '[' << t.text << ']';
            }
            os << " at offset " << t.ofs;
            return os;
        }
    };
    std::set<TokClass> discard;

    tokenizer(std::initializer_list<std::pair<TokClass, std::string>>&& classes)
        : token_classes()
    {
        for (const auto& cc: classes) {
            add(cc.first, cc.second);
        }
    }

    tokenizer&
        add(const TokClass& class_name, const std::string& pattern)
        {
            token_classes.emplace_back(class_name, std::regex(pattern));
            return *this;
        }

    struct tokenizer_state {
        const tokenizer* m_tok;
        const std::string* m_subject;

        struct iterator {
            const tokenizer* m_tok;
            std::string::const_iterator i, j, next_i;
            typedef token_type value_type;
            value_type value;

            iterator () : m_tok(NULL), i(), j(), next_i(), value() {}
            iterator (const tokenizer* _, std::string::const_iterator a, std::string::const_iterator b)
                : m_tok(_), i(a), j(b), next_i(a)
            { value.ofs = 0; next_match(); }

            iterator& operator = (const iterator& other)
            {
                m_tok = other.m_tok;
                i = other.i; j = other.j; next_i = other.next_i;
                value = other.value;
                return *this;
            }
            const value_type& operator * () const { return value; }
            const value_type* operator -> () const { return &value; }
            iterator& operator ++ () { next_match(); return *this; }
            bool operator == (const iterator& other) const { return i == other.i && j == other.j; }
            bool operator != (const iterator& other) const { return i != other.i || j != other.j; }

        private:
            void __next_match_all_classes()
            {
                std::smatch sm;
                for (const auto& cls: m_tok->token_classes) {
                    if (std::regex_search(i, j, sm, cls.second)) {
                        next_i = sm.suffix().first;
                        value.cls = cls.first;
                        value.text = sm[0].str();
                        /*MSG_DEBUG("matched token " << value);*/
                        return;
                    }
                }
                /*MSG_DEBUG("didn't match any token");*/
                value.text.clear();
                next_i = i = j;
            }

            void next_match()
            {
                /*MSG_DEBUG("NEXT TOKEN");*/
                do {
                    value.ofs += next_i - i; 
                    i = next_i; 
                    __next_match_all_classes();
                } while (i != j && m_tok->discard.find(value.cls) != m_tok->discard.end());
            }
        };

        iterator begin() const { return {m_tok, m_subject->begin(), m_subject->end()}; }
        iterator end() const { return {m_tok, m_subject->end(), m_subject->end()}; }
    };

    tokenizer_state
        operator () (const std::string& text)
        {
            return {this, &text};
        }
};

} /* namespace script */

#endif

