/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vector>
#include "error.h"
#include "cache.h"
#include <iostream>
/*#include "all.h"*/

using namespace cache;


struct toto {
    int data;
    std::string src;

    toto(const std::string& s)
        : src(s)
    {
        std::stringstream ss(src);
        ss >> data;
    }
};

static inline
md5_digest& operator << (md5_digest& md5, const toto& t)
{
    md5 << t.src;
    return md5;
}


static inline
std::ostream& operator << (std::ostream& os, const toto& t)
{
    return os << "toto(" << t.src << ")";
}


static inline
cache_input& operator & (cache_input& ci, toto*& t)
{
    std::string src;
    ci & src;
    t = new toto(src);
    return ci;
}


static inline
cache_output& operator & (cache_output& co, toto*& t)
{
    co & t->src;
    return co;
}


struct product : cache::computed_value<product, double, value<double>, value<double>> {
    using cache::computed_value<product, double, value<double>, value<double>>::computed_value;

    static double do_compute(const value<double>& d1, const value<double>& d2)
    {
        return d1.v() * d2.v();
    }
};



struct mult_table : cache::computed_collection<product> {
    using cache::computed_collection<product>::computed_collection;
};


struct mult : cache::computed_value<mult, int, value<int>, value<int>> {
    using cache::computed_value<mult, int, value<int>, value<int>>::computed_value;

    static int do_compute(const value<int>& d1, const value<int>& d2)
    {
        return d1.v() * d2.v();
    }
};


struct mult2 : cache::computed_value<mult2, int, mult, mult> {
    using cache::computed_value<mult2, int, mult, mult>::computed_value;

    static int do_compute(const mult& d1, const mult& d2)
    {
        return d1.v() * d2.v();
    }
};


struct toto_val : value<toto*> {
    toto_val(const std::string& src)
        : value<toto*>(new toto(src))
    {}
};

struct mult_val : cache::computed_value<mult_val, int, toto_val, toto_val> {
    using cache::computed_value<mult_val, int, toto_val, toto_val>::computed_value;

    static int do_compute(const toto_val& t1, const toto_val& t2)
    {
        return t1.v()->data * t2.v()->data;
    }
};


int main(int argc, char** argv)
{
#if 0
    auto rng = std::make_tuple(range<double>(1, 23), value<double>(37));
    auto table = make_task<product>(rng);
    for (auto k: table) {
        double x = k.v();
        MSG_DEBUG(k.task->dependencies << " => " << x);
    }

    MSG_DEBUG("====================");
#endif
    mult m1(std::make_tuple(value<int>(10), value<int>(2)));
    mult m2(std::make_tuple(value<int>(17), value<int>(3)));

    mult2 m3(std::make_tuple(m1, m2));

    MSG_DEBUG("m1 => " << m1.v());
    MSG_DEBUG("m2 => " << m2.v());
    MSG_DEBUG("m3 => " << m3.v());

    toto_val t1("23");
    toto_val t2("42");

    mult_val tm(std::make_tuple(t1, t2));
    MSG_DEBUG("toto! " << tm.v());

    return 0;
}

