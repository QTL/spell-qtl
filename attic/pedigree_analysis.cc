/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <iostream>
#include <iomanip>
#include "file.h"
#include "chrono.h"
#include "generation_rs.h"

#include <boost/dynamic_bitset.hpp>


GenoMatrix lump_gen(const GenoMatrix& gm)
{
    MSG_DEBUG("PRE LUMP " << MATRIX_SIZE(gm));
    MSG_DEBUG(gm);
    GenerationRS tmp = convert(gm);
    ::lumper<GenerationRS, exact_compare<algebraic_genotype>> l(tmp);
    auto partition = l.refine_all();
    /*auto partition2 = l.try_refine(partition);*/
    /*MSG_DEBUG(partition);*/
    /*MSG_DEBUG(partition2);*/
    auto ret_lump = l.to_matrix(partition);
    MSG_DEBUG("POST LUMP " << MATRIX_SIZE(ret_lump));
    return ret_lump.data;
}


template <typename MATRIX_TYPE>
struct iterable_generation_base {
    typedef typename MATRIX_TYPE::Scalar scalar_type;
    virtual void reset() = 0;
    virtual bool next() = 0;
    virtual size_t rows() const = 0;
    virtual size_t cols() const = 0;
    virtual size_t row() const = 0;
    virtual size_t col() const = 0;
    virtual const scalar_type& value() const = 0;

    friend std::ostream& operator << (std::ostream& os, std::shared_ptr<iterable_generation_base<MATRIX_TYPE>> igb)
    {
        return os << '[' << igb->row() << ',' << igb->col() << ' ' << igb->value() << ']';
    }
};


template <typename MATRIX_TYPE>
struct iterable_generation : iterable_generation_base<MATRIX_TYPE> {
    typedef typename MATRIX_TYPE::Scalar scalar_type;
    const MATRIX_TYPE* m_gen;
    int m_row;
    int m_col;
    iterable_generation(const MATRIX_TYPE* g) : m_gen(g), m_row(0), m_col(0) {}
    void reset() { m_row = m_col = 0; }
    bool next()
    {
        ++m_row;
        if (m_row >= m_gen->rows()) {
            m_row = 0;
            ++m_col;
            if (m_col >= m_gen->cols()) {
                m_col = 0;
                return true;
            }
        }
        return false;
    }
    size_t rows() const { return m_gen->rows(); }
    size_t cols() const { return m_gen->cols(); }
    size_t row() const { return m_row; }
    size_t col() const { return m_col; }
    const scalar_type& value() const { return (*m_gen)(m_row, m_col); }
};

template <>
struct iterable_generation<VectorLC> : iterable_generation_base<VectorLC> {
    typedef typename VectorLC::Scalar scalar_type;
    VectorLC m_gen;
    int m_row;
    int m_col;
    iterable_generation(const VectorLC& g) : m_gen(g), m_row(0), m_col(0) {}
    void reset() { m_row = m_col = 0; }
    bool next()
    {
        ++m_row;
        if (m_row >= m_gen.rows()) {
            m_row = 0;
            ++m_col;
            if (m_col >= m_gen.cols()) {
                m_col = 0;
                return true;
            }
        }
        return false;
    }
    size_t rows() const { return m_gen.rows(); }
    size_t cols() const { return m_gen.cols(); }
    size_t row() const { return m_row; }
    size_t col() const { return m_col; }
    const scalar_type& value() const { return m_gen(m_row, m_col); }
};


template <typename MATRIX_TYPE> struct generation_gamete;
template <typename MATRIX_TYPE> struct generation_doubling_gamete;
template <typename MATRIX_TYPE> struct generation_proxy;

template <>
struct generation_gamete<GenoMatrix> : iterable_generation<GenoMatrix> {
    generation_gamete() : iterable_generation(&gametes_factory<algebraic_genotype::Type::Gamete>()) {}
};


template <>
struct generation_gamete<VectorLC> : iterable_generation<VectorLC> {
    /*static VectorLC _get_matrix() { static VectorLC _ = Matrix<gencomb_type, 2, 1>::Constant(.5); return &_; }*/
    generation_gamete() : iterable_generation(Matrix<gencomb_type, 2, 1>::Constant(.5)) {}
};


template <>
struct generation_doubling_gamete<GenoMatrix> : iterable_generation<GenoMatrix> {
    generation_doubling_gamete() : iterable_generation(&gametes_factory<algebraic_genotype::Type::DoublingGamete>()) {}
};

template <>
struct generation_doubling_gamete<VectorLC> : iterable_generation<VectorLC> {
    /*static const VectorLC* _get_matrix() { static VectorLC _ = Matrix<gencomb_type, 2, 1>::Constant(.5); return &_; }*/
    generation_doubling_gamete() : iterable_generation(Matrix<gencomb_type, 2, 1>::Constant(.5)) {}
};

template <>
struct generation_proxy<GenoMatrix> : iterable_generation_base<GenoMatrix> {
    std::shared_ptr<iterable_generation_base> target;
    algebraic_genotype tmp;
    generation_proxy(std::shared_ptr<iterable_generation_base> ig) : target(ig) {}
    void reset() {}
    bool next() { return true; }
    size_t rows() const { return 1; }
    size_t cols() const { return 1; }
    size_t row() const { return 0; }
    size_t col() const { return 0; }
    const algebraic_genotype& value() const
    {
        const algebraic_genotype& ag = target->value();
        /*MSG_DEBUG("proxy " << ag);*/
        return *const_cast<algebraic_genotype*>(&tmp) = {ag.genotype, ag.type, fast_polynom::one};
    }
};


template <>
struct generation_proxy<VectorLC> : iterable_generation_base<VectorLC> {
    std::shared_ptr<iterable_generation_base> target;
    algebraic_genotype tmp;
    generation_proxy(std::shared_ptr<iterable_generation_base> ig) : target(ig) {}
    void reset() {}
    bool next() { return true; }
    size_t rows() const { return 1; }
    size_t cols() const { return 1; }
    size_t row() const { return 0; }
    size_t col() const { return 0; }
    const typename VectorLC::Scalar& value() const
    {
        return target->value();
    }
};


enum Operation { PUSH, KRON };

template <typename MATRIX_TYPE>
struct generation_kronecker {
    typedef std::vector<std::shared_ptr<iterable_generation_base<MATRIX_TYPE>>> matrix_vector_type;
    typedef typename MATRIX_TYPE::Scalar scalar_type;
    matrix_vector_type matrices;
    std::vector<Operation> ops;
    std::vector<size_t> row_strides;
    std::vector<size_t> col_strides;
    MATRIX_TYPE result;

    generation_kronecker(const matrix_vector_type& vm, const std::vector<Operation>& vo)
        : matrices(vm)
        , ops(vo)
        , result()
    {
        row_strides.reserve(matrices.size() + 1);
        col_strides.reserve(matrices.size() + 1);
        row_strides.push_back(1);
        col_strides.push_back(1);
        for (const auto& m: matrices) {
            row_strides.push_back(row_strides.back() * m->rows());
            col_strides.push_back(col_strides.back() * m->cols());
            m->reset();
            MSG_DEBUG("matrix has size (" << m->rows() << ", " << m->cols() << ')');
        }
        compute_result();
        MSG_DEBUG(MATRIX_SIZE(result));
    }
    void reset() { for (auto& m: matrices) { m->reset(); } }

    bool next_cell()
    {
        bool ret = false;
        for (auto& i: matrices) {
            /*MSG_DEBUG("incr");*/
            if (i->next()) {
                /*MSG_DEBUG("overflow");*/
                /*i->reset();*/
            } else {
                /*MSG_DEBUG("ok " << i->row() << ',' << i->col());*/
                ret = true;
                break;
            }
        }
        /*MSG_DEBUG(matrices);*/
        return ret;
    }

    size_t compute_col()
    {
        size_t accum = 0;
        for (size_t i = 0; i < matrices.size(); ++i) {
            accum += col_strides[i] * matrices[i]->col();
        }
        return accum;
    }

    size_t compute_row()
    {
        size_t accum = 0;
        for (size_t i = 0; i < matrices.size(); ++i) {
            accum += row_strides[i] * matrices[i]->row();
        }
        return accum;
    }

    void compute_cell()
    {
        size_t m_i = 0;
        std::vector<scalar_type> stack;
        stack.reserve(matrices.size());
        size_t tmp;
        for (Operation o: ops) {
            switch (o) {
                case PUSH:
                    stack.emplace_back(matrices[m_i]->value());
                    ++m_i;
                    break;
                case KRON:
                    tmp = stack.size() - 2;
                    stack[tmp] = stack[tmp] * stack.back();
                    stack.pop_back();
                    break;
            };
        }
        /* assert(stack.size() == 1); */
        size_t r = compute_row();
        size_t c = compute_col();
        /*MSG_DEBUG("row=" << r << " col=" << c << " ag=" << stack.back());*/
        result(r, c) = stack.back();
    }

    void compute_result()
    {
        result.resize(row_strides.back(), col_strides.back());
        /*MSG_DEBUG(matrices);*/
        /*reset();*/
        do {
            compute_cell();
        } while (next_cell());
        /*if (strides.back() < 1024) {*/
            /*result = lump_gen(result);*/
        /*}*/
    }
};


typedef std::map<std::string, const generation_rs*> generation_table;
struct kron_component_description_base;
typedef std::vector<std::pair<std::string, size_t>> individual_list;


/*
bool operator < (const std::pair<std::string, size_t>& p1, const std::pair<std::string, size_t>& p2)
{
    return p1.first < p2.first || (p1.first == p2.first && p1.second < p2.second);
}
//*/

template <typename MATRIX_TYPE>
struct kron_builder {
    std::vector<std::shared_ptr<iterable_generation_base<MATRIX_TYPE>>> iterators;
    std::map<std::string, std::shared_ptr<iterable_generation_base<MATRIX_TYPE>>> proxy_targets;
    std::vector<Operation> operations;
    void push_iterator(const MATRIX_TYPE* gen, const std::string& name)
    {
        if (name == "") {
            iterators.emplace_back(std::make_shared<iterable_generation<MATRIX_TYPE>>(&*gen));
        } else {
            auto it = proxy_targets.find(name);
            if (it == proxy_targets.end()) {
                iterators.emplace_back(std::make_shared<iterable_generation<MATRIX_TYPE>>(&*gen));
                proxy_targets[name] = iterators.back();
            } else {
                iterators.emplace_back(std::make_shared<generation_proxy<MATRIX_TYPE>>(it->second));
            }
        }
    }
    void push_gamete() { iterators.emplace_back(std::make_shared<generation_gamete<MATRIX_TYPE>>()); }
    void push_doubling_gamete() { iterators.emplace_back(std::make_shared<generation_doubling_gamete<MATRIX_TYPE>>()); }
    void op_push() { operations.push_back(PUSH); }
    void op_kron() { operations.push_back(KRON); }
};


template <>
struct kron_builder<VectorLC> {
    std::vector<std::shared_ptr<iterable_generation_base<VectorLC>>> iterators;
    std::map<std::string, std::shared_ptr<iterable_generation_base<VectorLC>>> proxy_targets;
    std::vector<Operation> operations;
    void push_iterator(const VectorLC& gen, const std::string& name)
    {
        if (name == "") {
            iterators.emplace_back(std::make_shared<iterable_generation<VectorLC>>(gen));
        } else {
            auto it = proxy_targets.find(name);
            if (it == proxy_targets.end()) {
                iterators.emplace_back(std::make_shared<iterable_generation<VectorLC>>(gen));
                proxy_targets[name] = iterators.back();
            } else {
                iterators.emplace_back(std::make_shared<generation_proxy<VectorLC>>(it->second));
            }
        }
    }
    void push_gamete() { iterators.emplace_back(std::make_shared<generation_gamete<VectorLC>>()); }
    void push_doubling_gamete() { iterators.emplace_back(std::make_shared<generation_doubling_gamete<VectorLC>>()); }
    void op_push() { operations.push_back(PUSH); }
    void op_kron() { operations.push_back(KRON); }
};


template <typename MATRIX_TYPE>
std::ostream& operator << (std::ostream& os, const kron_builder<MATRIX_TYPE>& kb)
{
    int i = -1;
    for (Operation o: kb.operations) {
        switch (o) {
            case PUSH:
                os << " PUSH ";
                ++i;
                if (dynamic_cast<const generation_gamete<MATRIX_TYPE>*>(&*kb.iterators[i])) {
                    os << "g";
                } else if (dynamic_cast<const generation_doubling_gamete<MATRIX_TYPE>*>(&*kb.iterators[i])) {
                    os << "d";
                } else if (dynamic_cast<const iterable_generation<MATRIX_TYPE>*>(&*kb.iterators[i])) {
                    os << "i";
                } else if (dynamic_cast<const generation_proxy<MATRIX_TYPE>*>(&*kb.iterators[i])) {
                    os << "p";
                } else {
                    os << '?';
                }
                break;
            case KRON:
                os << " KRON";
                break;
        };
    }
    return os;
}

#if 0
struct kb_base {
    virtual ~kb_base() {}
    virtual void mark_constraint(const branch_type& b, size_t i, size_t constraint_num, bool proxy) = 0;
    virtual kb_base* clone() = 0;
};

struct kb_individual : public kb_base {
    std::string gen_name;
    bool is_proxy;
    kb_individual(const std::string& g, size_t i) : gen_name(g), index(i) {}
    kb_individual(const kb_individual& other) : gen_name(other.gen_name), index(other.index) {}
    void mark_constraint(const branch_type& b, size_t i, size_t constraint_num, bool proxy) {}
    kb_base* clone() {}
};

struct kb_cross : public kb_individual {
    bool expand;
    kb_base* b1;
    kb_base* b2;
    kb_cross(const std::string& g, size_t i, kb_base* k1, kb_base* k2) : kb_individual(g, i), expand(false), b1(k1), b2(k2) {}
    kb_cross(const kb_cross& other)
        : kb_individual(other), expand(false), b1(other.b1->clone()), b2(other.b2->clone())
    {}
    kb_base* clone() { return new kb_cross(*this); }
    void mark_constraint(const branch_type& b, size_t i, size_t constraint_num, bool proxy)
    {
        if (i < b.size()) {
            expand = true;
            if (b[i]) {
                b2->mark_constraint(b, i + 1, proxy);
            } else {
                b1->mark_constraint(b, i + 1, proxy);
            }
        } else {
        }
    }
    void make(kron_builder& kb) const
    {
        if (expand) {
            b1->make(kb);
            kb.push_gamete();
            kb.op_kron();
            b2->make(kb);
            kb.push_gamete();
            kb.op_kron();
            kb.op_kron();
        } else {
            kb.push_iterator(gen_name
        }
    }
};
#endif

struct generation_computer_type;


void dump_mat(const generation_rs* grs)
{
    /*GenoMatrix lum = lump_gen(tmp);*/
    /*MSG_DEBUG("=========================================================");*/
    /*MSG_DEBUG("RAW " << MATRIX_SIZE(tmp));*/
    /*MSG_DEBUG(tmp);*/
    /*MSG_DEBUG("LUMPED " << MATRIX_SIZE(lum));*/
    /*MSG_DEBUG(lum);*/
    /*MSG_DEBUG("ROWSUMS " << lum.rowwise().sum().transpose());*/
    /*MSG_DEBUG("COLSUMS " << lum.colwise().sum());*/
    /*MSG_DEBUG("=========================================================");*/
    /*generation_rs* grs = generation_rs::from_matrix(name, tmp);*/
    MSG_DEBUG((*grs));
#if 0
    if (grs->name.size() < 60) {
        return;
    }
    std::map<char, VectorXd> LVtable;
    allele_pair A = {'a', 'a'};
    allele_pair B = {'b', 'b'};
    LVtable['A'] = VectorXd::Zero(grs->main_process().innerSize());
    LVtable['B'] = VectorXd::Zero(grs->main_process().innerSize());
    LVtable['H'] = VectorXd::Zero(grs->main_process().innerSize());
    for (size_t i = 0; i < grs->unique_labels.size(); ++i) {
        allele_pair ap = grs->unique_labels[i];
        if (ap == A) {
            LVtable['A'] = grs->unique_unphased_LV[i];
        } else if (ap == B) {
            LVtable['B'] = grs->unique_unphased_LV[i];
        } else {
            LVtable['H'] += grs->unique_unphased_LV[i];
        }
    }
    for (const auto& kv: LVtable) {
        MSG_DEBUG(kv.first << " => " << kv.second.transpose());
    }
    std::vector<char> obs = {'A', 'H', 'B'};
    chromosome chr = {
        "test_vs_sim",
        {"M_1_1", "M_1_9", "M_1_100"},
        {0., 20., 100.}
    };
    qtl_chromosome qtl_chr(&chr);
    auto sc = grs->segment_computer(&qtl_chr, 1, 0);
    MatrixXd LV(grs->main_process().innerSize(), 3);
    for (char m1: obs) {
        for (char m2: obs) {
            for (char m3: obs) {
                LV.col(0) = LVtable[m1];
                LV.col(1) = LVtable[m2];
                LV.col(2) = LVtable[m3];
                auto probs = sc.compute(LV);
                /*MSG_DEBUG("############# " << m1 << m2 << m3);*/
                /*MSG_DEBUG(probs.transpose());*/
                ofile o(SPELL_STRING("spell_" << grs->name << '_' << m1 << m2 << m3 << ".txt"));
                o << probs.transpose();
            }
        }
    }
#endif
}


#if 0
void test_kron()
{
    GenoMatrix A(1, 1); A(0, 0) = {{{{'a'}, {'a'}}, {{'a'}, {'a'}}}, algebraic_genotype::Type::Genotype,
                                   fast_polynom::one};
    GenoMatrix B(1, 1); B(0, 0) = {{{{'b'}, {'b'}}, {{'b'}, {'b'}}}, algebraic_genotype::Type::Genotype,
                                   fast_polynom::one};
    std::shared_ptr<iterable_generation_base<GenoMatrix>> ia(new iterable_generation<GenoMatrix>(&A));
    std::shared_ptr<iterable_generation_base<GenoMatrix>> ib(new iterable_generation<GenoMatrix>(&B));
    std::shared_ptr<iterable_generation_base<GenoMatrix>> ig1(new generation_gamete<GenoMatrix>());
    std::shared_ptr<iterable_generation_base<GenoMatrix>> ig2(new generation_gamete<GenoMatrix>());
    std::vector<std::shared_ptr<iterable_generation_base<GenoMatrix>>> mv = {ia, ig1, ib, ig2};
    std::vector<Operation> ov = {PUSH, PUSH, KRON, PUSH, PUSH, KRON, KRON};
    GenoMatrix f1 = generation_kronecker<GenoMatrix>(mv, ov).result;
    MSG_DEBUG("F1");
    dump_mat("F1", f1);;
    f1 = lump_gen(f1);

    std::shared_ptr<iterable_generation_base<GenoMatrix>> if11(new iterable_generation<GenoMatrix>(&f1));
    std::shared_ptr<iterable_generation_base<GenoMatrix>> if12(new iterable_generation<GenoMatrix>(&f1));
    mv = {if11, ig1, if12, ig2};
    ov = {PUSH, PUSH, KRON, PUSH, PUSH, KRON, KRON};
    GenoMatrix f2 = generation_kronecker<GenoMatrix>(mv, ov).result;
    MSG_DEBUG("F2");
    dump_mat("F2", f2);;
    f2 = lump_gen(f2);

    std::shared_ptr<iterable_generation_base<GenoMatrix>> if2(new iterable_generation<GenoMatrix>(&f2));
    std::shared_ptr<iterable_generation_base<GenoMatrix>> pf2(new generation_proxy<GenoMatrix>(if2));
    mv = {if2, ig1, pf2, ig2};
    ov = {PUSH, PUSH, KRON, PUSH, PUSH, KRON, KRON};
    GenoMatrix f3 = generation_kronecker<GenoMatrix>(mv, ov).result;
    MSG_DEBUG("F3");
    dump_mat("F3", f3);;
    f3 = lump_gen(f3);

#if 0
    std::shared_ptr<iterable_generation_base> if21(new iterable_generation(&f2));
    std::shared_ptr<iterable_generation_base> if22(new iterable_generation(&f2));
    std::shared_ptr<iterable_generation_base> pf21(new generation_proxy(if21));
    std::shared_ptr<iterable_generation_base> pf22(new generation_proxy(if22));
    std::shared_ptr<iterable_generation_base> ig3(new generation_gamete());
    std::shared_ptr<iterable_generation_base> ig4(new generation_gamete());
    std::shared_ptr<iterable_generation_base> ig5(new generation_gamete());
    std::shared_ptr<iterable_generation_base> ig6(new generation_gamete());
    mv = {if21, ig1, if22, ig2,
          ig3,
          pf21, ig4, pf22, ig5,
          ig6
    };
    ov = {PUSH, PUSH, KRON, PUSH, PUSH, KRON, KRON,
          PUSH, KRON,
          PUSH, PUSH, KRON, PUSH, PUSH, KRON, KRON,
          PUSH, KRON,
          KRON
    };
    GenoMatrix f4 = generation_kronecker(mv, ov).result;
    MSG_DEBUG("F4");
    dump_mat("F4", f4);;
    f4 = lump_gen(f4);
#endif
}
#endif







template <typename Arg>
int read_field(std::stringstream& s, char sep, Arg& arg)
{
    std::string field;
    std::getline(s, field, sep);
    /*MSG_DEBUG("CSV FIELD |" << field << "|");*/
    std::stringstream ss(field);
    ss >> arg;
    return 0;
}


#define do_with_arg_pack(_expr) do { using _ = int[]; (void)_{0, ((_expr), void(), 0)...}; } while(0)

template <typename... Args>
void read_csv_line(std::istream& is, char sep, Args&... args)
{
    std::string line;
    std::getline(is, line);
    /*MSG_DEBUG("CSV LINE |" << line << "|");*/
    std::stringstream ss(line);
    do_with_arg_pack(read_field(ss, sep, args));
}


struct pedigree_item {
    std::string gen_name;
    int id;
    int p1;
    int p2;

    pedigree_item(const char* gn, int i, int a, int b)
        : gen_name(gn), id(i), p1(a), p2(b)
    {}

    pedigree_item(std::istream& is, char field_sep=';')
        : gen_name()
    {
        id = p1 = p2 = 0;
        read_csv_line(is, field_sep, gen_name, id, p1, p2);
        if (id == p1 && id == p2 && id == 0) {
            return;
        }
        if (id <= p1 || id <= p2) {
            throw std::runtime_error("Bad ID! ID must be greater than p1 AND p2");
            /*MSG_DEBUG("BAD ID!! " << id << " must be greater than " << p1 << " AND " << p2);*/
        }
    }

    bool is_ancestor() const { return p1 == 0 && p2 == 0; }
    bool is_self() const { return p1 > 0 && p1 == p2; }
    bool is_cross() const { return p1 > 0 && p2 > 0 && p1 != p2; }
    bool is_dh() const { return p1 != p2 && p1 >= 0 && p2 >= 0 && (p1 * p2) == 0; }
    bool is_bullshit() const { return !(is_ancestor() || is_self() || is_cross() || is_dh()); }
    bool is_delete() const { return id == -1; }
};


std::vector<pedigree_item>
read_csv(const std::string& pedigree_file, char field_sep=';')
{
    std::vector<pedigree_item> ret;
    ifile pef(pedigree_file);
    if (!pef.good()) {
        MSG_DEBUG("prout");
    }
    std::string col_name, col_id, col_p1, col_p2;
    read_csv_line(pef, field_sep, col_name, col_id, col_p1, col_p2);
    /*MSG_DEBUG("col_name=" << col_name << " col_id=" << col_id << " col_p1=" << col_p1 << " col_p2=" << col_p2);*/
    while (!pef.eof()) {
        ret.emplace_back(pef, field_sep);
        if (ret.back().id == 0) {
            ret.pop_back();
        }
    }
    return ret;
}


std::vector<int> merge(const std::vector<int>& s1, const std::vector<int>& s2)
{
    std::vector<int> ret(s1.size() + s2.size());
    auto it = std::set_union(s1.begin(), s1.end(), s2.begin(), s2.end(), ret.begin());
    ret.resize(it - ret.begin());
    return ret;
}


struct ascendant_categories {
    std::vector<int> bound;
    std::vector<int> free;
    ascendant_categories(int m, const std::vector<int>& M, int p, const std::vector<int>& P)
        : bound(M.size() + P.size() + 2), free(M.size() + P.size() + 2)
    {
        std::vector<int> Mi = M;
        if (m) {
            Mi.push_back(m);
        }
        std::vector<int> Pi = P;
        if (p) {
            Pi.push_back(p);
        }
        auto bi = std::set_intersection(Mi.begin(), Mi.end(), Pi.begin(), Pi.end(), bound.begin());
        bound.resize(bi - bound.begin());
        auto fi = std::set_symmetric_difference(Mi.begin(), Mi.end(), Pi.begin(), Pi.end(), free.begin());
        free.resize(fi - free.begin());
    }
};


typedef std::map<int, std::pair<int, int>> genealogy_type;
typedef std::map<int, std::vector<int>> ancestor_set_map_type;
typedef boost::dynamic_bitset<> branch_type;

bool find_branch_rec(int start, const std::set<int>& target, const genealogy_type& genealogy, int depth, branch_type& path)
{
    const auto& p = genealogy.find(start)->second;
    if (target.find(p.first) != target.end()) {
        path.resize(depth + 1);
        path.reset(depth);
        return true;
    } else if (target.find(p.second) != target.end()) {
        path.resize(depth + 1);
        path.set(depth);
        return true;
    } else if (find_branch_rec(p.first, target, genealogy, depth + 1, path)) {
        path.reset(depth);
        return true;
    } else if (find_branch_rec(p.second, target, genealogy, depth + 1, path)) {
        path.set(depth);
        return true;
    }
    return false;
}


bool find_branch_rec(int start, int target, const genealogy_type& genealogy, int depth, branch_type& path)
{
    if (start == 0) {
        return false;
    }
    const auto& p = genealogy.find(start)->second;
    if (target == p.first) {
        path.resize(depth + 1);
        path.reset(depth);
        return true;
    } else if (target == p.second) {
        path.resize(depth + 1);
        path.set(depth);
        return true;
    } else if (find_branch_rec(p.first, target, genealogy, depth + 1, path)) {
        path.reset(depth);
        return true;
    } else if (find_branch_rec(p.second, target, genealogy, depth + 1, path)) {
        path.set(depth);
        return true;
    }
    return false;
}


branch_type find_branch(int start, const std::set<int>& target, const genealogy_type& genealogy)
{
    branch_type branch;
    find_branch_rec(start, target, genealogy, 0, branch);
    return branch;
}

branch_type find_branch(int start, int target, const genealogy_type& genealogy, bool init)
{
    branch_type branch;
    if (start == target) {
        branch.resize(1);
        branch.set(0, init);
    } else {
        find_branch_rec(start, target, genealogy, 1, branch);
        branch.set(0, init);
    }
    return branch;
}

std::ostream& operator << (std::ostream& os, const branch_type& b)
{
    /*for (size_t i = b.find_first(); i != branch_type::npos; i = b.find_next(i)) {*/
    for (size_t i = 0; i < b.size(); ++i) {
        os << (b[i] ? 'P' : 'M');
    }
    return os;
}


/*std::ostream& operator << (std::ostream& os, const std::vector<int>& v) { auto i = v.begin(), j = v.end(); if (i != j) { os << (*i); for (++i; i != j; ++i) { os << ' ' << (*i); } } return os; }*/


std::vector<int> keep_only_nearest_degree(const ancestor_set_map_type& ancestor_sets, const std::vector<int>& set)
{
    std::vector<int> all_ancestors;
    /*if (p1 == p2) { all_ancestors = {p1}; } else if (p1 < p2) { all_ancestors = {p1, p2}; } else { all_ancestors = {p2, p1}; }*/
    for (int a: set) {
        /*MSG_DEBUG("JOINING SUBTREE FOR " << a);*/
        std::vector<int> tmp(ancestor_sets.size());
        const auto& as = ancestor_sets.find(a)->second;
        /*MSG_DEBUG(as.size());*/
        auto it = std::set_union(as.begin(), as.end(), all_ancestors.begin(), all_ancestors.end(), tmp.begin());
        /*MSG_DEBUG("TMP SIZE IS " << (it - tmp.begin()));*/
        tmp.resize(it - tmp.begin());
        all_ancestors.swap(tmp);
    }
    MSG_DEBUG("all_ancestors " << all_ancestors);

    /* diff with set of ancestors */
    std::vector<int> tmp(ancestor_sets.size());
    auto it = std::set_difference(set.begin(), set.end(), all_ancestors.begin(), all_ancestors.end(), tmp.begin());
    tmp.resize(it - tmp.begin());
    return tmp;
}


struct individual_constraints {
    std::vector<std::pair<branch_type, branch_type>> constraints;
    individual_constraints(const genealogy_type& genealogy, const ancestor_set_map_type& ancestor_sets, int p1, int p2)
        : constraints()
    {
        ascendant_categories ac(p1, ancestor_sets.find(p1)->second, p2, ancestor_sets.find(p2)->second);
        /* pick only "first-degree" common and free ancestors */
        MSG_DEBUG("ANCESTORS OF " << p1 << ": " << ancestor_sets.find(p1)->second);
        MSG_DEBUG("ANCESTORS OF " << p2 << ": " << ancestor_sets.find(p2)->second);
        MSG_DEBUG("ac.free " << ac.free);
        MSG_DEBUG("ac.bound " << ac.bound);
        ac.bound = keep_only_nearest_degree(ancestor_sets, ac.bound);
        ac.free = keep_only_nearest_degree(ancestor_sets, ac.free);
        MSG_DEBUG("nearest ac.free " << ac.free);
        MSG_DEBUG("nearest ac.bound " << ac.bound);
        /* compute constraints */
        for (int a: ac.bound) {
            constraints.emplace_back(find_branch(p1, a, genealogy, false), find_branch(p2, a, genealogy, true));
        }
    }

    individual_constraints() : constraints() {}

    individual_constraints(const std::vector<std::pair<std::string, std::string>>& cons)
        : constraints()
    {
        /* TODO: compute constraint bitsets from strings */
        constraints.resize(cons.size());
        for (size_t c = 0; c < cons.size(); ++c) {
            const auto& ps = cons[c];
            branch_type& b1 = constraints[c].first;
            branch_type& b2 = constraints[c].second;
            b1.resize(ps.first.size());
            b2.resize(ps.second.size());
            for (size_t i = 0; i < ps.first.size(); ++i) { b1.set(i, ps.first[i] == 'P'); }
            for (size_t i = 0; i < ps.second.size(); ++i) { b2.set(i, ps.second[i] == 'P'); }
        }
    }

    friend std::ostream& operator << (std::ostream& os, const individual_constraints& ic)
    {
        if (ic.constraints.size() > 0) {
            auto i = ic.constraints.begin();
            auto j = ic.constraints.end();
            os << i->first << '=' << i->second;
            for (++i; i != j; ++i) {
                os << ", " << i->first << '=' << i->second;
            }
        }
        return os;
    }
};


enum CrossType { Ancestor, Cross, DH };

struct generation_spec_type : individual_constraints {
    std::string name;
    CrossType type;
    std::string fp1, fp2;
    generation_spec_type(const std::string& n, const genealogy_type& genealogy, const ancestor_set_map_type& ancestor_sets, int p1, int p2, const std::map<int, std::string>& family_by_id)
        : individual_constraints(genealogy, ancestor_sets, p1, p2)
        , name(n)
        , type(p2 == 0 ? CrossType::DH : CrossType::Cross)
        , fp1(family_by_id.find(p1)->second), fp2(family_by_id.find(p2)->second)
    {}

    generation_spec_type(const std::string& n, char haplo)
        : individual_constraints()
        , name(n)
        , type(CrossType::Ancestor), fp1(std::string() + haplo), fp2()
    {}
};


typedef std::map<std::string, generation_spec_type> design_type;



struct gen_tree_node_type;
typedef std::shared_ptr<gen_tree_node_type> gen_tree_node;
struct gen_tree_node_type {
    std::string gen_name;
    gen_tree_node p1, p2;
    const generation_rs* pop;
    std::string name;
};

template <typename... Args>
gen_tree_node make_tree_node(Args&&... args) { return std::make_shared<gen_tree_node_type>(args...); }

gen_tree_node make_tree(const design_type& design, const generation_table& generations, const std::string& entrypoint, bool do_constraints=false)
{
    gen_tree_node ret = make_tree_node();
    const auto& d = design.find(entrypoint)->second;
    ret->gen_name = d.name;
    auto it = generations.find(entrypoint);
    if (it != generations.end() && it->second) {
        ret->pop = it->second;
        MSG_DEBUG("INIT POP " << entrypoint);
        MSG_DEBUG(it->second);
    } else {
        ret->pop = NULL;
        ret->p1 = make_tree(design, generations, d.fp1);
        ret->p2 = make_tree(design, generations, d.fp2);
    }

    if (!do_constraints) {
        return ret;
    }

    int cons_index = 0;

    auto follow_branch = [&] (const branch_type& br)
    {
        gen_tree_node ptr = ret;
        for (size_t i = 0; i < br.size(); ++i) {
            const auto& ptrdesign = design.find(ptr->gen_name)->second;
            if (br[i]) {
                /* expand father */
                if (!ptr->p2) {
                    ptr->p2 = make_tree(design, generations, ptrdesign.fp2, false);
                }
                if (!ptr->p1) {
                    ptr->p1 = make_tree(design, generations, ptrdesign.fp1, false);
                }
                ptr = ptr->p2;
            } else {
                /* expand mother */
                if (!ptr->p1) {
                    ptr->p1 = make_tree(design, generations, ptrdesign.fp1, false);
                }
                if (!ptr->p2) {
                    ptr->p2 = make_tree(design, generations, ptrdesign.fp2, false);
                }
                ptr = ptr->p1;
            }
        }
        ptr->name = SPELL_STRING("constraint#" << cons_index);
    };

    for (const auto& cp: d.constraints) {
        follow_branch(cp.first);
        follow_branch(cp.second);
        ++cons_index;
    }
    return ret;
}


template <typename MATRIX_TYPE> struct node_data_getter;
template <> struct node_data_getter<GenoMatrix> { const GenoMatrix* operator () (gen_tree_node n, size_t) const { return n->pop ? &n->pop->main_process().data : NULL; } };
template <> struct node_data_getter<VectorLC> { VectorLC operator () (gen_tree_node n, size_t i) const { return n->pop->raw_lincomb(i); } };


template <typename MATRIX_TYPE>
size_t rec_build_kron(gen_tree_node node, kron_builder<MATRIX_TYPE>& kb, size_t parent_i = 0)
{
    if (node->p1 && node->p2) {
        parent_i = rec_build_kron(node->p1, kb, parent_i);
        kb.push_gamete();
        kb.op_push();
        kb.op_kron();
        parent_i = rec_build_kron(node->p2, kb, parent_i);
        kb.push_gamete();
        kb.op_push();
        kb.op_kron();
        kb.op_kron();
    } else {
        kb.push_iterator(node_data_getter<MATRIX_TYPE>()(node, parent_i), node->name);
        kb.op_push();
    }
    return parent_i + 1;
}


std::ostream& operator << (std::ostream& os, gen_tree_node node)
{
    if (node->p1 && node->p2) {
        os << '(' << node->p1 << " ⊗ G ⊗ " << node->p2 << " ⊗ G)";
    } else {
        os << node->gen_name;
    }
    if (node->name != "") {
        os << '#' << node->name;
    }
    return os;
}


generation_rs* tree_to_kron(const std::string& name, gen_tree_node node)
{
    MSG_DEBUG("TREE " << node);
    MSG_QUEUE_FLUSH();
    kron_builder<GenoMatrix> kb;
    kron_builder<VectorLC> kb_lc;
    rec_build_kron(node, kb);
    MSG_DEBUG("KB" << kb);
    MSG_QUEUE_FLUSH();
    rec_build_kron(node, kb_lc);
    auto ret = generation_kronecker<GenoMatrix>(kb.iterators, kb.operations).result;
    VectorLC lc = generation_kronecker<VectorLC>(kb_lc.iterators, kb_lc.operations).result;
    MSG_DEBUG("COMPUTED LC");
    MSG_DEBUG(lc.transpose());
    return generation_rs::from_matrix(name, ret, lc);
}


std::ostream& operator << (std::ostream& os, const std::set<std::string>& nl)
{
    os << '{';
    if (nl.size()) {
        auto i = nl.begin();
        auto j = nl.end();
        os << (*i);
        for (++i; i != j; ++i) {
            os << ' ' << (*i);
        }
    }
    return os << '}';
}

GenoMatrix
create_ancestor(char haplo) {
    GenoMatrix tmp(1, 1);
    tmp(0, 0) = {{{{haplo}, {haplo}}, {{haplo}, {haplo}}},
        algebraic_genotype::Type::Genotype,
        fast_polynom::one};
    return tmp;
}


void
pedigree_analysis(const std::vector<pedigree_item>& pedigree, design_type& design, generation_table& generations)
{
    genealogy_type genealogy = {{0, {0, 0}}};
    ancestor_set_map_type ancestor_sets = {{0, {}}};
    std::map<int, int> depth;
    std::map<int, std::string> family_by_id;
    std::string fname;
    auto new_gen = [&]() { return design.find(fname) == design.end(); };
    char haplo = 'a';

    std::set<std::string> families;
    std::set<std::string> nl;

    for (const auto& pi: pedigree) {
        genealogy[pi.id] = {pi.p1, pi.p2};
        MSG_DEBUG("INDIVIDUAL " << pi.id << " / " << pi.p1 << ',' << pi.p2);
        scoped_indent _si;
        individual_constraints ic(genealogy, ancestor_sets, pi.p1, pi.p2);
        MSG_DEBUG("constraints for " << pi.id << ": " << ic);
        if (pi.is_delete()) {
            generations.erase(family_by_id[pi.p1]);
        } if (pi.is_ancestor()) {
            ancestor_sets.insert({pi.id, {}});
            fname = family_by_id[pi.id] = pi.gen_name;
            if (new_gen()) {
                const auto& this_design = design.insert({fname, {fname, haplo}}).first->second;
                nl.clear();
                /*generations[fname] = generation_rs::from_matrix(fname, create_ancestor(haplo));*/
                generations[fname] = generation_rs::ancestor(fname, haplo, {{0, 0}});
                dump_mat(generations[fname]);
                ++haplo;
            }
        } else if (pi.is_self()) {
            ancestor_sets.insert({pi.id, merge(ancestor_sets[pi.p1], {pi.p1})});
            fname = family_by_id[pi.id] = SPELL_STRING('S' << family_by_id[pi.p1]);
            if (new_gen()) {
                const auto& this_design = design.insert({fname, {fname, genealogy, ancestor_sets, pi.p1, pi.p2, family_by_id}}).first->second;
                nl.clear();
                generations[fname] = tree_to_kron(fname, make_tree(design, generations, fname, true));
                dump_mat(generations[fname]);
            }
        } else if (pi.is_dh()) {
            ancestor_sets.insert({pi.id, merge(ancestor_sets[pi.p1], {pi.p1})});
            fname = family_by_id[pi.id] = SPELL_STRING("D" << family_by_id[pi.p1]);
            if (new_gen()) {
                /*design[fname] = design[family_by_id[pi.p1]]->to_doubled_haploid(fname);*/
                const auto& this_design = design.insert({fname, {fname, genealogy, ancestor_sets, pi.p1, pi.p2, family_by_id}}).first->second;
                nl.clear();
                /*generations[fname] = generation_computer_type(design, this_design, generations, nl).gen;*/
                generations[fname] = tree_to_kron(fname, make_tree(design, generations, fname, true));
                dump_mat(generations[fname]);
            }
        } else if (pi.is_cross()) {
            int a, b;
            a = pi.p1 < pi.p2 ? pi.p1 : pi.p2;
            b = pi.p1 + pi.p2 - a;
            ancestor_sets.insert({pi.id, merge(ancestor_sets[pi.p1], merge(ancestor_sets[pi.p2], {a, b}))});
            if (ic.constraints.size()) {
                fname = family_by_id[pi.id] = SPELL_STRING('(' << family_by_id[pi.p1] << " * " << family_by_id[pi.p2] << ')' << '[' << ic << ']');
                const auto& this_design = design.insert({fname, {fname, genealogy, ancestor_sets, pi.p1, pi.p2, family_by_id}}).first->second;
                nl.clear();
                /*generations[fname] = generation_computer_type(design, this_design, generations, nl).gen;*/
                generations[fname] = tree_to_kron(fname, make_tree(design, generations, fname, true));
                dump_mat(generations[fname]);
            } else {
                fname = family_by_id[pi.id] = SPELL_STRING('(' << family_by_id[pi.p1] << " * " << family_by_id[pi.p2] << ')');
                const auto& this_design = design.insert({fname, {fname, genealogy, ancestor_sets, pi.p1, pi.p2, family_by_id}}).first->second;
                nl.clear();
                /*generations[fname] = generation_computer_type(design, this_design, generations, nl).gen;*/
                generations[fname] = tree_to_kron(fname, make_tree(design, generations, fname, true));
                dump_mat(generations[fname]);
            }
            if (new_gen()) {
                /*design[fname] = design[family_by_id[pi.p1]]->crossing(fname, design[family_by_id[pi.p2]]);*/
            }
        }
        families.insert(fname);
    }
    MSG_DEBUG("FAMILIES");
    for (const std::string& f: families) {
        MSG_DEBUG(" * " << f);
        MSG_DEBUG((*generations[f]));
    }
}


int main(int argc, char** argv)
{
    /*generation_rs::clear_dict();*/
    /*test_kron();*/
    std::vector<pedigree_item> ped = {
        {"B", 2, 0, 0},
        {"A", 1, 0, 0},
        {"F1", 3, 1, 2},
        {"F1", 4, 1, 2},
        {"F2", 5, 3, 4},
        {"F2", 6, 3, 4},
        {"F3", 7, 5, 6},
        {"F3", 8, 5, 6},
        {"F3", 9, 5, 5},
        /*{"F4", 10, 9, 9},*/
        /*{"del A", -1, 1, 0},*/
        /*{"del B", -1, 2, 0},*/
        {"F4", 20, 7, 8},
        /*{"F4", 21, 7, 8},*/
        /*{"F5", 30, 20, 21},*/
        /*{"F5", 31, 20, 21},*/
        /*{"F6", 40, 30, 31},*/
    };
    {
        /*generation_rs::clear_dict();*/
        design_type design;
        generation_table generations;
        pedigree_analysis(ped, design, generations);
    }
    return 0;
    {
        (void) argc;
        auto ped = read_csv(argv[1]);
        design_type design;
        generation_table generations;
        pedigree_analysis(ped, design, generations);
    }
    return 0;
}

