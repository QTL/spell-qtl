#if defined(_SPEL_FAST_POLY_H_) && !defined(_SPEL_FAST_F_POLY_H_)
#define _SPEL_FAST_F_POLY_H_

namespace impl {

    inline f_polynom::f_polynom(const f_polynom& f)
        : r_exp(f.r_exp), s_exp(f.s_exp), P(f.P)
    {DEBUG_SCOPE;}

    inline f_polynom::f_polynom(f_polynom&& f)
        : r_exp(f.r_exp), s_exp(f.s_exp), P(std::forward<std::vector<coef_t>&&>(f.P))
    {DEBUG_SCOPE;}

    inline f_polynom::f_polynom(int r, int s)
        : r_exp(r), s_exp(s), P({1})
        {DEBUG_SCOPE;}

    inline f_polynom::f_polynom(int r, int s, size_t n)
        : r_exp(r), s_exp(s), P()
    {DEBUG_SCOPE; P.resize(n); }

    inline f_polynom::f_polynom(int r, int s, std::initializer_list<coef_t> x)
        : r_exp(r), s_exp(s), P(x)
    {DEBUG_SCOPE;}

    inline f_polynom::f_polynom(int r, int s, const std::vector<coef_t>& x)
        : r_exp(r), s_exp(s), P(x)
          /*{DEBUG_SCOPE; std::cout << (*this) << std::endl; }*/
    {DEBUG_SCOPE;}

    inline bool f_polynom::operator == (const f_polynom& f) const
    {DEBUG_SCOPE;
        bool ok = r_exp == f.r_exp && s_exp == f.s_exp && P.size() == f.P.size();
#if 0
        auto ai = P.begin(), aj = P.end(), bi = f.P.begin();
#else
        auto ai = P.rbegin(), aj = P.rend(), bi = f.P.rbegin();
#endif
        while (ok && ai != aj) {
            ok = *ai++ == *bi++;
        }
        return ok;
    }

    inline f_polynom f_polynom::mul(const f_polynom& d) const
    {DEBUG_SCOPE;
        /*std::cout << "f mul !" << std::endl;*/
        f_polynom ret(r_exp + d.r_exp, s_exp + d.s_exp, P.size() + d.P.size() - 1);
        for (size_t i = 0; i < P.size(); ++i) {
            for (size_t j = 0; j < d.P.size(); ++j) {
                ret.P[i + j] += P[i] * d.P[j];
            }
        }
        return ret;
    }

    inline f_polynom f_polynom::div(coef_t scalar) const
    {DEBUG_SCOPE;
        f_polynom ret(r_exp, s_exp, P.size());
        for (size_t i = 0; i < P.size(); ++i) {
            ret.P[i] = P[i] / scalar;
        }
        return ret;
    }

    inline f_polynom f_polynom::add(const f_polynom& f) const
    {DEBUG_SCOPE;
        int min_r = r_exp < f.r_exp ? r_exp : f.r_exp;
        int min_s = s_exp < f.s_exp ? s_exp : f.s_exp;
        int deg1 = s_exp - min_s + P.size();
        int deg2 = f.s_exp - min_s + f.P.size();
        d_polynom sn1 = s_poly_from_exp(s_exp - min_s);
        d_polynom sn2 = s_poly_from_exp(f.s_exp - min_s);
        d_polynom tmp1(r_exp - min_r, deg1);
        d_polynom tmp2(f.r_exp - min_r, deg2);
        tmp1.assign(P.begin(), P.end());
        tmp2.assign(f.P.begin(), f.P.end());
        d_polynom result = tmp1.mul(sn1).add(tmp2.mul(sn2));
        f_polynom ret = result;
        ret.r_exp += min_r;
        ret.s_exp += min_s;
        return ret;
    }

    inline f_polynom& f_polynom::operator = (f_polynom&& d)
    {DEBUG_SCOPE;
        r_exp = d.r_exp;
        s_exp = d.s_exp;
        P = std::move(d.P);
        return *this;
    }

    inline int f_polynom::degree() const
    {
        return r_exp + s_exp + P.size();
    }

    inline f_polynom::operator d_polynom () const
    {DEBUG_SCOPE;
        d_polynom se = s_poly_from_exp(s_exp);
        d_polynom p(P);
        d_polynom ret = se.mul(p);
        /*std::cout << "(converting to d_poly) " << ret << std::endl;*/
        ret.valuation += r_exp;
        /*std::cout << "(converting to d_poly) " << ret << std::endl;*/
        ret.cleanup();
        /*std::cout << "(converting to d_poly) final " << ret << std::endl;*/
        return ret;
    }

    inline bool f_polynom::inf(const f_polynom& f) const
    {
        if (r_exp < f.r_exp
                || (r_exp == f.r_exp
                    && (s_exp < f.s_exp
                        || (s_exp == f.s_exp
                            && P.size() < f.P.size())))) {
            return true;
        }
        if (P.size() == f.P.size()) {
            auto mismatch = std::mismatch(P.begin(), P.end(), f.P.begin());
            return mismatch.first != P.end()
                && mismatch.second != f.P.end()
                && (*mismatch.first < *mismatch.second);
        }
        return false;
    }

    inline std::ostream& f_polynom::serialize(std::ostream& os) const
    {
        os.write(reinterpret_cast<const char*>(&r_exp), sizeof(r_exp));
        os.write(reinterpret_cast<const char*>(&s_exp), sizeof(s_exp));
        size_t n_coef = P.size();
        os.write(reinterpret_cast<const char*>(&n_coef), sizeof(n_coef));
        for (auto c: P) {
            os.write(reinterpret_cast<const char*>(&c), sizeof(c));
        }
        return os;
    }

    inline f_polynom f_polynom::unserialize(std::istream& is)
    {
        int r_exp, s_exp;
        size_t n_coef;
        is.read(reinterpret_cast<char*>(&r_exp), sizeof(r_exp));
        is.read(reinterpret_cast<char*>(&s_exp), sizeof(s_exp));
        is.read(reinterpret_cast<char*>(&n_coef), sizeof(n_coef));
        f_polynom fp(r_exp, s_exp, n_coef);
        fp.P.reserve(n_coef);
        coef_t c;
        for (size_t i = 0; i < n_coef; ++i) {
            is.read(reinterpret_cast<char*>(&c), sizeof(c));
            fp.P.push_back(c);
        }
        return fp;
    }
}

#endif

