#if defined(_SPEL_FAST_POLY_H_) && !defined(_SPEL_FAST_POLY_TABLES_H_)
#define _SPEL_FAST_POLY_TABLES_H_

namespace impl {
    struct registry_t : std::pair<p2i_t, i2p_t> {
        registry_t()
            : std::pair<p2i_t, i2p_t>()
        {DEBUG_SCOPE;
            reset();
        }

        void reset()
        {DEBUG_SCOPE;
            first.clear();
            second.clear();
            polynom zero(d_polynom(), f_polynom(0, 0, 0));
            polynom one(d_polynom({1}), f_polynom(0, 0));
            polynom r(d_polynom({0, 1}), f_polynom(1, 0));
            polynom s(d_polynom({1, -1}), f_polynom(0, 1));
            /*f_polynom finf(100, 100);*/
            /*d_polynom dinf = finf;*/
            /*polynom pinf(d_polynom(f_polynom(100, 100)), f_polynom(100, 100));*/
            /*fast_polynom::infinity = reg(pinf);*/
            fast_polynom::zero = reg(zero);
            fast_polynom::one = reg(one);
            fast_polynom::r = reg(r);
            fast_polynom::s = reg(s);
            fast_polynom::infinity = fast_polynom::one;
        }

        fast_polynom reg(const polynom& p)
        {DEBUG_SCOPE;
            /*std::cout << "registering " << p.to_f() << std::endl;*/
            /*std::cout << "query registry ";*/
            /*if (p.d_init) {*/
                /*std::cout << p.to_d();*/
            /*} else if (p.f_init) {*/
                /*std::cout << p.to_f();*/
            /*}*/
            /*std::cout << " vec.size " << second.size() << std::endl;*/
            auto inserted = first.insert(p2i_t::value_type(p, (fast_polynom) fast_polynom::not_initialized));
            if (inserted.second) {DEBUG_SCOPE;
                /*inserted.first->second = (fast_polynom) second.size();*/
                inserted.first->second.value = second.size();
                second.push_back(&inserted.first->first);
                /*std::cout << " -> regg'ed " << p << " / " << inserted.first->first*/
                    /*<< " as " << ((int)inserted.first->second)*/
                    /*<< " hash " << std::hash<polynom>()(p)*/
                    /*<< " vec.size " << second.size()*/
                    /*<< std::endl;*/
            }
            /*MSG_DEBUG("REG " << p << " VALUE " << inserted.first->second.value);*/
            return inserted.first->second;
        }
    };

    struct polynom_tables {
        static registry_t& registry()
        {DEBUG_SCOPE;
            static registry_t table;
            return table;
        }

        static operation_table_t& mul_table()
        {DEBUG_SCOPE;
            static operation_table_t mult;
            return mult;
        }

        static operation_table_t& o_table()
        {DEBUG_SCOPE;
            static operation_table_t ot;
            return ot;
        }

        static operation_table_t& add_table()
        {DEBUG_SCOPE;
            static operation_table_t addt;
            return addt;
        }

        static operation_table_t& sub_table()
        {DEBUG_SCOPE;
            static operation_table_t subt;
            return subt;
        }

        static operation_table_t& pow_table()
        {DEBUG_SCOPE;
            static operation_table_t powt;
            return powt;
        }

        static fast_polynom reg(const polynom& f)
        {DEBUG_SCOPE;
            /*std::cout << "ptable::registering " << f.to_f() << std::endl;*/
            return registry().reg(f);
        }

        static void reset()
        {
            registry().reset();
            mul_table().clear();
            add_table().clear();
            sub_table().clear();
            pow_table().clear();
            o_table().clear();
        }
    };
}

#endif

