#if defined(_SPEL_FAST_POLY_H_) && !defined(_SPEL_FAST_POLY_FRONTEND_H_)
#define _SPEL_FAST_POLY_FRONTEND_H_


inline fast_polynom::fast_polynom()
    : value(not_initialized)
{DEBUG_SCOPE;}

inline fast_polynom::fast_polynom(const fast_polynom& fp)
    : value(fp.value)
{DEBUG_SCOPE;}

inline fast_polynom::fast_polynom(int v)
    : value(v)
{DEBUG_SCOPE;}

inline bool fast_polynom::operator == (const fast_polynom& f) const
{DEBUG_SCOPE;
    return value == f.value;
}

inline bool fast_polynom::operator < (const fast_polynom& f) const
{DEBUG_SCOPE;
    return value < f.value;
}

inline bool fast_polynom::operator == (int f) const
{DEBUG_SCOPE;
    return value == f;
}

inline fast_polynom::operator const impl::polynom& () const
{
    return *impl::polynom_tables::registry().second[value];
}

inline fast_polynom::operator const impl::f_polynom& () const
{
    return impl::polynom_tables::registry().second[value]->to_f();
}

inline fast_polynom::operator const impl::d_polynom& () const
{
    return impl::polynom_tables::registry().second[value]->to_d();
}

inline bool fast_polynom::inf(const fast_polynom& f) const
{
    return ((const impl::f_polynom&)(*this)).inf(f);
}


inline fast_polynom fast_polynom::operator + (const fast_polynom& q) const
{DEBUG_SCOPE;
    fast_polynom& result = impl::polynom_tables::add_table()[std::minmax(value, q.value)];
    if (result == fast_polynom::not_initialized) {
        const impl::d_polynom& P = *this;
        const impl::d_polynom& Q = q;
        /*std::cout << "P[" << value << "]=" << P << " Q[" << q.value << "]=" << Q << std::endl;*/
        /*impl::d_polynom R = P.add(Q);*/
        /*std::cout << "P+Q=[" << result.value << "] " << R << std::endl;*/
        result = impl::polynom_tables::reg(P.add(Q));
        /*MSG_DEBUG("COMP; " << (*this) << " + " << q << " = " << result);*/
    } else {
        /*const impl::d_polynom& P = *this;*/
        /*const impl::d_polynom& Q = q;*/
        /*std::cout << "(precomputed) P[" << value << "]=" << P << " Q[" << q.value << "]=" << Q << std::endl;*/
        /*std::cout << "(precomputed) P+Q=[" << result.value << "] " << result << std::endl;*/
        /*MSG_DEBUG("PRECOMP; " << (*this) << " + " << q << " = " << result);*/
    }
    return result;
}

inline fast_polynom fast_polynom::operator - (const fast_polynom& q) const
{DEBUG_SCOPE;
    fast_polynom& result = impl::polynom_tables::sub_table()[std::make_pair(value, q.value)];
    if (result == fast_polynom::not_initialized) {
        const impl::d_polynom& P = *this;
        const impl::d_polynom& Q = q;
        result = impl::polynom_tables::reg(P.sub(Q));
        /*MSG_DEBUG("COMP; " << (*this) << " - " << q << " = " << result);*/
    } else {
        /*MSG_DEBUG("PRECOMP; " << (*this) << " - " << q << " = " << result);*/
    }
    return result;
}

inline fast_polynom fast_polynom::operator * (const fast_polynom& q) const
{DEBUG_SCOPE;
    fast_polynom& result = impl::polynom_tables::mul_table()[std::minmax(value, q.value)];
    if (result == fast_polynom::not_initialized) {
        const impl::polynom& P = *this;
        const impl::polynom& Q = q;
        result = impl::polynom_tables::reg(P.mul(Q));
        /*MSG_DEBUG("COMP; " << (*this) << " * " << q << " = " << result);*/
    } else {
        /*MSG_DEBUG("PRECOMP; " << (*this) << " * " << q << " = " << result);*/
    }
    return result;
}

inline fast_polynom fast_polynom::operator / (coef_t scalar) const
{DEBUG_SCOPE;
    impl::f_polynom tmp = *this;
    return tmp.div(scalar);
}

inline fast_polynom fast_polynom:: o(const fast_polynom& p) const
{
    fast_polynom& result = impl::polynom_tables::o_table()[std::minmax(value, p.value)];
    if (result == fast_polynom::not_initialized) {
        const impl::d_polynom& Q = *this;
        result = impl::polynom_tables::reg(Q.o(p));
    }
    return result;
}

inline fast_polynom& fast_polynom::operator += (const fast_polynom& q)
{
    value = (*this) + q;
    return *this;
}

inline fast_polynom& fast_polynom::operator *= (const fast_polynom& q)
{
    value = (*this) * q;
    return *this;
}

inline double fast_polynom::norm() const
{
    return ((const impl::d_polynom&)(*this)).norm();
}

/*inline fast_polynom fast_polynom::operator ^ (int n)*/
inline fast_polynom fast_polynom::pow(int n) const
{DEBUG_SCOPE;
    fast_polynom& result = impl::polynom_tables::pow_table()[impl::operands_t(value, n)];
    if (result == fast_polynom::not_initialized) {
        if (n == 1) {
            result.value = value;
        } else if (n == 0) {
            result.value = fast_polynom::one.value;
        } else {
            fast_polynom prev_result = pow(n - 1);
            result.value = impl::polynom_tables::reg(prev_result * (*this)).value;
        }
    }
    /*std::cout << (*this) << " ^ " << n << " = " << result << std::endl;*/
    return result;
}


inline fast_polynom& fast_polynom::operator = (const impl::f_polynom& f)
{
    value = impl::polynom_tables::reg(impl::polynom(f));
    return *this;
}

inline fast_polynom& fast_polynom::operator = (const impl::d_polynom& d)
{
    value = impl::polynom_tables::reg(impl::polynom(d));
    return *this;
}

inline fast_polynom& fast_polynom::operator = (std::initializer_list<coef_t> ild)
{
    value = impl::polynom_tables::reg(impl::polynom(impl::d_polynom(ild)));
    return *this;
}

inline fast_polynom& fast_polynom::operator = (const std::vector<coef_t>& vd)
{
    value = impl::polynom_tables::reg(impl::polynom(impl::d_polynom(vd)));
    return *this;
}

inline fast_polynom::fast_polynom(std::initializer_list<coef_t> ild)
    : value(impl::polynom_tables::reg(impl::polynom(impl::d_polynom(ild))))
{}

inline fast_polynom::fast_polynom(const impl::d_polynom& d)
    : value(impl::polynom_tables::reg(impl::polynom(impl::d_polynom(d))))
{}

inline fast_polynom::fast_polynom(const impl::f_polynom& f)
    : value(impl::polynom_tables::reg(impl::polynom(impl::f_polynom(f))))
{}

inline int fast_polynom::degree() const
{
    return ((const impl::polynom&)*this).degree();
}

inline int fast_polynom::valuation() const
{
    return ((const impl::polynom&)*this).valuation();
}

inline double fast_polynom::operator () (double x) const
{
    const impl::f_polynom& f = *this;
    double r = ::pow(x, f.r_exp);
    double s = ::pow(1. - x, f.s_exp);
    double ret = f.P.front();
    double x0 = x;
    /*for (int i = f.P.size() - 2; i >= 0; --i) {*/
    size_t sz = f.P.size();
    for (size_t i = 1; i < sz; ++i) {
        ret += x * f.P[i];
        x *= x0;
    }
    return ret * r * s;
}

inline std::ostream& fast_polynom::serialize(std::ostream& os) const
{
    return ((impl::f_polynom)*this).serialize(os);
}

inline fast_polynom unserialize(std::istream& is)
{
    return impl::f_polynom::unserialize(is);
}

#endif

