/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "input.h"
#include "generation_rs.h"
#include <x2c/x2c.h>

namespace x2c {
static inline void from_string(const std::string& s, bool& x)
{
    std::string word;
    std::stringstream(s) >> word;
    if (word == "true" || word == "yes") {
        x = true;
    } else if (word == "false" || word == "no") {
        x = false;
    } else {
        MSG_ERROR("Expected 'true', 'false', 'yes', or 'no' instead of '" << word << "'", "");
        x = false;
    }
}
}


using namespace x2c;


template <typename DTD>
typename attr_func<typename DTD::type>::func_type
make_loader(DTD& dtd)
{
    return [&](const std::string* filename, typename DTD::type* ret)
    {
        if (check_file(*filename, false, false)) {
            MSG_INFO("Processing " << (*filename));
            ifile ifs(*filename);
            auto result = dtd.parse(ifs);
            if (result) {
                *ret = *result;
                delete result;
                ret->filename = *filename;
                return true;
            }
        }
        return false;
    };
}

std::function<bool(const std::string*, population*)>
xml_read_traits = [] (const std::string* filename, population* pop)
{
    pop->observed_traits_filename = *filename;
    bool ret = check_file(*filename, false, false);
    if (ret) {
        ifile ifs(*filename);
        pop->observed_traits = read_data::read_trait(ifs);
    }
    return ret;
};

/*
std::function<bool(const std::string*, bool*)>
convert_bool = [] (const std::string* s, bool* x)
{
    if (*s == "true" || *s == "yes") {
        *x = true;
        return true;
    } else if (*s == "false" || *s == "no") {
        *x = false;
        return true;
    }
    return false;
};

std::function<bool(const std::string*, settings_t*)>
set_pleiotropy = [] (const std::string* s, settings_t* x)
{
    return convert_bool(s, &x->pleiotropy);
};
*/


bool check_true(const std::string* s)
{
    return *s == "true" || *s == "yes";
}

bool check_false(const std::string* s)
{
    return *s == "false" || *s == "no";
}


bool check_mapmaker(const std::string* s)
{
    return *s == "mapmaker";
}


typedef std::pair<std::string, generation_rs*> generation_keyvalue;

struct ancestor_manip {
    char haplo;

    void operator () (generation_keyvalue & g)
    {
        g.second = generation_rs::ancestor(g.first, haplo);
    }
};

struct cross_manip {
    std::string p1;
    std::string p2;

    void operator () (generation_keyvalue & g)
    {
        generation_rs* g1 = generation_rs::get(p1);
        generation_rs* g2 = generation_rs::get(p2);
        g.second = g1->crossing(g.first, g2);
    }
};

struct self_manip {
    std::string p1;

    void operator () (generation_keyvalue & g)
    {
        generation_rs* g1 = generation_rs::get(p1);
        g.second = g1->selfing(g.first);
    }
};

struct ril_manip {
    std::string p1;

    void operator () (generation_keyvalue & g)
    {
        generation_rs* g1 = generation_rs::get(p1);
        std::stringstream s;
        s << "_" << g.first << "_dh";
        g.second = g1->to_doubled_haploid(s.str())->to_ril(g.first);
    }
};

struct dh_manip {
    std::string p1;

    void operator () (generation_keyvalue & g)
    {
        generation_rs* g1 = generation_rs::get(p1);
        g.second = g1->to_doubled_haploid(g.first);
    }
};

DTD_START_WITH_ROOT_NAME(design_dtd, design, "breeding-design", design_type)
    ELEMENT(ancestor, ancestor_manip);
    ELEMENT(generation, generation_keyvalue);
    ELEMENT(cross, cross_manip);
    ELEMENT(self, self_manip);
    ELEMENT(ril, ril_manip);
    ELEMENT(dh, dh_manip);
    ancestor
        = A("haplotype", &ancestor_manip::haplo);
    cross
        = (A("p1", &cross_manip::p1), A("p2", &cross_manip::p2));
    self
        = A("p1", &self_manip::p1);
    ril
        = A("p1", &ril_manip::p1);
    dh
        = A("p1", &dh_manip::p1);
    generation
        = A("name", &generation_keyvalue::first)
        & (E(ancestor) | E(cross) | E(self) | E(ril) | E(dh));
    design
        = A("file", make_loader(*this))
        | E(generation, &design_type::generation);
DTD_END(design_dtd);

/*struct format_specification_t {*/
    /*std::string filename;*/
    /*std::map<std::string, marker_observation_spec> map;*/
/**/
    /*void operator () (settings_t& s)*/
    /*{*/
        /*s.marker_observation_specs_filename = filename;*/
        /*s.marker_observation_specs = map;*/
    /*}*/
/*};*/

typedef std::pair<std::string, marker_observation_spec> format_specification_keyvalue;


struct copy_manip {
    std::string generation;
    std::string from;

    void operator () (format_specification_t& fs)
    {
        fs.map[generation] = fs.map[from];
    }
};


DTD_START_WITH_ROOT_NAME(format_dtd, format_spec, "format-specification", format_specification_t)
    ELEMENT_WITH_NAME(obs_spec, "marker-observations", format_specification_keyvalue);
    ELEMENT(observation, marker_observation_spec::value_type);
    ELEMENT(genotype, allele_pair);
    ELEMENT(copy, copy_manip);

    format_spec
        = A("file", make_loader(*this))
        | (E(obs_spec, &format_specification_t::map) , M(E(copy)));
    obs_spec
        = (A("generation", &format_specification_keyvalue::first),
           A("format", &ignore::entity) / check_mapmaker)
        & E(observation, &format_specification_keyvalue::second);
    observation
        = A("symbol", &marker_observation_spec::value_type::first)
        & E(genotype, &marker_observation_spec::value_type::second);
    genotype
        = chardata();
    copy
        = (A("generation", &copy_manip::generation),
           A("from", &copy_manip::from));
DTD_END(format_dtd);

typedef std::pair<std::string, population> pop_keyvalue;
typedef std::pair<std::string, population_marker_observation> pmo_keyvalue;



struct map_loader_manip {
    std::string filename;

    void operator () (settings_t& s)
    {
        s.map_filename = filename;
        check_file(filename, false, false);
        ifile ifs(filename);
        s.map = read_data::read_map(ifs);
    }
};

struct marker_loader_manip {
    std::string generation;
    std::string filename;

    void operator () (population& p)
    {
        auto& om = p.observed_mark[generation];
        check_file(filename, false, false);
        ifile ifs(filename);
        om.filename = filename;
        om.observations = read_data::read_marker(ifs);
    }
};


std::istream& operator >> (std::istream& is, allele_pair& ap)
{
    return is >> ap.first >> ap.second;
}


DTD_START(analysis_dtd, analysis, settings_t)
    ELEMENT(notes, std::string);
    ELEMENT_WITH_NAME(map, "consensus-map", map_loader_manip);
    /* format-specification */
    ELEMENT(dataset, pop_keyvalue);
    ELEMENT_WITH_NAME(pop, "population", population);
    ELEMENT(markers, marker_loader_manip);
    ELEMENT(noise, double);
    ELEMENT(settings, settings_t);
    ELEMENT(step, double);
    ELEMENT(parallel, int);
    ELEMENT_WITH_NAME(wd, "work-directory", std::string);
    ELEMENT(model, settings_t);
    ELEMENT(connected, bool);
    ELEMENT(epistasis, bool);
    ELEMENT(pleiotropy, settings_t);
    ELEMENT_WITH_NAME(sets, "working-sets", settings_t);
    ELEMENT_WITH_NAME(chromsel, "chromosome-selection", settings_t);
    ELEMENT(chromosome, std::string);
    ELEMENT_WITH_NAME(cofactorsel, "cofactor-marker-selection", settings_t);
    ELEMENT_WITH_NAME(epistasissel, "epistasis-initial-qtl-selection", std::string);

    analysis
        = A("name", &settings_t::name)
        & (E(notes, &settings_t::notes),
           E(design_dtd.root(), &settings_t::design),
           E(format_dtd.root(), &settings_t::marker_observation_specs),
           E(map),
           E(dataset, &settings_t::populations),
           E(settings),
           E(sets));
    notes
        = chardata();
    map
        = (A("file", &map_loader_manip::filename),
           A("format", &ignore::entity) / check_mapmaker);
    dataset
        = A("name", &pop_keyvalue::first)
        & E(pop, &pop_keyvalue::second);
    pop
        = (A("generation", &population::qtl_generation_name),
           /*A("traits", &population::observed_traits_filename))*/
           A("traits", xml_read_traits))
        & (M(E(markers)),
           E(noise, &population::noise));
    markers
        = (A("generation", &marker_loader_manip::generation),
           A("file", &marker_loader_manip::filename));
    noise
        = A("value");
    settings
        = (E(step, &settings_t::step),
           E(parallel, &settings_t::parallel),
           E(wd, &settings_t::work_directory),
           E(model),
           E(sets));
    step
        = A("value");
    parallel
        = A("value");
    wd
        = A("path");
    model
        = (E(connected, &settings_t::connected),
           E(epistasis, &settings_t::epistasis),
           E(pleiotropy));
    connected
        /*= A("enabled", convert_bool);*/
        = A("enabled");
    epistasis
        = A("enabled");
    pleiotropy
        /*= (A("enabled", set_pleiotropy),*/
        = (A("enabled", &settings_t::pleiotropy),
        /*= (A("enabled", &ignore::entity),*/
           A("tolerance", &settings_t::pleiotropy_tolerance));
    sets
        = (E(chromsel),
           E(cofactorsel),
           E(epistasissel, &settings_t::epistasis_qtl_selection_filename));
    chromsel
        = (A("all", &ignore::entity) / check_true)
        | ((A("all", &ignore::entity) / check_false)
           & E(chromosome, &settings_t::chromosome_selection));
    chromosome
        = A("name");
    cofactorsel
        = (A("automatic", &ignore::entity) / check_true
          & A("distance-threshold", &settings_t::cofactor_marker_selection_distance))
        | (A("automatic", &ignore::entity) / check_false
          & A("file", &settings_t::cofactor_marker_selection_filename));
    epistasissel
        = A("file");
DTD_END(analysis_dtd);


settings_t* read_settings(ifile& is)
{
    if (!is.good()) {
        throw input_exception(is, "couldn't read from file");
    }
    return analysis_dtd.parse(is);
}


format_specification_t* read_format(std::istream& is)
{
    if (!is.good()) {
        throw input_exception(is, "couldn't read from file");
    }
    return format_dtd.parse(is);
}


design_type* read_design(std::istream& is)
{
    if (!is.good()) {
        throw input_exception(is, "couldn't read from file");
    }
    return design_dtd.parse(is);
}


