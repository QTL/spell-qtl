/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "markov_population.h"
#include "polynom.h"
#include "fmin.h"
/*#include "all.h"*/

double magic = 1.e-15;


template <typename F>
double integral(double x0, double x1, F f, int steps=1000)
{
    double accum = 0;
    double dx = (x1 - x0)/steps;
    double xa = x0;
    double fa = f(x0);
    double xb;
    double fb;
    double dx_2 = dx * .5;
    while (steps--) {
        xb = xa + dx;
        fb = f(xb);
        accum += (fb + fa) * dx_2;
        xa = xb;
        fa = fb;
    }
    return accum;
}



int main(int argc, char** argv)
{
#ifdef NORMALIZE_GENERATION
    std::cout << "with normalization" << std::endl;
#endif
    auto P1 = homozygous('a');
    auto P2 = homozygous('b');
    auto F1 = cross(P1, P2);
    auto F2 = self(F1);
    auto F = F2;

    Generation M = pop_to_matrix(F2);
    Generation tmp;

    size_t count_gen = 2;

    PolynomMatrix pm;

    std::cout << "integral(0, 1, x²) = " << integral(0, 1, [](double x) { return x*x; }) << std::endl;
    std::cout << "integral(0, 1, e^x) = " << integral(0, 1, [](double x) { return exp(x); }) << std::endl;
    std::cout << "integral(0, 1, x) = " << integral(0, 1, [](double x) { return x; }) << std::endl;

    for (;; ++count_gen) {
        std::cout << '\r' << count_gen << std::flush;
        F = canonify_population(self(F));
        tmp = pop_to_matrix(F);

        pm = tmp.data - M.data;
#if 0
        std::pair<double, double> maximum = optimize(0, .5, [&] (double x) { return -eval_poly(pm, x).norm(); }, 0);
        if (maximum.second > -magic) {
            break;
        }
        std::cout << '\t' << maximum.second << "                " << std::flush;
#else
        double sum = integral(0, 0.5, [&] (double x) { return eval_poly(pm, x).norm(); });
        if (sum < magic) {
            break;
        }
        std::cout << '\t' << sum << "                " << std::flush;
#endif
        M = tmp;
    }
    std::cout << std::endl;
    std::cout << std::endl;
    /*std::cout << tmp.data << std::endl;*/
    /*std::cout << std::endl;*/
    /*std::cout << pm << std::endl;*/

    std::cout << std::endl << "after " << count_gen << " generations" << std::endl;

    return 0;
    (void) argc;
    (void) argv;
}

