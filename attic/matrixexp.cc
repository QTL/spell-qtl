/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "eigen.h"
#include "error.h"
#include "labelled_matrix.h"
#include "lumping2.h"
#include <limits>

using namespace Eigen;

typedef std::pair<char, char> label_type;
enum geno_matrix_variant_type { Gamete, DoublingGamete, SelfingGamete, Haplo, Geno };


struct geno_matrix {
    geno_matrix_variant_type variant;
    std::vector<label_type> labels;
    MatrixXd inf_mat;
    MatrixXd p, p_inv, diag;
    /*double norm_factor;*/

    size_t rows() const { return inf_mat.rows(); }
    size_t cols() const { return inf_mat.cols(); }

    geno_matrix& operator = (const geno_matrix& gm)
    {
        variant = gm.variant;
        labels.assign(gm.labels.begin(), gm.labels.end());
        inf_mat = gm.inf_mat;
        p = gm.p;
        p_inv = gm.p_inv;
        diag = gm.diag;
        return *this;
    }

    MatrixXd exp(double d) const
    {
        MatrixXd ret = p * ((d * diag).array().exp().matrix().asDiagonal()) * p_inv;
        /*MatrixXd check = (d * inf_mat).exp();*/
        /*if (!ret.isApprox(check)) {*/
            /*MSG_ERROR("BAD EXP" << std::endl << "with diag:" << std::endl << check << std::endl << "with exp:" << std::endl << ret, "");*/
        /*} else {*/
            /*MSG_INFO("GOOD EXP");*/
        /*}*/
        return ret;
    }

    MatrixXd lim_inf() const
    {
        /*MSG_DEBUG("LIM_INF ####### LIM_INF");*/
        /*MSG_DEBUG((*this));*/
        MatrixXd diag_inf = (diag.array() == 0).select(VectorXd::Ones(diag.size()), VectorXd::Zero(diag.size())).matrix();
        return p * diag_inf.asDiagonal() * p_inv;
    }
};

inline
std::ostream& operator << (std::ostream& os, const label_type& l)
{
    return os << l.first << l.second;
}

inline
std::ostream& operator << (std::ostream& os, const std::vector<label_type>& vl)
{
    for (const auto& l: vl) { os << ' ' << l; }
    return os;
}


#define P_NORM_FACTOR (.707106781186547524400844362104849039284835937688474036588339868995366239231053519425193767163820786367506923115456148512462418027925)


geno_matrix
    gamete = {
        Gamete,
        {{0, 0}, {1, 0}},
        (MatrixXd(2, 2) <<
         -1,  1,
          1, -1).finished(),
        /* BEWARE these matrices (p and p_inv) SHOULD be * 1/sqrt(2) */
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         P_NORM_FACTOR, -P_NORM_FACTOR).finished(),
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         P_NORM_FACTOR, -P_NORM_FACTOR).finished(),
        (MatrixXd(2, 1) << 0, -2).finished(),
        /*.5*/
    },
    doubling_gamete = {
        DoublingGamete,
        {{0, 0}, {1, 1}},
        (MatrixXd(2, 2) <<
         -1,  1,
          1, -1).finished(),
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         P_NORM_FACTOR, -P_NORM_FACTOR).finished(),
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         P_NORM_FACTOR, -P_NORM_FACTOR).finished(),
        (MatrixXd(2, 1) << 0, -2).finished(),
        /*.5*/
    };

#define SELECT(__p, __b) ((__b) ? (__p).second : (__p).first)


geno_matrix kronecker(const geno_matrix& m1, const geno_matrix& m2)
{
    geno_matrix ret;
    ret.inf_mat.resize(m1.rows() * m2.rows(), m1.cols() * m2.cols());
    ret.labels.reserve(m1.labels.size() * m2.labels.size());
    switch (m1.variant) {
        case Gamete:
            if (m2.variant == Gamete) {
                for (const auto& l1: m1.labels) {
                    for (const auto& l2: m2.labels) {
                        ret.labels.emplace_back(l1.first, l2.first);
                    }
                }
                ret.variant = SelfingGamete;
                break;
            }
        case SelfingGamete:
        case DoublingGamete:
            MSG_ERROR("Gamete matrices can only be the right operand in a kronecker product", "");
            MSG_QUEUE_FLUSH();
            throw 0;
        case Haplo:
            if (m2.variant != Haplo) {
                MSG_ERROR("Only Haplo (x) Haplo is defined", "");
                MSG_QUEUE_FLUSH();
                throw 0;
            }
            ret.variant = Geno;
            for (const auto& l1: m1.labels) {
                for (const auto& l2: m2.labels) {
                    ret.labels.emplace_back(l1.first, l2.first);
                }
            }
            break;
        case Geno:
            switch (m2.variant) {
                case Gamete:
                    ret.variant = Haplo;
                    for (const auto& l1: m1.labels) {
                        for (const auto& l2: m2.labels) {
                            ret.labels.emplace_back(SELECT(l1, (int)l2.first), 0);
                        }
                    }
                    break;
                case DoublingGamete:
                    ret.variant = Geno;
                    for (const auto& l1: m1.labels) {
                        for (const auto& l2: m2.labels) {
                            ret.labels.emplace_back(SELECT(l1, (int)l2.first), SELECT(l1, (int)l2.first));
                        }
                    }
                    break;
                case SelfingGamete:
                    ret.variant = Geno;
                    for (const auto& l1: m1.labels) {
                        for (const auto& l2: m2.labels) {
                            ret.labels.emplace_back(SELECT(l1, (int)l2.first), SELECT(l1, (int)l2.second));
                        }
                    }
                    break;
                default:
                    MSG_ERROR("Only Geno (x) {any gamete} is defined", "");
                    MSG_QUEUE_FLUSH();
                    throw 0;
            };
            break;
    };
    ret.inf_mat = kroneckerProduct(m1.inf_mat, MatrixXd::Identity(m2.rows(), m2.cols()))
                + kroneckerProduct(MatrixXd::Identity(m1.rows(), m1.cols()), m2.inf_mat);
    ret.p = kroneckerProduct(m1.p, m2.p);
    ret.p_inv = kroneckerProduct(m1.p_inv, m2.p_inv);
    ret.diag = (kroneckerProduct(m1.diag, VectorXd::Ones(m2.cols()))
             + kroneckerProduct(VectorXd::Ones(m1.cols()), m2.diag));
    /*ret.norm_factor = m1.norm_factor * m2.norm_factor;*/

    /* Check! */
    /*MSG_DEBUG("CHECK");*/
    /*MSG_DEBUG((ret.p * ret.diag.asDiagonal() * ret.p_inv * ret.norm_factor) - ret.inf_mat);*/
    /*MSG_DEBUG("END CHECK");*/
    return ret;
}


geno_matrix
    selfing_gamete = kronecker(gamete, gamete);



std::ostream& operator << (std::ostream& os, const geno_matrix& gm)
{
    Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic> tmp(gm.rows() + 1, gm.cols() + 1);
    for (size_t i = 0; i < gm.labels.size(); ++i) {
        tmp(0, 1 + i) = "  ";
        tmp(0, 1 + i)[0] = (gm.labels[i].first < 32 ? '0' : '\0') + gm.labels[i].first;
        tmp(0, 1 + i)[1] = (gm.labels[i].second < 32 ? '0' : '\0') + gm.labels[i].second;
        tmp(1 + i, 0) = tmp(0, 1 + i);
    }
    for (size_t i = 0; i < gm.rows(); ++i) {
        for (size_t j = 0; j < gm.rows(); ++j) {
            std::stringstream ss; ss << gm.inf_mat(i, j);
            tmp(i + 1, j + 1) = ss.str();
        }
    }
    os << tmp;
    os << std::endl;
    os << "P" << std::endl << gm.p << std::endl;
    os << "Pinv" << std::endl << gm.p_inv << std::endl;
    MatrixXd diag = gm.diag.asDiagonal();
    os << "D" << std::endl << diag << std::endl;
    return os;
}



geno_matrix ancestor_matrix(char a)
{
    std::vector<label_type> l;
    l.emplace_back(a, a);
    return {Geno, l, (MatrixXd(1, 1) << 0).finished(), (MatrixXd(1, 1) << 1).finished(), (MatrixXd(1, 1) << 1).finished(), (MatrixXd(1, 1) << 0).finished()/*, 1.*/};
}

namespace std {
    template <>
    struct hash<label_type> {
        size_t operator () (const label_type& l) const
        {
            return hash<unsigned short>()(*(unsigned short*)&l);
        }
    };
}

/*#define DEBUG_LUMPING*/
#ifdef DEBUG_LUMPING
#define LUMP_DEBUG(_expr_) MSG_DEBUG(_expr_)
#define LUMP_QUEUE_FLUSH() MSG_QUEUE_FLUSH()
#else
#define LUMP_DEBUG(_expr_)
#define LUMP_QUEUE_FLUSH()
#endif

double cosV(const VectorXd& v1, const VectorXd& v2)
{
    double n1 = v1.lpNorm<2>();
    double n2 = v2.lpNorm<2>();

    return n1 ? n2 ? abs(v1.transpose() * v2) / (n1 * n2)
                   : 0
              : 1;
}

double cosM(const VectorXd& v1, const MatrixXd& mat)
{
    MatrixXd tmp = v1.transpose() * mat;
    double ret = cosV(v1, mat * tmp.transpose());
    /*MSG_DEBUG("CosM");*/
    /*MSG_DEBUG(v1.transpose());*/
    /*MSG_DEBUG("-------------");*/
    /*MSG_DEBUG(mat);*/
    /*MSG_DEBUG("-------------");*/
    /*MSG_DEBUG(ret);*/
    return ret;
}


geno_matrix lump_using_partition(const geno_matrix& m, const std::set<subset>& lumping_partition)
{
    MatrixXd ortho_p;

    ::lumper<MatrixXd, label_type> l(m.inf_mat, m.labels);
    geno_matrix ret_lump;
    ret_lump.inf_mat = l.to_matrix(lumping_partition, ret_lump.labels);
    LUMP_DEBUG("LUMPED");
    LUMP_DEBUG(ret_lump.inf_mat);

    MatrixXd L = MatrixXd::Zero(lumping_partition.size(), m.labels.size());

    int i = -1;
    for (auto& part: lumping_partition) {
        ++i;
        for (auto& j: part) {
            L(i, j) = 1;
        }
    }
    LUMP_DEBUG("LUMPING MATRIX");
    LUMP_DEBUG(L);
    MatrixXd Linv = L.transpose();
    VectorXd csum = Linv.array().colwise().sum();
    LUMP_DEBUG(MATRIX_SIZE(L));
    LUMP_DEBUG(MATRIX_SIZE(csum));
    LUMP_QUEUE_FLUSH();
    Linv.array().rowwise() /= (csum.array() == 0).select(VectorXd::Ones(csum.size()).array(), csum.array()).transpose();

    LUMP_DEBUG("PARANO L.Linv.L - L");
    LUMP_DEBUG((L * Linv * L - L));
    LUMP_DEBUG("PARANO Linv.L.Linv - Linv");
    LUMP_DEBUG((Linv * L * Linv - Linv));
    LUMP_DEBUG("PARANO  L.Linv = t(L.Linv)");
    LUMP_DEBUG((L * Linv - (L * Linv).transpose()));
    LUMP_DEBUG("PARANO  Linv.L = t(Linv.L)");
    LUMP_DEBUG((Linv * L - (Linv * L).transpose()));
    LUMP_DEBUG("####################################");
    LUMP_QUEUE_FLUSH();

    MatrixXd my_lump = L * m.inf_mat * Linv;
    if (!my_lump.isApprox(ret_lump.inf_mat)) {
        /*MSG_WARNING("Weird lumping in progress. Proceed at your own risk.");*/
        /*MSG_DEBUG("from lumper:");*/
        /*MSG_DEBUG(ret_lump.inf_mat);*/
        /*MSG_DEBUG("from product:");*/
        /*MSG_DEBUG(my_lump);*/
        ret_lump.inf_mat = my_lump;
        ret_lump.labels.clear();
        ret_lump.labels.reserve(lumping_partition.size());
        for (const auto& C: lumping_partition) {
            ret_lump.labels.push_back(m.labels[C.front()]);
        }
        ret_lump.p = MatrixXd::Ones(1, 1);
        ret_lump.p_inv = MatrixXd::Ones(1, 1);
        ret_lump.diag = MatrixXd::Ones(1, 1);
        /*MSG_DEBUG(ret_lump);*/
        /*MSG_QUEUE_FLUSH();*/
    }

    LUMP_DEBUG("P");
    LUMP_DEBUG(m.p);
    LUMP_QUEUE_FLUSH();

    MatrixXd LP = L * m.p;
    LUMP_DEBUG("LP");
    LUMP_DEBUG(LP);
    LUMP_QUEUE_FLUSH();

#ifdef WE_CREATE_P_AND_Pinv_OURSELVES
    ret_lump.p = MatrixXd::Zero(L.rows(), L.rows());
    ortho_p = MatrixXd::Zero(L.rows(), L.rows());
    int dest = 0, src = 0;

#if 0
    auto
        select_next_vec =
        [&] ()
        {
            double best_cos = 1;
            int best_col = 0;
            if (dest > 0) {
                for (int i = 0; i < LP.cols(); ++i) {
                    double c = cosM(LP.col(i), ret_lump.p.leftCols(dest));
                    if (c < best_cos) {
                        best_cos = c;
                        best_col = i;
                    }
                }
            }
            return LP.col(best_col);
        };
#endif

    while (dest < ret_lump.p.cols() && src < LP.cols()) {
#if 0
        auto vec = select_next_vec();
        ret_lump.p.col(dest++) = vec / vec.lpNorm<2>();
#else
        auto vec = LP.col(src);
        MatrixXd projection;
        if (dest == 0) {
            projection = VectorXd::Zero(LP.rows());
        } else {
            /*auto block = ret_lump.p.leftCols(dest);*/
            auto block = ortho_p.leftCols(dest);
            MatrixXd tmp1 = vec.transpose() * block;
            LUMP_DEBUG("tmp1");
            LUMP_DEBUG(tmp1);
            LUMP_QUEUE_FLUSH();
            projection = block * tmp1.transpose();
        }
        LUMP_DEBUG("vec       : " << vec.transpose());
        LUMP_DEBUG("projection: " << projection.transpose());
        LUMP_QUEUE_FLUSH();
        VectorXd tmp_vec = vec - projection;
        if (!tmp_vec.isZero(1.e-1)) {
            LUMP_DEBUG("ADDING " << vec.transpose() << " TO FAMILY norm(tmp_vec)=" << tmp_vec.lpNorm<2>());
            ortho_p.col(dest) = tmp_vec / tmp_vec.lpNorm<2>();
            ret_lump.p.col(dest++) = vec / vec.lpNorm<2>();
        } else {
            LUMP_DEBUG("NOT ADDING.");
        }
        ++src;
#endif
    }
    /*ret_lump.p = ortho_p;*/
    LUMP_DEBUG("PARANO LM=NL");
    LUMP_DEBUG((L * m.inf_mat - ret_lump.inf_mat * L));
    LUMP_DEBUG("PARANO M.Linv=Linv.N");
    LUMP_DEBUG((m.inf_mat * Linv - Linv * ret_lump.inf_mat));
    LUMP_DEBUG("DEBUG P");
    LUMP_DEBUG(ret_lump.p);
    LUMP_DEBUG("DEBUG P.Pt");
    LUMP_DEBUG((ret_lump.p * ret_lump.p.transpose()));
    LUMP_QUEUE_FLUSH();
    if (ret_lump.inf_mat.isApprox(ret_lump.inf_mat.transpose())
            &&
            (ret_lump.p * ret_lump.p.transpose()).isApprox(MatrixXd::Identity(ret_lump.p.rows(), ret_lump.p.cols()), 1.e-10)
            ) {
        ret_lump.p_inv = ret_lump.p.transpose();  /* Theorem of inversion à la Chuck Norris */
        MSG_DEBUG("CHUCK NORRIS WAS HERE. I CAN HAZ QUICK INVERSE.");
    } else {
        ret_lump.p_inv = ret_lump.p.inverse();
    }
    MatrixXd tmp_diag = ret_lump.p_inv * ret_lump.inf_mat * ret_lump.p;
    MatrixXd tmp_diag_diag = tmp_diag.diagonal().asDiagonal();
    MatrixXd diff = tmp_diag - tmp_diag_diag;
    if (!diff.isMuchSmallerThan(tmp_diag_diag)) {
        MSG_ERROR("COULDN'T DIAGONALIZE MATRIX!", "");
        MSG_DEBUG(_RED << "P.Pt");
        MSG_DEBUG((ret_lump.p.transpose() * ret_lump.p.transpose()));
        MSG_DEBUG("P");
        MSG_DEBUG(ret_lump.p);
        MSG_DEBUG("P^-1");
        MSG_DEBUG(ret_lump.p_inv);
        MSG_DEBUG("TMP DIAG");
        /*MatrixXd tmp = tmp_diag;*/
        /*for (int j = 0; j < tmp.cols(); ++j) {*/
            /*for (int i = 0; i < tmp.rows(); ++i) {*/
                /*if (abs(tmp(i, j)) < 1.e-15) {*/
                    /*tmp(i, j) = 0;*/
                /*}*/
            /*}*/
        /*}*/
        /*MSG_DEBUG(tmp);*/
        MSG_DEBUG(tmp_diag);
        MSG_DEBUG("TMP_DIAG_DIAG");
        MSG_DEBUG(tmp_diag_diag);
        MSG_DEBUG("diff");
        MSG_DEBUG(diff);
        MSG_DEBUG(_NORMAL);
        MSG_QUEUE_FLUSH();
        throw 0;
    }
    ret_lump.diag = tmp_diag.diagonal();
    for (int i = 0; i < ret_lump.diag.size(); ++i) {
        if (abs(ret_lump.diag(i)) < _EPSILON) { ret_lump.diag(i) = 0; }
    }

    LUMP_DEBUG("DEBUG DIAG");
    LUMP_DEBUG(ret_lump.diag);
    LUMP_DEBUG("---------------------------------------------------------------");
    if (src == LP.cols() && dest < ret_lump.p.cols()) {
        MSG_ERROR("BUGGY BUG ON ITS BUGGY BUGS BUGS BUNNY.", "");
        LUMP_QUEUE_FLUSH();
        throw 0;
    }

#else

#if 0
    auto ldlt = ret_lump.inf_mat.ldlt();
    ret_lump.diag = ldlt.vectorD();
    MatrixXd eL = ldlt.matrixL();
    MatrixXd eU = ldlt.matrixU();
    auto eP = ldlt.transpositionsP();
    ret_lump.p = eP * eL;
    /*ret_lump.p_inv = eU * eP;*/
    ret_lump.p_inv = ret_lump.p.transpose();
#endif

    EigenSolver<MatrixXd> eig(ret_lump.inf_mat, true);

    ret_lump.diag = eig.eigenvalues().real();
    ret_lump.p = eig.eigenvectors().real();
    ret_lump.p_inv = ret_lump.p.inverse();

    for (int i = 0; i < ret_lump.diag.size(); ++i) {
        if (abs(ret_lump.diag(i)) < _EPSILON) { ret_lump.diag(i) = 0; }
    }

    LUMP_DEBUG("DEBUG P");
    LUMP_DEBUG(ret_lump.p);
    LUMP_DEBUG("DEBUG P.Pt");
    LUMP_DEBUG((ret_lump.p * ret_lump.p.transpose()));
    LUMP_QUEUE_FLUSH();
#endif
    /*
    ret_lump.labels.reserve(L.rows());
    for (int i = 0; i < L.rows(); ++i) {
        int j;
        for (j = 0; j < L.cols() && L(i, j) == 0; ++j);
        ret_lump.labels.push_back(m.labels[j]);
    }
    */

    LUMP_DEBUG("PARANO PERMUTATION");
    LUMP_DEBUG((ret_lump.p * ret_lump.diag.asDiagonal() * ret_lump.p_inv));
    LUMP_DEBUG("vs");
    LUMP_DEBUG(ret_lump.inf_mat);
    LUMP_DEBUG("FINAL RESULT");

    /*LUMP_DEBUG(ret_lump.labels);*/
    /*for (const auto& l: ret_lump.labels) {*/
        /*LUMP_DEBUG(l.first << l.second);*/
    /*}*/
    /*LUMP_QUEUE_FLUSH();*/

    ret_lump.variant = m.variant;
    /*ret_lump.norm_factor = 1;*/

    LUMP_DEBUG(ret_lump);
    MSG_QUEUE_FLUSH();

    return ret_lump;
}




/*
 * De lumpibus

(%i1) kill(all);
load(diag);
(%o0) done
(%o1) "/usr/share/maxima/5.32.1/share/contrib/diag.mac"
-->  / * création manuelle du BC2 * /
     bc2: matrix([-2, 2, 2],
                 [1, -2, 0],
                 [1, 0, -2]);
(%o2) matrix([-2,2,2],[1,-2,0],[1,0,-2])
(%i3) jbc2: jordan(bc2);
(%o3) [[-4,1],[-2,1],[0,1]]
(%i4) p: ModeMatrix(bc2, jbc2);
      p^^-1;
(%o4) matrix([1,0,1],[-1/2,1,1/2],[-1/2,-1,1/2])
(%o5) matrix([1/2,-1/2,-1/2],[0,1/2,-1/2],[1/2,1/2,1/2])
(%i6) pt: args(transpose(p));
(%o6) [[1,-1/2,-1/2],[0,1,-1],[1,1/2,1/2]]
(%i7) Ptmp:transpose(apply(matrix, map(uvect, pt)));
(%o7) matrix([sqrt(2)/sqrt(3),0,sqrt(2)/sqrt(3)],[-1/(sqrt(2)*sqrt(3)),1/sqrt(2),1/(sqrt(2)*sqrt(3))],[-1/(sqrt(2)*sqrt(3)),-1/sqrt(2),1/(sqrt(2)*sqrt(3))])
(%i8) float(Ptmp);
(%o8) matrix([0.81649658092772,0.0,0.81649658092772],[-0.40824829046386,0.70710678118654,0.40824829046386],[-0.40824829046386,-0.70710678118654,0.40824829046386])
-->  / * permutation pour être cohérent avec l'implémentation C++ de matrixexp (au 30/01/2015) * /
     zzz: matrix([1, 0, 0], [0, 0, 1], [0, 1, 0]);
     zzz2: matrix([-1, 0, 0], [0, 1, 0], [0, 0, 1]);
(%o9) matrix([1,0,0],[0,0,1],[0,1,0])
(%o10) matrix([-1,0,0],[0,1,0],[0,0,1])
(%i11) p: zzz . Ptmp . zzz . zzz2;
(%o11) matrix([-sqrt(2)/sqrt(3),sqrt(2)/sqrt(3),0],[1/(sqrt(2)*sqrt(3)),1/(sqrt(2)*sqrt(3)),-1/sqrt(2)],[1/(sqrt(2)*sqrt(3)),1/(sqrt(2)*sqrt(3)),1/sqrt(2)])
(%i12) float(p);
(%o12) matrix([-0.81649658092772,0.81649658092772,0.0],[0.40824829046386,0.40824829046386,-0.70710678118654],[0.40824829046386,0.40824829046386,0.70710678118654])
(%i13) pinv: p^^-1;
(%o13) matrix([-sqrt(3)/2^(3/2),sqrt(3)/2^(3/2),sqrt(3)/2^(3/2)],[sqrt(3)/2^(3/2),sqrt(3)/2^(3/2),sqrt(3)/2^(3/2)],[0,-1/sqrt(2),1/sqrt(2)])
(%i14) p . pinv;
(%o14) matrix([1,0,0],[0,1,0],[0,0,1])
-->  d:pinv.bc2.p;
     / * On est contents c'est bien la bonne diag, les bonnes p et pinv, tout va bien * /
(%o15) matrix([-4,0,0],[0,0,0],[0,0,-2])
(%i16) dinf: diag([0, 1, 0]);
(%o16) matrix([0,0,0],[0,1,0],[0,0,0])
(%i17) p.dinf.pinv;
(%o17) matrix([1/2,1/2,1/2],[1/4,1/4,1/4],[1/4,1/4,1/4])
(%i18) limit(matrixexp(bc2, ddd), ddd, inf);
(%o18) matrix([1/2,1/2,1/2],[1/4,1/4,1/4],[1/4,1/4,1/4])
-->  / * Note à benêts :
        l1 est la matrice de lumping.
        l2 est sa transposée pondérée par les abondances normalisées PAR SOUS-ENSEMBLE DE LA PARTITION.
        Il semble (sic) que cette magouille (sic) est nécessaire pour que
        l'abondance de la somme soit la même que la somme des abondances.
      * /
     l1: matrix([1, 1, 0], [0, 0, 1]);
     l2: matrix([2/3, 0], [1/3, 0], [0, 1]);
(%o19) matrix([1,1,0],[0,0,1])
(%o20) matrix([2/3,0],[1/3,0],[0,1])
(%i30) ab: [1/2, 1/4, 1/4];
(%o30) [1/2,1/4,1/4]
(%i31) l1 . ab;
(%o31) matrix([3/4],[1/4])
(%i21) l1 . l2;
(%o21) matrix([1,0],[0,1])
(%i22) bc2l: l1 . bc2 . l2;
(%o22) matrix([-2/3,2],[2/3,-2])
(%i23) eigenvectors(bc2l);
(%o23) [[[-8/3,0],[1,1]],[[[1,-1]],[[1,1/3]]]]
(%i24) pl: ModeMatrix(bc2l);
(%o24) matrix([1,1],[-1,1/3])
(%i25) plinv: pl^^-1;
(%o25) matrix([1/4,-3/4],[3/4,3/4])
(%i26) dl: plinv . bc2l . pl;
(%o26) matrix([-8/3,0],[0,0])
(%i27) limit(matrixexp(bc2l, dd), dd, inf);
(%o27) matrix([3/4,3/4],[1/4,1/4])
-->  / * Hou qu'il est beau le Youki * /
     wxplot2d(flatten(args(l1 . float(matrixexp(bc2, dd)) . l2 - float(matrixexp(bc2l, dd)))), [dd, 0, 5]);
(%t29)  << Graphics >> 
(%o29) 

 */


void compute_LR(const geno_matrix& m, const std::set<subset>& lumping_partition, MatrixXd& L1, MatrixXd& L2)
{
    /* Creation of L1 */
    L1 = MatrixXd::Zero(lumping_partition.size(), m.cols());
    int i = 0;
    for (const auto& D: lumping_partition) {
        for (int s: D) {
            L1(i, s) = 1.;
        }
        ++i;
    }
    /* Creation of L2 */
    L2 = m.lim_inf().col(0).asDiagonal() * L1.transpose();
    L2.array().rowwise() /= L2.array().colwise().sum();
}


void compute_LR2(const geno_matrix& m, const std::set<subset>& lumping_partition, MatrixXd& L1, MatrixXd& L2)
{
    /* Creation of L1 */
    L1 = MatrixXd::Zero(lumping_partition.size(), m.cols());
    int i = 0;
    for (const auto& D: lumping_partition) {
        for (int s: D) {
            L1(i, s) = 1.;
        }
        ++i;
    }
    /* Creation of L2 */
    /*L2 = m.lim_inf().col(0).asDiagonal() * L1.transpose();*/
    VectorXd diag = m.inf_mat.fullPivLu().kernel().col(0);
    L2 = diag.asDiagonal() * L1.transpose();
    L2.array().rowwise() /= L2.array().colwise().sum();
}


geno_matrix lump_using_partition_weighted(const geno_matrix& m, const std::set<subset>& lumping_partition)
{
    MatrixXd ortho_p;

    ::lumper<MatrixXd, label_type> l(m.inf_mat, m.labels);
    geno_matrix ret_lump;
    /*ret_lump.inf_mat = l.to_matrix(lumping_partition, m.lim_inf().col(0), ret_lump.labels);*/
    /* Creation of L1 */
    MatrixXd L1 = MatrixXd::Zero(lumping_partition.size(), m.cols());
    int i = 0;
    ret_lump.labels.clear();
    ret_lump.labels.reserve(lumping_partition.size());
    for (const auto& D: lumping_partition) {
        ret_lump.labels.push_back(m.labels[D.front()]);
        for (int s: D) {
            L1(i, s) = 1.;
        }
        ++i;
    }
    /* Creation of L2 */
    MatrixXd L2 = m.lim_inf().col(0).asDiagonal() * L1.transpose();
    L2.array().rowwise() /= L2.array().colwise().sum();
    /* Let's be blunt. */
    ret_lump.inf_mat =
        /* <em>And bold.</em> */
        L1 * m.inf_mat * L2;
#if 0
    MSG_DEBUG(MATRIX_SIZE(L1));
    MSG_DEBUG(L1);
    MSG_DEBUG(MATRIX_SIZE(L2));
    MSG_DEBUG(L2);
    MSG_DEBUG("Id?");
    MSG_DEBUG((L1 * L2));
#endif
    LUMP_DEBUG("LUMPED");
    LUMP_DEBUG(ret_lump.inf_mat);

    MatrixXd L = MatrixXd::Zero(lumping_partition.size(), m.labels.size());

    i = -1;
    for (auto& part: lumping_partition) {
        ++i;
        for (auto& j: part) {
            L(i, j) = 1;
        }
    }
    LUMP_DEBUG("LUMPING MATRIX");
    LUMP_DEBUG(L);
    MatrixXd Linv = L.transpose();
    VectorXd csum = Linv.array().colwise().sum();
    LUMP_DEBUG(MATRIX_SIZE(L));
    LUMP_DEBUG(MATRIX_SIZE(csum));
    LUMP_QUEUE_FLUSH();
    Linv.array().rowwise() /= (csum.array() == 0).select(VectorXd::Ones(csum.size()).array(), csum.array()).transpose();

    LUMP_DEBUG("PARANO L.Linv.L - L");
    LUMP_DEBUG((L * Linv * L - L));
    LUMP_DEBUG("PARANO Linv.L.Linv - Linv");
    LUMP_DEBUG((Linv * L * Linv - Linv));
    LUMP_DEBUG("PARANO  L.Linv = t(L.Linv)");
    LUMP_DEBUG((L * Linv - (L * Linv).transpose()));
    LUMP_DEBUG("PARANO  Linv.L = t(Linv.L)");
    LUMP_DEBUG((Linv * L - (Linv * L).transpose()));
    LUMP_DEBUG("####################################");
    LUMP_QUEUE_FLUSH();

#if 0
    MatrixXd my_lump = L * m.inf_mat * Linv;
    if (!my_lump.isApprox(ret_lump.inf_mat)) {
        /*MSG_WARNING("Weird lumping in progress. Proceed at your own risk.");*/
        /*MSG_DEBUG("from lumper:");*/
        /*MSG_DEBUG(ret_lump.inf_mat);*/
        /*MSG_DEBUG("from product:");*/
        /*MSG_DEBUG(my_lump);*/
        ret_lump.inf_mat = my_lump;
        ret_lump.labels.clear();
        ret_lump.labels.reserve(lumping_partition.size());
        for (const auto& C: lumping_partition) {
            ret_lump.labels.push_back(m.labels[C.front()]);
        }
        ret_lump.p = MatrixXd::Ones(1, 1);
        ret_lump.p_inv = MatrixXd::Ones(1, 1);
        ret_lump.diag = MatrixXd::Ones(1, 1);
        /*MSG_DEBUG(ret_lump);*/
        /*MSG_QUEUE_FLUSH();*/
    }
#endif

    LUMP_DEBUG("P");
    LUMP_DEBUG(m.p);
    LUMP_QUEUE_FLUSH();

    MatrixXd LP = L * m.p;
    LUMP_DEBUG("LP");
    LUMP_DEBUG(LP);
    LUMP_QUEUE_FLUSH();

#ifdef WE_CREATE_P_AND_Pinv_OURSELVES
    ret_lump.p = MatrixXd::Zero(L.rows(), L.rows());
    ortho_p = MatrixXd::Zero(L.rows(), L.rows());
    int dest = 0, src = 0;

#if 0
    auto
        select_next_vec =
        [&] ()
        {
            double best_cos = 1;
            int best_col = 0;
            if (dest > 0) {
                for (int i = 0; i < LP.cols(); ++i) {
                    double c = cosM(LP.col(i), ret_lump.p.leftCols(dest));
                    if (c < best_cos) {
                        best_cos = c;
                        best_col = i;
                    }
                }
            }
            return LP.col(best_col);
        };
#endif

    while (dest < ret_lump.p.cols() && src < LP.cols()) {
#if 0
        auto vec = select_next_vec();
        ret_lump.p.col(dest++) = vec / vec.lpNorm<2>();
#else
        auto vec = LP.col(src);
        MatrixXd projection;
        if (dest == 0) {
            projection = VectorXd::Zero(LP.rows());
        } else {
            /*auto block = ret_lump.p.leftCols(dest);*/
            auto block = ortho_p.leftCols(dest);
            MatrixXd tmp1 = vec.transpose() * block;
            LUMP_DEBUG("tmp1");
            LUMP_DEBUG(tmp1);
            LUMP_QUEUE_FLUSH();
            projection = block * tmp1.transpose();
        }
        LUMP_DEBUG("vec       : " << vec.transpose());
        LUMP_DEBUG("projection: " << projection.transpose());
        LUMP_QUEUE_FLUSH();
        VectorXd tmp_vec = vec - projection;
        if (!tmp_vec.isZero(1.e-1)) {
            LUMP_DEBUG("ADDING " << vec.transpose() << " TO FAMILY norm(tmp_vec)=" << tmp_vec.lpNorm<2>());
            ortho_p.col(dest) = tmp_vec / tmp_vec.lpNorm<2>();
            ret_lump.p.col(dest++) = vec / vec.lpNorm<2>();
        } else {
            LUMP_DEBUG("NOT ADDING.");
        }
        ++src;
#endif
    }
    /*ret_lump.p = ortho_p;*/
    LUMP_DEBUG("PARANO LM=NL");
    LUMP_DEBUG((L * m.inf_mat - ret_lump.inf_mat * L));
    LUMP_DEBUG("PARANO M.Linv=Linv.N");
    LUMP_DEBUG((m.inf_mat * Linv - Linv * ret_lump.inf_mat));
    LUMP_DEBUG("DEBUG P");
    LUMP_DEBUG(ret_lump.p);
    LUMP_DEBUG("DEBUG P.Pt");
    LUMP_DEBUG((ret_lump.p * ret_lump.p.transpose()));
    LUMP_QUEUE_FLUSH();
    if (ret_lump.inf_mat.isApprox(ret_lump.inf_mat.transpose())
            &&
            (ret_lump.p * ret_lump.p.transpose()).isApprox(MatrixXd::Identity(ret_lump.p.rows(), ret_lump.p.cols()), 1.e-10)
            ) {
        ret_lump.p_inv = ret_lump.p.transpose();  /* Theorem of inversion à la Chuck Norris */
        MSG_DEBUG("CHUCK NORRIS WAS HERE. I CAN HAZ QUICK INVERSE.");
    } else {
        ret_lump.p_inv = ret_lump.p.inverse();
    }
    MatrixXd tmp_diag = ret_lump.p_inv * ret_lump.inf_mat * ret_lump.p;
    MatrixXd tmp_diag_diag = tmp_diag.diagonal().asDiagonal();
    MatrixXd diff = tmp_diag - tmp_diag_diag;
    if (!diff.isMuchSmallerThan(tmp_diag_diag)) {
        MSG_ERROR("COULDN'T DIAGONALIZE MATRIX!", "");
        MSG_DEBUG(_RED << "P.Pt");
        MSG_DEBUG((ret_lump.p.transpose() * ret_lump.p.transpose()));
        MSG_DEBUG("P");
        MSG_DEBUG(ret_lump.p);
        MSG_DEBUG("P^-1");
        MSG_DEBUG(ret_lump.p_inv);
        MSG_DEBUG("TMP DIAG");
        /*MatrixXd tmp = tmp_diag;*/
        /*for (int j = 0; j < tmp.cols(); ++j) {*/
            /*for (int i = 0; i < tmp.rows(); ++i) {*/
                /*if (abs(tmp(i, j)) < 1.e-15) {*/
                    /*tmp(i, j) = 0;*/
                /*}*/
            /*}*/
        /*}*/
        /*MSG_DEBUG(tmp);*/
        MSG_DEBUG(tmp_diag);
        MSG_DEBUG("TMP_DIAG_DIAG");
        MSG_DEBUG(tmp_diag_diag);
        MSG_DEBUG("diff");
        MSG_DEBUG(diff);
        MSG_DEBUG(_NORMAL);
        MSG_QUEUE_FLUSH();
        throw 0;
    }
    ret_lump.diag = tmp_diag.diagonal();
    for (int i = 0; i < ret_lump.diag.size(); ++i) {
        if (abs(ret_lump.diag(i)) < _EPSILON) { ret_lump.diag(i) = 0; }
    }

    LUMP_DEBUG("DEBUG DIAG");
    LUMP_DEBUG(ret_lump.diag);
    LUMP_DEBUG("---------------------------------------------------------------");
    if (src == LP.cols() && dest < ret_lump.p.cols()) {
        MSG_ERROR("BUGGY BUG ON ITS BUGGY BUGS BUGS BUNNY.", "");
        LUMP_QUEUE_FLUSH();
        throw 0;
    }

#else

#if 0
    auto ldlt = ret_lump.inf_mat.ldlt();
    ret_lump.diag = ldlt.vectorD();
    MatrixXd eL = ldlt.matrixL();
    MatrixXd eU = ldlt.matrixU();
    auto eP = ldlt.transpositionsP();
    ret_lump.p = eP * eL;
    /*ret_lump.p_inv = eU * eP;*/
    ret_lump.p_inv = ret_lump.p.transpose();
#endif

    EigenSolver<MatrixXd> eig(ret_lump.inf_mat, true);

    ret_lump.diag = eig.eigenvalues().real();
    ret_lump.p = eig.eigenvectors().real();
    ret_lump.p_inv = ret_lump.p.inverse();

    for (int i = 0; i < ret_lump.diag.size(); ++i) {
        if (abs(ret_lump.diag(i)) < _EPSILON) { ret_lump.diag(i) = 0; }
    }

    LUMP_DEBUG("DEBUG P");
    LUMP_DEBUG(ret_lump.p);
    LUMP_DEBUG("DEBUG P.Pt");
    LUMP_DEBUG((ret_lump.p * ret_lump.p.transpose()));
    LUMP_QUEUE_FLUSH();
#endif
    /*
    ret_lump.labels.reserve(L.rows());
    for (int i = 0; i < L.rows(); ++i) {
        int j;
        for (j = 0; j < L.cols() && L(i, j) == 0; ++j);
        ret_lump.labels.push_back(m.labels[j]);
    }
    */

    LUMP_DEBUG("PARANO PERMUTATION");
    LUMP_DEBUG((ret_lump.p * ret_lump.diag.asDiagonal() * ret_lump.p_inv));
    LUMP_DEBUG("vs");
    LUMP_DEBUG(ret_lump.inf_mat);
    LUMP_DEBUG("FINAL RESULT");

    /*LUMP_DEBUG(ret_lump.labels);*/
    /*for (const auto& l: ret_lump.labels) {*/
        /*LUMP_DEBUG(l.first << l.second);*/
    /*}*/
    /*LUMP_QUEUE_FLUSH();*/

    ret_lump.variant = m.variant;
    /*ret_lump.norm_factor = 1;*/

    LUMP_DEBUG(ret_lump);
    MSG_QUEUE_FLUSH();

    return ret_lump;
}


geno_matrix lump(const geno_matrix& m, bool harder=false)
{
    /*MSG_DEBUG("LUMPING=====================================================");*/
    /*MSG_DEBUG(MATRIX_SIZE(m.inf_mat));*/
    /*MSG_DEBUG(m);*/
    /*MSG_DEBUG("------------------------------------------------------------");*/
    ::lumper<MatrixXd, label_type> l(m.inf_mat, m.labels);
    auto lumping_partition = l.refine_all();
    /*if (harder) {*/
        /*l.try_harder(lumping_partition);*/
    /*}*/
    /*MSG_DEBUG("" << lumping_partition);*/
    return lump_using_partition(m, lumping_partition);
    (void) harder;
}


geno_matrix split_matrix(const geno_matrix& m)
{
    std::set<label_type> labels(m.labels.begin(), m.labels.end());
    int row, col;
    std::set<subset> partition;
    /*std::map<label_type, std::map<double, std::vector<int>>> lump_states;*/
    for (const auto& l: labels) {
        int rows = 0;
        for (const auto& ml: m.labels) {
            if (ml == l) { ++rows; }
        }
        MatrixXd splitter = MatrixXd::Zero(rows, m.cols());
        col = 0;
        row = 0;
        std::vector<int> state_index;
        for (const auto& ml: m.labels) {
            if (ml == l) {
                splitter(row, col) = 1;
                ++row;
                state_index.push_back(col);
            }
            ++col;
        }
        /*MSG_DEBUG("SPLITTER FOR " << l);*/
        /*MSG_DEBUG(splitter);*/
        /*MSG_DEBUG("SUB-MATRIX FOR " << l);*/
        MatrixXd mat = splitter * m.inf_mat * splitter.transpose();
        /*MSG_DEBUG(mat);*/
        VectorXd sums = mat.colwise().sum();
        /*MSG_DEBUG("SUMS " << sums);*/
        std::map<double, std::vector<int>> state_groups;
        for (int i = 0; i < sums.size(); ++i) {
            state_groups[sums[i]].push_back(state_index[i]);
            /*state_groups[0].push_back(state_index[i]);*/
        }
        for (const auto& kv: state_groups) {
            partition.insert(kv.second);
        }
    }
    /*MSG_DEBUG("OVER-LUMP");*/
    /*MSG_DEBUG("" << partition);*/
    for (const auto& C: partition) {
        std::stringstream msg;
        for (int s: C) {
            msg << ' ' << m.labels[s] << '[' << s << ']';
        }
        /*MSG_DEBUG(msg.str());*/
    }
    MSG_DEBUG_INDENT;

    geno_matrix ret = lump_using_partition(m, partition);

    /*MSG_DEBUG(ret);*/
    MSG_DEBUG_DEDENT;

    return ret;
}


void output_sur_son_balcon(const MatrixXd& sums, const std::set<subset>& P)
    /* manifestement parce que derisavi par procuration. */
{
    VectorXd part(sums.cols());
    int col = 0;
    for (const auto& C: P) {
        for (int s: C) {
            part(s) = col;
        }
        ++col;
    }

    MatrixXd transp = MatrixXd::Zero(sums.cols(), sums.cols());
    int row = 0;
    for (const auto& C: P) {
        for (int s: C) {
            transp(s, row) = 1;
            ++row;
        }
    }

    /*part = transp * part;*/

    /*MSG_DEBUG("=========" << std::endl << transp << std::endl << "=========");*/

    MatrixXd output(sums.rows() + 2, sums.cols());
    for (int i = 0; i < output.cols(); ++i) {
        output(0, i) = i;
    }
    output.row(1) = part.transpose();
    output.bottomRows(sums.rows()) = sums;
    output *= transp;
    /*MSG_DEBUG(output);*/
}


/*bool operator < (const VectorXd& v1, const VectorXd& v2)*/
namespace std {
    template <int L, typename _Scalar>
        struct less<Eigen::Matrix<_Scalar, L, 1>> {
            bool operator () (const Eigen::Matrix<_Scalar, L, 1>& v1, const Eigen::Matrix<_Scalar, L, 1>& v2) const
            {
                /*if ((v1 - v2).isZero()) {*/
                    /*return false;*/
                /*}*/
                for (int i = 0; i < v1.size(); ++i) {
                    if (v1(i) < v2(i)) return true;
                    if (v2(i) < v1(i)) return false;
                }
                return false;
            }
        };
}


std::set<subset> partition_on_labels(const std::vector<label_type>& labels)
{
    std::set<subset> P;
    lumper_map<label_type, subset> tmp;
    for (size_t i = 0; i < labels.size(); ++i) {
        auto& l = labels[i];
        /*if (tmp[l].size() == 0) {*/
            /*tmp[l].resize(G.column_labels.size());*/
        /*}*/
        /*tmp[l].set(i);*/
        tmp[l]./*insert*/push_back(i);
    }
    for (auto& kv: tmp) {
        /*std::cout << kv.first << "\t" << kv.second << std::endl;*/
        P.insert(kv.second);
    }

    return P;
}


typedef Eigen::Matrix<bool, Eigen::Dynamic, Eigen::Dynamic> MatrixXb;
typedef Eigen::Matrix<bool, Eigen::Dynamic, 1> VectorXb;


void compute_U(const MatrixXd inf_mat, VectorXd& U, VectorXd& Uinv)
{
}


double compute_delta(const geno_matrix& m, const std::set<subset>& P)
{
    MatrixXd C, D;
    compute_LR2(m, P, C, D);
    MatrixXd DC = D * C;
#ifdef MAIS_C_EST_DES_CONNERIES_TOUT_CA
    MatrixXd Ker = (DC - MatrixXd::Identity(DC.rows(), DC.cols())).fullPivLu().kernel();
    MSG_DEBUG("DC" << std::endl << DC);
    MSG_DEBUG("Ker" << std::endl << Ker);
    if (Ker.cols() != 1) {
        MSG_ERROR("et merde.", "");
    }
    VectorXd s = Ker.col(0).array().sqrt().matrix();
#else
    VectorXd s = m.lim_inf().col(0).array().sqrt().matrix();
#endif
    MatrixXd Uinv = s.asDiagonal();
    MatrixXd U = s.array().inverse().matrix().asDiagonal();
#if 1
    MatrixXd tmp = U * m.inf_mat * Uinv;
    if (!(tmp - tmp.transpose()).isZero(1.e-6)) {
        MSG_DEBUG("PARANO SYM " << (tmp - tmp.transpose()).lpNorm<Eigen::Infinity>());
        /*MSG_DEBUG(tmp);*/
        /*MSG_DEBUG("U" << std::endl << U);*/
        /*MSG_DEBUG("Uinv" << std::endl << Uinv);*/
    }
#endif
    return (U * (m.inf_mat * DC - m.inf_mat) * Uinv).lpNorm<2>();
}



void subset_union(const subset& s1, const subset& s2, subset& output)
{
    output.resize(s1.size() + s2.size());
    auto it = std::set_union(s1.begin(), s1.end(), s2.begin(), s2.end(), output.begin());
    output.resize(it - output.begin());
}


std::set<subset> init_partition(const geno_matrix& m)
{
    std::set<subset> ret;
    for (int i = 0; i < m.inf_mat.cols(); ++i) {
        ret.insert({i});
    }
    return ret;
}

std::set<subset>::const_iterator merge(const subset& S1, const subset& S2, std::set<subset>& P)
{
    P.erase(S1);
    P.erase(S2);
    subset Stot;
    subset_union(S1, S2, Stot);
    return P.insert(Stot).first;
}

void unmerge(const subset& S1, const subset& S2, std::set<subset>::const_iterator Stot, std::set<subset>& P)
{
    P.erase(Stot);
    P.insert(S1);
    P.insert(S2);
}



MatrixXd compute_lumping_costs(const geno_matrix& m, const std::set<subset>& P0)
{
    MatrixXd ret = MatrixXd::Constant(P0.size(), P0.size(), std::numeric_limits<double>::infinity());
    auto P = P0;
    int i = 0;
    int j;
    for (const auto& S1: P0) {
        j = 0;
        for (const auto& S2: P0) {
            if (j < i && m.labels[S1.front()] == m.labels[S2.front()]) {
                auto Stot = merge(S1, S2, P);
                ret(i, j) = compute_delta(m, P);
                unmerge(S1, S2, Stot, P);
            }
            ++j;
        }
        ++i;
    }
    return ret;
}


std::pair<int, int> find_min(const MatrixXd& mat)
{
    int row, col;
    mat.minCoeff(&row, &col);
    return {row, col};
}

std::set<subset>::const_iterator find_nth(const std::set<subset>& P, int n)
{
    auto x = P.begin();
    while (n--) {
        ++x;
    }
    return x;
}


bool merge_best_parts(const geno_matrix& m, std::set<subset>& P)
{
    MatrixXd costs = compute_lumping_costs(m, P);
    /*MSG_DEBUG("CURRENT PARTITION");*/
    /*MSG_DEBUG("" << P);*/
    auto rc = find_min(costs);
    /*MSG_DEBUG("COSTS");*/
    /*MSG_DEBUG(costs);*/
    /*MSG_DEBUG("MIN: " << costs(rc.first, rc.second) << " at " << rc.first << ',' << rc.second);*/
    if (costs(rc.first, rc.second) < std::numeric_limits<double>::infinity()) {
        auto S1 = find_nth(P, rc.first);
        auto S2 = find_nth(P, rc.second);
        subset Stot;
        subset_union(*S1, *S2, Stot);
        P.erase(S1);
        P.erase(S2);
        P.insert(Stot).first;
        return true;
    }
    return false;
}


geno_matrix max_lumping(const geno_matrix& m, size_t max_states=0)
{
    auto P = init_partition(m);
    while (P.size() > max_states && merge_best_parts(m, P));
    /*MSG_DEBUG("FINAL PARTITION");*/
    /*MSG_DEBUG("" << P);*/

    return lump_using_partition_weighted(m, P);
}



MatrixXd compute_lumping_costs0(const geno_matrix& m, const std::set<subset>& P0)
{
    MatrixXd ret = MatrixXd::Constant(m.inf_mat.rows(), m.inf_mat.cols(), std::numeric_limits<double>::infinity());
    std::set<subset> P;
    for (int i = 0; i < m.inf_mat.cols(); ++i) {
        P.insert({i});
    }

    for (const auto& S0: P0) {
        for (int s1: S0) {
            P.erase({s1});
            for (int s2: S0) {
                if (s1 <= s2) {
                    continue;
                }
                P.erase({s2});
                P.insert({s1, s2});

                ret(s1, s2) = compute_delta(m, P);
                /*ret(s2, s1) = ret(s1, s2);*/

                P.erase({s1, s2});
                P.insert({s2});
            }
            P.insert({s1});
        }
    }
    return ret;
}



bool experiment_derisavi0(const MatrixXd& mat, std::set<subset>& P)
{
    /* Derisavi sur son balcon... */
    
    /* Partition on labels */
    /*MSG_DEBUG("INITIAL PARTITION" << std::endl << P);*/

    MatrixXd sums = MatrixXd::Zero(P.size(), mat.cols());

    int row = 0;
    for (const auto& C: P) {
        for (int col = 0; col < mat.cols(); ++col) {
            for (int s: C) {
                sums(row, col) += mat(s, col);
            }
        }
        ++row;
    }

    output_sur_son_balcon(sums, P);

    std::vector<subset> to_remove, to_add;

    for (const auto& C: P) {
        std::map<VectorXd, subset> sets;
        for (int s: C) {
            sets[sums.col(s)].push_back(s);
        }
        if (sets.size() > 1) {
            /*P.erase(C);*/
            to_remove.push_back(C);
            for (const auto& kv: sets) {
                to_add.push_back(kv.second);
            }
        }
    }
    for (const auto& t: to_remove) { P.erase(t); }
    for (const auto& t: to_add) { P.insert(t); }

    /*MSG_DEBUG("NEW PARTITION");*/
    /*MSG_DEBUG("" << P);*/
    return to_remove.size();
}





bool experiment_derisavi(const geno_matrix& m, const MatrixXd& mat, std::set<subset>& P)
{
    /* Derisavi sur son balcon... */
    
    /* Partition on labels */
    /*MSG_DEBUG("INITIAL PARTITION" << std::endl << P);*/

    MatrixXd sums = MatrixXd::Zero(P.size(), mat.cols());

    int row = 0;
    for (const auto& C: P) {
        for (int col = 0; col < mat.cols(); ++col) {
            for (int s: C) {
                sums(row, col) += mat(s, col);
            }
        }
        ++row;
    }

    /*output_sur_son_balcon(sums, P);*/

    std::vector<subset> to_remove, to_add;

    for (const auto& C: P) {
        std::map<VectorXd, subset> sets;
        for (int s: C) {
            sets[sums.col(s)].push_back(s);
        }
        if (sets.size() > 1) {
            MatrixXb closeEnough = MatrixXb::Zero(sets.size(), sets.size());
            std::vector<subset> subsets;
            int i = 0;
            for (const auto& s1: sets) {
                subsets.push_back(s1.second);
                int j = 0;
                for (const auto& s2: sets) {
                    if ((s1.first - s2.first).isZero()) {
                        closeEnough(i, j) = 1;
                    }
                    ++j;
                }
                ++i;
            }
            MatrixXb tmp;
            MatrixXb backup = closeEnough;
            do {
                tmp = closeEnough;
                closeEnough *= closeEnough;
            } while (tmp != closeEnough);
            /*if (closeEnough != MatrixXb::Identity(sets.size(), sets.size())) {*/
                /*MSG_DEBUG("CLOSE ENOUGH");*/
                /*MSG_DEBUG(backup);*/
                /*MSG_DEBUG("--");*/
                /*MSG_DEBUG(closeEnough);*/
            /*}*/
            std::set<VectorXb> cliques;
            for (int i = 0; i < closeEnough.cols(); ++i) {
                cliques.insert(closeEnough.col(i));
            }
            
            /*P.erase(C);*/
            to_remove.push_back(C);
            /*for (const auto& kv: sets) {*/
                /*to_add.push_back(kv.second);*/
            /*}*/
            for (const auto& c: cliques) {
                subset s;
                for (int i = 0; i < c.size(); ++i) {
                    if (c(i)) {
                        s.insert(s.end(), subsets[i].begin(), subsets[i].end());
                    }
                }
                to_add.push_back(s);
            }
        }
    }
    for (const auto& t: to_remove) { P.erase(t); }
    for (const auto& t: to_add) { P.insert(t); }

    /*MSG_DEBUG("NEW PARTITION");*/
    /*MSG_DEBUG("" << P);*/
    /*if (to_remove.size()) {*/
        /*MSG_DEBUG("Found finer partition of size " << P.size());*/
        /*MSG_DEBUG("" << to_remove);*/
    /*} else {*/
        /*MSG_DEBUG("Did not modify partition");*/
    /*}*/
    return !!to_remove.size();
}



geno_matrix approx_lump(const geno_matrix& m, size_t max_size)
{
    auto P = partition_on_labels(m.labels);
    auto P0 = P;
    /*MSG_DEBUG("current partition size " << P.size());*/
    for (; experiment_derisavi(m, m.inf_mat, P);) {
        if (P.size() <= max_size && P.size() > P0.size()) {
            /*MSG_DEBUG("current partition size " << P.size());*/
            P0 = P;
        } else {
            break;
        }
    }
    return lump_using_partition_weighted(m, P0);
    /*return lump_using_partition(m, P0);*/
}


std::map<label_type, double> abundances(geno_matrix& m)
{
    Eigen::Map<Eigen::Array<label_type, Eigen::Dynamic, 1>> labels(m.labels.data(), m.labels.size(), 1);
    MatrixXd abund = m.lim_inf().col(0);
    std::set<label_type> unique(m.labels.begin(), m.labels.end());
    std::map<label_type, double> ret;
    for (const auto& l: unique) {
        ret[l] = ((labels == l).cast<double>().array() * abund.array()).sum();
    }
    return ret;
}


std::ostream& operator << (std::ostream& os, const std::map<label_type, double>& ab)
{
    os << "abundances:";
    for (const auto& kv: ab) {
        os << ' ' << kv.first << ": " << std::left << std::setw(12) << SPELL_STRING((kv.second * 100.) << '%');
    }
    return os;
}


bool operator == (const std::map<label_type, double>& ab1, const std::map<label_type, double>& ab2)
{
    bool ret = true;
    for (const auto& kv: ab1) {
        ret &= abs(kv.second - ab2.find(kv.first)->second) < 1.e-6;
    }
    return ret;
}


void save_matrix(const MatrixXd& m, const std::string& filename)
{
    ofile f(filename);
    int tmpi;
    tmpi = m.rows(); f.write((const char*)&tmpi, sizeof(tmpi));
    tmpi = m.cols(); f.write((const char*)&tmpi, sizeof(tmpi));
    f.write((const char*)m.data(), m.size() * sizeof(double));
}


MatrixXd load_matrix(const std::string& filename)
{
    MatrixXd ret;
    int rows, cols;
    ifile f(filename);
    f.read((char*)&rows, sizeof(rows));
    f.read((char*)&cols, sizeof(cols));
    ret.resize(rows, cols);
    f.read((char*)ret.data(), ret.size() * sizeof(double));
    return ret;
}


MatrixXd test_geno_matrix(const std::string& name, geno_matrix& GEN)
{
    bool do_output = name != "";
    Eigen::IOFormat r_table(Eigen::StreamPrecision, Eigen::DontAlignCols, "\t", "\n", "", "", "", "");
    /*VectorXd A(4);*/
    /*VectorXd H(4);*/
    /*VectorXd B(4);*/
    /*A << 1, 0, 0, 0;*/
    /*H << 0, 1, 1, 0;*/
    /*B << 0, 0, 0, 1;*/
    ofile ofs(SPELL_STRING("values-HA." << name << ".txt"));

    MatrixXd abundance = GEN.lim_inf().col(0);
    /*MSG_DEBUG("### " << name);*/
    /*MSG_DEBUG(GEN);*/
    /*MSG_DEBUG("" << abundances(GEN));*/

    Eigen::Map<Eigen::Array<label_type, Eigen::Dynamic, 1>> labels(GEN.labels.data(), GEN.labels.size(), 1);
    VectorXd A = (labels == label_type{'a', 'a'}).cast<double>();
    VectorXd B;
    if ((labels == label_type('b', 'b')).any()) {
        B = (labels == label_type{'b', 'b'}).cast<double>();
    } else {
        B = A;
    }
    VectorXd H = ((labels == label_type{'a', 'b'}) + (labels == label_type{'b', 'a'})).cast<double>();
    A = (A.array() * abundance.array()).matrix();
    H = (H.array() * abundance.array()).matrix();
    B = (B.array() * abundance.array()).matrix();
    A /= A.sum();
    B /= B.sum();
    H /= H.sum();
    /*MSG_DEBUG("A " << A.transpose());*/
    /*MSG_DEBUG("B " << B.transpose());*/
    /*MSG_DEBUG("H " << H.transpose());*/
    /*A << 1,1,1, 0,0, 0,0,0;*/
    /*H << 0,0,0, 1,1, 0,0,0;*/
    /*B << 0,0,0, 0,0, 1,1,1;*/
    /*VectorXd A(2);*/
    /*VectorXd H(2);*/
    /*A << 1, 0;*/
    /*H << 0, 1;*/
    VectorXd U = A + H + B;

    MatrixXd result(GEN.rows(), 121);
    /*MatrixXd result(GEN.rows(), 3);*/
    int i = 0;
    {
        std::stringstream labelstr;
        labelstr << GEN.labels[0].first << GEN.labels[0].second;
        for (size_t l = 1; l < GEN.labels.size(); ++l) {
            labelstr << '\t' << GEN.labels[l].first << GEN.labels[l].second;
        }
        if (do_output) { ofs << labelstr.str() << std::endl; }
    }
    /* map: A - 30 - B - 5 - H */
    VectorXd outer = (B.array() * (GEN.exp(.05) * H).array()).matrix();
    outer /= outer.sum();
    for (double d = 0; d <= 30; d += .25) {
        MatrixXd prob = ((GEN.exp(d * .01) * A).array() * (GEN.exp(.3 - d * .01) * outer).array()).matrix();
        /*MSG_DEBUG("##DUMP TR(" << d << ")");*/
        /*MSG_DEBUG(GEN.exp(d * .01));*/
        result.col(i++) = prob / prob.sum();
    }

    /*MSG_DEBUG(result.transpose().format(r_table));*/
    if (do_output) { ofs << result.transpose().format(r_table); }
    if (A == B) {
        MatrixXd redux(2, A.size());
        redux.row(0) = (labels == label_type{'a', 'a'}).cast<double>().transpose();
        redux.row(1) = ((labels == label_type{'a', 'b'}) + (labels == label_type{'b', 'a'})).cast<double>().transpose();
        result = redux * result;
    } else {
        MatrixXd redux(3, A.size());
        redux.row(0) = (labels == label_type{'a', 'a'}).cast<double>().transpose();
        redux.row(1) = ((labels == label_type{'a', 'b'}) + (labels == label_type{'b', 'a'})).cast<double>().transpose();
        redux.row(2) = (labels == label_type{'b', 'b'}).cast<double>().transpose();
        result = redux * result;
    }
    return result;
}

int main(int argc, char** argv)
{
    auto A = ancestor_matrix('a');
    auto B = ancestor_matrix('b');

    /*MSG_DEBUG("#############################################################################################");*/
    /*MSG_DEBUG("#######################                  F1                  ################################");*/
    /*MSG_DEBUG("#############################################################################################");*/
    auto F1 = lump(kronecker(kronecker(A, gamete), kronecker(B, gamete)));

    if (0)
    {
        auto BC = lump(kronecker(kronecker(F1, gamete), kronecker(A, gamete)));
        auto BC2 = lump(kronecker(kronecker(BC, gamete), kronecker(A, gamete)));
        VectorXd ab = BC2.lim_inf().col(0);
        MSG_DEBUG("BC2 . ab = " << (BC2.inf_mat * ab).transpose());
        MSG_DEBUG("Ker(BC2) =" << std::endl << BC2.inf_mat.fullPivLu().kernel());
        VectorXd s = BC2.inf_mat.fullPivLu().kernel();

        return 0;
    }

#if 0
    {
        geno_matrix BCn = F1;
        geno_matrix BCa = F1;
        geno_matrix BCa2;

        auto BC = lump(kronecker(kronecker(F1, gamete), kronecker(A, gamete)));
        auto BC2 = lump(kronecker(kronecker(BC, gamete), kronecker(A, gamete)));

        MSG_DEBUG(BC2);

        MSG_DEBUG(std::setw(5) << ""
                  << " | "
                  << std::setw(70) << "                                 FULL                                 "
                  << " | "
                  << std::setw(70) << "                           APPROX (ALL GENS)                          "
                  << " | "
                  << std::setw(70) << "                           APPROX (LAST GEN)                          ");

        for (int i = 0; i < 50; ++i) {
            BCn = lump(kronecker(kronecker(BCn, gamete), kronecker(A, gamete)));
            BCa = approx_lump(kronecker(kronecker(BCa, gamete), kronecker(A, gamete)), 5);
            /*MSG_DEBUG("BC" << i << ' ' << BCn.cols() << ' ' << abundances(BCn));*/
            /*MSG_DEBUG("ap." << ' ' << BCa.cols() << ' ' << abundances(BCa));*/
            /*BCa2 = approx_lump(BCn, 3);*/
            BCa2 = max_lumping(BCn, 5);
            auto Mn = test_geno_matrix("", BCn);
            auto Ma = test_geno_matrix("", BCa);
            auto Ma2 = test_geno_matrix("", BCa2);
            double ntoa = (Mn - Ma).lpNorm<1>();
            double ntoa2 = (Mn - Ma2).lpNorm<1>();
            MSG_DEBUG("=========== DELTA (FORCE) ===========");
            MSG_DEBUG(BCn.inf_mat);
            MSG_DEBUG("--");
            auto P = partition_on_labels(BCn.labels);
            MSG_DEBUG("" << P);
            MSG_DEBUG("delta = " << compute_delta(BCn, P));
            MSG_DEBUG("COSTS" << std::endl << compute_lumping_costs(BCn, P));
            MSG_DEBUG(std::setw(5) << i
                      << " | "
                      << WHITE
                      << std::setw(70) << SPELL_STRING(BCn.cols() << ' ' << abundances(BCn) << " e=0")
                      << NORMAL
                      << " | "
                      << (abundances(BCn) == abundances(BCa) ? GREEN : YELLOW)
                      << std::setw(70) << SPELL_STRING(BCa.cols() << ' ' << abundances(BCa) << " e=" << ntoa)
                      << NORMAL
                      << " | "
                      << (abundances(BCn) == abundances(BCa2) ? GREEN : YELLOW)
                      << std::setw(70) << SPELL_STRING(BCa2.cols() << ' ' << abundances(BCa2) << " e=" << ntoa2)
                      << NORMAL
                      );
        }
    }
#endif

#if 1
    {
        /*geno_matrix Fn = F1;*/
        geno_matrix Fa = F1;
        geno_matrix Fa2 = F1;

        MSG_DEBUG(std::setw(5) << ""
                  << " | "
                  << std::setw(62) << "                             FULL                             "
                  << " | "
                  << std::setw(62) << "                       APPROX (ALL GENS)                      "
                  << " | "
                  << std::setw(62) << "                        APPROX MAX LUMP                       ");

        for (int i = 0; i < 15; ++i) {
            /*Fn = lump(kronecker(Fn, selfing_gamete));*/
            Fa = approx_lump(kronecker(Fa, selfing_gamete), 32);
            /*Fa2 = approx_lump(Fn, 32 + i * 4);*/
            Fa2 = max_lumping(lump(kronecker(Fa2, selfing_gamete)), 32);
            /*auto Mn = test_geno_matrix("", Fn);*/
            MatrixXd Mn = load_matrix(SPELL_STRING("test_geno_matrix_" << i << ".matrix"));
            /*save_matrix(Mn, SPELL_STRING("test_geno_matrix_" << i << ".matrix"));*/
            auto Ma = test_geno_matrix("", Fa);
            auto Ma2 = test_geno_matrix("", Fa2);
            double ntoa = (Mn - Ma).lpNorm<1>();
            double ntoa2 = (Mn - Ma2).lpNorm<1>();
#define OPT(_x) ""
            /*MSG_DEBUG("=========== DELTA (FORCE) ===========");*/
            /*MSG_DEBUG(Fn.inf_mat);*/
            /*MSG_DEBUG("--");*/
            /*auto P = partition_on_labels(Fn.labels);*/
            /*MSG_DEBUG("" << P);*/
            /*MSG_DEBUG("delta = " << compute_delta(Fn, P));*/
            /*MSG_DEBUG("COSTS" << std::endl << compute_lumping_costs(Fn, P));*/
            MSG_DEBUG(std::setw(5) << i
                      << " | "
                      << WHITE
                      << std::setw(62) << OPT(SPELL_STRING(Fn.cols() << ' ' << OPT(abundances(Fn)) << " e=0"))
                      << NORMAL
                      << " | "
                      /*<< (abundances(Fn) == abundances(Fa) ? GREEN : YELLOW)*/
                      << std::setw(62) << SPELL_STRING(Fa.cols() << ' ' << OPT(abundances(Fa)) << " e=" << ntoa)
                      << NORMAL
                      << " | "
                      /*<< (abundances(Fn) == abundances(Fa2) ? GREEN : YELLOW)*/
                      << std::setw(62) << SPELL_STRING(Fa2.cols() << ' ' << OPT(abundances(Fa2)) << " e=" << ntoa2)
                      << NORMAL
                      );
        }
    }
#endif

#if 0
    /*MSG_DEBUG("#########################################################");*/
    /*MSG_DEBUG("F1");*/
    /*MSG_DEBUG(F1);*/
    /*MSG_QUEUE_FLUSH();*/

    /*MSG_DEBUG("#############################################################################################");*/
    /*MSG_DEBUG("#######################                  F2                  ################################");*/
    /*MSG_DEBUG("#############################################################################################");*/
    auto F2 = lump(kronecker(kronecker(F1, gamete), kronecker(F1, gamete)));

    /*MSG_DEBUG("#########################################################");*/
    /*MSG_DEBUG("F2");*/
    /*MSG_DEBUG(F1);*/
    /*MSG_QUEUE_FLUSH();*/

    /*MSG_DEBUG("#############################################################################################");*/
    /*MSG_DEBUG("#######################                  F3                  ################################");*/
    /*MSG_DEBUG("#############################################################################################");*/
    auto F3 = lump(kronecker(F2, selfing_gamete), true);

    /*MSG_DEBUG("#############################################################################################");*/
    /*MSG_DEBUG("#######################               SPLIT F3               ################################");*/
    /*MSG_DEBUG("#############################################################################################");*/
    auto F3s = split_matrix(F3);
    /* F3s is boring, because it is exactly F3 */

    /*MSG_DEBUG("#############################################################################################");*/
    /*MSG_DEBUG("#######################                  F4                  ################################");*/
    /*MSG_DEBUG("#############################################################################################");*/
    auto F4 = lump(kronecker(F3, selfing_gamete), true);

    /*MSG_DEBUG("#############################################################################################");*/
    /*MSG_DEBUG("#######################               SPLIT F4               ################################");*/
    /*MSG_DEBUG("#############################################################################################");*/
    auto F4s = split_matrix(F4);

    /*MSG_DEBUG("#############################################################################################");*/
    /*MSG_DEBUG("#######################                  F5                  ################################");*/
    /*MSG_DEBUG("#############################################################################################");*/
    auto F5 = lump(kronecker(F4, selfing_gamete), true);

    /*MSG_DEBUG("#############################################################################################");*/
    /*MSG_DEBUG("#######################               SPLIT F5               ################################");*/
    /*MSG_DEBUG("#############################################################################################");*/
    auto F5s = split_matrix(F5);

    /*auto P = partition_on_labels(F4.labels);*/
    /*while (experiment_derisavi(F4.inf_mat, P));*/
    /*experiment_derisavi(F4.inf_mat, P);*/
    /*auto F4s = lump_using_partition(F4, P);*/

    /*MSG_DEBUG("#############################################################################################");*/
    /*MSG_DEBUG("#######################                  F6                  ################################");*/
    /*MSG_DEBUG("#############################################################################################");*/
    auto F6 = lump(kronecker(F5, selfing_gamete));

    /*MSG_DEBUG("#############################################################################################");*/
    /*MSG_DEBUG("#######################               SPLIT F6               ################################");*/
    /*MSG_DEBUG("#############################################################################################");*/
    auto F6s = split_matrix(F6);

    /*MSG_DEBUG("#############################################################################################");*/
    /*MSG_DEBUG("#######################                  F7                  ################################");*/
    /*MSG_DEBUG("#############################################################################################");*/
    auto F7 = lump(kronecker(F6, selfing_gamete));

    /*MSG_DEBUG("#############################################################################################");*/
    /*MSG_DEBUG("#######################               SPLIT F7               ################################");*/
    /*MSG_DEBUG("#############################################################################################");*/
    auto F7s = split_matrix(F7);

    test_geno_matrix("F7", F7);

    auto P = partition_on_labels(F7.labels);
    for (int i = 0; experiment_derisavi(F7.inf_mat, P); ++i) {
        auto F = lump_using_partition(F7, P);
        /*auto fs = lump(kronecker(F, selfing_gamete), true);*/
        /*MSG_DEBUG("TEST SELFING F7-lump" << i << ": " << fs.inf_mat.cols());*/
        test_geno_matrix(SPELL_STRING("F7-lump" << i), F);
    }

    auto F3a = approx_lump(kronecker(F2, selfing_gamete), (size_t) (F2.cols() * 2));
    test_geno_matrix("F3-approx", F3a);
    auto F4a = approx_lump(kronecker(F3a, selfing_gamete), (size_t) (F3a.cols() * 2));
    test_geno_matrix("F4-approx", F4a);
    auto F5a = approx_lump(kronecker(F4a, selfing_gamete), (size_t) (F4a.cols() * 2));
    test_geno_matrix("F5-approx", F5a);
    auto F6a = approx_lump(kronecker(F5a, selfing_gamete), (size_t) (F5a.cols() * 1.5));
    test_geno_matrix("F6-approx", F6a);
    auto F7a = approx_lump(kronecker(F6a, selfing_gamete), (size_t) (F6a.cols() * 1.5));
    test_geno_matrix("F7-approx", F7a);

    MSG_DEBUG("CHECKING FOR FIXED POINT (IYD™)");

#if 0
    int n_gen_full;
    int n_gen_squeezed = 50;
    for (n_gen_full = 0; n_gen_full < 1; ++n_gen_full) {
        auto Ff = F2;
        for (int i = 0; i < n_gen_full; ++i) {
            Ff = lump(kronecker(Ff, selfing_gamete), true);
        }
        auto Fs = Ff;
        for (int i = 0; i < n_gen_squeezed; ++i) {
            auto tmp = approx_lump(kronecker(Fs, selfing_gamete), Ff.cols());
            if (tmp.cols() != Fs.cols()) {
                MSG_DEBUG(n_gen_full << "\t" << i << "\tNA");
                break;
            }
            /*MSG_DEBUG(MATRIX_SIZE(tmp.inf_mat) << " " << MATRIX_SIZE(Fs.inf_mat));*/
            /*MSG_QUEUE_FLUSH();*/
            MSG_DEBUG(n_gen_full << "\t" << i << "\t" << (tmp.inf_mat - Fs.inf_mat).lpNorm<2>());
            /*MSG_DEBUG(tmp);*/
            MSG_DEBUG("--");
            MSG_DEBUG(tmp.inf_mat);
            MSG_DEBUG("--");
            MSG_QUEUE_FLUSH();
            Fs = tmp;
        }
    }
#endif

    {
        ofile f("tailles.txt");
        auto F = approx_lump(kronecker(F7, selfing_gamete), F7.cols());
        int i;
        for (i = 0; i < 200; ++i) {
            MSG_DEBUG("SUMS ZERO " << F.inf_mat.colwise().sum().isZero());
            MSG_DEBUG("" << abundances(F));
            if (F.inf_mat.cols() == 4) {
                MSG_DEBUG("---- " << i);
                MSG_DEBUG(F.inf_mat);
            }
            f << i << "\t" << F.inf_mat.cols() << std::endl;
            F = approx_lump(kronecker(F, selfing_gamete), F7.cols());
        }
        MSG_DEBUG("SUMS ZERO " << F.inf_mat.colwise().sum().isZero());
        f << i << "\t" << F.inf_mat.cols() << std::endl;
    }

    /*MSG_DEBUG("A");*/
    /*MSG_DEBUG(A);*/
    /*MSG_DEBUG("B");*/
    /*MSG_DEBUG(B);*/
    /*MSG_DEBUG("F1");*/
    /*MSG_DEBUG(F1);*/
    /*MSG_DEBUG("F2");*/
    /*MSG_DEBUG(F2);*/
    /*MSG_DEBUG("F3");*/
    /*MSG_DEBUG(F3);*/

    /*MSG_DEBUG("F2@0");*/
    /*MSG_DEBUG(F2.exp(0));*/
    /*MSG_DEBUG("F2@1000");*/
    /*MSG_DEBUG(F2.exp(1000));*/
    /*MSG_DEBUG("F3@0");*/
    /*MSG_DEBUG(F3.exp(0));*/
    /*MSG_DEBUG("F3@1000");*/
    /*MSG_DEBUG(F3.exp(1000));*/
#endif
#if 0
    MSG_DEBUG("#############################################################################################");
    MSG_DEBUG("#######################                  BC                  ################################");
    MSG_DEBUG("#############################################################################################");
    auto BC = lump(kronecker(kronecker(F1, gamete), kronecker(A, gamete)));
    MSG_DEBUG("#############################################################################################");
    MSG_DEBUG("#######################                  BC2                 ################################");
    MSG_DEBUG("#############################################################################################");
    auto BC2 = lump(kronecker(kronecker(BC, gamete), kronecker(A, gamete)));
    /*MSG_DEBUG("BC");*/
    /*MSG_DEBUG(BC);*/
    /*MSG_DEBUG("BC2");*/
    /*MSG_DEBUG(BC2);*/
    /*MSG_DEBUG("BC@0");*/
    /*MSG_DEBUG(BC.exp(0));*/
    /*MSG_DEBUG("BC@1000");*/
    /*MSG_DEBUG(BC.exp(1000));*/
    /*MSG_DEBUG("BC2@0");*/
    /*MSG_DEBUG(BC2.exp(0));*/
    /*MSG_DEBUG("BC2@1000");*/
    /*MSG_DEBUG(BC2.exp(1000));*/
#endif

#if 0
    test_geno_matrix("F3", F3);
    test_geno_matrix("F3s", F3s);
    test_geno_matrix("F4", F4);
    test_geno_matrix("F4s", F4s);
    test_geno_matrix("F5", F5);
    test_geno_matrix("F5s", F5s);
    test_geno_matrix("F6", F6);
    test_geno_matrix("F6s", F6s);
#endif
    return 0;
    (void) argc; (void) argv;
}

