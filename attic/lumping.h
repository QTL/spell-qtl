/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_LUMPERJACK_H_
#define _SPEL_LUMPERJACK_H_

/*#include <boost/dynamic_bitset.hpp>*/
/*#include <Eigen/Jacobi>*/
/*#include <Eigen/IterativeLinearSolvers>*/
#include <deque>
#include <set>
#include <unordered_map>
#include <map>
#include "algebraic_genotype.h"
#include <iostream>
#include "chrono.h"

/*typedef boost::dynamic_bitset<> bitset;*/
/*typedef std::set<bitset> Partition;*/

/*typedef std::set<int> subset;*/
typedef std::vector<int> subset;

#if 1
template <typename K, typename V, typename H=std::hash<K>, typename A=std::allocator<std::pair<const K, V>>>
using lumper_map = std::unordered_map<K, V, H, std::equal_to<K>, A>;
#else
template <typename K, typename V, typename C=std::less<K>, typename A=std::allocator<std::pair<const K, V>>>
using lumper_map = std::map<K, V, C, A>;
#endif

inline std::ostream& operator << (std::ostream& os, const subset& s)
{
    os << '{';
    auto cur = s.begin(), end = s.end();
    if (cur != end) {
        os << (*cur);
        for (++cur; cur != end; ++cur) {
            os << ' ' << (*cur);
        }
    }
    return os << '}';
}


inline std::ostream& operator << (std::ostream& os, const std::set<subset>& S)
{
    for (auto s: S) {
        os << s << std::endl;
    }
    return os;
}

template <typename K>
std::ostream& operator << (std::ostream& os, const lumper_map<K, subset>& km)
{
    std::string sep = "";
    os << '{';
    for (const auto& kv: km) {
        os << sep << kv.first << " => " << kv.second;
        if (sep.size() == 0) { sep = ", "; }
    }
    return os << '}';
}

template <typename scalar_type>
struct exact_compare {
    typedef scalar_type key_type;
    bool operator () (const scalar_type& s1, const scalar_type& s2) const
    {
        return s1 < s2;
    }

    template <typename V, typename COMP, typename A>
        static V& get(lumper_map<key_type, V, COMP, A>& m, const scalar_type& k)
        {
            return m[k];
        }

    template <typename V, typename COMP, typename A>
        static lumper_map<key_type, V, COMP, A>
        postprocess(const lumper_map<key_type, V, COMP, A>& m) { return m; }
};


template <typename scalar_type>
struct fuzzy_compare;

template <>
struct fuzzy_compare<algebraic_genotype> {
    struct key_type {
        std::set<algebraic_genotype> keys;
        key_type() : keys() {}
        key_type(const algebraic_genotype& s) : keys() { keys.insert(s); }
        key_type operator = (const key_type& k)
        {
            keys = k.keys;
            return *this;
        }
        friend std::ostream& operator << (std::ostream& os, const key_type& k)
        {
            auto ki = k.keys.begin(), kj = k.keys.end();
            if (ki == kj) {
                return os;
            }
            os << *ki++;
            for (; ki != kj; ++ki) {
                os << ' ' << *ki;
            }
            return os;
        }

        void amend(const algebraic_genotype& ag) const
        {
            const_cast<std::set<algebraic_genotype>*>(&keys)->insert(ag);
        }

        void amend(const key_type& k) const
        {
            for (auto ag: k.keys) {
                const_cast<std::set<algebraic_genotype>*>(&keys)->insert(ag);
            }
        }
    };
    bool operator () (const algebraic_genotype& s1, const algebraic_genotype& s2) const
    {
        return s1.poly.inf(s2.poly);
    }
    bool operator () (const key_type& s1, const algebraic_genotype& s2) const
    {
        bool ok = true;
        for (auto s: s1.keys) {
            ok &= s.poly.inf(s2.poly);
        }
        return ok;
    }
    bool operator () (const algebraic_genotype& s1, const key_type& s2) const
    {
        bool ok = true;
        for (auto s: s2.keys) {
            ok &= s1.poly.inf(s.poly);
        }
        return ok;
    }
    bool operator () (const key_type& s1, const key_type& s2) const
    {
        bool ok = true;
        for (auto a: s1.keys) {
            for (auto b: s2.keys) {
                ok &= a.poly.inf(b.poly);
            }
        }
        return ok;
    }

    template <typename V, typename COMP, typename A>
        static V& get(lumper_map<key_type, V, COMP, A>& m, const algebraic_genotype& k)
        {
            auto i = m.find(k);
            if (i == m.end()) {
                /*std::cout << "new key " << k << std::endl;*/
                return m.insert({k, V()}).first->second;
            }
            /*const_cast<std::set<algebraic_genotype>*>(&i->first.keys)->insert(k);*/
            i->first.amend(k);
            return i->second;
        }

    template <typename V, typename COMP, typename A>
        static V& get(lumper_map<key_type, V, COMP, A>& m, const key_type& k)
        {
            auto i = m.find(k);
            if (i == m.end()) {
                /*std::cout << "new key " << k << std::endl;*/
                return m.insert({k, V()}).first->second;
            }
            /*const_cast<std::set<algebraic_genotype>*>(&i->first.keys)->insert(k);*/
            i->first.amend(k);
            return i->second;
        }

    template <typename V, typename COMP, typename A>
        static lumper_map<key_type, V, COMP, A>
        postprocess(const lumper_map<key_type, V, COMP, A>& m)
        {
            lumper_map<key_type, V, COMP, A> ret;
            for (auto kv: m) {
                get(ret, kv.first).insert(kv.second.begin(), kv.second.end());
            }
            return ret;
        }
};

template <typename scalar_type>
struct greedy_compare;

template <>
struct greedy_compare<algebraic_genotype> {
    typedef locus_pair key_type;

    bool operator () (const algebraic_genotype& s1, const algebraic_genotype& s2) const
    {
        return s1.genotype < s2.genotype;
    }
    bool operator () (const key_type& s1, const algebraic_genotype& s2) const
    {
        return s1 < s2.genotype;
    }
    bool operator () (const algebraic_genotype& s1, const key_type& s2) const
    {
        return s1.genotype < s2;
    }
    bool operator () (const key_type& s1, const key_type& s2) const
    {
        return s1 < s2;
    }

    template <typename V, typename COMP, typename A>
        static V& get(lumper_map<key_type, V, COMP, A>& m, const algebraic_genotype& k)
        {
            return m[k.genotype];
        }

    template <typename V, typename COMP, typename A>
        static V& get(lumper_map<key_type, V, COMP, A>& m, const key_type& k)
        {
            return m[k];
        }

    template <typename V, typename COMP, typename A>
        static lumper_map<key_type, V, COMP, A>
        postprocess(const lumper_map<key_type, V, COMP, A>& m)
        {
            return m;
        }
};

#define LUMP_BY_COLS

#define OLD_IMPL_WITH_ALGEBRAIC_GENOTYPES

#ifdef OLD_IMPL_WITH_ALGEBRAIC_GENOTYPES

namespace std {
    template <>
        struct hash<algebraic_genotype> {
            size_t operator () (const algebraic_genotype& ag) const
            {
#if 0
                std::hash<impl::f_polynom> hi;
                /*std::hash<decltype(fast_polynom::value)> hi;*/
                /*return hi(*(size_t*) &ag.genotype)*/
                std::hash<allele_pair> ha;
                size_t ret = (((ha(ag.genotype.first) * 0xd3adbe3f)
                              ^ ha(ag.genotype.second)) * 0xd3adbe3f)
                             ^ hi(ag.poly);
                             /*^ hi(ag.poly.value);*/
                /*MSG_DEBUG("ag_hash(" << ag << ")=" << ret);*/
                return ret;
#else
                std::hash<decltype(fast_polynom::value)> hi;
                std::hash<locus_pair> hg;
                return (hg(ag.genotype) * 0xdeadbeef) ^ hi(ag.poly.value);
#endif
            }
        };
}

template <typename GENERATION, typename key_traits>
struct lumper {
    typedef typename GENERATION::col_label_type label_type;
    typedef typename GENERATION::matrix_type::Scalar scalar_type;
    typedef lumper_map<typename key_traits::key_type, subset> key_map;

    const GENERATION& G;
    bool exact;
    chrono pol, cs, ck, tr, tm;

    lumper(const GENERATION& x, bool e=false)
        : G(x), exact(e)
        , pol(), cs(), ck(), tr(), tm()
    {}

    scalar_type compute_sum(size_t s, const subset& B)
    {
        /*MSG_DEBUG("COMPUTE_SUM S " << s << " B " << B);*/
        cs.start();
        auto bi = B.begin();
        auto bj = B.end();
        if (bi == bj) {
            return {};
        }
        scalar_type accum(G.data(*bi++, s));
        for (; bi != bj; ++bi) {
            accum += G.data(*bi, s);
        }
        cs.stop();
        return accum;
    }

    key_map compute_keys(const subset& C, const subset& B)
        {
            ck.start();
            /*lumper_map<scalar_type, subset> ret;*/
            key_map ret;
            /*for (size_t i = C.find_first(); i != subset::npos; i = C.find_next(i)) {*/
            for (int i: C) {
                scalar_type accum = compute_sum(i, B);
                /*if (ret[accum].size() == 0) {*/
                    /*ret[accum].resize(G.column_labels.size());*/
                /*}*/
                /*ret[accum].set(i);*/
                //key_traits::get(ret, accum)./*insert*/push_back(i);
                ret[accum].push_back(i);
                /*MSG_DEBUG("CK " << accum << " " << ret[accum]);*/
                /*std::cout << "added " << accum << ", " << i << " to ";*/
                /*auto kv = ret.find(accum);*/
                /*if (kv == ret.end()) {*/
                    /*std::cout << "BUG!" << std::endl;*/
                /*} else {*/
                    /*std::cout << kv->first << ", " << kv->second << std::endl;*/
                /*}*/
            }
#if 0
            std::cout << std::endl << "Subsets " << C << " vs " << B << std::endl;
            for (auto kv: ret) {
                std::cout << kv.first << std::endl << "    " << kv.second << std::endl;
            }
#endif
            ck.stop();
            return ret;
        }

    std::vector<subset> partition_on_labels()
    {
        pol.start();
        std::vector<subset> ret;
        lumper_map<label_type, subset> tmp;
        for (size_t i = 0; i < G.column_labels.size(); ++i) {
            auto& l = G.column_labels[i];
            /*if (tmp[l].size() == 0) {*/
                /*tmp[l].resize(G.column_labels.size());*/
            /*}*/
            /*tmp[l].set(i);*/
            tmp[l]./*insert*/push_back(i);
        }
        ret.reserve(tmp.size());
        for (auto& kv: tmp) {
            /*std::cout << kv.first << "\t" << kv.second << std::endl;*/
            /*MSG_DEBUG(kv.first << "\t" << kv.second);*/
            ret.push_back(kv.second);
        }
        pol.stop();
        return ret;
    }


    std::set<subset> try_refine(std::set<subset>& P)
    {
        tr.start();
        std::deque<const subset*> stack;
        /*std::deque<const subset*> stack;*/
        for (auto& x: P) {
            stack.push_back(&x);
        }
        /* It seems removing from the front makes the algorithm faster */
        while (stack.size()) {
            /*const subset& C = *stack.back();*/
            const subset& C = *stack.front();
            /*std::cout << "** " << C << std::endl;*/
            /*std::cout << '.' << std::flush;*/
            /*stack.pop_back();*/
            stack.pop_front();
            /*MSG_DEBUG("TACKLING " << C);*/
            if (C.size() <= 1) {
                continue;
            }
            for (auto& B: P) {
                /*if (B.size() <= 1) {*/
                    /*continue;*/
                /*}*/
                key_map subsets = compute_keys(C, B);
                /*size_t size = 0;*/
                /*while (size != subsets.size()) {*/
                    /*size = subsets.size();*/
                    /*subsets = key_traits::postprocess(subsets);*/
                /*}*/
                /*MSG_DEBUG("KEYS: " << subsets);*/
                if (subsets.size() > 1) {
                    MSG_DEBUG("KEYS " << G.column_labels[subsets.begin()->second.front()] << ": " << subsets);
                    /*MSG_DEBUG("CONFLICT IN " << C);*/
                    /*for (const auto& kv: subsets) {*/
                        /*MSG_DEBUG(" * " << kv.first << ' ' << kv.second);*/
                    /*}*/
                    P.erase(C);
                    auto si = subsets.begin();
                    auto sj = subsets.end();
                    while (si != sj) {
                        auto io = P.insert(si->second);
                        stack.push_back(&*io.first);
                        ++si;
                    }
                    /*for (auto& s: subsets) {*/
                        /*auto io = P.insert(s.second);*/
                        /*stack.push_back(&*io.first);*/
                    /*}*/
                    break;
                }
            }
        }
        tr.stop();
        /*std::cout << std::endl;*/
        /*std::cout << P.size() << std::endl;*/
        /*std::cout << P << std::endl;*/
        /*MSG_DEBUG("P.size() = " << P.size());*/
        /*MSG_DEBUG("P = " << P);*/
        /*MSG_DEBUG("Lumping time: ck=" << ck.accum << " cs=" << cs.accum << " pol=" << pol.accum << " tr=" << tr.accum);*/
        return P;
    }


    std::set<subset> refine_all()
    {
        std::vector<subset> tmp = partition_on_labels();
        /*MSG_DEBUG("INITIAL PARTITION" << std::endl << tmp);*/
        std::set<subset> P(tmp.begin(), tmp.end());
        return try_refine(P);
    }

    GENERATION to_matrix(const std::set<subset>& P)
    {
        tm.start();
        std::vector<label_type> labels;
        for (auto& S: P) {
            /*labels.push_back(G.column_labels[S.find_first()]);*/
            labels.push_back(G.column_labels[*S.begin()]);
        }
        GENERATION ret(labels, labels);
        /*Eigen::MatrixXd norms(labels.size(), labels.size());*/
        size_t i = 0;
        size_t j;
        for (auto& C: P) {
            j = 0;
            /*size_t s = C.find_first();*/
            /*size_t s = *C.begin();*/
            for (auto& B: P) {
#if 1
                algebraic_genotype accum = compute_sum(*C.begin(), B);
#ifdef LUMP_BY_COLS
                ret.data(j, i) = accum;
                /*norms(j, i) = ret.data(j, i).poly.norm();*/
#else
                ret.data(i, j) = accum;
                /*norms(i, j) = ret.data(i, j).poly.norm();*/
#endif
#else
                auto cur = C.begin();
                auto end = C.end();
                algebraic_genotype accum = compute_sum(*cur, B);
                for (++cur; cur != end; ++cur) {
                    accum += compute_sum(*cur, B);
                }
#ifdef LUMP_BY_COLS
                ret.data(j, i) = accum * algebraic_genotype((rs_polynom){0, 0, {1. / C.size()}});
                norms(j, i) = ret.data(j, i).poly.norm();
#else
                ret.data(i, j) = accum * algebraic_genotype((rs_polynom){0, 0, {1. / C.size()}});
                norms(i, j) = ret.data(i, j).poly.norm();
#endif
#endif
                ++j;
            }
            ++i;
        }
        tm.stop();
        /*MSG_DEBUG("Created lumped matrix in " << tm.accum << " seconds");*/
        /*std::cout << "NORMS" << std::endl << norms << std::endl;*/
        /*std::cout << "BEFORE LUMPING" << std::endl << G << std::endl;*/
        /*std::cout << "AFTER LUMPING" << std::endl << ret << std::endl;*/
        return ret;
    }
};

#else

struct fp_comp {
    bool operator () (fast_polynom p1, fast_polynom p2) const { return p1.value < p2.value; }
};

template <typename GENERATION, typename key_traits>
struct lumper {
    typedef typename GENERATION::col_label_type label_type;
    typedef typename GENERATION::matrix_type::Scalar scalar_type;
    /*typedef lumper_map<typename key_traits::key_type, subset, key_traits> key_map;*/
    typedef lumper_map<fast_polynom, subset, fp_comp> key_map;
    const GENERATION& G;
    bool exact;
    chrono pol, cs, ck, tr, tm;

    lumper(const GENERATION& x, bool e=false)
        : G(x), exact(e)
        , pol(), cs(), ck(), tr(), tm()
    {}

    fast_polynom compute_sum(size_t s, const subset& B)
    {
        cs.start();
        /*scalar_type accum = scalar_type();*/
        fast_polynom accum = fast_polynom::zero;
        /*std::cout << "accum = " << accum << std::endl;*/
        /*for (size_t j = B.find_first(); j != subset::npos; j = B.find_next(j)) {*/
        for (int j: B) {
            /*std::cout << "accum + " << G.data(i, j);*/
#ifdef LUMP_BY_COLS
            accum += G.data(j, s).poly;
#else
            accum += G.data(s, j);
#endif
            /*std::cout << " = " << accum << std::endl;*/
        }
        cs.stop();
        return accum;
    }

    key_map compute_keys(const subset& C, const subset& B)
        {
            ck.start();
            /*lumper_map<scalar_type, subset> ret;*/
            key_map ret;
            /*for (size_t i = C.find_first(); i != subset::npos; i = C.find_next(i)) {*/
            for (int i: C) {
                fast_polynom accum = compute_sum(i, B);
                /*if (ret[accum].size() == 0) {*/
                    /*ret[accum].resize(G.column_labels.size());*/
                /*}*/
                /*ret[accum].set(i);*/
                //key_traits::get(ret, accum)./*insert*/push_back(i);
                ret[accum].push_back(i);
                /*std::cout << "added " << accum << ", " << i << " to ";*/
                /*auto kv = ret.find(accum);*/
                /*if (kv == ret.end()) {*/
                    /*std::cout << "BUG!" << std::endl;*/
                /*} else {*/
                    /*std::cout << kv->first << ", " << kv->second << std::endl;*/
                /*}*/
            }
#if 0
            std::cout << std::endl << "Subsets " << C << " vs " << B << std::endl;
            for (auto kv: ret) {
                std::cout << kv.first << std::endl << "    " << kv.second << std::endl;
            }
#endif
            ck.stop();
            return ret;
        }

    std::vector<subset> partition_on_labels()
    {
        pol.start();
        std::vector<subset> ret;
        lumper_map<label_type, subset> tmp;
        for (size_t i = 0; i < G.column_labels.size(); ++i) {
            auto& l = G.column_labels[i];
            /*if (tmp[l].size() == 0) {*/
                /*tmp[l].resize(G.column_labels.size());*/
            /*}*/
            /*tmp[l].set(i);*/
            tmp[l]./*insert*/push_back(i);
        }
        ret.reserve(tmp.size());
        for (auto& kv: tmp) {
            /*std::cout << kv.first << "\t" << kv.second << std::endl;*/
            ret.push_back(kv.second);
        }
        pol.stop();
        return ret;
    }


    std::set<subset> try_refine(std::set<subset>& P)
    {
        tr.start();
        std::vector<const subset*> stack;
        /*std::deque<const subset*> stack;*/
        for (auto& x: P) {
            stack.push_back(&x);
        }
        while (stack.size()) {
            const subset& C = *stack.back();
            /*const subset& C = *stack.front();*/
            /*std::cout << "** " << C << std::endl;*/
            /*std::cout << '.' << std::flush;*/
            /*MSG_DEBUG("TACKLING " << C);*/
            stack.pop_back();
            /*stack.pop_front();*/
            if (C.size() <= 1) {
                continue;
            }
            for (auto& B: P) {
                /*if (B.size() <= 1) {*/
                    /*continue;*/
                /*}*/
                key_map subsets = compute_keys(C, B);
                /*MSG_DEBUG("KEYS: " << subsets);*/
                /*size_t size = 0;*/
                /*while (size != subsets.size()) {*/
                    /*size = subsets.size();*/
                    /*subsets = key_traits::postprocess(subsets);*/
                /*}*/
                /* MARK */
                if (subsets.size() > 1) {
                    P.erase(C);
                    for (auto& s: subsets) {
                        auto io = P.insert(s.second);
                        if (!io.second) {
                            /*MSG_DEBUG("PROUT SUBSET ALREADY IN P " << (*io.first));*/
                        }
                        if (io.first->size() > 1) {
                            stack.push_back(&*io.first);
                        }
                    }
                    break;
                }
            }
        }
        tr.stop();
        /*std::cout << std::endl;*/
        /*std::cout << P.size() << std::endl;*/
        /*std::cout << P << std::endl;*/
        /*MSG_DEBUG("P.size() = " << P.size());*/
        /*MSG_DEBUG("P = " << P);*/
        /*MSG_DEBUG("Lumping time: ck=" << ck.accum << " cs=" << cs.accum << " pol=" << pol.accum << " tr=" << tr.accum);*/
        return P;
    }


    std::set<subset> refine_all()
    {
        std::vector<subset> tmp = partition_on_labels();
        std::set<subset> P(tmp.begin(), tmp.end());
        return try_refine(P);
    }

    GENERATION to_matrix(const std::set<subset>& P)
    {
        tm.start();
        std::vector<label_type> labels;
        for (auto& S: P) {
            labels.push_back(G.column_labels[*S.begin()]);
        }
        GENERATION ret(labels, labels);
        size_t i = 0;
        size_t j;
        for (auto& C: P) {
            j = 0;
            for (auto& B: P) {
                algebraic_genotype accum = compute_sum(*C.begin(), B);
                accum.type = algebraic_genotype::Type::Genotype;
                accum.genotype = {labels[B.front()], labels[C.front()]};

#ifdef LUMP_BY_COLS
                ret.data(j, i) = accum;
#else
                ret.data(i, j) = accum;
#endif
                ++j;
            }
            ++i;
        }
        tm.stop();
        /*MSG_DEBUG("Created lumped matrix in " << tm.accum << " seconds");*/
        return ret;
    }
};
#endif


#endif

