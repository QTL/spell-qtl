/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cache2.h"


msg_handler_t::lock_type msg_handler_t::mutex;



double toto(double a, double b)
{
    return a * b;
}

double prout(double a, double b)
{
    return (a + b) + a * b;
}


double sum(const std::vector<double>& d)
{
    double accum = 0;
    for (auto x: d) { accum += x; }
    return accum;
}




int main(int argc, char** argv)
{
    /*value<double> pouet = use_factory(&toto, 23., 42.);*/
    value<double> a = 23.;
    value<double> b = 42.;
    value<double> pouet = make_value(toto, a, b);
    std::cout << (*pouet) << std::endl;
    pouet = make_cached_value(toto, a, b);
    std::cout  << (*pouet) << std::endl;
    std::cout << typeid(prout).name() << std::endl;
    std::cout << typeid(toto).name() << std::endl;
    for (auto k: range<int>(0, 10, 2)) {
        std::cout << "range " << (*k) << std::endl;
    }
    collection<double> coll = make_collection(toto, value<double>(37.), range<double>(1, 11, 1));
    for (value<double>& c: coll) {
        std::cout << "table of 37: " << (*c) << std::endl;
    }
    collection<double> coll2 = make_collection(toto, range<double>(1, 11, 1), range<double>(1, 11, 1));
    for (value<double>& c: coll2) {
        std::cout << "tables of 1 to 10: " << (*c) << std::endl;
    }
    collection<double> coll3 = make_collection(toto, value<double>(1), value<double>(1));
    for (value<double>& c: coll3) {
        std::cout << "1 x 1 = " << (*c) << std::endl;
    }
    auto test_reuse1 = make_collection(toto, value<double>(1), value<double>(1));
    auto test_reuse2 = make_collection(toto, value<double>(1), value<double>(1));
    value<double> test_eq = 10.;
    std::cout << "valid with init ? " << test_eq.valid() << std::endl;
    value<double> invalid;
    std::cout << "valid without init ? " << invalid.valid() << std::endl;
    invalid = 32.;
    std::cout << "valid with late init ? " << invalid.valid() << std::endl;
    for (auto& c: range<double>(0, 21, 10)) {
        std::cout << test_eq << " == " << c << " ? " << (test_eq == c) << std::endl;
    }
    value<std::vector<double>> d = std::vector<double>{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    std::cout << "sum(1-10) = " << (*make_value(sum, d)) << std::endl;
    return 0;
    (void)argc; (void)argv;
}


