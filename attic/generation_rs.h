#ifndef _SPEL_GENERATION_RS_H_
#define _SPEL_GENERATION_RS_H_

#include "generation_rs_fwd.h"

inline
std::vector<double> compute_steps(const std::vector<double>& loci,
                                  double step,
                                  const std::vector<std::pair<double, double>>& exclusion)
{
    std::vector<double> ret;
    auto next_locus = loci.begin();
    auto done = loci.end();
    double cursor = *next_locus;
    auto eit = exclusion.begin();
    auto eend = exclusion.end();
    while (next_locus != done) {
        while (next_locus != done && (cursor + LOCUS_EPSILON) >= *next_locus) {
            cursor = *next_locus;
            ++next_locus;
        }
        /* FIXME check that no segment outside the exclusion zones will be skipped */
        while (eit != eend && cursor > eit->second) { ++eit; } // skip past segments
        if (eit == eend || cursor < eit->first) {
                ret.push_back(cursor);
        }
        cursor += step;
    }
    return ret;
}


inline
std::vector<double> compute_steps(const std::vector<double>& loci,
                                  double step)
{
    std::vector<std::pair<double, double>> excl;
    return compute_steps(loci, step, excl);
}


template <typename T, typename M>
std::ostream& dump_binmat(std::ostream& os, const std::vector<T>& labels, const M& m)
{
    labelled_matrix<M, T, int> tmp;
    tmp.data = m;
    tmp.row_labels = labels;
    os << tmp << std::endl;
    return os;
}




inline
GenerationRS convert(const GenoMatrix& gm)
{
    std::vector<allele_pair> labels;
    std::map<allele_pair, int> label_counts;
    for (int i = 0; i < gm.innerSize(); ++i) {
        labels.push_back(gm(i, 0).genotype.first);
        label_counts[labels.back()]++;
    }
    /*for (auto kv: label_counts) {*/
        /*CREATE_MESSAGE(msg_channel::Log, MESSAGE(kv.first << ':' << kv.second << ' '));*/
    /*}*/
    /*MSG_DEBUG(std::endl << labels << std::endl);*/
    for (int i = 0; i < gm.outerSize(); ++i) {
        if (!(labels[i] == gm(0, i).genotype.second)) {
            MSG_ERROR("oops label mismatch @" << i << ": " << labels[i] << ' ' << gm(0, i).genotype.second, "");
        }
    }
    GenerationRS ret(labels);
    ret.data = gm;
    return ret;
}


template <algebraic_genotype::Type T>
inline
const GenoMatrix& gametes_factory()
{
    static GenoMatrix ret(2, 2);
    static bool init = false;
    if (!init) {
#define _(x) {x}
        ret(0, 0) = {{{_(0), _(0)},
                      {_(0), _(0)}},
                     T, fast_polynom::s};
        ret(0, 1) = {{{_(0), _(0)},
                      {_(1), _(0)}},
                     T, fast_polynom::r};
        ret(1, 0) = {{{_(1), _(0)},
                      {_(0), _(0)}},
                     T, fast_polynom::r};
        ret(1, 1) = {{{_(1), _(0)},
                      {_(1), _(0)}},
                     T, fast_polynom::s};
#undef _
        /*MSG_DEBUG(ret);*/
        init = true;
    }
    return ret;
}



inline
GenoMatrix sib_gametes_factory()
{
    static GenoMatrix ret(2, 2);
    static bool init = false;
    if (!init) {
        /*fast_polynom half = {.5};*/
#define _(x) {x}
        ret(0, 0) = {{{_(0), _(0)},
                      {_(0), _(0)}},
                     algebraic_genotype::Type::Polynom, fast_polynom::s};
        ret(0, 1) = {{{_(0), _(0)},
                      {_(1), _(0)}},
                     algebraic_genotype::Type::Polynom, fast_polynom::r};
        ret(1, 0) = {{{_(1), _(0)},
                      {_(0), _(0)}},
                     algebraic_genotype::Type::Polynom, fast_polynom::r};
        ret(1, 1) = {{{_(1), _(0)},
                      {_(1), _(0)}},
                     algebraic_genotype::Type::Polynom, fast_polynom::s};
#undef _
        /*MSG_DEBUG(ret);*/
        init = true;
    }
    return ret;
}




inline
GenoMatrix doubling_gametes()
{
    return gametes_factory<algebraic_genotype::Type::DoublingGamete>();
}

inline
GenoMatrix gametes()
{
    return gametes_factory<algebraic_genotype::Type::Gamete>();
}

inline
GenoMatrix self_gametes()
{
    static bool init = false;
    static GenoMatrix ret;
    if (!init) {
        ret = kroneckerProduct(gametes(), gametes());
        init = true;
        /*MSG_DEBUG(ret);*/
    }
    return ret;
}

inline
GenoMatrix sib_gametes()
{
    static bool init = false;
    static GenoMatrix ret;
    if (!init) {
        /*ret = kroneckerProduct(self_gametes(), kroneckerProduct(sib_gametes_factory(), sib_gametes_factory()));*/
        /*ret = kroneckerProduct(sib_gametes_factory(), self_gametes());*/
        ret = kroneckerProduct(self_gametes(), self_gametes());
        /*GenoMatrix tmp = kroneckerProduct(sib_gametes_factory(), self_gametes());*/
        /*ret = kroneckerProduct(self_gametes(), tmp);*/
        init = true;
        /*MSG_DEBUG(ret);*/
    }
    return ret;
}

inline
GenerationRS selfing(const GenerationRS& M)
{
    return convert(kroneckerProduct(M.data, self_gametes()));
}

inline
GenerationRS sibling(const GenerationRS& M)
{
    return convert(kroneckerProduct(M.data, sib_gametes()));
}


inline
GenerationRS haplotypes(const GenerationRS& M)
{
#ifdef LUMP_GAMETES
    GenerationRS ret = convert(kroneckerProduct(M.data, gametes()));
    /*MSG_DEBUG("GAMETE" << std::endl << ret);*/
    ::lumper<GenerationRS, exact_compare<algebraic_genotype>> l(ret);
    auto ret_lump = l.to_matrix(l.refine_all());
    /*MSG_DEBUG("LUMPED GAMETE" << std::endl << ret_lump);*/
    return ret_lump;
#else
    return convert(kroneckerProduct(M.data, gametes()));
#endif
}


inline
GenerationRS crossing(const GenerationRS& M, const GenerationRS& P)
{
    return convert(kroneckerProduct(haplotypes(M).data, haplotypes(P).data));
}


#if 0
inline
GenerationRS reduce_alleles(const GenerationRS& G)
{
    GenerationRS ret = G;
    for (auto& l: ret.column_labels) { l.first.tag = 0; l.second.tag = 0; }
    for (auto& l: ret.row_labels) { l.first.tag = 0; l.second.tag = 0; }
    size_t sz = ret.data.size();
    auto* ptr = ret.data.data();
    for (size_t i = 0; i < sz; ++i) {
        auto& geno = ptr[i].genotype;
        auto& l1 = geno.first;
        auto& l2 = geno.second;
        l1.first.tag = 0; l1.second.tag = 0;
        l2.first.tag = 0; l2.second.tag = 0;
    }
    return ret;
}
#endif


inline
GenerationRS double_haploid(const GenerationRS& M)
{
    return convert(kroneckerProduct(M.data, doubling_gametes()));
}

inline
std::map<char, VectorXd> marker_observation_spec::compile() const
{
    std::map<char, VectorXd> ret;
    for (const auto& score: scores) {
        VectorXd obs = VectorXd::Zero(domain_size * domain_size);
        for (const auto& s: score.second) {
            obs(s.first + s.second * domain_size) = 1.;
        }
        ret[score.first] = obs;
    }
    return ret;
}


static inline std::ostream& operator << (std::ostream& os, const marker_observation_spec& mos)
{
    os << "MARKER OBS[" << std::endl;
    if (mos.scores.front().first) {
        for (auto& p: mos.scores) {
            os << " * " << p.first << ": " << p.second << std::endl;
        }
    } else {
        os << "RELATIVE unknown='" << mos.scores.back().first << '\'';
    }
    return os << ']';
}



template <typename G, typename M>
void check_LV(const G* g, int z, const M& ret)
{
    for (int i = 0; i < ret.outerSize(); ++i) {
        if ((ret.array().col(i) == 0).all()) {
            MSG_ERROR("LV null at " << g->name << '#' << z << ':' << i, "Fix the bugz, bunny.");
            throw -23;
        }
    }
}

static inline bool operator < (const mgo_key_type& k1, const mgo_key_type& k2)
{
    return k1.first < k2.first || (k1.first == k2.first && k1.second < k2.second);
}


inline
MatrixXd
population_marker_observation::raw_observations(const std::vector<char>& observations) const
{
    MatrixXd ret = MatrixXd::Ones(process_size, observations.size());
    std::string debug_obs(observations.begin(), observations.end());
    /*MSG_DEBUG("raw_observations[start] " << name << " obs=" << debug_obs);*/
    /*MSG_DEBUG(main_process().column_labels);*/
    /*for (auto& kv: obs_vectors) {*/
        /*MSG_DEBUG(" * " << kv.first << "   " << kv.second.transpose());*/
    /*}*/
    for (size_t i = 0; i < observations.size(); ++i) {
        std::map<char, VectorXd>::const_iterator ovi = observation_vectors.find(observations[i]);
        if (ovi != observation_vectors.end()) {
            ret.col(i) = ovi->second;
        } else {
            /* error! */
            MSG_ERROR("OBSERVATION NOT FOUND! obs #" << i << " chr(" << ((int)observations[i]) << ')', "FIX IT");
        }
    }
    /*MSG_DEBUG("raw_observations " << name << " obs=" << debug_obs << std::endl << ret);*/
    /*MSG_DEBUG("raw_observations obs=" << debug_obs << std::endl << ret);*/
    return ret;
}



#if 0
static inline
std::ostream&
operator << (std::ostream& os, const population&)
{
    return os << "<population>";
}
#endif


namespace impl {

inline
generation_rs::generation_design_base::generation_design_base(const generation_rs* my_gen, bool _is_anc) : g(my_gen), is_ril(0), is_ancestor(_is_anc) {}
inline
generation_rs::generation_design_base::~generation_design_base() {}
inline
generation_rs::generation_design_base* generation_rs::generation_design_base::clone(const generation_rs* my_gen) const
{
    generation_design_base* ret = _clone(my_gen);
    ret->is_ril = is_ril;
    return ret;
}


inline
std::ostream& operator << (std::ostream& os, const impl::generation_rs::generation_design_base& d)
{
    os << d.g->name << '=';
    d.to_str(os);
    return os;
}

inline
generation_rs::generation_design_ancestor::generation_design_ancestor(const generation_rs* my_gen)
    : generation_design_base(my_gen, true)
{}
inline
VectorLC generation_rs::generation_design_ancestor::raw_lincomb() const { return make_parent_comb((size_t) -1, 1); }
inline
generation_rs::generation_design_base* generation_rs::generation_design_ancestor::_clone(const generation_rs* my_gen) const { return new generation_design_ancestor(my_gen); }
inline
void generation_rs::generation_design_ancestor::to_str(std::ostream& os) const
{
    os << g->name;
}
inline
std::pair<const generation_rs*, const generation_rs*> generation_rs::generation_design_ancestor::get_parents() const { return {NULL, NULL}; }


inline
generation_rs::generation_design_adhoc::generation_design_adhoc(const generation_rs* my_gen)
    : generation_design_base(my_gen, true)
{}
inline
VectorLC generation_rs::generation_design_adhoc::raw_lincomb() const { return make_parent_comb(/*FIXME*/(size_t)g, g->main_process().innerSize()); }
inline
generation_rs::generation_design_base* generation_rs::generation_design_adhoc::_clone(const generation_rs* my_gen) const { return new generation_design_adhoc(my_gen); }
inline
void generation_rs::generation_design_adhoc::to_str(std::ostream& os) const
{
    os << g->name;
}
inline
std::pair<const generation_rs*, const generation_rs*> generation_rs::generation_design_adhoc::get_parents() const { return {NULL, NULL}; }


inline
generation_rs::generation_design_self::generation_design_self(const generation_rs* my_gen, const generation_rs* p)
    : generation_design_base(my_gen), parent(p)
{}
inline
VectorLC generation_rs::generation_design_self::raw_lincomb() const
{
    auto ct = make_parent_comb(/*FIXME*/(size_t)parent, parent->main_process().data.cols());
    /*return g->lumper.cast<gencomb_type>() * kroneckerProduct(ct, Matrix<gencomb_type, 4, 1>::Constant(.25));*/
    return kroneckerProduct(ct, Matrix<gencomb_type, 4, 1>::Constant(.25));
}
inline
generation_rs::generation_design_base* generation_rs::generation_design_self::_clone(const generation_rs* my_gen) const { return new generation_design_self(my_gen, parent); }
inline
void generation_rs::generation_design_self::to_str(std::ostream& os) const
{
    os << "S(";
    os << (*parent->design);
    os << ')';
}
inline
std::pair<const generation_rs*, const generation_rs*> generation_rs::generation_design_self::get_parents() const { return {parent, NULL}; }


inline
generation_rs::generation_design_reduce_alleles::generation_design_reduce_alleles(const generation_rs* my_gen, const generation_rs* p)
    : generation_design_base(my_gen), parent(p)
{}
inline
VectorLC generation_rs::generation_design_reduce_alleles::raw_lincomb() const
{
    return {};
}
inline
generation_rs::generation_design_base* generation_rs::generation_design_reduce_alleles::_clone(const generation_rs* my_gen) const { return new generation_design_self(my_gen, parent); }
inline
void generation_rs::generation_design_reduce_alleles::to_str(std::ostream& os) const
{
    os << "RA(";
    os << (*parent->design);
    os << ')';
}
inline
std::pair<const generation_rs*, const generation_rs*> generation_rs::generation_design_reduce_alleles::get_parents() const { return {parent, NULL}; }


inline
generation_rs::generation_design_cross::generation_design_cross(const generation_rs* my_gen, const generation_rs* p1, const generation_rs* p2)
    : generation_design_base(my_gen), parent1(p1), parent2(p2)
{}
inline
VectorLC generation_rs::generation_design_cross::raw_lincomb() const
{
    auto ct1 = make_parent_comb(/*FIXME*/(size_t)parent1, parent1->main_process().data.cols());
    auto ct2 = make_parent_comb(/*FIXME*/(size_t)parent2, parent2->main_process().data.cols());
    VectorLC p1, p2, ret;
    p1 = kroneckerProduct(ct1, Matrix<gencomb_type, 2, 1>::Constant(.5));
    p2 = kroneckerProduct(ct2, Matrix<gencomb_type, 2, 1>::Constant(.5));
    /*return g->lumper.cast<gencomb_type>() * kroneckerProduct(p2, p1);*/
    return kroneckerProduct(p2, p1);
}
inline
generation_rs::generation_design_base* generation_rs::generation_design_cross::_clone(const generation_rs* my_gen) const { return new generation_design_cross(my_gen, parent1, parent2); }
inline
void generation_rs::generation_design_cross::to_str(std::ostream& os) const
{
    os << "(" << (*parent1->design) << " x " << (*parent2->design) << ')';
}
inline
std::pair<const generation_rs*, const generation_rs*> generation_rs::generation_design_cross::get_parents() const { return {parent1, parent2}; }


inline
generation_rs::generation_design_sib::generation_design_sib(const generation_rs* my_gen, const generation_rs* p)
    : generation_design_base(my_gen), parent1(p)
{}
inline
VectorLC generation_rs::generation_design_sib::raw_lincomb() const
{
    return {};
}
inline
generation_rs::generation_design_base* generation_rs::generation_design_sib::_clone(const generation_rs* my_gen) const { return new generation_design_sib(my_gen, parent1); }
inline
void generation_rs::generation_design_sib::to_str(std::ostream& os) const
{
    os << "Sib(" << (*parent1->design) << ')';
}
inline
std::pair<const generation_rs*, const generation_rs*> generation_rs::generation_design_sib::get_parents() const { return {parent1, NULL}; }


inline
generation_rs::generation_design_broman::generation_design_broman(const generation_rs* my_gen,
        const generation_rs* p1, const generation_rs* p2,
        const generation_rs* p3, const generation_rs* p4,
        const generation_rs* p5, const generation_rs* p6,
        const generation_rs* p7, const generation_rs* p8)
    : generation_design_base(my_gen)
    , parent1(p1), parent2(p2), parent3(p3), parent4(p4)
    , parent5(p5), parent6(p6), parent7(p7), parent8(p8)
{}
inline
VectorLC generation_rs::generation_design_broman::raw_lincomb() const
{
    return {};
}
inline
generation_rs::generation_design_base* generation_rs::generation_design_broman::_clone(const generation_rs* my_gen) const { return new generation_design_cross(my_gen, parent1, parent2); }
inline
void generation_rs::generation_design_broman::to_str(std::ostream& os) const
{
    os << "(" << (*parent1->design) << " x " << (*parent2->design) << (*parent3->design) << " x " << (*parent4->design)
       << (*parent5->design) << " x " << (*parent2->design) << (*parent1->design) << " x " << (*parent2->design) << ')';
}
inline
std::pair<const generation_rs*, const generation_rs*> generation_rs::generation_design_broman::get_parents() const { return {parent1, parent2}; }


inline
generation_rs::generation_design_double_haploid::generation_design_double_haploid(const generation_rs* my_gen, const generation_rs* p)
    : generation_design_base(my_gen), parent(p)
{}
inline
VectorLC generation_rs::generation_design_double_haploid::raw_lincomb() const
{
    auto ct = make_parent_comb(/*FIXME*/(size_t)parent, parent->main_process().data.cols());
    /*return g->lumper.cast<gencomb_type>() * kroneckerProduct(ct, Matrix<gencomb_type, 2, 1>::Constant(.5));*/
    return kroneckerProduct(ct, Matrix<gencomb_type, 2, 1>::Constant(.5));
}
inline
generation_rs::generation_design_base* generation_rs::generation_design_double_haploid::_clone(const generation_rs* my_gen) const { return new generation_design_double_haploid(my_gen, parent); }
inline
void generation_rs::generation_design_double_haploid::to_str(std::ostream& os) const
{
    os << "DH(" << (*parent->design) << ')';
}
inline
std::pair<const generation_rs*, const generation_rs*> generation_rs::generation_design_double_haploid::get_parents() const { return {parent, NULL}; }


inline
generation_rs::generation_design_ril::generation_design_ril(const generation_rs* my_gen, const generation_rs* p)
    : generation_design_base(my_gen), parent(p)
{ is_ril = true; }
inline
VectorLC generation_rs::generation_design_ril::raw_lincomb() const
{
    return {};
}
inline
generation_rs::generation_design_base* generation_rs::generation_design_ril::_clone(const generation_rs* my_gen) const { return new generation_design_ril(my_gen, parent); }
inline
void generation_rs::generation_design_ril::to_str(std::ostream& os) const
{
    os << "RIL(" << (*parent->design) << ')';
}
inline
std::pair<const generation_rs*, const generation_rs*> generation_rs::generation_design_ril::get_parents() const { return {parent, NULL}; }


inline
bool generation_rs::process_equiv(const GenerationRS& g1, const GenerationRS& g2)
{
    auto labels = g1.column_labels;
    labels.insert(labels.end(), g2.column_labels.begin(), g2.column_labels.end());
    GenerationRS g(labels, labels);
    int size = g1.innerSize();
    int size2 = g1.innerSize() + g2.innerSize();
    g.data.resize(size2, size2);
    g.data.block(0, 0, size, size) = g1.data;
    g.data.block(size, size, size, size) = g2.data;
    ::lumper<GenerationRS, exact_compare<algebraic_genotype>> l(g);
    GenerationRS result = l.to_matrix(l.refine_all());
    /*MSG_DEBUG("INITIAL MATRIX" << std::endl << g);*/
    /*MSG_DEBUG("LUMPED MATRIX" << std::endl << result);*/
    return result.innerSize() == g1.innerSize();
}

inline
void generation_rs::reduce_processes()
{
    /*MSG_DEBUG(name << " PROCESS COUNT before " << P.size());*/
    auto p0 = P.begin();
    while (p0 != P.end()) {
        auto p1 = p0;
        ++p1;
        while (p1 != P.end()) {
            /*MSG_DEBUG("COMPARING" << std::endl << (p0->G) << std::endl << "AND" << std::endl << (p1->G) << std::endl);*/
            /*if (p1->G == p0->G) {*/
            if (process_equiv(p1->G, p0->G)) {
                p0->weight += p1->weight;
                P.erase(p1);
                /*++p1;*/
                /*MSG_DEBUG(name << " Generation " << name << ": merged two processes");*/
                /*std::cout << "1 ";*/
            } else {
                ++p1;
                /*std::cout << "0 ";*/
            }
        }
        /*std::cout << std::endl;*/
        ++p0;
    }
    /*MSG_DEBUG("PROCESS COUNT after " << P.size());*/
}

inline
void generation_rs::precompute_redux()
{
    redux_matrix = make_redux(matrix_labels_to_cliques(main_process().column_labels, unique_labels));
}

inline
const MatrixXd& generation_rs::get_redux() const
{
    return redux_matrix;
}

inline
void generation_rs::precompute_unique_labels()
{
    /*
    const MatrixXd& redux = get_redux();
    const auto& breakmat = main_process();
    size_t i = 0;
    size_t j = 0;
    unique_labels.clear();
    while (i < (size_t)redux.innerSize()) {
        while (j < breakmat.column_labels.size() && redux(i, j) != 1) ++j;
        unique_labels.push_back(breakmat.column_labels[j]);
        while (j < breakmat.column_labels.size() && redux(i, j) == 1) ++j;
        ++i;
    }
    MSG_DEBUG("COMPUTING UNIQUE LABELS IN " << name << ": " << unique_labels);
    */
}

inline
const std::vector<allele_pair>& generation_rs::get_unique_labels() const
{
    return unique_labels;
}


inline
generation_rs& generation_rs::operator = (const generation_rs& grs)
{
    name = grs.name;
    lumping_partition = grs.lumping_partition;
    lumper = grs.lumper;
    P = grs.P;
    if (design != NULL) {
        delete design;
    }
    design = grs.design->clone(this);
    return *this;
}

inline
const GenerationRS& generation_rs::main_process() const { return P.front().G; }

inline
void generation_rs::compute_lumping_partition()
{
    const GenerationRS& G = main_process();
    /*if (G.innerSize() <= MAX_EXACT_LUMP_SIZE) {*/
    ::lumper<GenerationRS, exact_compare<algebraic_genotype>> l(G);
    lumping_partition = l.refine_all();
}

inline
void generation_rs::lump()
{
    chrono c;
    /*MSG_DEBUG("Lumping " << name);*/
    c.start();
    compute_lumping_partition();
    size_t orig_size = main_process().innerSize();
    for (auto& proc: P) {
        ::lumper<GenerationRS, exact_compare<algebraic_genotype>> l(proc.G);
        proc.G = l.to_matrix(lumping_partition);
    }
    lumper = MatrixXd::Zero(lumping_partition.size(), orig_size);
    int i = -1;
    for (auto& part: lumping_partition) {
        ++i;
        for (auto& j: part) {
            lumper(i, j) = 1;
        }
    }
    c.stop();
    /*MSG_DEBUG("Done lumping " << name << " in " << c.accum << " seconds");*/
    /*MSG_DEBUG("Lumper" << std::endl << lumper);*/
}


#define COMPUTE_SEGMENT fast_compute_over_segment
/*#define COMPUTE_SEGMENT compute_over_segment*/

inline
MatrixXd generation_rs::noisy_LV(const MatrixXd& LV, double noise) const
{
    MatrixXd ret = LV;
    /*MSG_DEBUG(ret);*/
    /*MatrixXd ret = LV.cast<double>();*/
    /*double epsilon = noise / LV.innerSize();*/
    double epsilon = noise;
    double one = 1. - 2 * epsilon;
    ret *= one;  /* 0 => 0, 1 => 1 - 2*e */
    ret.array() += epsilon;  /* 0 => e, 1 => 1 - e */
    for (int i = 0; i < ret.outerSize(); ++i) {
        ret.col(i) /= ret.col(i).sum();
    }
    if ((ret.array() < 0.).any()) {
        MSG_ERROR("INVALID LV : LESS_THAN_ZERO", "");
        MSG_DEBUG(ret);
        throw 0;
    }
    if ((ret.array() > 1.).any()) {
        MSG_ERROR("INVALID LV : GREATER_THAN_ONE", "");
        MSG_DEBUG(ret);
        throw 0;
    }
/*
* 1 0 0 0   =>  || 1 + 2e   =>   (1-e) / (1+2e)   e / (1+2e)
* 1 1 0 0   =>  || 2        =>   (1-e) / 2        e / 2
* 1 1 1 0   =>  || 3 - 2e   =>   (1-e) / (3-2e)   e / (3-2e)
* 1 1 1 1   =>  || 4 - 4e   =>   .25
*/
    /*MSG_DEBUG("noisy LV noise=" << noise << " epsilon=" << epsilon << std::endl << ret);*/
    return ret;
}


inline
bool generation_rs::segment_computer_t::operator == (const segment_computer_t& sc) const
{
    return g_this == sc.g_this && steps == sc.steps && noise == sc.noise;
}

inline std::ostream& operator << (std::ostream& os, const generation_rs::segment_computer_t& sc)
{
    return os << '<' << sc.g_this->name << ' '
              << (sc.g_this->design->is_ril ? " RIL" : "")
              << " n_steps=" << sc.steps.size()
              << " noise=" << sc.noise << '>';
}

inline
generation_rs::segment_computer_t::segment_computer_t() : g_this(0), gpc_vec(), steps(), noise(0) {}
/*segment_computer_t(const segment_computer_t& sc)*/
    /*: g_this(sc.g_this), chr(sc.chr), gpc_vec(sc.gpc_vec), steps(sc.steps), loci(sc.loci), noise(sc.noise)*/
/*{*/
    /*gpc_vec.locus = &loci;*/
/*}*/
inline
generation_rs::segment_computer_t::segment_computer_t(segment_computer_t&& sc)
    : g_this(sc.g_this), chr(sc.chr), gpc_vec(std::move(sc.gpc_vec)), steps(std::move(sc.steps)), loci(std::move(sc.loci)), noise(sc.noise)
{}

inline
generation_rs::segment_computer_t::segment_computer_t(const generation_rs& g, const qtl_chromosome* c, double step, double nz)
    : g_this(&g), chr(c), gpc_vec(), noise(nz)
{
    chr->copy_loci(loci);
    /*std::cout << (*chr) << " loci =";*/
    /*for (auto l: loci) std::cout << ' ' << l;*/
    /*std::cout << std::endl;*/
    steps = compute_steps(loci, step);
    /*std::cout << "steps =";*/
    /*for (auto s: steps) std::cout << ' ' << s;*/
    /*std::cout << std::endl;*/
    init();
}

inline
generation_rs::segment_computer_t::segment_computer_t(const generation_rs& g, const qtl_chromosome* c, double nz, const std::vector<double>& loc_vec)
    : g_this(&g), chr(c), gpc_vec(), noise(nz)
{
    chr->copy_loci(loci);
    /*std::cout << (*chr) << " loci =";*/
    /*for (auto l: loci) std::cout << ' ' << l;*/
    /*std::cout << std::endl;*/
    steps = loc_vec;
    /*std::cout << "steps =";*/
    /*for (auto s: steps) std::cout << ' ' << s;*/
    /*std::cout << std::endl;*/
    init();
}

inline
void generation_rs::segment_computer_t::init()
{
    /*MSG_DEBUG("init !");*/
    /*gpc_vec.resize(g_this->P.size());*/
    /*size_t i = 0;*/
    /*for (auto& p: g_this->P) {*/
        gpc_vec.locus = &loci;
        gpc_vec.init(const_cast<impl::generation_rs*>(g_this));
        /*++i;*/
    /*}*/
}

inline
generation_rs::segment_computer_t& generation_rs::segment_computer_t::operator = (segment_computer_t&& sc)
{
    /*MSG_DEBUG("OPERATOR = :" << __LINE__);*/
    g_this = sc.g_this;
    chr = sc.chr;
    chr->copy_loci(loci);
    steps.swap(sc.steps);
    /*gpc_vec.swap(sc.gpc_vec);*/
    gpc_vec = std::move(sc.gpc_vec);
    noise = sc.noise;
    init();
    return *this;
}

inline
labelled_matrix<MatrixXd, allele_pair, double>
    generation_rs::segment_computer_t::compute(const MatrixXd& LV)
    {
        /*DUMP_FILE_LINE();*/
        auto joint_qtl_iterator = chr->qtl_state_iterator(g_this->unphased_LV());
        /*MatrixXd lv = g_this->noisy_LV(LV, noise);*/
        MatrixXd qtl_lv(LV.innerSize(), loci.size());
        /*MSG_DEBUG(MATRIX_SIZE(lv) << std::endl << lv.transpose());*/
        gpc_vec.LV = &qtl_lv;
        joint_qtl_iterator.init_expansion(LV, qtl_lv);
        /*MSG_DEBUG(MATRIX_SIZE(qtl_lv));*/
        /*MSG_DEBUG(qtl_lv.transpose());*/
        auto tmp = compute();
        int n_states = tmp.innerSize();
        std::vector<allele_pair> row_labels;
        row_labels.reserve(n_states * joint_qtl_iterator.size());
        /*MSG_DEBUG("qtl_iterator.size = " << joint_qtl_iterator.size());*/
        for (int i = 0; i < joint_qtl_iterator.size(); ++i) {
            row_labels.insert(row_labels.end(), tmp.row_labels.begin(), tmp.row_labels.end());
        }
        labelled_matrix<MatrixXd, allele_pair, double> ret(row_labels, steps);
        /*MSG_DEBUG("ret.data$dim(" << ret.data.innerSize() << ',' << ret.data.outerSize() << ") tmp.data$dim(" << tmp.innerSize() << ',' << tmp.outerSize() << ") n_states=" << n_states);*/
        /*ret.data.topRows(n_states) = compute().data;*/
        ret.data.topRows(n_states) = tmp.data;
        /*ret.emplace_back(compute());*/
        int row_ofs = n_states;
        while (!joint_qtl_iterator.next()) {
            if (joint_qtl_iterator.update(LV, qtl_lv)) {
                /*ret.emplace_back(compute());*/
                /*MSG_DEBUG(MATRIX_SIZE(qtl_lv));*/
                /*MSG_DEBUG(qtl_lv.transpose());*/
                ret.data.middleRows(row_ofs, n_states) = compute().data;
            } else {
                ret.data.middleRows(row_ofs, n_states) = MatrixXd::Zero(n_states, ret.data.outerSize());
            }
            row_ofs += n_states;
        }
        return ret;
    }

inline
labelled_matrix<MatrixXd, allele_pair, double>
    generation_rs::segment_computer_t::compute()
    {
        /*auto pbegin = g_this->P.begin(), pend = g_this->P.end();*/
        /*auto& process = gpc_vec.front();*/
#ifndef USE_RIL_SERIES
        if (g_this->design->is_ril) {
            auto output = gpc_vec.COMPUTE_SEGMENT(steps, 1);
            output.data *= process_weight(1);
            for (int i = 2; i <= RIL_ORDER; ++i) {
                output.data += gpc_vec.COMPUTE_SEGMENT(steps, i).data
                               * process_weight(i);
            }
            return output;
        } else {
            return gpc_vec.COMPUTE_SEGMENT(steps, 1);
        }
#else
        auto output = gpc_vec.COMPUTE_SEGMENT(steps, 0);
        output.data *= g_this->P[0].weight;
        /*MSG_DEBUG("compute()#0");*/
        /*MSG_DEBUG(output);*/
        for (size_t i = 1; i < g_this->P.size(); ++i) {
            output.data += gpc_vec.COMPUTE_SEGMENT(steps, i).data
                           * g_this->P[i].weight;
            /*MatrixXd tmp = gpc_vec.COMPUTE_SEGMENT(steps, i).data*/
                           /** g_this->P[i].weight;*/
            /*MSG_DEBUG("compute()#" << i);*/
            /*MSG_DEBUG(tmp);*/
            /*output.data += tmp;*/
            /*MSG_DEBUG(output);*/
                           /** process_weight(i);*/
        }
        return output;

#endif
    }

inline
generation_rs::segment_computer_t generation_rs::segment_computer(
        const qtl_chromosome* chr,
        double step,
        double noise) const
{
    segment_computer_t ret(*this, chr, step, noise);
    return ret;
}

inline
generation_rs::segment_computer_t generation_rs::segment_computer(
        const qtl_chromosome* chr,
        double noise,
        const std::vector<double>& loci) const
{
    return segment_computer_t(*this, chr, noise, loci);
}

inline std::ostream& operator << (std::ostream& os, const generation_rs& g)
{
    os << "== Generation: " << g.name << " ======================" << std::endl;
    /*os << "State combination: " << g.this_lincomb << std::endl;*/
    os << g.genotype_probabilities() << std::endl;
    os << "Locus Transition" << std::endl;
    os << "    Process size: " << g.main_process().innerSize() << std::endl;
/*#define GENERATION_PRINT_ALL_PROCESSES*/
#ifdef GENERATION_PRINT_ALL_PROCESSES
    for (auto& p: g.P) {
        os << "* weight " << p.weight << std::endl;
        os << p.G << std::endl;
        /*os << "colsums " << p.G.data.colwise().sum() << std::endl;*/
        /*os << "rowsums " << p.G.data.rowwise().sum().transpose() << std::endl;*/
    }
#else
    os << g.main_process() << std::endl;
    if (g.P.size() > 1) {
        os << "(" << (g.P.size() - 1) << " other parallel processes not shown)" << std::endl;
    }
#endif
    /*os << "Weights:";*/
    /*double sum = 0;*/
    /*for (auto& p: g.P) {*/
        /*os << ' ' << p.weight;*/
        /*sum += p.weight;*/
    /*}*/
    /*os << std::endl;*/
    /*os << "Total: " << sum << " in " << g.P.size() << " processes" << std::endl;*/
    /*os << "LUMPER" << std::endl << g.lumper << std::endl;*/
    return os;
}

inline
void generation_rs::init_epo()
{
    epo.clear();
    epo.reserve(P.size());
    for (const auto& p: P) {
        epo.emplace_back();
        epo.back().init(p.G);

    }
    /*MSG_DEBUG("epo size = " << epo.size());*/
    /*epo.init(main_process());*/
}

inline
generation_rs::generation_rs(const std::string& n, char haplo, const std::vector<std::pair<char, char>>& tagpairs)
    : name(n), lumping_partition(), lumper(), P(), design(NULL), unique_unphased_LV()
    , stpo_max(-1), ul_computed(false), ulv_computed(false)
{
    GenoMatrix tmp(tagpairs.size(), tagpairs.size());
    design = new generation_design_ancestor(this);
#ifdef POLYNOM_COEF_INT
    fast_polynom prob = fast_polynom::one;
#else
    fast_polynom prob = {1. / (double) tagpairs.size()};
#endif
    for (size_t i = 0; i < tagpairs.size(); ++i) {
        for (size_t j = 0; j < tagpairs.size(); ++j) {
            tmp(i, j) = {{{{haplo}, {haplo}},
                          {{haplo}, {haplo}}},
                         algebraic_genotype::Type::Genotype,
                         prob};
            /*MSG_DEBUG(i << ',' << j << ": " << tmp(i, j));*/
        }
    }
    this_lincomb = make_parent_comb(/*FIXME*/(size_t)this, 1);
    P.emplace_back((process){convert(tmp), 1.});
    precompute();
    /*raw_lincomb = design->raw_lincomb();*/
}

inline
generation_rs::generation_rs(const std::string& n, generation_design_base* gdb)
    : name(n), lumping_partition(), lumper(), P(), design(gdb), unique_unphased_LV()
    , stpo_max(-1), ul_computed(false), ulv_computed(false)
{}

inline
void generation_rs::precompute()
{
    init_epo();
    precompute_redux();
    precompute_unique_labels();
    precompute_uulv();
}









inline
void generation_rs::precompute_lincomb()
{
    if (name == "F3rc") {
        MSG_DEBUG("###########################MAXIMA#####################");
        MSG_DEBUG(main_process());
        MSG_DEBUG("###########################MAXIMA#####################");
    }
#if 1
    /* Ascendant subtree set */
    std::vector<const generation_rs*> subtree_p1, subtree_p2;
    auto parents = design->get_parents();
    ancestor_subtree.clear();
    if (parents.first) {
        subtree_p1.reserve(parents.first->ancestor_subtree.size() + 1);
        subtree_p1 = parents.first->ancestor_subtree;
        subtree_p1.push_back(parents.first);
        std::sort(subtree_p1.begin(), subtree_p1.end());
    }
    if (parents.second) {
        subtree_p2.reserve(parents.second->ancestor_subtree.size() + 1);
        subtree_p2 = parents.second->ancestor_subtree;
        subtree_p2.push_back(parents.second);
        std::sort(subtree_p2.begin(), subtree_p2.end());
    }
    ancestor_subtree.resize(subtree_p1.size() + subtree_p2.size());
    auto it = std::set_union(subtree_p1.begin(), subtree_p1.end(), subtree_p2.begin(), subtree_p2.end(), ancestor_subtree.begin());
    ancestor_subtree.resize(it - ancestor_subtree.begin());

    /* lincomb */
    this_lincomb = design->raw_lincomb();
    /*MSG_DEBUG("RAW LINCOMB");*/
    /*MSG_DEBUG(this_lincomb);*/

    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
    /* Consistent states redux */
    if (design->type() == design_type::DCross) {
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        size_t s1 = subtree_p1.size();
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        size_t s2 = subtree_p2.size();
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        //std::vector<size_t> common_ancestors(s1 > s2 ? s1 : s2);
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        //auto it = std::set_intersection(a1.begin(), a1.end(), a2.begin(), a2.end(), common_ancestors.begin());
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        //common_ancestors.resize(it - common_ancestors.begin());
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        std::vector<const impl::generation_rs*> tmp_expand(s1 + s2);
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        auto it = std::set_symmetric_difference(subtree_p1.begin(), subtree_p1.end(), subtree_p2.begin(), subtree_p2.end(), tmp_expand.begin());
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        tmp_expand.resize(it - tmp_expand.begin());
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        std::map<size_t, VectorLC> expansions;
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        std::vector<const impl::generation_rs*> expand;
        expand.reserve(tmp_expand.size());
        for (auto g: tmp_expand) {
            if (g->main_process().data.size() > 1) {
                expand.push_back(g);
            }
        }
        for (const impl::generation_rs* g: expand) {
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
            expansions[/*FIXME*/(size_t)g] = g->this_lincomb;
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        }
        MSG_DEBUG("EXPANSIONS " << expansions.size());
        for (const auto& kv: expansions) {
            /*MSG_DEBUG(" * " << kv.first->name);*/
            MSG_DEBUG(" * " << kv.first);
            MSG_DEBUG(kv.second);
        }
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        auto lincomb = this_lincomb | expansions;
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        /*MSG_DEBUG("EXPANDED LINCOMB");*/
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        /*MSG_DEBUG(lincomb);*/
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/

    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        std::set<subset> redux_partition;
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        int n_col = 0;
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        for (int i = 0; i < lincomb.size(); ++i) {
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
            n_col += lincomb(i).is_consistent();
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        }
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
#if 0
        VectorLC tmp(n_col);
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        n_col = 0;
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        for (int i = 0; i < lincomb.size(); ++i) {
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
            if (lincomb(i).is_consistent()) {
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
                std::vector<int> one(1);
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
                one[0] = i;
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
                redux_partition.insert(one);
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
                tmp(n_col) = this_lincomb(i);
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
                ++n_col;
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
            }
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        }
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        MSG_DEBUG("REDUX_PARTITION.size = " << redux_partition.size());
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        MSG_DEBUG("new lincomb size = " << tmp.size());
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        for (auto& p: P) {
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
            MSG_DEBUG("BEFORE " << MATRIX_SIZE(p.G));
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
            ::lumper<GenerationRS, exact_compare<algebraic_genotype>> l(p.G);
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
            p.G = l.to_matrix(redux_partition);
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
            MSG_DEBUG("AFTER " << MATRIX_SIZE(p.G));
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
        }
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/

    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
#else
        consistent_states_redux.data = MatrixXd::Zero(this_lincomb.size(), n_col);
        consistent_states_redux.row_labels = main_process().row_labels;
        consistent_states_redux.column_labels.reserve(n_col);
        VectorLC tmp(n_col);
        n_col = 0;
        int bad = 0;
        for (int i = 0; i < this_lincomb.size(); ++i) {
            /*if (lincomb(i).not_empty()) {*/
            if (lincomb(i).is_consistent()) {
                consistent_states_redux.data(i, n_col) = 1;
                consistent_states_redux.column_labels.push_back(consistent_states_redux.row_labels[i]);
                tmp(n_col) = this_lincomb(i);
                ++n_col;
            } else if (0) {
                MSG_DEBUG("GOT INCONSISTENT STATE at #" << i);
                MSG_DEBUG(lincomb(i));
                if (name == "F3") {
                   int j = i >= 8 ? bad - 4 : bad + 4;
                   MSG_DEBUG("bad at " << i << ',' << j);
                   MSG_QUEUE_FLUSH();
                   consistent_states_redux.data(i, j) = 1;
                   ++bad;
                }
            }
        }

        auto CSR = consistent_states_redux.data.cast<algebraic_genotype>();
        MSG_DEBUG("consistent_states_redux");
        MSG_DEBUG(consistent_states_redux);

        for (auto& p: P) {
            p.G.data = CSR.transpose() * p.G.data * CSR;
            p.G.column_labels = consistent_states_redux.column_labels;
            p.G.row_labels = consistent_states_redux.column_labels;
        }

        MSG_QUEUE_FLUSH();
#endif
        this_lincomb = tmp;
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
    }
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
#endif
    /*    MSG_DEBUG(__FILE__ << ':' << __LINE__); MSG_QUEUE_FLUSH();*/
}


#if 0
inline
void generation_rs::precompute()
{
    init_epo();
    precompute_redux();
    precompute_unique_labels();
    precompute_uulv();

    /* TODO:
     * - init ascendant subtree set
     * - compute lincomb
     * - compute consistent_states_redux (if cross)
     */
    /* Ascendant subtree set */
    auto parents = design->get_parents();
    ancestor_subtree.clear();
    if (parents.first) {
        ancestor_subtree.push_back(parents.first);
        const auto& as = parents.first->ancestor_subtree;
        ancestor_subtree.insert(ancestor_subtree.end(), as.begin(), as.end());
    }
    if (parents.second) {
        ancestor_subtree.push_back(parents.second);
        const auto& as = parents.second->ancestor_subtree;
        ancestor_subtree.insert(ancestor_subtree.end(), as.begin(), as.end());
    }
    std::sort(ancestor_subtree.begin(), ancestor_subtree.end());
    auto it = std::unique(ancestor_subtree.begin(), ancestor_subtree.end());
    ancestor_subtree.resize(it - ancestor_subtree.begin());

    /* lincomb */
    this_lincomb = design->raw_lincomb();

    /* Consistent states redux */
    if (design->type() == design_type::DCross) {
        int n_col = 0;
        for (int i = 0; i < this_lincomb.size(); ++i) {
            n_col += this_lincomb(i).not_empty();
        }
        consistent_states_redux = MatrixXd::Zero(this_lincomb.size(), n_col);
        n_col = 0;
        for (int i = 0; i < this_lincomb.size(); ++i) {
            if (this_lincomb(i).not_empty()) {
                consistent_states_redux(i, n_col) = 1;
                ++n_col;
            }
        }
    }
}
#endif


inline
generation_rs::~generation_rs() { if (design) { delete design; } }

inline
bool generation_rs::__allele_pairs_equal(const allele_pair& a, const allele_pair& b)
{
    return (a.first == b.first && a.second == b.second)
        || (a.first == b.second && a.second == b.first);
}

/*spin_lock ulv_lock;*/

inline
void generation_rs::precompute_uulv()
{
    int sz = unique_labels.size();
    unique_unphased_LV.reserve(sz);
    const auto& labels = main_process().column_labels;
    for (int i = 0; i < sz; ++i) {
        VectorXd tmp = VectorXd::Zero(main_process().innerSize());
        for (size_t s = 0; s < main_process().innerSize(); ++s) {
            if (labels[s] == unique_labels[i]) {
                tmp(s) = 1;
            }
        }
        unique_unphased_LV.push_back(tmp);
    }
}

inline
const std::vector<VectorXd>& generation_rs::unphased_LV() const
{
    return unique_unphased_LV;
}

inline
labelled_matrix<MatrixXd, std::string, allele_pair>
generation_rs::genotype_probabilities() const
{
    chromosome chr = {
        "abundances",
        {"nil"},
        {0}
    };
    qtl_chromosome qtl_chr(&chr);
    auto sc = segment_computer(&qtl_chr, 1, 0);
    MatrixXd LV = MatrixXd::Ones(main_process().innerSize(), 1);
    MSG_DEBUG(MATRIX_SIZE(main_process()));
    MSG_DEBUG(MATRIX_SIZE(LV));
    auto probs = sc.compute(LV);
    std::vector<std::string> label = {"probabilities"};
    labelled_matrix<MatrixXd, std::string, allele_pair> ret(label, probs.row_labels);
    ret.data = probs.data.transpose();
    return ret;
}

} /* namespace impl */



#if 0
inline
bool population::is_observed(const impl::generation_rs* g, int i) const
{
    return i >= 0 && ((size_t) i) < get_observed_mark(g->name).observations.n_obs;
}

inline
std::vector<char> population::get_observations(const chromosome* chr, const impl::generation_rs* g, int i) const
{
    return get_observed_mark(g->name).observations.get_obs(chr->raw.marker_name.begin(), chr->raw.marker_name.end(), i);
}

inline
void population::init_observations(
        MatrixXd& ret,
        const chromosome* chr, const impl::generation_rs* g, int i) const
{
    const population_marker_observation& om = get_observed_mark(g->name);
    if (i == -1 || !is_observed(g, i)) {
        ret = MatrixXd::Ones(g->main_process().innerSize(), chr->raw.marker_name.size());
    } else {
        std::vector<char> obs = get_observations(chr, g, i);
        if (obs.size()) {
            ret = om.raw_observations(obs);
        } else {
            ret = MatrixXd::Ones(g->main_process().innerSize(), chr->raw.marker_name.size());
        }
    }
    /*MSG_DEBUG("init_observations" << std::endl << MATRIX_SIZE(ret) << std::endl << ret);*/
}


inline
pedigree_node population::pedigree_get_parents(const impl::generation_rs* g, int i) const
{
    /*MSG_DEBUG("pedigree_get_parents(" << g->name << ", " << i << ')');*/
    auto it = pedigree.find(g);
    if (it != pedigree.end()) {
        return it->second[i];
    } else {
        return {-1, -1};
    }
}
#endif


inline
void population_marker_observation::compute_observation_vectors(
        const impl::generation_rs* g,
        const marker_observation_spec& obs_spec)
{
#if 0
    const GenerationRS& G = g->main_process();
    process_size = G.innerSize();
    /*MSG_DEBUG("compute_observation_vectors");*/
    for (auto& obs: obs_spec) {
        int i = 0;
        observation_vectors[obs.first] = VectorXd::Zero(G.innerSize());
        for (auto& l: G.column_labels) {
            observation_vectors[obs.first](i)
                /*= std::find(obs.second.begin(),*/
                            /*obs.second.end(),*/
                            /*l) != obs.second.end();*/
                = std::find_if(obs.second.begin(),
                               obs.second.end(),
                               [&] (const allele_pair& ap) {
                                   /*if (ap.first.ancestor == 0) {*/
                                       /*MSG_DEBUG("tag check "*/
                                                 /*<< ((int)ap.first.tag) << '/' << ((int)l.first.tag)*/
                                                 /*<< ' '*/
                                                 /*<< ((int)ap.second.tag) << '/' << ((int)l.second.tag)*/
                                                 /*);*/
                                   /*} else {*/
                                       /*MSG_DEBUG("ancestor check "*/
                                                 /*<< ap.first.ancestor << '/' << l.first.ancestor*/
                                                 /*<< ' '*/
                                                 /*<< ap.second.ancestor << '/' << l.second.ancestor*/
                                                 /*);*/
                                   /*}*/
                                   return
                                       ap.first.ancestor == 0
                                       ? ap.first.tag == l.first.tag && ap.second.tag == l.second.tag
                                       : ap.first.ancestor == l.first.ancestor && ap.second.ancestor == l.second.ancestor
                                       ;
                               }) != obs.second.end();
            ++i;
        }
        /*MSG_DEBUG(obs.first << ": " << observation_vectors[obs.first].transpose());*/
        /*MSG_DEBUG(obs.first << ": " << observation_vectors[obs.first].transpose());*/
    }
    /*return observation_vectors;*/
#else
    (void)g; (void)obs_spec;
#endif
}



inline
generation_rs::gen_map::~gen_map()
{
    for (auto& kv: *this) {
        delete kv.second;
    }
}

inline
generation_rs::gen_map& generation_rs::dict()
{
    static gen_map _;
    return _;
}

inline
generation_rs* generation_rs::fetch_impl(const std::string& n)
{
    generation_rs*& i = dict()[n];
    if (i == NULL) {
        i = new generation_rs(n);
    }
    return i;
}

inline
generation_rs* generation_rs::fetch_impl(const std::string& n, char haplo, const std::vector<std::pair<char, char>>& tagpairs)
{
    generation_rs*& i = dict()[n];
    if (i == NULL) {
        i = new generation_rs(n, haplo, tagpairs);
    }
    return i;
}

inline
generation_rs::generation_rs(const std::string& n)
    : impl::generation_rs(n, (generation_design_base*)NULL)
{}

inline
generation_rs::generation_rs(const std::string& n, char haplo, const std::vector<std::pair<char, char>>& tagpairs)
    : impl::generation_rs(n, haplo, tagpairs)
{}

inline
void generation_rs::clear_dict()
{
    auto& gm = dict();
    for (auto& kv: gm) {
        delete kv.second;
    }
    gm.clear();
}

inline generation_rs* generation_rs::ancestor(const std::string& new_name, char haplo, const std::vector<std::pair<char, char>>& tagpairs)
{
    return fetch_impl(new_name, haplo, tagpairs);
}

inline
generation_rs* generation_rs::selfing(const std::string& new_name) const
{
    /*MSG_DEBUG("selfing to " << new_name);*/
    generation_rs* ret = fetch_impl(new_name);
    if (ret->design != NULL) {
        return ret;
    }
    MSG_DEBUG("NEW GENERATION " << new_name);
    ret->design = new generation_design_self(ret, this);
    for (auto& p1: P) {
        GenerationRS p = ::selfing(p1.G);
        ret->P.emplace_back((process){p, p1.weight});
    }
    ret->precompute_lincomb();
    ret->lump();
    ret->reduce_processes();
    /*ret->epo.init(ret->main_process());*/
    ret->precompute();
    /*MSG_DEBUG(name << " with " << P.size() << " processes SELFING gives " << ret->P.size() << " processes" << std::endl);*/
    /*MSG_DEBUG(name << '(' << main_process().innerSize() << ") SELFING gives " << new_name << '(' << ret->main_process().innerSize() << ") lumper " << ret->lumper.innerSize() << 'x' << ret->lumper.outerSize());*/
    return ret;
}

inline
VectorLC impl::generation_rs::raw_lincomb(size_t i) const { return make_parent_comb(i, main_process().innerSize()); }

inline
generation_rs* generation_rs::from_matrix(const std::string& new_name, const GenoMatrix& gm, const VectorLC& lc)
{
    generation_rs* ret = fetch_impl(new_name);
    if (ret->design != NULL) {
        return ret;
    }
    ret->design = new generation_design_adhoc(ret);
    ret->P.emplace_back(convert(gm), 1.);
    ret->lump();
    ret->this_lincomb = ret->lumper.cast<gencomb_type>() * lc;
    ret->precompute();
    /*ret->raw_lincomb = ret->design->raw_lincomb();*/
    return ret;
}

inline
generation_rs* generation_rs::blank(const std::string& new_name)
{
    generation_rs* ret = fetch_impl(new_name);
    if (ret->design != NULL) {
        return ret;
    }
    ret->design = new generation_design_adhoc(ret);
    return ret;
}

inline
generation_rs* generation_rs::from_matrix(const std::string& new_name, const GenoMatrix& gm)
{
    generation_rs* ret = fetch_impl(new_name);
    if (ret->design != NULL) {
        return ret;
    }
    ret->design = new generation_design_adhoc(ret);
    ret->P.emplace_back(convert(gm), 1.);
    ret->lump();
    ret->precompute_lincomb();
    ret->precompute();
    /*ret->raw_lincomb = ret->design->raw_lincomb();*/
    return ret;
}


inline
generation_rs* generation_rs::sibling(const std::string& new_name) const
{
    /*MSG_DEBUG("sibling to " << new_name);*/
    generation_rs* ret = fetch_impl(new_name);
    if (ret->design != NULL) {
        return ret;
    }
    MSG_DEBUG("NEW GENERATION " << new_name);
    ret->design = new generation_design_sib(ret, this);
    for (auto& p1: P) {
        GenerationRS p = ::sibling(p1.G);
        ret->P.emplace_back((process){p, p1.weight});
    }
    ret->precompute_lincomb();
    ret->lump();
    ret->reduce_processes();
    /*ret->epo.init(ret->main_process());*/
    ret->precompute();
    /*MSG_DEBUG(name << " with " << P.size() << " processes SELFING gives " << ret->P.size() << " processes" << std::endl);*/
    /*MSG_DEBUG(name << '(' << main_process().innerSize() << ") SELFING gives " << new_name << '(' << ret->main_process().innerSize() << ") lumper " << ret->lumper.innerSize() << 'x' << ret->lumper.outerSize());*/
    return ret;
}

inline
generation_rs* generation_rs::to_doubled_haploid(const std::string& new_name) const
{
    generation_rs* ret = fetch_impl(new_name);
    if (ret->design != NULL) {
        return ret;
    }
    MSG_DEBUG("NEW GENERATION " << new_name);
    ret->design = new generation_design_double_haploid(ret, this);
    for (auto& p1: P) {
        GenerationRS p = ::double_haploid(p1.G);
        ret->P.emplace_back((process){p, p1.weight});
    }
    ret->precompute_lincomb();
    ret->lump();
    ret->reduce_processes();
    /*ret->epo.init(ret->main_process());*/
    ret->precompute();
    /*MSG_DEBUG(name << '(' << main_process().innerSize() << ") DH gives " << new_name << '(' << ret->main_process().innerSize() << ") lumper " << ret->lumper.innerSize() << 'x' << ret->lumper.outerSize());*/
    /*MSG_DEBUG(name << " with " << P.size() << " processes DOUBLE gives " << ret->P.size() << " processes" << std::endl);*/
    return ret;
}

inline
generation_rs* generation_rs::clone_and_lump() const
{
    generation_rs* ret = new generation_rs(name);
    ret->design = design->clone(ret);
    ret->P = P;
    ret->precompute_lincomb();
    ret->lump();
    return ret;
}

inline
generation_rs* generation_rs::crossing(const std::string& new_name, const generation_rs* sylvester) const /* parce que l'étalon */
{
    generation_rs* ret = fetch_impl(new_name);
    if (ret->design != NULL) {
        return ret;
    }
    MSG_DEBUG("NEW GENERATION " << new_name);
    ret->design = new generation_design_cross(ret, this, sylvester);
    for (auto& p1: P) {
        for (auto& p2: sylvester->P) {
            GenerationRS pg = ::crossing(p2.G, p1.G);
            ret->P.emplace_back((process){pg, p1.weight * p2.weight});
        }
    }
    /* TODO: insert LC reduction here */
    ret->precompute_lincomb();
    ret->lump();
    ret->this_lincomb = ret->lumper.cast<gencomb_type>() * ret->this_lincomb;
    ret->reduce_processes();
    /*ret->epo.init(ret->main_process());*/
    ret->precompute();
    /*MSG_DEBUG(name << '(' << main_process().innerSize() << ") CROSSING gives " << new_name << '(' << ret->main_process().innerSize() << ") lumper " << ret->lumper.innerSize() << 'x' << ret->lumper.outerSize());*/
    return ret;
}

/* Attention
 * TODO: pour RIL sibling (R=4r/(1+6r)), le poids est -2^(-2*i-1)*3^(i-1).
 * TODO: vérifier le reste pour RIL sibling.
 * TODO: traiter les autosomes à part etc etc etc blabla.
 */
inline
generation_rs* generation_rs::to_ril(const std::string& ril_name) const
{
    /*fast_polynom one = {1};*/
    fast_polynom half = {.5};
    generation_rs* ret = fetch_impl(ril_name);
    if (ret->design != NULL) {
        return ret;
    }
    GenoMatrix tmp(1, 1);
    fast_polynom ril_dl = {1, -2};
    ret->design = new generation_design_ril(ret, this);
    ret->design->is_ril = true;
    generation_rs* tmp_gen = clone_and_lump();
    auto dgam = doubling_gametes();
    for (const process& p: tmp_gen->P) {
        /*process p = P.front();*/
        /* the "RIL-ification" process is thought as follows :
         * - take the main process (the one based on the Haldane relation r=(1-exp(-2*distance))/2),
         * - for each degree of the RIL development R[i],
         *      - apply for each cell (cell.polynom o R[i])
         *   giving a new process per i.
         */
        fast_polynom ril = ril_dl;
        double w = .5;
        for (int i = 1; i < RIL_ORDER; ++i) {
            /*fast_polynom q = (fast_polynom::one - ril) / (coef_t)2;*/
            fast_polynom q = (fast_polynom::one - ril) * half;
            /*MSG_DEBUG("q = " << q);*/
            /*MSG_DEBUG("ril=" << ril << " q=" << q);*/
            tmp(0, 0) = {{{0, 0}, {0,0}},
                         algebraic_genotype::Type::RilPolynom,
                         q};
            /*MSG_DEBUG("tmp = " << tmp);*/
            GenoMatrix tmp_gam = kroneckerProduct(tmp, dgam);
            /*MSG_DEBUG("tmp_gam =" << std::endl << tmp_gam);*/
            GenoMatrix tmp_proc = kroneckerProduct(p.G.data, tmp_gam);
            /*MSG_DEBUG_DEDENT;*/
            /*MSG_DEBUG_DEDENT;*/
            /*MSG_DEBUG_DEDENT;*/
            /*MSG_DEBUG_DEDENT;*/
            /*MSG_DEBUG("tmp_proc =" << std::endl << tmp_proc);*/
            ret->P.emplace_back((process){
                    convert(tmp_proc),
                    p.weight * w});
            /*MSG_DEBUG("=>" << std::endl << ret->P.back().G);*/
            /*MSG_DEBUG("New process w=" << ret->P.back().weight << std::endl << ret->P.back().G);*/
            ril *= ril_dl;
            w *= .5;
        }
        ril *= ril_dl;
        fast_polynom q = (fast_polynom::one - ril) * half;
        /*fast_polynom q = (fast_polynom::one - ril) / (coef_t)2;*/
        /*MSG_DEBUG("ril=" << ril << " q=" << q);*/
        tmp(0, 0) = {{{0, 0}, {0,0}},
                     algebraic_genotype::Type::RilPolynom,
                     q};
        ret->P.emplace_back((process){
                convert(kroneckerProduct(p.G.data, kroneckerProduct(tmp, dgam))),
                /*convert(kroneckerProduct(tmp, p.G.data)),*/
                p.weight * 2 * w});
    }
    /*MSG_DEBUG(ret);*/
    ret->precompute_lincomb();
    ret->lump();
    /*MSG_DEBUG("RIL LUMPING PARTITION" << std::endl << ret->lumping_partition);*/
    /*ret->lumping_partition = lumping_partition;*/
    /*ret->lumper = lumper;*/
    ret->reduce_processes();
    /*ret->epo.init(ret->main_process());*/
    ret->precompute();
    delete tmp_gen;
    /*MSG_DEBUG((*ret));*/
    return ret;
}

inline
    generation_rs* generation_rs::broman8(const std::string& name,
                           generation_rs* A, generation_rs* B, generation_rs* C, generation_rs* D,
                           generation_rs* E, generation_rs* F, generation_rs* G, generation_rs* H)
    {
        generation_rs* ret = fetch_impl(name);
        if (ret->design != NULL) {
            return ret;
        }
        ret->design = new generation_design_broman(ret, A, B, C, D, E, F, G, H);
        /*double v = .25, w = .5;*/
        double v = .5, w = 1.;
        std::vector<generation_rs*> parents = {A, B, C, D, E, F, G, H};
        /*std::vector<size_t> sizes = {*/
            /*A->main_process().innerSize(), B->main_process().innerSize(),*/
            /*C->main_process().innerSize(), D->main_process().innerSize(),*/
            /*E->main_process().innerSize(), F->main_process().innerSize(),*/
            /*G->main_process().innerSize(), H->main_process().innerSize()*/
        /*};*/
        /*size_t size = std::accumulate(sizes.begin(), sizes.end(), 0);*/

        fast_polynom quarter = {.25};
        fast_polynom two = {-2.};

        /*MSG_DEBUG("quarter=" << quarter);*/
        /*MSG_DEBUG("two=" << two);*/

        algebraic_genotype
            ag_quarter
                = {{{0, 0}, {0,0}},
                   algebraic_genotype::Type::Polynom,
                   quarter},
            ag_one
                = {{{0, 0}, {0,0}},
                   algebraic_genotype::Type::Polynom,
                   fast_polynom::one},
            ag_two
                = {{{0, 0}, {0,0}},
                   algebraic_genotype::Type::Polynom,
                   two},
            ag_minus_one
                = {{{0, 0}, {0,0}},
                   algebraic_genotype::Type::Polynom,
                   fast_polynom::zero - fast_polynom::one};

        /*MSG_DEBUG("ag_quarter=" << ag_quarter);*/
        /*MSG_DEBUG("ag_one=" << ag_one);*/
        /*MSG_DEBUG("ag_two=" << ag_two);*/
        /*MSG_DEBUG("ag_minus_one=" << ag_minus_one);*/

        GenoMatrix I4 = GenoMatrix::Constant(4, 1, ag_one).asDiagonal();
        GenoMatrix I8 = GenoMatrix::Constant(8, 1, ag_two).asDiagonal();

        GenoMatrix M2seed(2, 2);
        M2seed << ag_minus_one, ag_one, ag_one, ag_minus_one;
        GenoMatrix M1 = GenoMatrix::Constant(8, 8, ag_quarter) + I8,
                   M2 = kroneckerProduct(I4, M2seed),
                   M_I = GenoMatrix::Constant(8, 1, ag_one).asDiagonal();

        for (int i = 0; i < 8; ++i) {
            for (int j = 0; j < 8; ++j) {
                M1(i, j).type = algebraic_genotype::Type::Genotype;
                M2(i, j).type = algebraic_genotype::Type::Genotype;
                M_I(i, j).type = algebraic_genotype::Type::Genotype;
                M1(i, j).genotype = {parents[i]->main_process().column_labels[0],
                                     parents[j]->main_process().column_labels[0]};
                M2(i, j).genotype = M1(i, j).genotype;
                M_I(i, j).genotype = M1(i, j).genotype;
            }
        }

        /*MSG_DEBUG(MATRIX_SIZE(M1) << std::endl << M1);*/
        /*MSG_DEBUG(MATRIX_SIZE(M2) << std::endl << M2);*/
        /*MSG_DEBUG(MATRIX_SIZE(M_I) << std::endl << M_I);*/

        fast_polynom ril_dl = {1, -2};
        fast_polynom ril = ril_dl;
        /*GenoMatrix tmp(1, 1);*/
        algebraic_genotype tmp = {{{0, 0}, {0,0}},
                         algebraic_genotype::Type::Polynom,
                         fast_polynom::zero};
        /*ret->P.emplace_back((process){convert(M1 + M_I), -v});*/
        /*ret->P.emplace_back((process){convert(M2 + M_I), w});*/
        for (int i = 1; i < MAGIC8_ORDER; ++i) {
            v *= .5;
            w *= .5;
            tmp.poly = (fast_polynom::one - ril) / (coef_t)2;

            ret->P.emplace_back((process){convert(M1 * tmp + M_I), w});
            ret->P.emplace_back((process){convert(M2 * tmp + M_I), v});

            ril *= ril_dl;
        }

        /* the rest is approximately ril_dl[BROMAN_ORDER+1] with the same weight */
        ril *= ril_dl;
        tmp.poly = (fast_polynom::one - ril) / (coef_t)2;
        ret->P.emplace_back((process){convert(M1 * tmp + M_I), w});
        ret->P.emplace_back((process){convert(M2 * tmp + M_I), v});

        ret->P[1].weight *= -1;  /* fix first weight */
        ret->precompute_lincomb();
        ret->lump();
        ret->reduce_processes();
        ret->precompute();
        /*MSG_DEBUG(ret);*/
        return ret;
    }


inline
generation_rs* generation_rs::get(const std::string& name)
{
    auto i = dict().find(name);
    if (i == dict().end()) {
        throw "This generation doesn't exist."; /* FIXME: throw real exception */
    }
    return i->second;
}

inline
std::pair<const generation_rs*, const generation_rs*>
    generation_rs::get_parents() const
    {
        auto tmp = design->get_parents();
        return {dynamic_cast<const generation_rs*>(tmp.first),
                dynamic_cast<const generation_rs*>(tmp.second)};
    }


#if 1
inline Eigen::MatrixXd eval_poly(const GenoMatrix& gm, double r, std::vector<double>& r_pow, std::vector<double>& s_pow)
{
    double s = 1. - r;
    double r_tmp = r, s_tmp = s;
    size_t pow_max = r_pow.size();

    for (size_t i = 1; i < pow_max; ++i) {
        r_pow[i] = r_tmp;
        s_pow[i] = s_tmp;
        r_tmp *= r;
        s_tmp *= s;
    }

    /*double s = 1. - r;*/
    MatrixXd ret(gm.innerSize(), gm.outerSize());
    for (int i = 0; i < gm.innerSize(); ++i) {
        for (int j = 0; j < gm.outerSize(); ++j) {
            const impl::f_polynom& poly = gm(i, j).poly;
            double accum = r_pow[poly.r_exp] * s_pow[poly.s_exp];
            double tmp = poly.P[0];
            for (size_t i = 1; i < poly.P.size(); ++i) {
                tmp += poly.P[i] * r_pow[i];
            }
            ret(i, j) = accum * tmp;
        }
    }
    return ret;
}
#else

struct eval_poly_cache {
    std::unordered_map<const rs_polynom&, double> cache;
    std::vector<double> r_pow, s_pow;

    eval_poly_cache(double r, size_t pow_max)
        : cache(), r_pow(pow_max), s_pow(pow_max)
    {
        double s = 1. - r;
        double r_tmp = r, s_tmp = s;

        r_pow.resize(pow_max, 1.);
        s_pow.resize(pow_max, 1.);

        for (size_t i = 1; i < pow_max; ++i) {
            r_pow[i] = r_tmp;
            s_pow[i] = s_tmp;
            r_tmp *= r;
            s_tmp *= s;
        }
    }

    double get(const rs_polynom& p)
    {
        auto it = cache.find(p);
        if (it == cache.end()) {
            double accum = r_pow[p.r_exp] * s_pow[p.s_exp];
            double tmp = 0;
            for (size_t i = 0; i < p.R.size(); ++i) {
                tmp += p.R[i] * r_pow[i];
            }
            return cache[p] = accum * tmp;
        }
        return it->second;
    }
};

inline Eigen::MatrixXd eval_poly(const GenoMatrix& gm, double r)
{

    auto& poly = gm(0, 0).poly;
    eval_poly_cache epc(r, poly.r_exp + poly.s_exp + poly.R.size());

    /*double s = 1. - r;*/
    MatrixXd ret(gm.innerSize(), gm.outerSize());
    for (int i = 0; i < gm.innerSize(); ++i) {
        for (int j = 0; j < gm.outerSize(); ++j) {
            ret(i, j) = epc.get(gm(i, j).poly);
        }
    }
    return ret;
}
#endif

#endif

