/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*#include <x2c/x2c.h>*/
/*#include <vector>*/
/*#include <sstream>*/
/*#include <iostream>*/
#include "all.h"

using namespace x2c;

struct toto_t {
    std::string pouet;
    std::vector<int> coin;
};


DTD_START(test_dtd, toto, toto_t)
    ELEMENT(pouet, std::string);
    ELEMENT(coin, int);
    toto = (E(pouet, &toto_t::pouet), E(coin, &toto_t::coin));
    pouet = chardata();
    coin = chardata();
DTD_END(test_dtd);

int main(int argc, char** argv)
{
    std::stringstream s;
    s << "<toto><coin>42</coin><pouet>hop</pouet><coin>23</coin></toto>";
    toto_t* ret = test_dtd.parse(s);
    std::cout << "got ret = " << ret << std::endl;
    if (ret != NULL) {
        std::cout << ret->pouet << std::endl;
        for (auto& i: ret->coin) {
            std::cout << i << ' ';
        }
        std::cout << std::endl;
    }
    return ret == NULL;
}

