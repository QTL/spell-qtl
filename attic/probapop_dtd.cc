/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <x2c/x2c.h>
#include "probapop_dtd.h"
#include <vector>
#include "probapop_dtd.h"

using namespace x2c;

struct matrix_p {
    std::string mark;
    /*double p1, p2, p3, p4;*/
    std::vector<double> p;
    double locus;
    void operator () (probapop_output& po)
    {
        /*if (mark == "yes") {*/
            if (p.size() == 3) {
                std::vector<double> col;
                col.push_back(p[0]);
                col.push_back(p[1]*.5);
                col.push_back(p[1]*.5);
                col.push_back(p[2]);
                po.prob.push_back(col);
            } else {
                po.prob.push_back(p);
            }
            /*std::cout << "new column";*/
            /*for (auto& x: po.prob.back()) {*/
                /*std::cout << ' ' << x;*/
            /*}*/
            /*std::cout << std::endl;*/
            po.loci.push_back(locus);
            /*std::cout << "locus " << locus << std::endl;*/
        /*}*/
    }
};

struct matrix_p_list {
    std::vector<matrix_p> list;
    void operator () (probapop_output& po) {
        for (auto m: list) {
            if (!po.loci.size() || po.loci.back() != m.locus) {
                m(po);
            }
        }
    }
};

DTD_START(probapop_dtd, POPULATION, probapop_output)
    ELEMENT(CHROMOSOME, matrix_p_list);
    ELEMENT(POSITION, matrix_p);
    ELEMENT(MATRIX_P, matrix_p);
    ELEMENT(LINE, matrix_p);
    ELEMENT(PROBABILITY, double);

    PROBABILITY = A("value");
    LINE = A("number", &ignore::entity) & E(PROBABILITY, &matrix_p::p);
    MATRIX_P = A("number_rows", &ignore::entity) & A("number_columns", &ignore::entity) & E(LINE);
    POSITION = A("value", &matrix_p::locus) & A("marker", &matrix_p::mark) & A("name", &ignore::entity) & A("weight", &ignore::entity) & E(MATRIX_P);
    CHROMOSOME = A("name", &ignore::entity) & A("number", &ignore::entity) & E(POSITION, &matrix_p_list::list);
    POPULATION = A("name", &ignore::entity) & A("type", &ignore::entity) & E(CHROMOSOME);
DTD_END(probapop_dtd);


probapop_output* read_probapop(std::istream& is)
{
    probapop_output* po = NULL;
    try {
        po = probapop_dtd.parse(is);
    } catch(xml_exception xe) {
        std::cerr << "Error reading file: " << xe.what() << std::endl;
    }
    return po;
}

