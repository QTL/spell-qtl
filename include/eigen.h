/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EIGEN_INCLUDED
#define EIGEN_INCLUDED

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-register"
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#endif

#include <unsupported/Eigen/MatrixFunctions>
#include <Eigen/Core>
#include <Eigen/SparseCore>
#include <Eigen/Eigenvalues>
#include <Eigen/QR>
#include <Eigen/SVD>
#include <unsupported/Eigen/KroneckerProduct>

using Eigen::VectorXi;
using Eigen::VectorXd;
using Eigen::MatrixXi;
using Eigen::MatrixXd;
template <typename X> using SelfAdjointEigenSolver = Eigen::SelfAdjointEigenSolver<X>;

#ifdef __clang__
#pragma clang diagnostic pop
#endif


// namespace Eigen {
//     template<>
//     struct NumTraits<std::string> {
//         typedef std::string Real;
//         typedef std::string NonInteger;
//         typedef std::string Nested;
//         enum {
//             IsComplex = 0,
//             IsInteger = 1,
//             ReadCost = 100,
//             WriteCost = 100,
//             MulCost = 1000,
//             IsSigned = 0,
//             RequireInitialization = 1
//         };
//         static Real epsilon() { return { std::string() }; }
//         static Real dummy_precision() { return { std::string() }; }
//         static std::string highest() { return std::string(); }
//         static std::string lowest() { return std::string(); }
//     };
// }



#endif

