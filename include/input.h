/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef _SPEL_INPUT_H_
#error "_SPEL_INPUT_H_ already defined!"
#else
#define _SPEL_INPUT_H_

extern "C" {
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
}

#include "io/error.h"

#include "io/basic_file_checks.h"

#include "data/qtl_chromosome.h"
#include "input/input.h"
#include "input/read_map.h"
#include "input/read_mark.h"
#include "input/read_trait.h"
//#include "input/design.h"
#include "data/geno_matrix.h"
#include "data/pedigree.h"
#include "settings.h"

settings_t* read_settings(ifile& is);
/*format_specification_t* read_format(std::istream& is);*/
//design_type* read_design(std::istream& is);

//pedigree_type read_pedigree(const design_type* design, const std::string& filename, std::istream& is, bool with_LC);
void read_ld(settings_t* settings, const std::string& qtl_gen, const std::string& filename, std::istream& is);

#endif

