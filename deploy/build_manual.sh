#!/bin/bash

BUILD_ROOT=$1

cd ../doc/user_manual && make BINDIR=$BUILD_ROOT/bin SRCDIR=$2 && cp user_manual.pdf $BUILD_ROOT/doc
