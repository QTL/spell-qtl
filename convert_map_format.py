#!/usr/bin/env python
from itertools import chain


def accumulate_interval(_map, x):
    name, loc = x.split()
    return _map + ((name, loc, round(float(loc) - float(_map[-1][1]), 5)),)


def reduce_lg(lg_name, markers):
    intervals = reduce(accumulate_interval, markers, ((lg_name, 0., 0.),))
    return [intervals[1][0]] + ['%3.1f %s' % (interval, m_name)
                                for (m_name, loc, interval) in intervals[2:]]


def convert(filename, output_filename):
    lines = [l for l in (l.strip()
                         for l in open(filename).xreadlines())
               if (not l.startswith(';')) and len(l) > 0]
    groups = [i for (i, l) in enumerate(lines)
                if l.lower().startswith('group')]
    LG = [(lines[x].split(' ')[1], lines[x + 1:y])
          for (x, y) in zip(chain([0], groups),
                            chain(groups, [len(lines)]))[1:]]

    o = open(output_filename, 'w')

    for lg_name, markers in LG:
        intervals = reduce_lg(lg_name, markers)
        print >> o, '*%s' % lg_name, len(markers), ' '.join(intervals)


if __name__ == '__main__':
    import sys
    if len(sys.argv) != 3:
        print "Usage:", sys.argv[0], " group_file output_file"
        sys.exit(1)
    convert(sys.argv[1], sys.argv[2])
